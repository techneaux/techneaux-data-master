﻿using GenericRuleModel.Rules;
using System.Collections.Generic;
using XmlDataModelUtility;

namespace ExpressionRuleModel.Rules
{
    public class CygNetExpression : NotifyCopyDataModel
    {
        public CygNetExpression() : this(true)
        {
        }

        public CygNetExpression(bool IsFullRuleVal = true)
        {
            ExpressionStr = "";
            FormatConfig = new FormattingOptions();
            ExpressionElements = new List<CygNetExpressionElement>();
            IsFullRule = IsFullRuleVal;
        }


        //********************************** Constant Attributes **********************************

        public bool IsFullRule;

        [HelperClasses.ControlTitle("Expression", "Exp")]
        public string ExpressionStr
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        //Output configuration
        public FormattingOptions FormatConfig
        {
            get => GetPropertyValue<FormattingOptions>();
            set => SetPropertyValue(value);
        }

        public List<CygNetExpressionElement> ExpressionElements
        {
            get => GetPropertyValue<List<CygNetExpressionElement>>();
            set => SetPropertyValue(value);
        }

        internal bool HasRandomNumberGeneratorChild()
        {
            foreach (var element in ExpressionElements)
            {
                if (element.HasRandomNumberGeneratorChild())
                    return true;
            }

            return false;
        }
    }
}
