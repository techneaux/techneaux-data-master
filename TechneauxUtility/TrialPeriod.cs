﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TechneauxUtility
{
    public static class TrialPeriod
    {
        public enum TrialStatuses
        {
            DomainApproved,
            ComputerNameApproved,
            UserNameApproved,
            TrialApproved,
            TrialExpired,
            Denied
        }

        private const string AllowedDomains = "wpx;";
        private const string AllowedComputerNames = "EsProductDemo;DENWS0003;DENWS0004;BenWalkerWDSK";

        private static HashSet<string> AllowedDomainSet => new HashSet<string>(AllowedDomains.Split(';'));

        private static HashSet<string> AllowedComputerNameSet => new HashSet<string>(AllowedComputerNames.Split(';'));

        private const string ExpireDateConst = @"10/20/2018";

        private static DateTime ExpireDate => DateTime.Parse(ExpireDateConst);

        private static bool IsTrialExpired => DateTime.Now >= ExpireDate;

        public static int DaysTillExpiration => IsTrialExpired ? 0 : Convert.ToInt16((ExpireDate - DateTime.Now).TotalDays);

        private static bool IsDomainAllowed =>
            AllowedDomainSet.Any(name => string.Equals(Environment.UserDomainName, name, StringComparison.InvariantCultureIgnoreCase));

        private static bool IsComputerNameAllowed =>
            AllowedComputerNameSet.Any(name => string.Equals(Environment.MachineName, name, StringComparison.InvariantCultureIgnoreCase));

        public static (bool approvedToRun, TrialStatuses approvalType) LicenseStatus
        {
            get
            {
                if (IsDomainAllowed)
                    return (true, TrialStatuses.DomainApproved);

                if (IsComputerNameAllowed)
                    return (true, TrialStatuses.ComputerNameApproved);

                if (!IsTrialExpired)
                    return (true, TrialStatuses.TrialApproved);

                return (false, TrialStatuses.TrialExpired);
            }
        }
    }
}