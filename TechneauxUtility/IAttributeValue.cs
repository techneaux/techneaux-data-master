﻿namespace TechneauxUtility
{
    public interface IAttributeValue : IConvertibleValue
    {
        string AttributeId { get; }
        string AttributeDesc { get; }
    }

}