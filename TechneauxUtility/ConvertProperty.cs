﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechneauxUtility
{
    public static class ConvertBoolProperty
    {
        public static bool GetBoolFromYesNoString(string srcVal) => srcVal.ToLower() == "yes";

        public static bool GetBoolFromYnString(string  srcVal) => srcVal.ToLower() == "y";

        public static void SetBoolToYesNoString(string destVal, bool newVal) => destVal = newVal ? "Yes" : "No";

        public static void SetBoolToYnString(string destVal, bool newVal) => destVal = newVal ? "Y" : "N";

        public static bool GetBoolFrom01(uint srcVal) => srcVal == 1;

        public static void SetBoolTo01( uint destVal, bool newVal) => destVal = newVal ? (uint)1 : 0;

        public static bool GetBoolFromTrueFalseString(string srcVal) => srcVal.ToLower() == "true";

        public static void SetBoolToTrueFalseString(string destVal, bool newVal) => destVal = newVal ? "True" : "False";
    }
}
