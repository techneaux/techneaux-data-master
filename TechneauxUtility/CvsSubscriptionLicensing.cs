﻿using DotNetLicense;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Serilog.Context;
using static XmlDataModelUtility.GlobalLoggers;

namespace TechneauxUtility
{
    public static class CvsSubscriptionLicensing
    {
        public static bool InDeveloperMode => KnownDeveloperMachines.AllowedComputerNames.Any(dm =>
            dm.Equals(System.Environment.UserDomainName, StringComparison.InvariantCultureIgnoreCase));


        public static bool IsCvsAllowed(string servName)
        {
            
            if (InDeveloperMode)
                return true;

            if (AreServicesUnlimited)
                return true;

            if (AuthorizedCurrentValueServiceNames.Any(serv =>
                serv.Equals(servName, StringComparison.InvariantCultureIgnoreCase)))
                return true;

            return false;
        }

        public static int GracePeriodDays { get; set; } = 30;

        private static LicenseManager _dotLicManager = new LicenseManager();

        // Service restrictions
        public static bool AreServicesUnlimited { get; private set; }

        public static List<string> AuthorizedCurrentValueServiceNames { get; private set; }


        // Customer properties
        public static string CustomerNameLicensedTo { get; private set; }

        public static List<string> AllowedNetworkDomains { get; private set; }

        public static bool IsCurrentNetworkDomainAuthorized => AllowedNetworkDomains.Any(dm =>
            dm.Equals(System.Environment.UserDomainName, StringComparison.InvariantCultureIgnoreCase));


        // Regular expiration
        public static DateTime ExpirationDate { get; private set; }

        public static int DaysBeforeRegularExpiration => Convert.ToInt16((ExpirationDate.Date - DateTime.Now.Date).TotalDays);

        public static bool IsCurrentlyExpired => DaysBeforeRegularExpiration <= 0;


        // Grace period
        public static int DaysLeftInGracePeriod => Convert.ToInt16((ExpirationDate.Date.AddDays(GracePeriodDays) - DateTime.Now.Date).TotalDays);

        public static bool IsGracePeriodExpired => DaysLeftInGracePeriod <= 0;


        public static LicenseReadStatus TryReadingLicense(string publicKeyString)
        {
            try
            {
                var basePathFull = Assembly.GetExecutingAssembly().Location;
                var basePathFolder = Path.GetDirectoryName(basePathFull);
                var licenseFolder = $@"{basePathFolder}\License";

                if (!Directory.Exists(licenseFolder))
                    return LicenseReadStatus.NoLicenseFileFound;

                var licenseFiles = Directory.EnumerateFiles(licenseFolder)
                    .Where(file => file.EndsWith("License.lic", StringComparison.InvariantCultureIgnoreCase))
                    .ToList();

                if (!licenseFiles.Any())
                    return LicenseReadStatus.NoLicenseFileFound;

                if (licenseFiles.Count > 1)
                    return LicenseReadStatus.MultipleLicensesFound;

                var onlyLicFile = licenseFiles.Single();


                _dotLicManager.LoadPublicKeyFromString(publicKeyString);

                var thisLic = _dotLicManager.LoadLicenseFromDisk(onlyLicFile);

                CustomerNameLicensedTo = thisLic.GetAttribute("LicensedTo");
                ExpirationDate = DateTime.Parse(thisLic.GetAttribute("ExpirationDate"));

                var allowedDomainsString = thisLic.GetAttribute("NetworkDomain");
                AllowedNetworkDomains = allowedDomainsString
                    .Split(';')
                    .Where(dm => !string.IsNullOrWhiteSpace(dm))
                    .ToList();

                var thisLicenseType = (LicenseType)Convert.ToInt16(thisLic.GetAttribute("LicenseType"));

                var thisCvsLicenseLevel = (CvsLicenseLevel)Convert.ToInt16(thisLic.GetAttribute("CvsLicenseLevel"));

                AreServicesUnlimited = thisCvsLicenseLevel == CvsLicenseLevel.Unlimited;

                var allowedCvsServsString = thisLic
                    .GetAttribute("ServiceNamesAuthorized");
                AuthorizedCurrentValueServiceNames = allowedCvsServsString
                    .Split(';')
                    .Where(serv => !string.IsNullOrWhiteSpace(serv))
                    .ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                Log.Error(ex, "Unhandled exception while reading license file. Please contact Techneaux.");
                
                return LicenseReadStatus.CouldNotReadLicense;
            }

            return LicenseReadStatus.ValidLicenseFound;
        }

        public static string ReadPublicKeyStringFromAssembly(Assembly srcAssembly, string keyFileName)
        {
            string resourceName = srcAssembly.GetManifestResourceNames().Single(str => str.EndsWith(keyFileName));

            using (Stream stream = srcAssembly.GetManifestResourceStream(resourceName))
            using (StreamReader sr = new StreamReader(stream))
            {
                string thisKey = sr.ReadToEnd();
                return thisKey;
            }
        }

        public enum LicenseReadStatus
        {
            NoLicenseFileFound,
            PublicKeyMissing,
            CouldNotReadLicense,
            MultipleLicensesFound,
            ValidLicenseFound
        }

        public enum CvsLicenseLevel
        {
            Limited,
            Unlimited
        }

        public enum LicenseType
        {
            Subscription,
            Permanent,
            Trial
        }





    }
}
