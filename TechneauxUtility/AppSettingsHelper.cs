﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechneauxUtility
{
    public static class AppSettingsHelper
    {
        public static bool TryGetDoubleFromAppSettings(
            string settingKey,
            double minValue,
            double maxValue,
            double defaultValue,
            out double settingValue,
            out string errorReason)
        {
           
            var settingStr = ConfigurationManager.AppSettings.Get(settingKey);

            if(string.IsNullOrEmpty(settingStr))
            {
                settingValue = defaultValue;
                errorReason = $"Setting not found, default value of {defaultValue} was used";
                return false;
            }
            else
            {
                if(double.TryParse(settingStr, out var dblVal))
                {
                    if (dblVal < minValue)
                    {
                        settingValue = minValue;
                        errorReason = $"Setting ({dblVal}) smaller than the minimum value allowed ({minValue}); using the min value.";
                        return false;
                    }

                    if (dblVal > maxValue)
                    {
                        settingValue = maxValue;
                        errorReason = $"Setting ({dblVal}) greater than the maximum value allowed ({maxValue}); using the max value";
                        return false;
                    }

                    settingValue = dblVal;
                    errorReason = null;
                    return true;
                }
                else
                {
                    settingValue = defaultValue;
                    errorReason  = $"Setting ({settingStr}) is not numeric or is out of range, default value of ({defaultValue}) is being used";
                    return false;
                }
            }
        }
    }
}
