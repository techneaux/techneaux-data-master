﻿using System;

namespace TechneauxUtility
{

    public interface IConvertibleValue : IConvertible
    {
        object RawValue { get; }

        string StringValue { get; }

        string ToString();
        bool TryGetBool(out bool value);
        bool TryGetDateTime(out DateTime value);
        bool TryGetDouble(out double value);
        bool TryGetInt(out int value);
        bool IsClrDateTime { get; }
        bool IsSqlDateTime2 { get;}
        bool IsSqlDateTime { get;}
        DateTime ToSqlDateTime { get; }
        DateTime ToDateTime();
        bool IsDateTime { get; }


    }

}