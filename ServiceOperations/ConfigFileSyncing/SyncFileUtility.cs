﻿using CygNetRuleModel.Resolvers;
using ReactiveUI;
using Serilog;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services;
using TechneauxDataSyncService.Helper;
using TechneauxDataSyncService.PointSyncing;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxHistorySynchronization.SqlServerHistory.DatabaseOperations;
using TechneauxReportingDataModel.General;
using TechneauxReportingDataModel.Helper;
using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxDataSyncService.ConfigFileSyncing
{
    public partial class SyncFileUtility : NotifyModelBase, ICancellable
    {


        public BindingList<ConfigSyncOperationResults> PointSyncResults { get; set; } =
            new BindingList<ConfigSyncOperationResults>();


        private const int MinsBetweenPointCheck = 10;
        private double _minsBetweenPointSync = 5; // Comes from App.Config with default of 5 minutes (SEAN), no less than 30 secs, no more than 1 hour

        public ReportConfigModel SrcDataModel
        {
            get => GetPropertyValue<ReportConfigModel>();
            set => SetPropertyValue(value);
        }

        [XmlIgnore()]
        public string FilePath
        {
            get => GetPropertyValue<string>();
            private set
            {
                FileName = Path.GetFileName(value);
                SetPropertyValue(value);
            }
        }

        private int TestOpCount = 0;

        [XmlIgnore()]
        public string FileName
        {
            get => GetPropertyValue<string>();
            private set => SetPropertyValue(value);
        }

        [XmlIgnore()]
        public string Status
        {
            get => GetPropertyValue<string>();
            private set => SetPropertyValue(value);
        }

        public DateTime LastModifiedTimestamp
        {
            get => GetPropertyValue<DateTime>();
            set => SetPropertyValue(value);
        }

        private int MaxPoints { get; set; } = -1;


        public SyncFileUtility()
        {
            var sAttr = ConfigurationManager.AppSettings.Get("MaxPoints");

            if (int.TryParse(sAttr, out int num))
            {
                MaxPoints = num;
            }
        }

        private CancellationTokenSource Cts { get; }
        private ILogger _configFileLog;

        //private IDisposable _checkForNewPointsTimer;
        //private IDisposable _checkForNewPointDataTimer;

        public SyncFileUtility(
            string newFileName,
            ReportConfigModel newConfigModel,
            DateTime lastModDate,
            CancellationToken parentCt,
            bool usingTestHarness = false)
        {
            FilePath = newFileName;
            SrcDataModel = newConfigModel;
            LastModifiedTimestamp = lastModDate;
            Cts = CancellationTokenSource.CreateLinkedTokenSource(parentCt);

            InitLogging();

            _configFileLog.Information("================================================");
            _configFileLog.Information($"Initializing new sync file with path=[{FilePath}]");

            LoadSyncSettings();

            _configFileLog.Information($"Binding timer events: Point check every [{MinsBetweenPointCheck}] mins, Point sync every [{_minsBetweenPointSync}] mins");

            MyPoints = new List<PointSync>();

            //if (usingTestHarness)
            //{
            Observable.FromAsync(UpdatePointList)
                .RepeatAfterDelay(TimeSpan.FromMinutes(MinsBetweenPointCheck), RxApp.MainThreadScheduler)
                .Subscribe();

            Observable.FromAsync(CheckForNewPointData)
                .RepeatAfterDelay(TimeSpan.FromMinutes(_minsBetweenPointSync), RxApp.MainThreadScheduler)
                .Subscribe();

            //  CheckForNewPointsTimer = Observable.Interval(TimeSpan.FromMinutes(MinsBetweenPointCheck))
            //.ObserveOnDispatcher()
            //.Subscribe(evt =>
            //{
            //    UpdatePointList();
            //});

            //CheckForNewPointDataTimer = Observable.Interval(TimeSpan.FromMinutes(MinsBetweenPointSync))
            //                .ObserveOnDispatcher()
            //              .Subscribe(evt =>
            //              {
            //                  CheckForNewPointData();
            //              });
            //}
            //else
            //{
            //    CheckForNewPointsTimer = Observable.Interval(TimeSpan.FromMinutes(MinsBetweenPointCheck))
            //  .Subscribe(evt =>
            //  {
            //      UpdatePointList();
            //  });

            //    CheckForNewPointDataTimer = Observable.Interval(TimeSpan.FromMinutes(MinsBetweenPointSync))
            //                  .Subscribe(evt =>
            //                  {
            //                      CheckForNewPointData();
            //                  });
            //}


        }

        private void InitLogging()
        {
            var loggingLevelAttr = ConfigurationManager.AppSettings.Get("LogLevelConfig");
            Serilog.Events.LogEventLevel newLevel =
                (Serilog.Events.LogEventLevel)Enum.Parse(typeof(Serilog.Events.LogEventLevel), loggingLevelAttr);

            var logLevelSwitch = new LoggingLevelSwitch(newLevel);

            _configFileLog = new LoggerConfiguration()
                .MinimumLevel.ControlledBy(logLevelSwitch)
                .Enrich.FromLogContext()
                .WriteTo
                .File($@"{AppDomain.CurrentDomain.BaseDirectory}\Logs\Config Log [{Path.GetFileNameWithoutExtension(FilePath)}] {
                            DateTime.Now:MM-dd-yyyy hh-mm-ss}.txt")//, 
                                                                   //shared: true,
                                                                   //buffered: true)
                .CreateLogger();
        }

        private void LoadSyncSettings()
        {
            CurrentValueService.MaxCVRefreshRate = Convert.ToDouble(ConfigurationManager.AppSettings.Get("MaxCVRefreshRate"));
            if (CurrentValueService.MaxCVRefreshRate > 60)
                CurrentValueService.MaxCVRefreshRate = 60;
            if (CurrentValueService.MaxCVRefreshRate < 1)
            {
                CurrentValueService.MaxCVRefreshRate = 1;
            }
            var tempConfig = ConfigurationManager.AppSettings.Get("MinsBetweenPointSync");
            if (tempConfig == null)
            {
                _minsBetweenPointSync = 5;
            }
            else
            {
                _minsBetweenPointSync = Convert.ToDouble(tempConfig);
            }
            if (_minsBetweenPointSync > 60)
                _minsBetweenPointSync = 60;
            if (_minsBetweenPointSync < 1)
                _minsBetweenPointSync = 1;
        }



        public void CancelAll()
        {
            _configFileLog.Debug("Cancel all.");

            //_checkForNewPointsTimer.Dispose();
            //_checkForNewPointDataTimer.Dispose();

            Cts.Cancel();
        }

        private readonly SemaphoreSlim _lockPointCheck = new SemaphoreSlim(1, 1);

        private readonly SemaphoreSlim _schemaLock = new SemaphoreSlim(1, 1);
        private SimpleCombinedTableSchema _mySchema;
        private DateTime _lastSchemaRefreshTime;

        private async Task<SimpleCombinedTableSchema> GetTableSchema()
        {
            try
            {
                await _schemaLock.WaitAsync();

                _configFileLog.Debug($"Trying to read table schmema");

                if (_mySchema == null || (DateTime.Now - _lastSchemaRefreshTime).TotalMinutes > 15)
                {
                    _configFileLog.Debug("Schema needed to be refreshed, reading from table");

                    _mySchema = await Task.Run(() => SqlServerConnectionUtility.GetTableSchema(
                            SrcDataModel.SqlConfigModel.SqlGeneralOpts,
                            SrcDataModel.SqlConfigModel.TableMappingRules.ToList(),
                            Cts.Token),
                        Cts.Token);

                    _lastSchemaRefreshTime = DateTime.Now;
                }

                return _mySchema;
            }
            catch (Exception ex)
            {
                _configFileLog.Error(ex, "Failed to get schema with exception:");
                return null;
            }
            finally
            {
                _schemaLock.Release();
            }
        }

        public int NumberOfPointUpdates { get; private set; }

        private async Task<(bool isValid, SimpleCombinedTableSchema schema)> GetValidatedSchema()
        {
            Status = "Getting table schema";

            var thisSchema = await GetTableSchema();
            Status = "Validating Config";

            if (thisSchema != null && CygNetSqlDataCompareModel.ValidateRulesDataOnly(thisSchema).IsValid)
            {
                Status = "Config valid";
                return (true, thisSchema);
            }
            else
            {
                Status = "Config is NOT valid";
                return (false, null);
            }
        }

        public async Task UpdatePointList()
        {
            var swPoint = new Stopwatch();

            try
            {
                await _lockPointCheck.WaitAsync();

                // Start timer
                swPoint.Start();

                // Record num point checks
                _configFileLog.Debug($"Cancellation at start of updatepointlist is {Cts.IsCancellationRequested}");
                NumberOfPointUpdates += 1;
                _configFileLog.Information("Starting point update check #[{NumberOfPointUpdates}]", NumberOfPointUpdates);

                // Check if schema is valid
                var (configIsValid, thisSchema) = await GetValidatedSchema();
                if (!configIsValid)
                {
                    _configFileLog.Warning("Failed Validation in UpdatePointsList, removing all pointsync objects");
                    Status = "Failed Validation";

                    // Validation failed, cancel all point ops
                    foreach (var pnt in MyPoints)
                    {
                        pnt?.CancelAll();
                    }

                    MyPoints.Clear();
                }

                _configFileLog.Debug($@"Starting UpdatePointList");
                Status = "Updating PointsList";

                var pointsFound = await FetchCygNetPoints();

                var pointsFoundSet = new HashSet<CachedCygNetPointWithRule>(pointsFound);



                Cts.Token.ThrowIfCancellationRequested();
                _configFileLog.Debug("Cancellation");

                var newPointSyncObjs = new List<PointSync>();

                if (pointsFoundSet.IsAny())
                {
                    _configFileLog.Debug("Point search successful with [{FoundCount}] points found",
                        pointsFoundSet.Count);

                    HashSet<ICachedCygNetPoint> existingPoints =
                        new HashSet<ICachedCygNetPoint>(MyPoints.Select(ps => ps.SrcPoint));

                    var newPoints = pointsFoundSet.Where(pt => !existingPoints.Contains(pt)).ToList();

                    foreach (var newPoint in newPoints)
                    {
                        var newPointSync = new PointSync(this, newPoint, _configFileLog, Cts.Token);
                        newPointSyncObjs.Add(newPointSync);
                        MyPoints.Add(newPointSync);
                    }

                    var pointsNoLongerAvailable = MyPoints.Where(ps => !pointsFoundSet.Contains(ps.SrcPoint)).ToList();

                    if (pointsNoLongerAvailable.Any())
                    {
                        _configFileLog.Warning(
                            "Removing [{UnavailablePointCount}] points which are no longer available",
                            pointsNoLongerAvailable.Count);

                        foreach (var pnt in pointsNoLongerAvailable)
                        {
                            pnt?.CancelAll();
                            MyPoints.Remove(pnt);
                        }
                    }

                    //if (newPointSyncObjs.Any())
                    //{
                    //    await CheckForNewPointData();
                    //}
                }
                else
                {
                    _configFileLog.Warning(
                        $"No points were found, clearing all existing points and stopping all syncs");

                    MyPoints.ToList().ForEach(pnt => pnt?.CancelAll());

                    MyPoints.Clear();
                }

                Status = "Waiting";

            }
            catch (Exception ex)
            {
                _configFileLog.Error(ex, $"Exception during point check");
                Status = "Exception during point check";
            }
            finally
            {
                SqlConnection.ClearAllPools();
                //OdbcConnection.ReleaseObjectPool();
                GC.Collect();
                GC.WaitForPendingFinalizers();

                _configFileLog.Debug("Finished Point update check #[{NumberOfPointUpdates}] with duration [{dur}]", _numNewDataCheck, swPoint.Elapsed);

                _lockPointCheck.Release();
            }
        }

        private async Task<List<PointSync>> GetCurrentPoints(List<CachedCygNetPointWithRule> existingPointsToExclude)
        {
            var pointsFound = await FetchCygNetPoints();
            var pointsFoundSet = new HashSet<CachedCygNetPointWithRule>(pointsFound);

            Cts.Token.ThrowIfCancellationRequested();
            _configFileLog.Debug("Cancellation");

            if (pointsFoundSet.IsAny())
            {
                _configFileLog.Debug("Point search successful with [{FoundCount}] points found",
                    pointsFoundSet.Count);

                //var existingPoints =
                //    new HashSet<ICachedCygNetPoint>(MyPoints.Select(ps => ps.SrcPoint));

                var newPoints = pointsFoundSet   //.Where(pt => !existingPoints.Contains(pt))
                    .Select(pnt => new PointSync(this, pnt, _configFileLog, Cts.Token)).ToList();

                return newPoints;
            }

            return new List<PointSync>();
        }

        private async Task<List<CachedCygNetPointWithRule>> FetchCygNetPoints()
        {
            var resolver = new CachedFacilityResolver(SrcDataModel.CygNetGeneral);
            _configFileLog.Debug(
                $"CachedFacilityResolver made with {SrcDataModel.CygNetGeneral.FacSiteService} and {SrcDataModel.CygNetGeneral.FixedDomainId}");

            // Get base facilities
            var facResults = await Task.Run(() => resolver.FindFacilities(Cts.Token));

            if (facResults.Excp != null)
            {
                Status = "Facility search exception";
                throw facResults.Excp;
            }

            var pointsFound = new List<CachedCygNetPointWithRule>();

            if (facResults.SearchSucceeded && facResults.FacsFound.IsAny())
            {
                var (points, _) = await Task.Run(() =>
                    CygNetPointResolver.GetPointsAsync(
                        SrcDataModel.SqlConfigModel.SourcePointRules.ToList(),
                        facResults.FacsFound,
                        SrcDataModel.CygNetGeneral.FacilityFilteringRules.ChildGroups.ToList(),
                        Cts.Token));

                if (points.IsAny())
                {
                    pointsFound = MaxPoints >= 0 ? points.Take(MaxPoints).ToList() : points.ToList();
                }
                _configFileLog.Debug(
                    $"Point Search Succeeded = {points.IsAny()} and CancellationRequested is {Cts.IsCancellationRequested}");
            }

            return pointsFound;
        }

        private bool _validation = true;

        int _numNewDataCheck = 0;

        private TimeSpan optMaxTimePerMaintenanceOperation = TimeSpan.FromMinutes(4.5);

        private int _taskNum = 0;

        private async Task CheckForNewPointData()
        {
            Interlocked.Increment(ref _taskNum);

            var swNewData = new Stopwatch();

            try
            {
                await _lockPointCheck.WaitAsync();
                swNewData.Start();

                _numNewDataCheck += 1;
                _configFileLog.Warning("Starting point new data check #[{_numNewDataCheck}]", _numNewDataCheck);

                _validation = CygNetSqlDataCompareModel.ValidateRulesDataOnly((await GetTableSchema())).IsValid;
                if (_validation)
                {
                    _configFileLog.Warning($"Validation succeeded, looking for points due for checking");
                    Status = "Checking for New point Data";

                    Cts.Token.ThrowIfCancellationRequested();
                    var pointsDueForCheck = MyPoints
                        .Where(pnt => !pnt.IsBusy)
                        .ToList();

                    //var pointsDueForCheck = MyPoints
                    //    .Where(pnt => !pnt.IsBusy)
                    //    .Where(item => item.NextOperationScheduledTime <= DateTime.Now).ToList();

                    if (pointsDueForCheck.Any())
                    {
                        var thisResult = new ConfigSyncOperationResults(
                            totalPointCount: pointsDueForCheck.Count);

                        if (ServiceOps.LogAllOpResults)
                            PointSyncResults.Add(thisResult);

                        Interlocked.Increment(ref _taskNum);
                        _configFileLog.Warning("Starting sync tasks for [{PointCount}] points that are due for a sync, Task #{TaskNum}. Current Memory: {mem}",
                            pointsDueForCheck.Count, _taskNum, Process.GetCurrentProcess().WorkingSet64);

                        var thisSchema = await GetTableSchema();
                        if (thisSchema == null)
                        {
                            _configFileLog.Warning($"Failed to get SqL table schema, stopping sync");
                            return;
                        }

                        var thisSqlOp = new SqlOperation(thisSchema, Cts.Token);

                        //using (thisSqlOp.GetConnection(Cts.Token))
                        using (thisSqlOp.GetBulkCopier())
                        {
                            thisResult.StartNewDataMaintenance();

                            // general maintenance
                            await pointsDueForCheck.ForEachAsync(20, async pntTask =>
                            {
                                //Interlocked.Increment(ref TestOpCount);
                                //Console.WriteLine("testOpCount1: " + TestOpCount);
                                Cts.Token.ThrowIfCancellationRequested();
                                var (rowsInserted, rowsDeleted) = await Task.Run(() => pntTask.SyncData(thisSqlOp, thisSchema, false, Cts.Token));

                                await thisResult.RecordMaintenancePointProc(
                                    numRowsIns: rowsInserted,
                                    numRowsDel: rowsDeleted);
                                //Interlocked.Decrement(ref TestOpCount);
                                //Console.WriteLine("testOpCount2: " + TestOpCount);
                            });

                            thisResult.EndNewData();

                            _configFileLog.Warning(
                                "Finishing sync tasks for [{PointCount}] points that are due for a sync, Task #{TaskNum}. Current Memory: {mem}.",
                                pointsDueForCheck.Count,
                                _taskNum,
                                Process.GetCurrentProcess().WorkingSet64);

                            _configFileLog.Warning(
                                "Operation statistics for Task #{TaskNum} so far (after new data maintenance): {@opres}",
                                _taskNum,
                                thisResult);


                            //thisResult.EndOperation(); // remove later. test code:
                            //return;                    // remove later. Test Code;

                            // backfill maintenance

                            List<PointSync> pointDueForBackfill = pointsDueForCheck.Where(pnt => pnt.NeedsBackfill).ToList();
                            while (thisResult.OperationStopwatch.Elapsed < optMaxTimePerMaintenanceOperation &&
                                pointDueForBackfill.IsAny())
                            {
                                _configFileLog.Warning("thisResult.OperationStopwatch.Elapsed:" + thisResult.OperationStopwatch.Elapsed);
                                _configFileLog.Warning("optMaxTimePerMaintenanceOperation: " + optMaxTimePerMaintenanceOperation);


                                thisResult.StartBackfill(pointDueForBackfill.Count);

                                _configFileLog.Warning(
                                    "Starting Backfill for [{PointCount}] points that are due for a sync, Task #{TaskNum}. Current Memory: {mem}",
                                    pointDueForBackfill.Count, _taskNum, Process.GetCurrentProcess().WorkingSet64);

                                await pointDueForBackfill.ForEachAsync(10, thisResult.OperationStopwatch,
                                    optMaxTimePerMaintenanceOperation, async pntTask =>
                                    {
                                        Cts.Token.ThrowIfCancellationRequested();
                                        var (rowsInserted, rowsDeleted) = await Task.Run(() =>
                                            pntTask.SyncData(thisSqlOp, thisSchema, true, Cts.Token));

                                        await thisResult.RecordBackfillOperation(
                                            pntTask.SrcPoint.Tag.ToString(),
                                            rowsInserted);
                                    });

                                _configFileLog.Warning(
                                    "Finishing backfill tasks for [{PointCount}] points that are due for a sync, Task #{TaskNum}. Current Memory: {mem}.",
                                    pointDueForBackfill.Count,
                                    _taskNum,
                                    Process.GetCurrentProcess().WorkingSet64);

                                pointDueForBackfill = pointsDueForCheck.Where(pnt => pnt.NeedsBackfill).ToList();
                            }

                            thisResult.EndBackfill();

                            thisResult.EndOperation();

                            _configFileLog.Warning(
                                "Final operation statistics for Task #{TaskNum}: {@opres}",
                                _taskNum,
                                thisResult);
                        }
                    }
                }
                else
                {
                    _configFileLog.Debug("Failed Validation in CheckForNewPointData");
                    Status = "Failed config validation";
                }
            }
            catch (Exception ex)
            {
                _configFileLog.Error(ex, "Failed new data check #[{numCheck}]", _numNewDataCheck);
            }
            finally
            {
                //SqlConnection.ClearAllPools();

                if (_validation)
                    Status = "Waiting";

                _configFileLog.Debug(
                    "Finished point new data check #[{_numNewDataCheck}] with duration [{dur}]",
                    _numNewDataCheck,
                    swNewData.Elapsed);

                _lockPointCheck.Release();
            }
        }

        [XmlIgnore()]
        public List<PointSync> MyPoints
        {
            get => GetPropertyValue<List<PointSync>>();
            set => SetPropertyValue(value);
        }

        [XmlIgnore()]
        public PointSync SelectedPoint
        {
            get => GetPropertyValue<PointSync>();
            set => SetPropertyValue(value);
        }
    }
}
