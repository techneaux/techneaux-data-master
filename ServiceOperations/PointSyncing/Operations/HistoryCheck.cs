﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CygNetRuleModel.Resolvers;
using CygNetRuleModel.Resolvers.History;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxDataSyncService.Helper;
using TechneauxDataSyncService.PointSyncing.Operations.Base;
using TechneauxHistorySynchronization;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.Helper;
using TechneauxUtility;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    public class HistoryCheck : TimedOperation, ISqlSyncOperation
    {
        public HistoryCheck(CachedCygNetPointWithRule srcPnt)
        {
            if (srcPnt == null)
                throw new ArgumentNullException(nameof(srcPnt), "Src Point must not be empty");

            if (srcPnt.SrcRule == null || !srcPnt.SrcRule.IsRuleValid)
                throw new ArgumentException("Src point rule must be non-null and valid");

            SrcPoint = srcPnt;

            EarliestDate = DateTime.Now.AddDays(-RetentionDays);
        }

        #region LocalVars

        private CachedCygNetPointWithRule SrcPoint { get; }

        private DateTime EarliestDate { get; } 
        private DateTime LatestDate { get; } = DateTime.Now;

        #endregion

        private double RetentionDays => SrcPoint.SrcRule.PollingOptions.RetentionDays;

        private StringBuilder _pointSyncAuditTrailBuilder = new StringBuilder();

        public SimpleSqlSyncResults Results { get; } = new SimpleSqlSyncResults(nameof(HistoryCheck));
        public ISqlSyncDetailedResults DetailedHistoryResults { get; } = new SqlSyncDetailedResults();


        public async Task DoOperationAsync(
            SqlOperation sqlOp,
            IProgress<(int, string)> progress, 
            CancellationToken ct)
        {
            try
            {
                // Start timing operation
                StartTiming();

                Results.OperationInterval = (EarliestDate, LatestDate);
                                
                if (SrcPoint.SrcRule.HistoryNormalizationOptions.IsExtendedNormType)
                {
                    if (!(await SrcPoint.HasHistory(SrcPoint.SrcRule.GeneralHistoryOptions.IncludeUnreliable, true)))
                    {
                        Results.OpResult = OpStatuses.Fail;
                        Results.StatusMessage = "History does not exist.";
                        return;
                    }

                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage =
                        "This is an extended norm type and history does exist.";
                    return;
                }

                if (await SrcPoint.HasHistoryInInterval(
                    EarliestDate,
                    LatestDate,
                    SrcPoint.SrcRule.GeneralHistoryOptions.IncludeUnreliable,
                    includeNumericOnly: true))
                {
                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage =
                        "There is history in the retention period.";
                    return;
                }
                else
                {
                    Results.OpResult = OpStatuses.Fail;
                    Results.StatusMessage = "There is no history in the retention period";
                    return;
                }
            }
            catch (Exception ex)
            {
                Results.FailedException = ex;
                Results.OpResult = OpStatuses.Exception;
                Results.StatusMessage = "General operation failure - First Check";
            }
            finally
            {
                Results.TimeTakenPerformingOperation = GetTotalTimeTaken();
            }

        }

    }


}
