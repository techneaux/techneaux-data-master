﻿using System;
using System.Threading;
using System.Threading.Tasks;
using TechneauxDataSyncService.Helper;
using TechneauxDataSyncService.PointSyncing.Operations.Base;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.Helper;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    public class HistoryDeleteOld : TimedOperation, ISqlSyncOperation
    {
        public HistoryDeleteOld(
            SimpleCombinedTableSchema newSchema, 
            CachedCygNetPointWithRule srcPnt)
        {
            Schema = newSchema;

            if (Schema == null || Schema.IsEmptySqlSchema)
                throw new ArgumentNullException(nameof(newSchema), "Schema must not be null or empty");
            if (srcPnt == null)
                throw new ArgumentNullException(nameof(srcPnt), "Src Point must not be empty");

            if (srcPnt.SrcRule == null || !srcPnt.SrcRule.IsRuleValid)
                throw new ArgumentException("Src point rule must be non-null and valid");
            SrcPoint = srcPnt;
            
        }

        #region LocalVars

        private SimpleCombinedTableSchema Schema { get; }
        private CachedCygNetPointWithRule SrcPoint { get; }

        private double RetentionDays => SrcPoint.SrcRule.PollingOptions.RetentionDays;

        #endregion

        
        public SimpleSqlSyncResults Results { get; } = new SimpleSqlSyncResults(nameof(HistoryDeleteAll));
        public ISqlSyncDetailedResults DetailedHistoryResults { get; } = new SqlSyncDetailedResults();
        

        public async Task DoOperationAsync(
            SqlOperation sqlOp,
            IProgress<(int, string)> progress, 
            CancellationToken ct)
        {
            try
            {
                // Start timing operation
                StartTiming();
                
                StartSubtask(nameof(sqlOp.DeleteSqlRows));

                Results.NumSqlRowsDeleted =
                    await sqlOp.DeleteSqlRows(SrcPoint, RetentionDays, ct);

                //Results.NumSqlRowsDeleted = await SqlRowResolver.DeleteExpiredSqlForPoint(Schema, SrcPoint, SrcPoint.SrcRule.PollingOptions.RetentionDays,ct);
            
                ct.ThrowIfCancellationRequested();
                Results.LogAuditMessage(GetSubTaskDuration(nameof(sqlOp.DeleteSqlRows)).ToString());
                Results.LogAuditMessage($"Sql expired rows delete finished with {Results.NumSqlRowsDeleted} rows deleted.");
      
                Results.StatusMessage = "Finished expired history delete.";
            }
            catch (Exception ex)
            {
                Results.FailedException = ex;
                Results.OpResult = OpStatuses.Exception;
                Results.StatusMessage = "General operation failure - Old History Delete";
            }
            finally
            {
                Results.TimeTakenPerformingOperation = GetTotalTimeTaken();
            }
        }
    }
}
