﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper;
using TechneauxDataSyncService.Helper;
using TechneauxDataSyncService.PointSyncing.Operations.Base;
using TechneauxHistorySynchronization;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.Helper;
using TechneauxUtility;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    public class InitialSetup : TimedOperation, ISqlSyncOperation
    {
        public InitialSetup(
            SimpleCombinedTableSchema simpleSchema,
            CachedCygNetPointWithRule srcPnt,
            int numberRows)
        {
            if (simpleSchema == null || simpleSchema.IsEmptySqlSchema)
                throw new ArgumentNullException(nameof(simpleSchema), "Schema must not be null or empty");

            if (srcPnt == null)
                throw new ArgumentNullException(nameof(srcPnt), "Src Point must not be empty");

            if (srcPnt.SrcRule == null || !srcPnt.SrcRule.IsRuleValid)
                throw new ArgumentException("Src point rule must be non-null and valid");
            Schema = simpleSchema;

            SrcPoint = srcPnt;
            NumRows = numberRows;
        }

        #region LocalVars

        private SimpleCombinedTableSchema Schema { get; }
        private CachedCygNetPointWithRule SrcPoint { get; }

        public DateTime EarliestCommonBackfillTime
        {
            get;
            private set;
        }

        private DateTime EarliestDate { get; set; }
        private DateTime LatestDate { get; set; }

        private string TimeColId => Schema.TimeColumn.SqlTableFieldName;
        private int NumRows { get; }

        private double RetentionDays => SrcPoint.SrcRule.PollingOptions.RetentionDays;
        private DateTime RetentionPeriodEarliestDate => DateTime.Now.AddDays(-RetentionDays);
        private bool IsHistoryNormalized => SrcPoint.SrcRule.HistoryNormalizationOptions.EnableNormalization;
        private bool IsHistoryNormalizedExtended => SrcPoint.SrcRule.HistoryNormalizationOptions.IsExtendedNormType;
        private TimeSpan NormInterval => SrcPoint.SrcRule.HistoryNormalizationOptions.TimespanBetweenNormEntries;

        #endregion

        private enum InitialSetupStatus
        {
            [Description("Tried to get recent rows, but no matching rows found in SQL at all")]
            NoHistoryFoundAtAllWhenGettingRecentRows,

            [Description("The most recent rows have invalid timestamp column values")]
            RecentRowsBadTimestampValues,

            [Description("There was no CygNet history in the same range as most recent SQL rows")]
            NoCygNetHistInRangeOfRecentSqlRows
        }




        public SimpleSqlSyncResults Results { get; private set; } = new SimpleSqlSyncResults(nameof(InitialSetup));
        public ISqlSyncDetailedResults DetailedHistoryResults { get; } = new SqlSyncDetailedResults();

        public bool IsBackfillNeeded { get; private set; }

        public async Task DoOperationAsync(
            SqlOperation sqlOp,
            IProgress<(int, string)> progress,
            CancellationToken ct)
        {
            try
            {
                // Start timing operation
                StartTiming();

                DetailedHistoryResults.FixedCygNetValues = await Schema.GetFixedColumnsAndValues(SrcPoint, ct);

                if (!await IsRecentSqlRowsValidAndMatchingCygNet(sqlOp, ct))
                    return;

                IsBackfillNeeded = await IsBackfillRequired(sqlOp, ct);
            }
            catch (Exception ex)
            {
                Results.FailedException = ex;
                Results.OpResult = OpStatuses.Exception;
                Results.StatusMessage = "General operation failure - First Check";
            }
            finally
            {
                Results.TimeTakenPerformingOperation = GetTotalTimeTaken();
            }
        }

        private async Task<bool> IsRecentSqlRowsValidAndMatchingCygNet(SqlOperation sqlOp, CancellationToken ct)
        {
            var compareModel = new CygNetSqlDataCompareModel(SrcPoint);

            // -- Get sql rows
            var latestSqlRows = await GetRecentSqlHistoryRows(sqlOp, ct);

            Results.NumTotalSqlRows = latestSqlRows?.Count;
            
            // Is there any sql data at all?
            if (!latestSqlRows.IsAny())
            {
                // No history is a Pass condition
                Results.OpResult = OpStatuses.Pass;
                Results.MostRecentRawHistTimestamp = DateTime.Now.AddHours(-1);
                Results.MostRecentNormTimestamp = DateTime.Now.AddHours(-1);
                Results.StatusMessage = InitialSetupStatus.NoHistoryFoundAtAllWhenGettingRecentRows.GetErrorDescription();
                EarliestCommonBackfillTime = DateTime.Now.AddHours(-1);
                IsBackfillNeeded = true;
                return false;
            }

            // Do the rows have valid timestamps?
            var rowsWithInvalidTimestamps = latestSqlRows.Where(row =>
                !row[TimeColId].IsDateTime).ToList();

            if (rowsWithInvalidTimestamps.Any())
            {
                Results.OpResult = OpStatuses.Fail;
                Results.StatusMessage = InitialSetupStatus.RecentRowsBadTimestampValues.GetErrorDescription();
                return false;
            }

            var sqlTs = latestSqlRows.Select(row => row[TimeColId]).ToList();

            // Check for cancellation
            ct.ThrowIfCancellationRequested();

            // Get earliest and latest dates
            EarliestDate = latestSqlRows.Min(row => row[TimeColId].ToDateTime()).AddSeconds(-1);
            LatestDate = latestSqlRows.Max(row => row[TimeColId].ToDateTime()).AddSeconds(1);

            Results.LogAuditMessage($"Latest: {LatestDate}");
            Results.LogAuditMessage($"Earliest: {EarliestDate}");

            // Get dates in local time (what is this for Sean?)
            var early = new DateTime(EarliestDate.Ticks, DateTimeKind.Local);
            var late = new DateTime(LatestDate.Ticks, DateTimeKind.Local);

            Results.LogAuditMessage($"newEarliest: {early}");
            Results.LogAuditMessage($"newLatest: {late}");

            // Collect CygNet history in the same timeframe as the SQL rows
            var includeNumericOnly = PointSync.CheckForNumeric(sqlOp);
            var cygHist = await compareModel.GetCygNetHistory(
                early,
                late,
                SrcPoint.SrcRule.GeneralHistoryOptions,
                SrcPoint.SrcRule.HistoryNormalizationOptions,
                null,
                null,
                SrcPoint.SrcRule.PollingOptions.RetentionDays,
                includeNumericOnly,
                ct);

            // If no CygNet history, there can't be any SQL rows (watch out, if we try to fill in gaps with 0's for normalized data this won't be true)
            if (!cygHist.IsAnyRawHistory)
            {
                Results.OpResult = OpStatuses.Fail;
                Results.StatusMessage = InitialSetupStatus.NoCygNetHistInRangeOfRecentSqlRows.GetErrorDescription();
                return false;
            }

            //note on this. need to explain this.
            Results.NumRawCygNetEntriesFound = cygHist.RawHistoryEntries.Count;
            if (Results.NumRawCygNetEntriesFound > 1)
            {
                Results.MostRecentRawHistTimestamp = cygHist
                    .RawHistoryEntries
                    .Select(entry => entry.AdjustedTimestamp)
                    .Max();
            }
            else
            {
                //Results.MostRecentRawHistTimestamp = cygHist
                //    .RawHistoryEntries
                //    .Select(entry => entry.AdjustedTimestamp)
                //    .Max()
                //    .Subtract(SrcPoint.SrcRule.HistoryNormalizationOptions.TimespanBetweenNormEntries);
                Results.MostRecentRawHistTimestamp = cygHist
                 .RawHistoryEntries
                .Select(entry => entry.AdjustedTimestamp)
                .Max();
            }

            if (cygHist.NormalizedHistoryEntries.IsAny())
            {
                var validNormEntries = cygHist.NormalizedHistoryEntries.Where(entry => entry.IsValid && !entry.IsValueUncertain).ToList();
                Results.LogAuditMessage($"validNormEntries Count: {validNormEntries.Count}");
                Results.NumNormEntriesFound = cygHist.NormalizedHistoryEntries.Count;
                Results.NumValidNormEntriesFound = cygHist.NormalizedHistoryEntries.Count(entry => entry.IsValid && !entry.IsValueUncertain);
                Results.NumCertainNormEntriesFound =
                    cygHist.NormalizedHistoryEntries.Count(entry => entry.IsValid && !entry.IsValueUncertain);

                if (validNormEntries.Any())
                    Results.MostRecentNormTimestamp = validNormEntries.Max(entry => entry.NormalizedHistoryValue.AdjustedTimestamp);
                else
                    Results.MostRecentNormTimestamp = Results.MostRecentRawHistTimestamp;
            }
            else
            {
                Results.MostRecentNormTimestamp = Results.MostRecentRawHistTimestamp;
            }

            DetailedHistoryResults.CygNetSrcRawHistory = cygHist.RawHistoryEntries;
            DetailedHistoryResults.CygNetSrcNormalizedHistory = cygHist.NormalizedHistoryEntries;

            var cygHistRows = await compareModel.GetCygNetRows(cygHist, Schema.CygNetMappingRules, ct);
            var cygTs = cygHistRows.Select(x => x[Schema.TimeColumn.SqlTableFieldName].StringValue);
            var rawTs = cygHist.RawHistoryEntries.Select(x => x.AdjustedTimestamp);

            Results.LogAuditMessage($"cygRawTS: {string.Join(" , ", rawTs)}");

            if (!cygHistRows.IsAny())
            {
                AuditCompareResultData(sqlTs, cygHist, cygTs);

                EarliestCommonBackfillTime = DateTime.Now.AddHours(-1);

                Results.OpResult = OpStatuses.Fail;
                Results.StatusMessage = "No cygnet rows resolved from history";
                return false;
            }

            Results.NumCygNetRows = cygHistRows.Count;

            StartSubtask(nameof(compareModel.GetComparedRowsCygRounded));
            //comparing cyg resolved and sql to determine if audit is valid

            var latestComparedRows = compareModel.GetComparedRowsCygRounded(Schema.TimeColumn.SqlTableFieldName, cygHistRows, latestSqlRows);

            Results.LogAuditMessage(GetSubTaskDuration(nameof(compareModel.GetComparedRowsCygRounded)).ToString());

            Results.LogAuditMessage($"cyg TS: {string.Join(" , ", cygTs)}");
            Results.LogAuditMessage($"sql TS: {string.Join(" , ", sqlTs)}");

            // All sql rows should match, so no "sql only" rows should be found

            bool compareIsEqual = !(latestComparedRows.Any(elm => elm.ComparisonValue != MappedSqlRowCompare.CygSqlCompare.Equal));


            DetailedHistoryResults.ComparedRows = latestComparedRows;
            DetailedHistoryResults.SqlSrcRows = latestSqlRows;


            Results.NumMatchingSqlRows =
                latestComparedRows.Count(row => row.ComparisonValue == MappedSqlRowCompare.CygSqlCompare.Equal);

            if (compareIsEqual)
            {
                Results.OpResult = OpStatuses.Pass;
                Results.StatusMessage = "Data matches";
                return true;
            }
            else
            {
                RecordSqlAndCygRowsToAuditLog(latestSqlRows, cygHistRows);

                Results.OpResult = OpStatuses.Fail;
                Results.StatusMessage = "Comparison failed";
                return false;
            }
        }

        private void AuditCompareResultData(List<IConvertibleValue> sqlTs, CygNetSqlDataCompareModel.CygNetHistoryResults cygHist, IEnumerable<string> cygTs)
        {
            Results.LogAuditMessage($"sql TS: {string.Join(" , ", sqlTs)}");
            Results.LogAuditMessage($"CygHistLatest: {cygHist.LatestTime}");
            Results.LogAuditMessage($"CygHistEarliest: {cygHist.EarliestTime}");
            Results.LogAuditMessage($"isnormalizesd: {cygHist.IsNormalized}");
            Results.LogAuditMessage($"cyghistrawcount: {cygHist.RawHistoryEntries?.Count}");
            Results.LogAuditMessage($"cyg TS: {string.Join(" , ", cygTs)}");
        }

        private async Task<bool> IsBackfillRequired(SqlOperation sqlOp, CancellationToken ct)
        {
            // -- Get sql rows
            StartSubtask(nameof(sqlOp.GetEarliestXRowsFromDb));

            List<SqlHistoryRow> earliestSqlRows = await sqlOp.GetEarliestXRowsFromDb(SrcPoint, ct);

            ct.ThrowIfCancellationRequested();
            Results.LogAuditMessage(GetSubTaskDuration(nameof(sqlOp.GetEarliestXRowsFromDb)).ToString());

            if (earliestSqlRows.IsAny() && (earliestSqlRows.Any(row => !row.ContainsKey(TimeColId)) ||
               earliestSqlRows.Any(row => !row[TimeColId].TryGetDateTime(out var dt))))
            {
                Results.OpResult = OpStatuses.Fail;
                Results.StatusMessage = "Some sql rows didn't have values for time column, or had invalid values";
                return true;
            }

            if (!earliestSqlRows.IsAny())
            {
                // If no sql data, backfill def needed
                return true;
            }

            // Next get earliest SQL entry
            var earliestSqlDate = earliestSqlRows.Min(row => row[TimeColId].ToDateTime());

            // Check if earliest SQL entry is within one norm window of the retention period
            if (earliestSqlDate <= RetentionPeriodEarliestDate.Add(NormInterval))
                return false;

            ct.ThrowIfCancellationRequested();

            EarliestCommonBackfillTime = earliestSqlDate.AddSeconds(-1);

            switch (IsHistoryNormalized)
            {
                case true:
                    var normLatestSearch = earliestSqlDate
                        .AddSeconds(-1)
                        .Add(NormInterval)
                        .Add(NormInterval);

                    switch (IsHistoryNormalizedExtended)
                    {
                        case true:
                            var extNormEarliestSearch = RetentionPeriodEarliestDate.AddYears(-5);

                            return await SrcPoint.HasHistoryInInterval(
                                extNormEarliestSearch,
                                normLatestSearch,
                                SrcPoint.SrcRule.GeneralHistoryOptions.IncludeUnreliable, includeNumericOnly: true);

                        default:
                            var regNormEarliestSearch = RetentionPeriodEarliestDate
                                .Subtract(NormInterval)
                                .Subtract(NormInterval);

                            return await SrcPoint.HasHistoryInInterval(
                                regNormEarliestSearch,
                                normLatestSearch,
                                SrcPoint.SrcRule.GeneralHistoryOptions.IncludeUnreliable, includeNumericOnly: true);
                    }

                default:
                    var rawEarliestTimestampSearch = RetentionPeriodEarliestDate;
                    var rawLatestTimestampSearch = earliestSqlDate.AddSeconds(-1);

                    return await SrcPoint.HasHistoryInInterval(
                        rawEarliestTimestampSearch,
                        rawLatestTimestampSearch,
                        SrcPoint.SrcRule.GeneralHistoryOptions.IncludeUnreliable, includeNumericOnly: true);
            }
        }



        private void RecordSqlAndCygRowsToAuditLog(List<SqlHistoryRow> latestSqlRows, List<CygNetRuleModel.Resolvers.CygNetHistoryRow> cygHistRows)
        {
            Results.LogAuditMessage("cygHistRows: ");
            foreach (var row in cygHistRows)
            {
                Results.LogAuditMessage($"{string.Join(" , ", row.Values.ToList().Select(x => x.StringValue))}");
            }

            Results.LogAuditMessage("latestSqlRows: ");
            foreach (var row in latestSqlRows)
            {
                Results.LogAuditMessage($"{string.Join(" , ", row.Values.ToList().Select(x => x.StringValue))}");
            }
        }

        private async Task<List<SqlHistoryRow>> GetRecentSqlHistoryRows(
            SqlOperation sqlOp,
            CancellationToken ct)
        {
            StartSubtask(nameof(sqlOp.GetLatestXRowsFromDb));

            // Collect recent SQL history
            var latestSqlRows = await sqlOp.GetLatestXRowsFromDb(SrcPoint, ct);

            Results.LogAuditMessage(GetSubTaskDuration(nameof(sqlOp.GetLatestXRowsFromDb)).ToString());
            Results.NumTotalSqlRows = latestSqlRows.Count;

            return latestSqlRows;
        }
    }
}
