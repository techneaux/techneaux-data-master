﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Serilog;
using System.Threading;
using System.Threading.Tasks;
using CygNetRuleModel.Resolvers;
using CygNetRuleModel.Resolvers.History;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxDataSyncService.Helper;
using TechneauxDataSyncService.PointSyncing.Operations.Base;
using TechneauxHistorySynchronization;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory; 
using TechneauxReportingDataModel.Helper;
using TechneauxUtility;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    public class MaintenanceSync : ISqlSyncOperation
    {
        private CachedCygNetPointWithRule SrcPoint { get; }
        private DateTime LastCygNetTimestamp { get; }
        private DateTime LastNormTimestamp { get; }
        private SimpleCombinedTableSchema TableSchema { get; }
        private IDictionary<string, IConvertibleValue> OldCachedFixedValues { get; }
        private DateTime NewNormTimestamp { get; set; }
        private DateTime EarliestDate { get; set; }
        private DateTime LatestDate { get; }

        public SimpleSqlSyncResults Results { get; private set; } = new SimpleSqlSyncResults(nameof(MaintenanceSync));
        public ISqlSyncDetailedResults DetailedHistoryResults { get; } = new SqlSyncDetailedResults();

        public MaintenanceSync(
            SimpleCombinedTableSchema newSchema,
            CachedCygNetPointWithRule newSrcPoint,
            IDictionary<string, IConvertibleValue> oldCachedFixedValues,
            DateTime lastCygNetRawHist,
            DateTime lastValidNormTimestamp)
        {
            TableSchema = newSchema;
            SrcPoint = newSrcPoint;
            OldCachedFixedValues = oldCachedFixedValues;
            LastCygNetTimestamp = lastCygNetRawHist;
            LastNormTimestamp = lastValidNormTimestamp;

            if (IsHistoryNormalized)
            {
                EarliestDate = LastNormTimestamp.Add(
                        SrcPoint.SrcRule.HistoryNormalizationOptions.TimespanBetweenNormEntries)
                    .AddSeconds(-5);
            }
            else
            {
                EarliestDate = LastCygNetTimestamp.AddSeconds(1);
            }
            LatestDate = DateTime.Now;
            Results.OperationInterval = (EarliestDate, LatestDate);
        }

        private bool IsHistoryNormalized => SrcPoint.SrcRule.HistoryNormalizationOptions.EnableNormalization;
        private bool IsExtendedNorm => SrcPoint.SrcRule.HistoryNormalizationOptions.IsExtendedNormType;


        public bool NewHistFound { get; private set; }

        private SqlOperation _ThisSqlOp;

        public async Task DoOperationAsync(
            SqlOperation sqlOp,
            IProgress<(int, string)> progress,
            CancellationToken ct)
        {
            _ThisSqlOp = sqlOp;
           
            NewHistFound = false;
            var sw = new Stopwatch();
            sw.Start();
            if (IsHistoryNormalized)
            {
                NewHistFound = await CheckNewNormalizedHistory();
            }
            else
            {
                NewHistFound = await CheckNewRawHistory();
            }
            sw.Stop();
            Log.Debug("Check before maintenance sync took " + sw.Elapsed + " for pnt :" + SrcPoint.LongId);
            sw.Reset();
            sw.Start();
            if(NewHistFound)
            {
                NewHistFound = await AddNewHistoryToSqlAsync(ct);
                
            }
            sw.Stop();
            Log.Debug("AddNewHistoryToSqlAsync took " + sw.Elapsed + " for pnt :" + SrcPoint.LongId);
            sw.Reset();
        }

        private async Task<bool> CheckNewRawHistory()
        {
            if (!await SrcPoint.HasNewValueAfter(LastCygNetTimestamp))
            {
                Results.MostRecentNormTimestamp = LastNormTimestamp;
                Results.MostRecentRawHistTimestamp = LastCygNetTimestamp;
                Results.OpResult = OpStatuses.Pass;
                Results.StatusMessage = "No new history.";
                return false;
            }               
            return true;
        }

        private async Task<bool> CheckNewNormalizedHistory()
        {
            // Is there a new norm timestamp possible between last written NORM TIMESTAMP (that was inserted into SQL) and NOW
            // -- i.e. Has the span of a single norm interval passed since the list NORM TIMESTAMP which was inserted into SQL
            var test = SrcPoint.SrcRule.HistoryNormalizationOptions.GetNormalizedTimestampsInWindow(LastNormTimestamp, DateTime.Now);
            if (!SrcPoint.SrcRule.HistoryNormalizationOptions
                .GetNormalizedTimestampsInWindow(LastNormTimestamp, DateTime.Now).Any())
            {
                Results.MostRecentNormTimestamp = LastNormTimestamp;
                Results.MostRecentRawHistTimestamp = LastCygNetTimestamp;
                Results.OpResult = OpStatuses.Pass;
                Results.StatusMessage = "Can't fit in new norm timestamp.";
                return false;                
            }
                
            // EXTENDED ASSUMES: No new history will ever be found in the VHS that is dated earlier than the last time we _LOOKED_
            if (!IsExtendedNorm)            
            {
                // REGULAR NORM ASSUMES: No new history will be found in VHS that is dated earlier than the last history timestamp we read
                if(! await SrcPoint.HasNewValueAfter(LastCygNetTimestamp)) //are there any new raw values after last cygnet value retrieved.
                {
                    Results.MostRecentNormTimestamp = LastNormTimestamp;
                    Results.MostRecentRawHistTimestamp = LastCygNetTimestamp;
                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage = "No new history.";                    
                    return false; 
                }                                           
            }            
            return true;
            //do maintenance sync
        }

        private async Task<bool> AddNewHistoryToSqlAsync(CancellationToken ct)
        {
            try
            {
                var swAddNew = new Stopwatch();
                
                var newCachedValues = await TableSchema.GetFixedColumnsAndValues(SrcPoint, ct);
                var (isEqual, keyNames, failureReason) = newCachedValues.IsEqualToCommonKeysVerbose(OldCachedFixedValues);
                if(isEqual == false)
                {
                    Results.OpResult = OpStatuses.Fail;
                    Results.LogAuditMessage($"New fixed values are : {string.Join(",", newCachedValues.Values.Select(x => x.StringValue))}");
                    Results.LogAuditMessage($"Old fixed values are: {string.Join(",", newCachedValues.Values.Select(x => x.StringValue))}");

                    Results.StatusMessage = "Fixed values do not match";
                    return false;
                }

                swAddNew.Stop();

                Log.Debug("Maint sync fixed vals check took " + swAddNew.Elapsed + " for pnt :" + SrcPoint.LongId);
                swAddNew.Reset();
                ct.ThrowIfCancellationRequested();
                //checks for new hist first before sync
                //if (!await SrcPoint.HistoryAvailableInInterval(EarliestDate, LatestDate))
                //{
                //    Results.OpResult = OpStatuses.Pass;
                //    Results.MostRecentNormTimestamp = LastNormTimestamp;
                //    Results.MostRecentRawHistTimestamp = LastCygNetTimestamp;
                //    Results.StatusMessage = "No new history found";
                //    return;
                //}
                swAddNew.Start();
                var compareModel = new CygNetSqlDataCompareModel(SrcPoint);
                ct.ThrowIfCancellationRequested();
                //if(EarliestDate == DateTime.MinValue)
                //{
                //    Console.WriteLine("minvalue maintenance");
                //}
                //if(EarliestDate < LatestDate.AddYears(-10))
                //{
                //    Console.WriteLine("toosmall earliest");
                //}
                var includeNumericOnly = PointSync.CheckForNumeric(_ThisSqlOp);
                var cygHist = await Task.Run(() => compareModel.GetCygNetHistory(EarliestDate, LatestDate, SrcPoint.SrcRule, null, includeNumericOnly,ct), ct);

                if (!cygHist.IsAnyRawHistory)
                {
                    Results.MostRecentNormTimestamp = LastNormTimestamp;
                    Results.MostRecentRawHistTimestamp = LastCygNetTimestamp;
                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage = "Didn't find any history on second attempt";
                    return false;
                }
                swAddNew.Stop();
                Log.Debug("Maint sync get history and history check took " + swAddNew.Elapsed + " for pnt :" + SrcPoint.LongId);
                swAddNew.Reset();
                swAddNew.Start();
                Results.NumRawCygNetEntriesFound = cygHist.RawHistoryEntries.Count;
                Results.MostRecentRawHistTimestamp = cygHist.RawHistoryEntries.Max(entry => entry.AdjustedTimestamp);
                DetailedHistoryResults.CygNetSrcRawHistory = cygHist.RawHistoryEntries;

                if (cygHist.NormalizedHistoryEntries.IsAny())
                {
                    var validNormEntries = cygHist
                        .NormalizedHistoryEntries
                        .Where(entry => entry.IsValid && !entry.IsValueUncertain)
                        .ToList();

                    if (validNormEntries.Any())
                    {
                        Results.MostRecentNormTimestamp =
                                            validNormEntries.Max(entry => entry.NormalizedHistoryValue.AdjustedTimestamp);
                    }
                    else
                    {
                        Results.MostRecentNormTimestamp = LastNormTimestamp;
                    }

                    Results.NumNormEntriesFound = cygHist.NormalizedHistoryEntries.Count;
                    Results.NumValidNormEntriesFound = cygHist.NormalizedHistoryEntries.Count(entry => entry.IsValid & !entry.IsValueUncertain);
                    Results.NumCertainNormEntriesFound =
                        cygHist.NormalizedHistoryEntries.Count(entry => entry.IsValid && !entry.IsValueUncertain);

                    DetailedHistoryResults.CygNetSrcNormalizedHistory = cygHist.NormalizedHistoryEntries;
                }
                else
                {
                    Results.MostRecentNormTimestamp = Results.MostRecentRawHistTimestamp;
                }

                ct.ThrowIfCancellationRequested();
                List<CygNetHistoryRow> cygRows = await compareModel.GetCygNetRows(cygHist, TableSchema.CygNetMappingRules, ct);
                swAddNew.Stop();
                Log.Debug("Maint sync normalized entry check and resolving took " + swAddNew.Elapsed + " for pnt :" + SrcPoint.LongId);
                swAddNew.Reset();
                swAddNew.Start();
                //if (!cygRows.IsAny() && cygHist.NormalizedHistoryEntries.IsAny())
                //    Console.WriteLine("dd");
                if (cygRows.IsAny())
                {
                    Results.NumCygNetRows = cygRows.Count;

                    //await Task.Run(() => compareModel.UpdateSqlTableFast(TableSchema, cygRows.Cast<ValueSet>().ToList(), ct), ct);
                    var insertCount = await Task.Run(() => _ThisSqlOp.AddNewRows(cygRows.Cast<ValueSet>().ToList(), ct), ct);

                    Results.NumSqlRowsInserted = insertCount;

                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage = "New rows inserted";
                    swAddNew.Stop();
                    Log.Debug("Maint sync update sql took " + swAddNew.Elapsed + " for pnt :" + SrcPoint.LongId);
                    return true;
                }
                else
                {
                    Results.MostRecentNormTimestamp = LastNormTimestamp;
                    Results.MostRecentRawHistTimestamp = LastCygNetTimestamp;
                    Results.OpResult = OpStatuses.Pass;

                    Results.StatusMessage = "No new rows inserted.";
                    swAddNew.Stop();
                    Log.Debug("Main sync no new rows inserted took " + swAddNew.Elapsed  + " for pnt :" + SrcPoint.LongId);
                    return false;
                }
            }
            catch (Exception ex)
            {
                Results.FailedException = ex;
                Results.OpResult = OpStatuses.Exception;
                Results.StatusMessage = "General operation failure - MaintenanceSync";
                return false;
            }
            finally
            {
                //Results.LoggingMessages = _pointSyncAuditTrailBuilder.ToString();
                //Results.TimeTakenPerformingOperation = GetTotalTimeTaken();
            }
        }

        // Possible results
        // - Not normalized, and no new raw history
        // - Isn't possible to fit new norms timestamp between last one and current datetime (don't care new history exists or not)
        // - New norm timestamp can fit, but regular normalization enabled, but no new raw history
        // - New norm timestamp can fit, but regular normalization enabled, and new history doesn't lead to new norm value
        // - New norm timestamp can fit, but regular normalization enabled, and new history does lead to new norm value 
        // - Got through all the above, found history, but failed to make cygnet rows because of a runtime data collection or sql table issue

    }
}
