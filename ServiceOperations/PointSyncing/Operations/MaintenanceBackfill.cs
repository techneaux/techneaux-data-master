﻿using CygNetRuleModel.Resolvers;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TechneauxDataSyncService.PointSyncing.Operations.Base;
using TechneauxHistorySynchronization.Helper;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.Helper;
using TechneauxUtility;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    public class MaintenanceBackfill : ISqlSyncOperation
    {
        public const int MaxEntriesPerBackfill = 10000;

        private CachedCygNetPointWithRule SrcPoint { get; }
        private DateTime LastCygNetTimestamp { get; set; }
        private DateTime LastNormTimestamp { get; set; }
        private SimpleCombinedTableSchema TableSchema { get; }
        private IDictionary<string, IConvertibleValue> OldCachedFixedValues { get; }
        private DateTime NewNormTimestamp { get; set; }
        private DateTime EarliestDate { get; set; }
        private DateTime LatestDate { get; }

        public SimpleSqlSyncResults Results { get; private set; } = new SimpleSqlSyncResults(nameof(MaintenanceBackfill));
        public ISqlSyncDetailedResults DetailedHistoryResults { get; } = new SqlSyncDetailedResults();

        private double RetentionDays => SrcPoint.SrcRule.PollingOptions.RetentionDays;

        private bool IncludeUnreliable => SrcPoint.SrcRule.GeneralHistoryOptions.IncludeUnreliable;

        public bool IsBackfillStillNeeded { get; private set; }
        public bool IsBackfillStillNeededNorm { get; private set; }
        private bool IncludeNumericOnly { get; set; }

        public MaintenanceBackfill(
            SimpleCombinedTableSchema newSchema,
            CachedCygNetPointWithRule newSrcPoint,
            IDictionary<string, IConvertibleValue> oldCachedFixedValues,
            DateTime lastCygNetRawBackfillHist,
            DateTime lastValidNormBackfillTimestamp)
        {
            TableSchema = newSchema;
            SrcPoint = newSrcPoint;
            OldCachedFixedValues = oldCachedFixedValues;
            LastCygNetTimestamp = lastCygNetRawBackfillHist;
            LastNormTimestamp = lastValidNormBackfillTimestamp;

            EarliestDate = DateTime.Now.AddDays(-RetentionDays);
            LatestDate = lastCygNetRawBackfillHist;
            Results.OperationInterval = (EarliestDate, LatestDate);
        }

        private bool IsHistoryNormalized => SrcPoint.SrcRule.HistoryNormalizationOptions.EnableNormalization;
        private bool IsExtendedNorm => SrcPoint.SrcRule.HistoryNormalizationOptions.IsExtendedNormType;

        private SqlOperation _thisSqlOp;

        public async Task DoOperationAsync(
            SqlOperation op,
            IProgress<(int, string)> progress,
            CancellationToken ct)
        {
            _thisSqlOp = op;

            IncludeNumericOnly = PointSync.CheckForNumeric(_thisSqlOp);

            try
            {
                // Check if any new history to add
                var newHistFound = false;
                if (IsHistoryNormalized)
                {
                    newHistFound = await CheckForBackfillNormalizedHistory();

                    if (!newHistFound)
                    {
                        Results.EarliestNormTimestamp = LastNormTimestamp;
                        Results.OpResult = OpStatuses.Pass;
                        Results.StatusMessage = "Can't fit in new norm timestamp in early part of retention period.";
                        return;
                    }

                    await AddNewHistoryToSqlAsync(ct);
                    IsBackfillStillNeeded = IsBackfillStillNeededNorm;                                       
                }
                else
                {
                    newHistFound = await CheckNewRawHistory();
                    if (!newHistFound)
                    {
                        Results.EarliestRawHistTimestamp = LastCygNetTimestamp;
                        Results.OpResult = OpStatuses.Pass;
                        Results.StatusMessage = "Can't fit in new norm timestamp.";
                        return;
                    }

                    IsBackfillStillNeeded = !(await AddNewHistoryToSqlAsync(ct));
                    if(!IsBackfillStillNeeded)
                    {
                        IsBackfillStillNeeded = await CheckNewRawHistory();

                    }                    
                }
            }
            catch (Exception ex)
            {
                Results.FailedException = ex;
                Results.OpResult = OpStatuses.Exception;
                Results.StatusMessage = "General operation failure - MaintenanceBackfill";
            }
        }

        private async Task<bool> CheckNewRawHistory()
        {
            if (!await SrcPoint.HasHistoryInInterval(EarliestDate, LastCygNetTimestamp, IncludeUnreliable, IncludeNumericOnly))
            {
                return false;
            }
            return true;
        }

        private async Task<bool> CheckForBackfillNormalizedHistory()
        {
            // Is there a new norm timestamp possible between last written NORM TIMESTAMP (that was inserted into SQL) and NOW
            // -- i.e. Has the span of a single norm interval passed since the list NORM TIMESTAMP which was inserted into SQL
            //var test = SrcPoint.SrcRule.HistoryNormalizationOptions.GetNormalizedTimestampsInWindow(LastNormTimestamp, DateTime.Now);

            if (!SrcPoint.SrcRule.HistoryNormalizationOptions
                .GetNormalizedTimestampsInWindow(EarliestDate, LastNormTimestamp.AddMilliseconds(-500)).Any())
            {
                return false;
            }
            return true;
            //do backfill
        }

        private async Task<bool> AddNewHistoryToSqlAsync(CancellationToken ct)
        {
            try
            {
                var swAddNew = new Stopwatch();

                var newCachedValues = await TableSchema.GetFixedColumnsAndValues(SrcPoint, ct);
                var (isEqual, keyNames, failureReason) = newCachedValues.IsEqualToCommonKeysVerbose(OldCachedFixedValues);
                if (isEqual == false)
                {
                    Results.OpResult = OpStatuses.Fail;
                    Results.LogAuditMessage($"New fixed value- are : {string.Join(",", newCachedValues.Values.Select(x => x.StringValue))}");
                    Results.LogAuditMessage($"Old fixed values are: {string.Join(",", newCachedValues.Values.Select(x => x.StringValue))}");

                    Results.StatusMessage = "Fixed values do not match";
                    IsBackfillStillNeededNorm = true;
                    return false;
                }

                swAddNew.Stop();

                Log.Debug("Maint sync fixed vals check took " + swAddNew.Elapsed + " for pnt :" + SrcPoint.LongId);
                swAddNew.Reset();
                ct.ThrowIfCancellationRequested();

                swAddNew.Start();
                var compareModel = new CygNetSqlDataCompareModel(SrcPoint);
                ct.ThrowIfCancellationRequested();
                CygNetSqlDataCompareModel.CygNetHistoryResults cygHist;
                if (IsHistoryNormalized)
                {
                    cygHist = await Task.Run(() => compareModel.GetCygNetHistory(EarliestDate, LastNormTimestamp, SrcPoint.SrcRule, null, IncludeNumericOnly, MaxEntriesPerBackfill, ct), ct);
                }
                else
                {
                    cygHist = await Task.Run(() => compareModel.GetCygNetHistory(EarliestDate, LatestDate, SrcPoint.SrcRule, null, IncludeNumericOnly, MaxEntriesPerBackfill, ct), ct);
                }
 
                if (!cygHist.IsAnyRawHistory)
                {
                    Results.MostRecentRawHistTimestamp = LastCygNetTimestamp;
                    Results.EarliestRawHistTimestamp = LastCygNetTimestamp;
                    Console.WriteLine("Sean check out you know what section of code.");
                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage = "Didn't find any history on second attempt";
                    IsBackfillStillNeededNorm = false;
                    return false;
                }

                swAddNew.Stop();
                Log.Debug("Maint sync get history and history check took " + swAddNew.Elapsed + " for pnt :" + SrcPoint.LongId);
                swAddNew.Reset();
                swAddNew.Start();

                Results.NumRawCygNetEntriesFound = cygHist.RawHistoryEntries.Count;
                Results.EarliestRawHistTimestamp = cygHist.RawHistoryEntries.Min(entry => entry.AdjustedTimestamp);
                Results.MostRecentRawHistTimestamp = cygHist.RawHistoryEntries.Max(entry => entry.AdjustedTimestamp);

                DetailedHistoryResults.CygNetSrcRawHistory = cygHist.RawHistoryEntries;

                if (IsHistoryNormalized)
                {
                    if (!cygHist.NormalizedHistoryEntries.IsAny())
                    {
                        Results.MostRecentNormTimestamp = LastNormTimestamp;
                        Results.EarliestNormTimestamp = LastNormTimestamp;

                        Results.OpResult = OpStatuses.Pass;
                        Results.StatusMessage = "Didn't find any history on second attempt";
                        IsBackfillStillNeededNorm = false;
                        return false;
                    }

                    var validNormEntries = cygHist
                        .NormalizedHistoryEntries
                        .Where(entry => entry.IsValid && !entry.IsValueUncertain)
                        .ToList();

                    if (validNormEntries.Any())
                    {
                        Results.MostRecentNormTimestamp =
                            validNormEntries.Max(entry => entry.NormalizedTimestamp);

                        Results.EarliestNormTimestamp = validNormEntries.Min(ent => ent.NormalizedTimestamp);
                    }
                    else
                    {
                        Results.MostRecentNormTimestamp = LastNormTimestamp;
                        Results.EarliestNormTimestamp = LastNormTimestamp;
                    }

                    Results.NumNormEntriesFound = cygHist.NormalizedHistoryEntries.Count;
                    Results.NumValidNormEntriesFound =
                        cygHist.NormalizedHistoryEntries.Count(entry => entry.IsValid & !entry.IsValueUncertain);
                    Results.NumCertainNormEntriesFound =
                        cygHist.NormalizedHistoryEntries.Count(entry => entry.IsValid && !entry.IsValueUncertain);

                    //DetailedHistoryResults.CygNetSrcNormalizedHistory = cygHist.NormalizedHistoryEntries;
                }
                
                ct.ThrowIfCancellationRequested();
                var cygRows = await compareModel.GetCygNetRows(cygHist, TableSchema.CygNetMappingRules, ct);

                cygRows = cygRows.Where(row =>
                    row[TableSchema.SqlTimeColumn.Name].ToDateTime().ToSqlDateTime().Value < 
                        LatestDate.ToSqlDateTime().Value).ToList();

                if (cygRows.IsAny())
                {
                    Results.NumCygNetRows = cygRows.Count;
                    
                    var insertCount = await Task.Run(() => _thisSqlOp.AddNewRows(cygRows.Cast<ValueSet>().ToList(), ct), ct);

                    Results.NumSqlRowsInserted = insertCount;
                    LastCygNetTimestamp = Results.EarliestRawHistTimestamp.AddSeconds(-1);
                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage = "New rows inserted";
                    if (cygHist.RawHistoryEntries.Count == 10000)
                    {
                        IsBackfillStillNeededNorm = true;
                    }
                    else
                    {
                        IsBackfillStillNeededNorm = false;
                    }
                    return true;
                }
                else
                {
                    Results.MostRecentNormTimestamp = LastNormTimestamp;
                    Results.MostRecentRawHistTimestamp = LastNormTimestamp;
                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage = "No new rows inserted.";
                    swAddNew.Stop();
                    Log.Debug("Main sync no new rows inserted took " + swAddNew.Elapsed + " for pnt :" + SrcPoint.LongId);
                    IsBackfillStillNeededNorm = false;
                    return false;
                }
            }
            catch (Exception ex)
            {
                Results.FailedException = ex;
                Results.OpResult = OpStatuses.Exception;
                Results.StatusMessage = "General operation failure - MaintenanceBackfill";
                return false;
            }
            finally
            {
                //Results.LoggingMessages = _pointSyncAuditTrailBuilder.ToString();
                //Results.TimeTakenPerformingOperation = GetTotalTimeTaken();
            }
        }

        // Possible results
        // - Not normalized, and no new raw history
        // - Isn't possible to fit new norms timestamp between last one and current datetime (don't care new history exists or not)
        // - New norm timestamp can fit, but regular normalization enabled, but no new raw history
        // - New norm timestamp can fit, but regular normalization enabled, and new history doesn't lead to new norm value
        // - New norm timestamp can fit, but regular normalization enabled, and new history does lead to new norm value 
        // - Got through all the above, found history, but failed to make cygnet rows because of a runtime data collection or sql table issue

    }
}
