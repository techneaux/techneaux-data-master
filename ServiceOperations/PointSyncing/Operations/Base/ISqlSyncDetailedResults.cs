﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CygNetRuleModel.Resolvers;
using CygNetRuleModel.Resolvers.History;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxHistorySynchronization;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxUtility;

namespace TechneauxDataSyncService.PointSyncing.Operations.Base
{
    public interface ISqlSyncDetailedResults
    {
        Dictionary<string, IConvertibleValue> FixedCygNetValues { get; set; }

        List<CygNetHistoryEntry> CygNetSrcRawHistory { get; set; }

        List<HistoryNormalization.NormalizedHistoryEntry> CygNetSrcNormalizedHistory { get; set; }

        List<SqlHistoryRow> SqlSrcRows { get; set; }

        List<MappedSqlRowCompare> ComparedRows { get; set; }
    }

    class SqlSyncDetailedResults : ISqlSyncDetailedResults
    {
        public Dictionary<string, IConvertibleValue> FixedCygNetValues { get; set; }
        public List<CygNetHistoryEntry> CygNetSrcRawHistory { get; set; }
        public List<HistoryNormalization.NormalizedHistoryEntry> CygNetSrcNormalizedHistory { get; set; }
        public List<SqlHistoryRow> SqlSrcRows { get; set; }
        public List<MappedSqlRowCompare> ComparedRows { get; set; }
    }
}
