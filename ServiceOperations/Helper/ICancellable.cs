﻿namespace TechneauxDataSyncService.Helper
{
    public interface ICancellable
    {
        void CancelAll();
    }
}
