﻿using System.Collections.Generic;
using System.ComponentModel;

namespace TechneauxDataSyncService.Helper
{
    public static class EnumExtensions
    {
        public static void RemoveAndCancel(this ICollection<ICancellable> srcList, ICancellable itemToRemove)
        {
            itemToRemove.CancelAll();
            srcList.Remove(itemToRemove);
        }

        public static void RemoveAndCancel(this BindingList<ICancellable> srcList, ICancellable itemToRemove)
        {
            itemToRemove.CancelAll();
            srcList.Remove(itemToRemove);
        }

    }
}
