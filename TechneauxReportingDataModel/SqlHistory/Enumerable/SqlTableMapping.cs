using System.Xml.Serialization;
using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.SqlHistory.Enumerable
{
    //Subclass containing column definition information
    public class SqlTableMapping : NotifyCopyDataModel, IValidatedRule
    {
        public SqlTableMapping()
        {
            SqlTableFieldName = "";
            TimeColumn = false;
            CygNetElementRule = new CygNetRuleBase();
        }

        [XmlAttribute]
        public bool TimeColumn
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string SqlTableFieldName
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public CygNetRuleBase CygNetElementRule
        {
            get => GetPropertyValue<CygNetRuleBase>();
            set => SetPropertyValue(value);
        }

        public bool IsRuleValid => CheckIfValid.isValid;
        public string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid
        {
            get
            {
                if (!CygNetElementRule.IsRuleValid)
                    return (CygNetElementRule.IsRuleValid, $"{CygNetElementRule.ValidationErrorMessage}");

                if (string.IsNullOrWhiteSpace(SqlTableFieldName))
                    return (false, $"{nameof(SqlTableFieldName)} must not be empty");

                return (true, "No Error");
            }
        }
    }
}