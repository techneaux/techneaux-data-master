using System.ComponentModel;
using System.Xml.Serialization;
using XmlDataModelUtility;


namespace TechneauxReportingDataModel.SqlHistory.SubOptions
{
    public class SqlGeneralOptions : NotifyCopyDataModel
    {

        public SqlGeneralOptions()
        {
            Username = "";
            Password = "";
            ConnectionErrorMessage = "";
            TableName = "";
            ServerName = "";
            DatabaseName = "";
            SelectedConnectionType = SqlConnectionType.SqlServer;
            AuthenticationType = AuthenticationTypes.Integrated;
        }
        

        public string GetSqlServerConnectionParamString()
        {
            var key = $"{ServerName}|{Username}|{Password}|{AuthenticationType}";
            return key;
        }

   
        public enum AuthenticationTypes
        {
            [Description("Integrated Security (NT)")]
            Integrated,

            [Description("Specific User/Password")]
            UserPassword
        }


        [XmlAttribute]
        public AuthenticationTypes AuthenticationType
        {
            get => GetPropertyValue<AuthenticationTypes>();
            set => SetPropertyValue(value);
        }

        public enum SqlConnectionType
        {
            [Description("SQL Server")]
            SqlServer
        }

        [XmlAttribute]
        public string TableName
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string Username
        {
            get {   if(AuthenticationType == AuthenticationTypes.Integrated)
                    {
                        SetPropertyValue(System.Environment.UserDomainName + "\\" + System.Environment.UserName);
                        Password = "";
                    }    
                    return GetPropertyValue<string>(); }
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string Password
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string DatabaseName
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string ServerName
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlIgnore]
        public string ConnectionErrorMessage
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public SqlConnectionType SelectedConnectionType
        {
            get => GetPropertyValue<SqlConnectionType>();
            set => SetPropertyValue(value);
        }

        public bool HasValidAuthenticationOpts => (AuthenticationType == AuthenticationTypes.UserPassword &&
                                               !string.IsNullOrWhiteSpace(Username) &&
                                               !string.IsNullOrWhiteSpace(Password)) ||
                                              AuthenticationType == AuthenticationTypes.Integrated;

        public bool HasValidServerOpts => HasValidAuthenticationOpts &&
                                       !string.IsNullOrWhiteSpace(ServerName);

        public bool HasValidDatabaseOpts => HasValidServerOpts &&
                                        !string.IsNullOrWhiteSpace(DatabaseName);

        public bool HasValidTableOpts => HasValidDatabaseOpts &&
                                         !string.IsNullOrWhiteSpace(TableName);


        public override bool Equals(object obj)
        {
            if (obj != null && obj is SqlGeneralOptions)
            {
                return obj.ToString() == this.ToString();
            }

            return false;
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override string ToString()
        {
            return $"{ServerName}|{DatabaseName}|{TableName}|{Username}|{Password}|{AuthenticationType}|{SelectedConnectionType}";
        }

    }

}
