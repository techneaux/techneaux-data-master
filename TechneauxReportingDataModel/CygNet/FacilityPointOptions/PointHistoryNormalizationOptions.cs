using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using TechneauxUtility;
using XmlDataModelUtility;
using static TechneauxReportingDataModel.CygNet.FacilityPointOptions.Helper.HistoryNormalization;

namespace TechneauxReportingDataModel.CygNet.FacilityPointOptions
{
    public class PointHistoryNormalizationOptions : NotifyCopyDataModel, IValidatedRule
    {
        public PointHistoryNormalizationOptions()
        {
            // init all vars here
            EnableNormalization = false;
            NormalizeWindowStartTime = new TimeSpan(0, 0, 0);
            NormalizeWindowIntervalLength = 1;
            //AveragingWindow = 0;
            //EnableTakeCurrentValueIfNoHist = false;
            NumWindowHistEntries = -1;
            SelectedSampleType = SamplingType.Take_Nearest_Before_After;
            SelectedNormalizationWindowUnit = NormalizationWindowUnit.Minutes;
        }

        [XmlAttribute]
        public bool EnableNormalization
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public int NumWindowHistEntries
        {
            get => GetPropertyValue<int>();
            set
            {
                if (value >= 1)
                {
                    SelectedNormalizationWindowUnit = NormalizationWindowUnit.Minutes;
                    NormalizeWindowIntervalLength = Math.Round(NormalizeWindowIntervalLength / value * 60);
                }
                SetPropertyValue(-1);
            }
        }

        [XmlAttribute]
        public long NormalizeWindowStartTimeTicks
        {
            get => NormalizeWindowStartTime.Ticks;
            set => NormalizeWindowStartTime = new TimeSpan(value);
        }


        [XmlIgnore]
        public TimeSpan NormalizeWindowStartTime
        {
            get => GetPropertyValue<TimeSpan>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public double NormalizeWindowMultiplier
        {
            get => GetPropertyValue<Double>();
            set => SetPropertyValue(value);
        }

        //[XmlElement("NormalizeWindowStartTime")]
        //public string LegacyProperty1 { get; set; }

        //[XmlElement("NormalizeWindowIntervalLength")]
        //public string LegacyProperty2 { get; set; }

        public enum NormalizationWindowUnit
        {
            [Description("Second(s)")]
            Seconds,
            [Description("Minute(s)")]
            Minutes,
            [Description("Hour(s)")]
            Hours,
            [Description("Day(s)")]
            Days,
        }

        //public TimeSpan NormalizeWindowInitialTime
        //{
        //    get => GetPropertyValue<TimeSpan>();
        //    set => SetPropertyValue(value);
        //}

        [XmlAttribute]
        public NormalizationWindowUnit SelectedNormalizationWindowUnit
        {
            get => GetPropertyValue<NormalizationWindowUnit>();
            set
            {
                SetPropertyValue(value);
                switch (SelectedNormalizationWindowUnit)
                {
                    case NormalizationWindowUnit.Seconds:
                        NormalizeWindowIntervalLength = Math.Min(NormalizeWindowIntervalLength,
                            Convert.ToDecimal(TimeSpan.FromDays(1).TotalSeconds));
                        break;
                    case NormalizationWindowUnit.Minutes:
                        NormalizeWindowIntervalLength = Math.Min(NormalizeWindowIntervalLength,
                            Convert.ToDecimal(TimeSpan.FromDays(1).TotalMinutes));
                        break;
                    case NormalizationWindowUnit.Hours:
                        NormalizeWindowIntervalLength = Math.Min(NormalizeWindowIntervalLength,
                            Convert.ToDecimal(TimeSpan.FromDays(1).TotalHours));
                        break;
                    case NormalizationWindowUnit.Days:
                        NormalizeWindowIntervalLength = Math.Min(NormalizeWindowIntervalLength,
                            Convert.ToDecimal(TimeSpan.FromDays(1).TotalDays));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        [XmlAttribute]
        public Decimal NormalizeWindowIntervalLength
        {
            get => GetPropertyValue<Decimal>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public SamplingType SelectedSampleType
        {
            get => GetPropertyValue<SamplingType>();
            set => SetPropertyValue(value);
        }

        //[XmlAttribute()]
        //public Decimal AveragingWindow
        //{
        //    get { return GetPropertyValue<Decimal>(); }
        //    set { SetPropertyValue(value); }
        //}

        // [XmlAttribute()]
        //public bool EnableTakeCurrentValueIfNoHist
        //{
        //    get { return GetPropertyValue<bool>(); }
        //    set { SetPropertyValue(value); }
        //}

        public enum SamplingType
        {
            [Description("Take Nearest (Before or After)")]
            Take_Nearest_Before_After,

            [Description("Take Nearest (Before)")]
            Take_Nearest_Before,

            [Description("Take Nearest (Before) Extended")]
            Take_Nearest_Before_Extended,

            [Description("Take Nearest (Before or After) Extended")]
            Take_Nearest_Before_After_Extended,

            [Description("Take Nearest (After)")]
            Take_Nearest_After,

            [Description("Simple Average")]
            Simple_Average,

            [Description("[Rollup] Simple Average Extended")]
            Rollup_Simple_Average_Extended,

            [Description("Weighted Average")]
            Weighted_Average,

            [Description("Linear Interpolation")]
            Linear_Interpolation,
        }

        [XmlIgnore]
        public bool IsRuleValid => CheckIfValid.isValid;

        [XmlIgnore]
        public string ValidationErrorMessage => CheckIfValid.message;

        [XmlIgnore]
        public bool IsCalculatedNormType => SelectedSampleType == SamplingType.Linear_Interpolation ||
SelectedSampleType == SamplingType.Simple_Average ||
SelectedSampleType == SamplingType.Rollup_Simple_Average_Extended ||
SelectedSampleType == SamplingType.Weighted_Average;

        [XmlIgnore]
        public bool IsExtendedNormType => SelectedSampleType == SamplingType.Take_Nearest_Before_After_Extended ||
            SelectedSampleType == SamplingType.Take_Nearest_Before_Extended ||
            SelectedSampleType == SamplingType.Rollup_Simple_Average_Extended;
        [XmlIgnore]
        public bool IsRollupNormType => SelectedSampleType ==  SamplingType.Rollup_Simple_Average_Extended;
        private (bool isValid, string message) CheckIfValid => (true, "No Error");

        [XmlIgnore]
        public TimeSpan TimespanBetweenNormEntries
        {
            get
            {
                var normIntervalDbl = Convert.ToDouble(NormalizeWindowIntervalLength);

                // Convert interval to timespan with chosen units
                TimeSpan intervalTs;
                switch (SelectedNormalizationWindowUnit)
                {
                    case NormalizationWindowUnit.Seconds:
                        intervalTs = TimeSpan.FromSeconds(normIntervalDbl);
                        break;
                    case NormalizationWindowUnit.Minutes:
                        intervalTs = TimeSpan.FromMinutes(normIntervalDbl);
                        break;
                    case NormalizationWindowUnit.Hours:
                        intervalTs = TimeSpan.FromHours(normIntervalDbl);
                        break;
                    case NormalizationWindowUnit.Days:
                        intervalTs = TimeSpan.FromDays(normIntervalDbl);
                        break;
                    default:
                        throw new NotImplementedException();
                };

                // Round timespan to nearest second
                return TimeSpan.FromSeconds(Math.Round(intervalTs.TotalSeconds));
            }
        }
        public IEnumerable<NormalizedHistDate> GetNormalizedRollupTimestampsInWindow(DateTime periodStart, DateTime periodEnd)
        {
            //[TEST] if there are no timestamp in the given interval, the function return empty list, no exceptions

            var rolloverBoundary = TimeSpan.FromSeconds(
                Math.Round(NormalizeWindowStartTime.TotalSeconds));

            var targetNormTs = periodStart.Date.AddDays(-1) + rolloverBoundary;

            var _timespanBetweenNormEntries = TimespanBetweenNormEntries;

            var normalizedTimestamps = new List<NormalizedHistDate>();
            
            while (targetNormTs <= periodEnd)
            {
                if (targetNormTs >= periodStart)
                    normalizedTimestamps.Add(new NormalizedHistDate()
                {
                        EarliestTimestampAllowed = targetNormTs - _timespanBetweenNormEntries,
                        TargetTimestamp = targetNormTs,
                        LatestTimestampAllowed = targetNormTs,
                });

                var currentNormDayBoundary = targetNormTs.Date + rolloverBoundary;
                // Handle non-integral rollovers to next 24 hr period when norm hour interval doesn't divide evenly into 24 hours
                if (targetNormTs < currentNormDayBoundary &&
                    targetNormTs + _timespanBetweenNormEntries >= currentNormDayBoundary)
                {
                    targetNormTs = currentNormDayBoundary;
                }
                else
                {
                    targetNormTs = targetNormTs + _timespanBetweenNormEntries;
                }
            }

            return normalizedTimestamps;
        }
        public IEnumerable<NormalizedHistDate> GetNormalizedTimestampsInWindow(DateTime periodStart, DateTime periodEnd)
        {
            //[TEST] if there are no timestamp in the given interval, the function return empty list, no exceptions

            var rolloverBoundary = TimeSpan.FromSeconds(
                Math.Round(NormalizeWindowStartTime.TotalSeconds));

            var targetNormTs = periodStart.Date.AddDays(-1) + rolloverBoundary;

            var _timespanBetweenNormEntries = TimespanBetweenNormEntries;

            var normalizedTimestamps = new List<NormalizedHistDate>();
            var twoWindowNormBuffer = _timespanBetweenNormEntries + _timespanBetweenNormEntries;

            while (targetNormTs <= periodEnd)
            {
                if (targetNormTs >= periodStart)
                    normalizedTimestamps.Add(new NormalizedHistDate()
                    {
                        EarliestTimestampAllowed = targetNormTs - twoWindowNormBuffer,
                        TargetTimestamp = targetNormTs,
                        LatestTimestampAllowed = targetNormTs + twoWindowNormBuffer
                    });

                var currentNormDayBoundary = targetNormTs.Date + rolloverBoundary;
                // Handle non-integral rollovers to next 24 hr period when norm hour interval doesn't divide evenly into 24 hours
                if (targetNormTs < currentNormDayBoundary &&
                    targetNormTs + _timespanBetweenNormEntries >= currentNormDayBoundary)
                {
                    targetNormTs = currentNormDayBoundary;
                }
                else
                {
                    targetNormTs = targetNormTs + _timespanBetweenNormEntries;
                }
            }

            return normalizedTimestamps;
        }

    }
}
