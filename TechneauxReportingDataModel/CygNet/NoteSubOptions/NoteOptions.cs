using GenericRuleModel.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;
using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.CygNet.NoteSubOptions
{
    public class NoteOptions : NotifyCopyDataModel, IValidatedRule
    {
        public NoteOptions()
        {
            NoteHistoryType = NoteHistoryTypesNew.OngoingL;
            NoteType = "";
            AttributeId = new IdDescKey();
            ExcludeBlanks = false;
            OnlyNumeric = false;
            ValueType = NoteValueType.Value;
          
            SmartSheetNoteHistoryTypes = EnumHelper.GetAllValuesAndDescriptions(NoteHistoryType.GetType()).ToList();
            SmartSheetNoteValueTypes = EnumHelper.GetAllValuesAndDescriptions(ValueType.GetType()).ToList();
        }
        [XmlIgnore]
        public List<ValueDescription> SmartSheetNoteHistoryTypes
        {
            get => GetPropertyValue<List<ValueDescription>>();
            set => SetPropertyValue(value);
        }
        [XmlIgnore]
        public List<ValueDescription> SmartSheetNoteValueTypes
        {
            get => GetPropertyValue<List<ValueDescription>>();
            set => SetPropertyValue(value);
        }

        public enum NoteHistoryTypesNew
        {
            [Description("Most Recent Note (by Start Date)")]
            MostRecentPiTNote,
            [Description("Ongoing in Period (Last)")]
            OngoingL,
            [Description("Ongoing in Period (First)")]
            OngoingF,
            [Description("Starts Inside (Last)")]
            StartInsideL,
            [Description("Starts Inside (First)")]
            StartInsideF,
            [Description("Ends Inside (Last)")]
            EndinsideL,
            [Description("Ends Inside (First)")]
            EndinsideF,
            [Description("Start/End Overlaps Contract (Last)")]
            OverlapContractL,
            [Description("Start/End Overlaps Contract (First)")]
            OverlapContractF
        }

        public enum NoteValueType
        {
            [Description("Note Value")]
            Value,
            [Description("Note ID")]
            ID,
            [Description("Note Description")]
            Description
        }

        public NoteHistoryTypesNew NoteHistoryType
        {
            get => GetPropertyValue<NoteHistoryTypesNew>();
            set => SetPropertyValue(value);
        }


        public NoteValueType ValueType
        {
            get => GetPropertyValue<NoteValueType>();
            set => SetPropertyValue(value);
        }

        public string NoteType
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public IdDescKey AttributeId
        {
            get => GetPropertyValue<IdDescKey>();
            set => SetPropertyValue(value);
        }
        public bool ExcludeBlanks
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }
        public bool OnlyNumeric
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        public bool IsRuleValid => CheckIfValid.isValid;
        public string ValidationErrorMessage => CheckIfValid.message;


        private (bool isValid, string message) CheckIfValid
        {
            get
            {
                if (!AttributeId.IsRuleValid)
                    return (false, $"{nameof(AttributeId)} is invalid because the Id is blank.");

                return (true, "No error");
            }
        }


    }
}
