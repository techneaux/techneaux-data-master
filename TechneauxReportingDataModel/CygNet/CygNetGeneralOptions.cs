using System;
using System.Xml.Serialization;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.CygNet
{
    public class CygNetGeneralOptions : NotifyCopyDataModel, IValidatedRule
    {       
        public CygNetGeneralOptions()
        {
            UseCurrentDomain = true;
            FixedDomainId = 5410;
            FacSiteService = "";
            FacilityFilteringRules = new FacilityFilteringOptions();
        }

        [XmlAttribute]
        public bool UseCurrentDomain
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public ushort FixedDomainId
        {
            get => GetPropertyValue<ushort>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string FacSiteService
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public FacilityFilteringOptions FacilityFilteringRules
        {
            get => GetPropertyValue<FacilityFilteringOptions>();
            set => SetPropertyValue(value);
        }

        public bool IsRuleValid => CheckIfValid.isValid;

        public string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid => (true, "No Error");
    }
}
