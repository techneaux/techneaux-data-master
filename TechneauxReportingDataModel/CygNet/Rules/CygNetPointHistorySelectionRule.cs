using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.CygNet.Rules
{
    public class CygNetPointHistorySelectionRule : NotifyCopyDataModel, IValidatedRule
    {
        public CygNetPointHistorySelectionRule()
        {
            PointSelection = new FacilityPointSelection();
            GeneralHistoryOptions = new PointHistoryGeneralOptions();
            HistoryNormalizationOptions = new PointHistoryNormalizationOptions();
        }

        public FacilityPointSelection PointSelection
        {
            get => GetPropertyValue<FacilityPointSelection>();
            set => SetPropertyValue(value);
        }

        public PointHistoryGeneralOptions GeneralHistoryOptions
        {
            get => GetPropertyValue<PointHistoryGeneralOptions>();
            set => SetPropertyValue(value);
        }

        public PointHistoryNormalizationOptions HistoryNormalizationOptions
        {
            get => GetPropertyValue<PointHistoryNormalizationOptions>();
            set => SetPropertyValue(value);
        }

        public bool IsRuleValid => CheckIfValid.isValid;
        public string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid
        {
            get
            {
                if (!PointSelection.IsRuleValid)
                    return (PointSelection.IsRuleValid, $"{PointSelection.ValidationErrorMessage}");

                if (!GeneralHistoryOptions.IsRuleValid)
                    return (GeneralHistoryOptions.IsRuleValid, $"{GeneralHistoryOptions.ValidationErrorMessage}");

                if (!HistoryNormalizationOptions.IsRuleValid)
                    return (HistoryNormalizationOptions.IsRuleValid, $"{HistoryNormalizationOptions.ValidationErrorMessage}");

                return (true, "No Error");
            }
        }

    }
}
