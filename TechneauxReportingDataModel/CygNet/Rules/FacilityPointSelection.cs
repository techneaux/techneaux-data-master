using System.ComponentModel;
using System.Xml.Serialization;
using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.CygNet.Rules
{
    public class FacilityPointSelection : FacilitySelection, ICygNetPointReference, IValidatedRule
    {
        //Default params
        public FacilityPointSelection()
        {
            UDC = "";
            UdcExclusion = "";
            FilteringChoice = FilteringTypes.UDC;
        }

        [XmlAttribute()]
        public string UDC
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
        
        public FilteringTypes FilteringChoice
        {
            get => GetPropertyValue<FilteringTypes>();
            set => SetPropertyValue(value);
        }

        public enum FilteringTypes
        {
            [Description("UDC")]
            UDC,

            [Description("UDC Description")]
            Description,

        }


        [XmlAttribute()]
        public string UdcExclusion
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if(obj != null && obj is FacilityPointSelection fps)
            {
                return fps.ToString() == ToString();
            }

            return false;
        }       

        public override string ToString()
        {
            return $"FacChoice={base.ToString()},Udc={UDC},Exclusion={UdcExclusion},FilteringChoice={FilteringChoice.ToString()}";
        }

        public new bool IsRuleValid => CheckIfValid.isValid;
        public new string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid
        {
            get
            {
                if (!base.IsRuleValid)
                    return (base.IsRuleValid, $"{base.ValidationErrorMessage}");

                if (string.IsNullOrWhiteSpace(UDC))
                    return (false, $"{nameof(UDC)} must not be blank");

                return (true, "No Error");
            }
        }

    }
}
