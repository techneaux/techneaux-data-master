using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.CygNet.Rules
{
    public class CygNetRollupRuleBase : CygNetRuleBase, IValidatedRule
    {
        public CygNetRollupRuleBase()
        {
            FacilityPointOptions = new FacilityPointSelection();
        }

        // Point and fac selection
        public FacilityPointSelection FacilityPointOptions
        {
            get => GetPropertyValue<FacilityPointSelection>();
            set => SetPropertyValue(value);
        }

        public new string ValidationErrorMessage => CheckIfValid.message;

        public new bool IsRuleValid => CheckIfValid.isValid;

        private (bool isValid, string message) CheckIfValid
        {
            get
            {
                if (!base.IsRuleValid)
                    return (false, $"{base.ValidationErrorMessage}");

                if (DataElement == DataElements.PointAttribute || 
                    DataElement == DataElements.PointCurrentValue || 
                    DataElement == DataElements.PointHistory)
                {
                    if(!FacilityPointOptions.IsRuleValid)
                        return (false, $"{FacilityPointOptions.ValidationErrorMessage}");
                }

                return (true, "No Error");


            }
        }


    }
}
