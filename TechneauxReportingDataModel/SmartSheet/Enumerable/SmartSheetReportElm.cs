﻿using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxReportingDataModel.SmartSheet.SubOptions;
using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.SmartSheet.Enumerable
{
    public class SmartSheetReportElm : CygNetRollupRuleBase, IValidatedRule
    {
        public SmartSheetReportElm()
        {
            SmartSheetColumn = "";
        }

        public string SmartSheetColumn
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }


        public new string ValidationErrorMessage => CheckIfValid.message;

        public new bool IsRuleValid => CheckIfValid.isValid;

        private (bool isValid, string message) CheckIfValid
        {
            get
            {
                if (!base.IsRuleValid)
                {
                    return (false, $"{base.ValidationErrorMessage}");
                }
                else if (SmartSheetColumn == null || string.IsNullOrWhiteSpace(SmartSheetColumn))
                {
                    return (false, $"{nameof(SmartSheetColumn)} is invalid because either not selected or blank.");
                }
                else
                {
                    return (true, "No Error");
                }

            }
        }

    }
}
