﻿using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.SmartSheet.SubOptions
{
    public class SheetSelectionOptions : NotifyCopyDataModel, IValidatedRule
    {
        public SheetSelectionOptions() { 
            SheetId = "";
            KeyColumn = "";
        }

        public string SheetId
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public string KeyColumn
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public bool IsRuleValid => CheckIfValid.isValid;

        public string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(SheetId))
                    return (false, $"Sheet Selection is invalid because SheetId is null or blank.");

                if (!string.IsNullOrWhiteSpace(KeyColumn))
                    return (false, $"Key Column Selection is invalid because KeyColumn is null or blank.");

                return (true, "");
            }
        }
    }
}
