using GenericRuleModel.Rules;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.DataRules
{
    public class CalculationRule : NotifyCopyDataModel
    {
        public CalculationRule()
        {
            Formula = "";
            DefaultIfError = "";
            FormatConfig = new FormattingOptions();
        }

        [HelperClasses.ControlTitle("Calculation Formula", "Formula")]
        public string Formula
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
        
        [HelperClasses.ControlTitle("Default If Error", "Def")]
        public string DefaultIfError
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        //Output configuration
        public FormattingOptions FormatConfig
        {
            get => GetPropertyValue<FormattingOptions>();
            set => SetPropertyValue(value);
        }
    }
}
