﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechneauxReportingDataModel.General
{
    public static class AvailableOptions
    {  
        public enum SyncOptions
        {
            [Description("SQL History Sync")]
            SqlHistory,

            [Description("SmartSheet Report")]
            SmartSheet,

            [Description("Sql Sync and SmartSheetReport")]
            SqlAndSmartSheet,

            [Description("Excel/Text Report")]
            Summary,

            [Description("History Export Report")]
            HistoryExport
        }

        public static SyncOptions MainType { get; } = SyncOptions.SqlHistory;

    }
}
