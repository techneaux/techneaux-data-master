﻿namespace TechneauxReportingDataModel.ExcelTextFile
{
    using System.ComponentModel;
    using Enumerable;
    using SubOptions;
    using GenericOptions;
    using XmlDataModelUtility;

    /// <summary>
    /// Defines the <see cref="ExcelTextReportConfigModel" />
    /// </summary>
    public class ExcelTextReportConfigModel : NotifyCopyDataModel
    {
        #region Enums

        /// <summary>
        /// Defines the ReportTypes
        /// </summary>
        public enum ReportTypes
        {
            /// <summary>
            /// Defines the DailyValueList
            /// </summary>
            [Description("List (one day per row)")]
            DailyValueList,
            /// <summary>
            /// Defines the DataTable
            /// </summary>
            [Description("Data Table (history populated across columns)")]
            DataTable,
            /// <summary>
            /// Defines the DataValueSummary
            /// </summary>
            [Description("Summary (single summary row per facility)")]
            DataValueSummary,
            /// <summary>
            /// Defines the FullHistoryExport
            /// </summary>
            [Description("Full History Export")]
            FullHistoryExport
        }

        /// <summary>
        /// Defines the RowConditionOperators
        /// </summary>
        public enum RowConditionOperators
        {
            /// <summary>
            /// Defines the GreaterThan
            /// </summary>
            [Description(">")]
            GreaterThan,
            /// <summary>
            /// Defines the GreaterThanOrEqual
            /// </summary>
            [Description(">=")]
            GreaterThanOrEqual,
            /// <summary>
            /// Defines the LessThan
            /// </summary>
            [Description("<")]
            LessThan,
            /// <summary>
            /// Defines the LessThanOrEqual
            /// </summary>
            [Description("<=")]
            LessThanOrEqual,
            /// <summary>
            /// Defines the NotEqual
            /// </summary>
            [Description("!=")]
            NotEqual,
            /// <summary>
            /// Defines the Like
            /// </summary>
            [Description("Like")]
            Like,
            /// <summary>
            /// Defines the NotLike
            /// </summary>
            [Description("Not Like")]
            NotLike
        }

        #endregion

        #region Properties

        // Report Columns      
        public BindingList<ReportElement> ReportElements
        {
            get => GetPropertyValue<BindingList<ReportElement>>();
            set => SetPropertyValue(value);
        }


        // Build test report options
        /// <summary>
        /// Gets or sets the BuildTestReportOpts
        /// </summary>
        public BuildTestReportOptions BuildTestReportOpts
        {
            get => GetPropertyValue<BuildTestReportOptions>();
            set => SetPropertyValue(value);
        }

        // Contract Hour Options
        /// <summary>
        /// Gets or sets the ContractPeriodOpts
        /// </summary>
        public ContractPeriodOptions ContractPeriodOpts
        {
            get => GetPropertyValue<ContractPeriodOptions>();
            set => SetPropertyValue(value);
        }

        // Report types
        /// <summary>
        /// Gets or sets the DailyValueListOpts
        /// </summary>
        public DailyValueListOptions DailyValueListOpts
        {
            get => GetPropertyValue<DailyValueListOptions>();
            set => SetPropertyValue(value);
        }

        /// <summary>
        /// Gets or sets the DataTableOpts
        /// </summary>
        public DataTableOptions DataTableOpts
        {
            get => GetPropertyValue<DataTableOptions>();
            set => SetPropertyValue(value);
        }

        /// <summary>
        /// Gets or sets the DataValueSummaryOpts
        /// </summary>
        public DataValueSummaryOptions DataValueSummaryOpts
        {
            get => GetPropertyValue<DataValueSummaryOptions>();
            set => SetPropertyValue(value);
        }

        // Email options
        /// <summary>
        /// Gets or sets the EmailOpts
        /// </summary>
        public EmailOptions EmailOpts
        {
            get => GetPropertyValue<EmailOptions>();
            set => SetPropertyValue(value);
        }

        // File options
        /// <summary>
        /// Gets or sets the ExportFileOpts
        /// </summary>
        public ExportFileOptions ExportFileOpts
        {
            get => GetPropertyValue<ExportFileOptions>();
            set => SetPropertyValue(value);
        }

        // Layout and sorting options
        /// <summary>
        /// Gets or sets the LayoutSortingOpts
        /// </summary>
        public LayoutSortingOptions LayoutSortingOpts
        {
            get => GetPropertyValue<LayoutSortingOptions>();
            set => SetPropertyValue(value);
        }

        // Scheduling options (reporting period)
        /// <summary>
        /// Gets or sets the SchedulingOpts
        /// </summary>
        public SchedulingOptions SchedulingOpts
        {
            get => GetPropertyValue<SchedulingOptions>();
            set => SetPropertyValue(value);
        }

        #endregion
    }
}
