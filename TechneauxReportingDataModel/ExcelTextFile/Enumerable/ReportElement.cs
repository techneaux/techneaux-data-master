using System.ComponentModel;
using TechneauxReportingDataModel.CygNet.Rules;

namespace TechneauxReportingDataModel.ExcelTextFile.Enumerable
{
    //Subclass containing column definition information
    public class ReportElement : XmlDataModelUtility.NotifyCopyDataModel
    {
        public ReportElement()
        {
            ColumnHeader = "";
            IsHidden = false;
            IsGrouped = false;
            FieldName = "";
            SummaryType = SummaryTypes.None;
            //PointIdentifier = false;
            SortType = SortTypes.None;
            SortingOrderNumber = 0;
           

            //Default params
            ConfiguredRule = new CygNetRollupRuleBase();
            RuleType = RuleTypes.CygNetRule;
        }

        // ---- Common =>
       
        public string ColumnHeader
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public string FieldName
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public bool IsHidden
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        public bool IsGrouped
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        public enum RuleTypes
        {
            [Description("CygNet Data")]
            CygNetRule,

            [Description("Date")]
            DateRule,

            [Description("Constant Value")]
            ConstantValue,

            [Description("String Expression")]
            StringExpression,

            [Description("Calculation")]
            CalculationRule

            // -- Add back in database options when code is supported
            //[Description("Datebase Entry")]
            //Database
        }

        public RuleTypes RuleType
        {
            get => GetPropertyValue<RuleTypes>();
            set => SetRuleType(value);
        }

        private void SetRuleType(RuleTypes value)
        {
            if (!value.Equals(RuleType))
            {
                switch (value)
                {
                    //TODO implement all types
                    case RuleTypes.CygNetRule:
                        if (!(ConfiguredRule is CygNetRollupRuleBase))
                            ConfiguredRule = new CygNetRollupRuleBase();
                        break;
                    default:
                        RuleType = RuleTypes.CygNetRule;
                        break;
                }
            }
            SetPropertyValue(value, nameof(RuleType));
        }

        public CygNetRollupRuleBase ConfiguredRule
        {
            get => GetPropertyValue<CygNetRollupRuleBase>();
            set => SetPropertyValue(value);
        }

        public enum SummaryTypes
        {
            [Description("None")]
            None,
            [Description("Sum")]
            Sum,
            [Description("Mean")]
            Mean,
            [Description("Max")]
            Max,
            [Description("Min")]
            Min,
            [Description("Count")]
            Count
        }
        public SummaryTypes SummaryType
        {
            get => GetPropertyValue<SummaryTypes>();
            set => SetPropertyValue(value);
        }

        public enum SortTypes
        {
            [Description("None")]
            None,

            [Description("Ascend")]
            Ascending,

            [Description("Descend")]
            Descending
        }
        public SortTypes SortType
        {
            get => GetPropertyValue<SortTypes>();
            set => SetPropertyValue(value);
        }

        public int SortingOrderNumber
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }

        public bool IsTableHistoryValue
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }
    }
}