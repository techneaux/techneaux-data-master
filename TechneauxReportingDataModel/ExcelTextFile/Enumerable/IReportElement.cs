namespace TechneauxReportingDataModel.ExcelTextFile.Enumerable
{
    public interface IReportElement
    {
        bool IsGrouped { get; set; }
        bool IsHidden { get; set; }
        bool IsTableHistoryValue { get; set; }
        string ColumnHeader { get; set; }
        int SortingOrderNumber { get; set; }
        ReportElement.SortTypes SortType { get; set; }
        ReportElement.SummaryTypes SummaryType { get; set; }
    }
}