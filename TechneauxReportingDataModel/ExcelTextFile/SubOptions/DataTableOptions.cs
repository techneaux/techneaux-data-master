using System.ComponentModel;
using System.Xml.Serialization;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.ExcelTextFile.SubOptions
{
    public class DataTableOptions : NotifyCopyDataModel
    {
        public DataTableOptions()
        {
            FileFormat = ExportFileOptions.FileFormatsExcelOnly.xlsx;

            ExcelTemplateOpts = new ExcelTemplateOptions();

            TableDateHeaderSource = DateHeaderSources.ContractStart;
            CustomTableDateHeaderFormat = "MM/dd/yyyy";            
        }

        public ExportFileOptions.FileFormatsExcelOnly FileFormat
        {
            get => GetPropertyValue<ExportFileOptions.FileFormatsExcelOnly>();
            set => SetPropertyValue(value);
        }

        public ExcelTemplateOptions ExcelTemplateOpts
        {
            get => GetPropertyValue<ExcelTemplateOptions>();
            set => SetPropertyValue(value);
        }

        public enum DateHeaderSources
        {
            [Description("Contract Start")]
            ContractStart,

            [Description("Contract End")]
            ContractEnd,
        }

        [XmlAttribute]
        public DateHeaderSources TableDateHeaderSource
        {
            get => GetPropertyValue<DateHeaderSources>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string CustomTableDateHeaderFormat
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
    }
}
