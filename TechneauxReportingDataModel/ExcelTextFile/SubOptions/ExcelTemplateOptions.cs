using System.Xml.Serialization;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.ExcelTextFile.SubOptions
{
    public class ExcelTemplateOptions : NotifyCopyDataModel
    {
        public ExcelTemplateOptions()
        {
            // Excel Options
            UseExcelStyleTemplate = false;
            ExcelTemplateFilePath = "";
        }

        [XmlAttribute]
        public bool UseExcelStyleTemplate
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string ExcelTemplateFilePath
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
    }
}
