using System.Xml.Serialization;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.ExcelTextFile.SubOptions
{
    public class DailyValueListOptions : NotifyCopyDataModel
    {
        public DailyValueListOptions()
        {
            IncludeHeaderRow = true;
            ExcelTemplateOpts = new ExcelTemplateOptions();

            FileFormat = ExportFileOptions.FileFormats.csv;
        }

        [XmlAttribute]
        public ExportFileOptions.FileFormats FileFormat
        {
            get => GetPropertyValue<ExportFileOptions.FileFormats>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public bool IncludeHeaderRow
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        public ExcelTemplateOptions ExcelTemplateOpts
        {
            get => GetPropertyValue<ExcelTemplateOptions>();
            set => SetPropertyValue(value);
        }


    }
}
