using System;
using System.Xml.Serialization;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.GenericOptions
{
    public class ContractPeriodOptions : NotifyCopyDataModel
    {
        public enum ContractPeriodTypes
        {
            HourDuration,
            CustomStartEnd
        }

        public ContractPeriodOptions()
        {
            ContractHour = 8;
            ContractPeriodLengthHours = 24;
        }

        [XmlAttribute]
        public ContractPeriodTypes ContractPeriodType
        {
            get => GetPropertyValue<ContractPeriodTypes>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public int ContractHour
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public int ContractPeriodLengthHours
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public DateTime FixedStartTime
        {
            get => GetPropertyValue<DateTime>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public DateTime FixedEndTime
        {
            get => GetPropertyValue<DateTime>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public bool IncludeDataAtStartTime
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public bool IncludeDataAtEndTime
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }
    }
}
