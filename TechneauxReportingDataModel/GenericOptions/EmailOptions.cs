using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.GenericOptions
{
    public class EmailOptions : NotifyCopyDataModel
    {
        public EmailOptions()
        {
            // Email Options
            FromEmail = "";

            EmailNotif = false;
            EmailReportStarting = false;
            EmailReportComplete = false;

            EmailReport = false;

            UseCustomEmailSubject = false;
            EmailSubject = "";
            EmailRecipientsText = "";

            SMTPPort = 0;
            SMTPServer = "";

            UseAnonymousSMTPSend = true;
            SMTPUser = "";
            SMTPPassword = "";
        }

        // Email Options
        public string EmailRecipientsSerialized
        {
            get => EmailRecipientsText.Replace(Environment.NewLine, ";");
            set
            {
                var Addresses = value.Split(new string[] { Environment.NewLine }, StringSplitOptions.None).ToList();
                EmailRecipientsText = string.Join(Environment.NewLine, Addresses.ToArray());
            }
        }

        [XmlIgnore]
        public List<string> EmailRecipientList
        {
            get
            {
                var Addresses = EmailRecipientsText.Split(new string[] { Environment.NewLine }, StringSplitOptions.None).ToList();
                Addresses = Addresses.Where(addr => !string.IsNullOrWhiteSpace(addr)).Distinct().ToList();

                return Addresses;
            }
        }

        [XmlIgnore]
        public string EmailRecipientsText
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public bool EmailNotif
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public bool EmailReportStarting
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public bool EmailReportComplete
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public bool EmailReport
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public bool UseCustomEmailSubject
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string EmailSubject
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string FromEmail
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public int SMTPPort
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string SMTPServer
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public bool UseAnonymousSMTPSend
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string SMTPUser
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string SMTPPassword
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
    }
}
