﻿using System.ComponentModel;
using XmlDataModelUtility;

namespace ExpressionRuleModel.Rules
{
    public class StringOperations : NotifyCopyDataModel
    {
        public StringOperations()
        {
            OpsType = OperationType.None;
            FirstIndexType = IndexRelation.Fixed;
            FirstIndex = 0;
            NumCharacters = 0;
            ReplaceMatch = "";
            ReplaceWith = "";
            SplitChar = "";
            DefaultIfNoMatch = "";
        }

        public enum OperationType
        {
            None,
            Left,
            Right,
            Mid,
            Replace,
            Split,
            Regex
        }
        [HelperClasses.ControlTitle("Operation Type", "Ops")]
        public OperationType OpsType
        {
            get => GetPropertyValue<OperationType>();
            set => SetPropertyValue(value);
        }

        //********** Left, Right, Mid, and Split only  **********
        public enum IndexRelation
        {
            [Description("Fixed")]
            Fixed,
            [Description("Len -")]
            LengthDiff
        }
        [HelperClasses.ControlTitle("Index Relation To String", "Index")]
        public IndexRelation FirstIndexType
        {
            get => GetPropertyValue<IndexRelation>();
            set => SetPropertyValue(value);
        }

        //********** Left, Right, Mid, and Split only  **********
        [HelperClasses.ControlTitle("Value of index", "Value")]
        public int FirstIndex
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }

        //********** Mid only  **********
        [HelperClasses.ControlTitle("Number of characters", "Chars")]
        public int NumCharacters
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }

        //********** Replace, Regex only  **********
        [HelperClasses.ControlTitle("String to match", "Match")]
        public string ReplaceMatch
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [HelperClasses.ControlTitle("Replace with", "Replace")]
        public string ReplaceWith
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        //********** Split only  **********
        [HelperClasses.ControlTitle("Split character(s)", "Char")]
        public string SplitChar
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        //********** Regex only  **********
        [HelperClasses.ControlTitle("Default if no match", "Def")]
        public string DefaultIfNoMatch
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
    }
}
