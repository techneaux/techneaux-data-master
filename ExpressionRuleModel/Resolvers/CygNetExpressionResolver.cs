﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using ExpressionRuleModel.Rules;
using GenericRuleModel.Rules;
using Serilog;
using Serilog.Context;
using Techneaux.CygNetWrapper;
using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace ExpressionRuleModel.Resolvers
{
    public class CygNetExpressionResolver
    {
        public static async Task<CygNetConvertableValue> Resolve(CygNetExpression cygExp, ICollection<EnumerationTable> GlobalEnums, ICygNetPointReference tagRule, CancellationToken cancelToken, bool testMode = false)
        {
            try
            {
                //get all expression definitions
                var expElemMap = new Dictionary<string, CygNetExpressionElement>();
                foreach (var element in cygExp.ExpressionElements)
                {
                    if (element.ExpName == null || element.ExpName == "")
                        return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.ExpressionErr, "Could not resolve expression, an expression with a empty key was defined: " + element.ExpName);
                    if (expElemMap.ContainsKey(element.ExpName))
                        return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.ExpressionErr, "Could not resolve expression, duplicate expression element definition found for: " + element.ExpName);
                    expElemMap.Add(element.ExpName, element);
                }

                //start parsing expression references
                var expressionString = cygExp.ExpressionStr;
                var regex = new Regex(@"{[A-Za-z0-9\-]+}");
                var match = regex.Match(expressionString);

                //loop until no references are found
                while (match.Success)
                {
                    var expressionKey = match.Value.Replace("{", "").Replace("}", "");
                    //Get refence if it exist
                    if (!expElemMap.ContainsKey(expressionKey))
                        return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.ExpressionErr, "Could not resolve expression, could not find definition for: " + expressionKey);
                    var expElem = expElemMap[expressionKey];

                    //resolve reference
                    var value = await CygNetExpressionElementResolver.Resolve(expElem, expElemMap, GlobalEnums, tagRule, cancelToken, testMode);

                    //if in error, fall out
                    if (value.HasError)
                    {
                        return new CygNetConvertableValue(value.ErrorState, "Expression with key {" + expressionKey + "} could not evaluate:" + value.ErrorDescription);
                    }

                    expressionString = expressionString.Replace(match.Value, value.RawValue?.ToString());

                    //look for another refence
                    match = regex.Match(expressionString);
                }

                //Expression fully resolved
                return new CygNetConvertableValue(expressionString);
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled Exception with expression resolver");
                
                return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.UnknownErr, "Uknown exception thrown[22]: " + ex.Message);
            }
        }

    }
}
