using System;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Markup;



namespace TechneauxWpfControls.DataConverters
{
    public class VisibilityBoolInverseConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            if (value == null)
            {
                return System.Windows.Visibility.Visible;
            }
            else if (value is bool && (bool)value == false)
            {
                return System.Windows.Visibility.Visible;

            }
            else if (value is bool && (bool)value == true)
            {
                return System.Windows.Visibility.Collapsed;
            }
            else
                return System.Windows.Visibility.Visible;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

    }
}
