using System;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Markup;

namespace TechneauxWpfControls.DataConverters
{
    public class ShowIfEmptyString : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var IdString = value as string;

            if(!string.IsNullOrWhiteSpace(IdString))
            {
                return System.Windows.Visibility.Collapsed;
            }
            else
            {
                return System.Windows.Visibility.Visible;
            }
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

    }
}

