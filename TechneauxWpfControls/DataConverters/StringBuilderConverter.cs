﻿using System;
using System.Text;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Markup;

namespace TechneauxWpfControls.DataConverters
{
    public class StringBuilderConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            
            var result = value as StringBuilder;
            return result.ToString();


        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

    }
}
