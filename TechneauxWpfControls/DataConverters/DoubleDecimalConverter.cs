
using System;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Markup;

namespace TechneauxWpfControls.DataConverters
{
    public class DoubleDecimalConverter : MarkupExtension, IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return System.Convert.ToDouble(0);
            }
            if (value as Decimal? == 0)
            {
                return System.Convert.ToDouble(0);
            }            
            
            return System.Convert.ToDouble(value);
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
 
            if (value == null)
            {
                return System.Convert.ToDecimal(0);
            }
            if (value as Double? == 0)
            {
                return System.Convert.ToDecimal(0);
            }

            return System.Convert.ToDecimal(value);
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

    }
}

