﻿using System.Windows.Controls;

namespace TechneauxWpfControls.CygNetFacilityFiltering.View.Sub
{
    /// <summary>
    /// Interaction logic for FacilityServicePicker.xaml
    /// </summary>
    public partial class FacilityServicePicker : UserControl
    {
        public FacilityServicePicker()
        {
            InitializeComponent();
        }
    }
}
