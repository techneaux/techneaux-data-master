﻿using System.Windows.Controls;
using System.Windows.Data;

namespace TechneauxWpfControls.CygNetFacilityFiltering.View.Sub
{
    /// <summary>
    /// Interaction logic for ChildFacilityFilter.xaml
    /// </summary>
    public partial class ChildFacilityFilter : UserControl
    {
        public ChildFacilityFilter()
        {
            InitializeComponent();
        }

        private void ChildGroupsListView_TargetUpdated(object sender, DataTransferEventArgs e)
        {

            for (var i = 0; i < ChildGroupsListView.Columns.Count; i++)
            {
                ChildGroupsListView.Columns[i].Width = 0;
            }

            ChildGroupsListView.UpdateLayout();

            for (var j = 0; j < ChildGroupsListView.Columns.Count; j++)
            {
                ChildGroupsListView.Columns[j].Width = new DataGridLength(1, DataGridLengthUnitType.Auto);
            }
        }

        private void MetroDataGrid_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            for (var i = 0; i < MetroDataGrid.Columns.Count; i++)
            {
                MetroDataGrid.Columns[i].Width = 0;
            }
            MetroDataGrid.UpdateLayout();
            for (var j = 0; j < MetroDataGrid.Columns.Count; j++)
            {
                MetroDataGrid.Columns[j].Width = new DataGridLength(1, DataGridLengthUnitType.Auto);
            }
        }

        //private void MyVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        //{
        //    if (e.PropertyName == "ChildGroups" || e.PropertyName =="ChildGroupsViewModels" || e.PropertyName == "ChildFacilityGroupViewModel")
        //    {
        //        ResizeColumns();
        //    }

        //}
        //private void ResizeColumns()
        //{
        //    for (int i = 0; i < ChildFacFilterGridView.Columns.Count; i++)
        //    {                           
        //        var thisCol = ChildFacFilterGridView.Columns[i];                
        //        thisCol.Width = thisCol.ActualWidth;
        //        thisCol.Width = double.NaN;


        //    }
        //}
        //private void ChildGroupsView_Loaded(object sender, RoutedEventArgs e)
        //{
        //    if (this.DataContext != null && this.DataContext is INotifyPropertyChanged)
        //    {
        //        var myVM = this.DataContext as INotifyPropertyChanged;

        //        myVM.PropertyChanged += MyVM_PropertyChanged;
        //    }

        //}

    }




    //    }
    //}
}

