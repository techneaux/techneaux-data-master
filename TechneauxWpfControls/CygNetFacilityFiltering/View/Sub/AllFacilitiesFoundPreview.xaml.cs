using System.Windows.Controls;

namespace TechneauxWpfControls.CygNetFacilityFiltering.View.Sub
{
    /// <summary>
    /// Interaction logic for AllFacilitiesFoundPreview.xaml
    /// </summary>
    public partial class AllFacilitiesFoundPreview : UserControl
    {
        public AllFacilitiesFoundPreview()
        {
            InitializeComponent();
        }
    }
}
