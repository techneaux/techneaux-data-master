﻿using System.Windows.Data;
using System.Windows.Controls;

namespace TechneauxWpfControls.CygNetFacilityFiltering.View.Sub
{
    /// <summary>
    /// Interaction logic for BaseFacilityFilter.xaml
    /// </summary>
    public partial class FacilityFilterGrid : UserControl
    {
        public FacilityFilterGrid()
        {

            InitializeComponent();
        }

        private void MetroDataGrid_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            MetroDataGrid.Columns[0].Width = new DataGridLength(1, DataGridLengthUnitType.Auto);
            for (var i = 0; i < MetroDataGrid.Columns.Count; i++)
            {
                MetroDataGrid.Columns[i].Width = 0;
            }

            MetroDataGrid.UpdateLayout();
            MetroDataGrid.Columns[0].Width = new DataGridLength(1, DataGridLengthUnitType.Auto);
            for (var j = 0; j < MetroDataGrid.Columns.Count; j++)
            {
                MetroDataGrid.Columns[j].Width = new DataGridLength(1, DataGridLengthUnitType.Auto);
            }
        }
    }
}

