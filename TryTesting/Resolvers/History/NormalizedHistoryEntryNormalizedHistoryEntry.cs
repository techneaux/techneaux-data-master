﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CygNetRuleModel.Resolvers.History;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using static CygNetRuleModel.Resolvers.History.HistoryNormalization;
using System.Windows.Forms;

namespace CygNetRuleModel.Resolvers.History.Tests
{
    [TestClass()]
    public class NormalizedHistoryEntryTest
    {
    //    [TestMethod()]
    //    public void NormalizedHistoryEntryConstructorTest()
    //    {
    //        //test #1 beforeafter 
    //        double windowIntervalHours = .10;
    //        DateTime target = new DateTime(2018, 1, 30, 9, 25, 00, DateTimeKind.Utc);
    //        List<CygNetHistoryEntry> srcRawEntriesInWindow = new List<CygNetHistoryEntry>();
    //        CygNetHistoryEntry temp1 = new CygNetHistoryEntry()
    //        {
    //            RawValue = 0,

    //            RawTimestamp = new DateTime(2018, 1, 30, 9, 15, 00, DateTimeKind.Utc),
    //            ServerUtcOffset = TimeSpan.FromHours(0)
    //        };
    //        CygNetHistoryEntry temp2 = new CygNetHistoryEntry()
    //        {
    //            RawValue = 0,

    //            RawTimestamp = new DateTime(2018, 1, 30, 9, 20, 00, DateTimeKind.Utc), 
    //            ServerUtcOffset = TimeSpan.FromHours(0)
    //        };
    //        CygNetHistoryEntry temp3 = new CygNetHistoryEntry()
    //        {
    //            RawValue = 0,

    //            RawTimestamp = new DateTime(2018, 1, 30, 9, 24,00, DateTimeKind.Utc),
    //            ServerUtcOffset = TimeSpan.FromHours(0)
    //        };
    //        CygNetHistoryEntry temp4 = new CygNetHistoryEntry()
    //        {
    //            RawValue = 0,
    //            RawTimestamp = new DateTime(2018, 1, 30, 9, 38, 00, DateTimeKind.Utc),
    //            ServerUtcOffset = TimeSpan.FromHours(0)
    //        };
    //        CygNetHistoryEntry temp5 = new CygNetHistoryEntry()
    //        {
    //            RawValue = 0,
    //            RawTimestamp = new DateTime(2018, 1, 30, 9, 10, 00, DateTimeKind.Utc),
    //            ServerUtcOffset = TimeSpan.FromHours(0)
    //        };
    //        srcRawEntriesInWindow.Add(temp1);
    //        srcRawEntriesInWindow.Add(temp2);
    //        srcRawEntriesInWindow.Add(temp3);
    //        List<CygNetHistoryEntry> srcRawEntriesInWindowExtended = new List<CygNetHistoryEntry>();
    //        srcRawEntriesInWindowExtended.Add(temp1);
    //        srcRawEntriesInWindowExtended.Add(temp2);
    //        srcRawEntriesInWindowExtended.Add(temp3);
    //        srcRawEntriesInWindowExtended.Add(temp4);
    //        srcRawEntriesInWindowExtended.Add(temp5);
    //        History.HistoryNormalization.NormalizedHistoryEntry testEntry = new NormalizedHistoryEntry(
    //target, srcRawEntriesInWindow, srcRawEntriesInWindowExtended,false, PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before_After, windowIntervalHours);
    //        DateTime checkCritical = target.Add(target - temp3.AdjustedTimestamp);
    //        Assert.IsTrue(testEntry.CriticalTimestamp == checkCritical);

    //        //test # 2 before
    //        HistoryNormalization.NormalizedHistoryEntry testEntry2 = new NormalizedHistoryEntry(target, srcRawEntriesInWindow, srcRawEntriesInWindowExtended, false, PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before, windowIntervalHours);
    //        Assert.IsTrue(testEntry2.CriticalTimestamp == target);

    //        ////test # 3 simple
    //        CygNetHistoryEntry temp6 = new CygNetHistoryEntry()
    //        {
    //            RawValue = 0,

    //            RawTimestamp = new DateTime(2018, 1, 30, 9, 27, 00, DateTimeKind.Utc),
    //            ServerUtcOffset = TimeSpan.FromHours(0)
    //        };
    //        srcRawEntriesInWindow.Add(temp6);
    //        srcRawEntriesInWindowExtended.Add(temp6);
    //        HistoryNormalization.NormalizedHistoryEntry testEntry3 = new NormalizedHistoryEntry(target, srcRawEntriesInWindow, srcRawEntriesInWindowExtended, false, PointHistoryNormalizationOptions.SamplingType.Simple_Average, windowIntervalHours);
         
            
    //        Assert.IsTrue(testEntry3.CriticalTimestamp == testEntry3.NormalizedTimestamp.AddHours(windowIntervalHours));

    //        ////test # 4 wa
    //        HistoryNormalization.NormalizedHistoryEntry testEntry4 = new NormalizedHistoryEntry(target, srcRawEntriesInWindow, srcRawEntriesInWindowExtended, false, PointHistoryNormalizationOptions.SamplingType.Weighted_Average, windowIntervalHours);
    //        Assert.IsTrue(testEntry4.CriticalTimestamp == testEntry4.NormalizedTimestamp.AddHours(windowIntervalHours));
    //        //test # 5 linear interp.
    //        CygNetHistoryEntry temp7 = new CygNetHistoryEntry()
    //        {
    //            RawValue = 0,
    //            RawTimestamp = new DateTime(2018, 1, 30, 9, 35, 00, DateTimeKind.Utc),
    //            ServerUtcOffset = TimeSpan.FromHours(0)
    //        };
    //        srcRawEntriesInWindow.Remove(temp6);
    //        srcRawEntriesInWindow.Add(temp7);
    //        srcRawEntriesInWindowExtended.Add(temp7);
    //        HistoryNormalization.NormalizedHistoryEntry testEntry5 = new NormalizedHistoryEntry(target, srcRawEntriesInWindow, srcRawEntriesInWindowExtended, false, PointHistoryNormalizationOptions.SamplingType.Linear_Interpolation, windowIntervalHours);
    //        Assert.IsTrue(testEntry5.CriticalTimestamp == temp7.AdjustedTimestamp);

    //        ////test # 6 take nearest before extended
    //        List<CygNetHistoryEntry> srcRawEntriesInWindowEmpty = new List<CygNetHistoryEntry>();
    //        List<CygNetHistoryEntry> srcRawEntriesInWindowExtendedOutsideRange = new List<CygNetHistoryEntry>();
    //        CygNetHistoryEntry temp8 = new CygNetHistoryEntry()
    //        {
    //            RawValue = 0,
    //            RawTimestamp = new DateTime(2018, 1, 30, 9, 13, 00, DateTimeKind.Utc),
    //            ServerUtcOffset = TimeSpan.FromHours(0)
    //        };
    //        srcRawEntriesInWindowExtendedOutsideRange.Add(temp8);
    //        HistoryNormalization.NormalizedHistoryEntry testEntry6 = new NormalizedHistoryEntry(target, srcRawEntriesInWindowEmpty, srcRawEntriesInWindowExtendedOutsideRange, false, PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before_Extended, windowIntervalHours);
    //        Assert.IsTrue(testEntry6.CriticalTimestamp == target);
    //        Assert.IsTrue(testEntry6.NormalizedHistoryValue.RawValue == temp8.RawValue);
    //        Assert.IsTrue(testEntry6.NormalizedHistoryValue.AdjustedTimestamp == target);
            
    //    }

    //    [TestMethod()]
    //    public void NormalizedHistoryEntryConstructorTestOld()
    //    {
    //        //test #1 beforeafter 
    //        double windowIntervalHours = .10;
    //        DateTime target = new DateTime(2018, 1, 30, 3, 25, 00, DateTimeKind.Utc);
    //        List<CygNetHistoryEntry> srcRawEntriesInWindow = new List<CygNetHistoryEntry>();
    //        CygNetHistoryEntry temp1 = new CygNetHistoryEntry()
    //        {
    //            RawValue = 0,

    //            RawTimestamp = new DateTime(2018, 1, 30, 9, 15, 00, DateTimeKind.Utc),
    //            ServerUtcOffset = TimeSpan.FromHours(0)
    //        };
    //        CygNetHistoryEntry temp2 = new CygNetHistoryEntry()
    //        {
    //            RawValue = 0,

    //            RawTimestamp = new DateTime(2018, 1, 30, 9, 20, 00, DateTimeKind.Utc),
    //            ServerUtcOffset = TimeSpan.FromHours(0)
    //        };
    //        CygNetHistoryEntry temp3 = new CygNetHistoryEntry()
    //        {
    //            RawValue = 0,

    //            RawTimestamp = new DateTime(2018, 1, 30, 9, 24, 00, DateTimeKind.Utc),
    //            ServerUtcOffset = TimeSpan.FromHours(0)
    //        };
    //        srcRawEntriesInWindow.Add(temp1);
    //        srcRawEntriesInWindow.Add(temp2);
    //        srcRawEntriesInWindow.Add(temp3);
    //        List<CygNetHistoryEntry> test = new List<CygNetHistoryEntry>();
    //        History.HistoryNormalization.NormalizedHistoryEntry testEntry = new NormalizedHistoryEntry(
    //target, srcRawEntriesInWindow, false, PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before_After, windowIntervalHours);
    //        DateTime checkCritical = target.Add(target - temp3.AdjustedTimestamp);
    //        Assert.IsTrue(testEntry.CriticalTimestamp == checkCritical);

    //        //test # 2 before
    //        HistoryNormalization.NormalizedHistoryEntry testEntry2 = new NormalizedHistoryEntry(target, srcRawEntriesInWindow,false, PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before, windowIntervalHours);
    //        Assert.IsTrue(testEntry2.CriticalTimestamp == target);

    //        //test # 3 simple
    //        CygNetHistoryEntry temp4 = new CygNetHistoryEntry()
    //        {
    //            RawValue = 0,

    //            RawTimestamp = new DateTime(2018, 1, 30, 9, 27, 00, DateTimeKind.Utc),
    //            ServerUtcOffset = TimeSpan.FromHours(0)
    //        };

    //        srcRawEntriesInWindow.Add(temp4);
    //        HistoryNormalization.NormalizedHistoryEntry testEntry3 = new NormalizedHistoryEntry(target, srcRawEntriesInWindow,false, PointHistoryNormalizationOptions.SamplingType.Simple_Average, windowIntervalHours);
    //        Assert.IsTrue(testEntry3.CriticalTimestamp == testEntry3.NormalizedTimestamp.AddHours(windowIntervalHours));

    //        //test # 4 wa
    //        HistoryNormalization.NormalizedHistoryEntry testEntry4 = new NormalizedHistoryEntry(target, srcRawEntriesInWindow,false, PointHistoryNormalizationOptions.SamplingType.Weighted_Average, windowIntervalHours);
    //        Assert.IsTrue(testEntry4.CriticalTimestamp == testEntry4.NormalizedTimestamp.AddHours(windowIntervalHours));
    //        //test # 5 linear interp.
    //        CygNetHistoryEntry temp5 = new CygNetHistoryEntry()
    //        {
    //            RawValue = 0,
    //            RawTimestamp = new DateTime(2018, 1, 30, 9, 35, 00, DateTimeKind.Utc),
    //            ServerUtcOffset = TimeSpan.FromHours(0)
    //        };
    //        srcRawEntriesInWindow.Remove(temp4);
    //        srcRawEntriesInWindow.Add(temp5);
    //        HistoryNormalization.NormalizedHistoryEntry testEntry5 = new NormalizedHistoryEntry(target, srcRawEntriesInWindow,false, PointHistoryNormalizationOptions.SamplingType.Linear_Interpolation, windowIntervalHours);
    //        Assert.IsTrue(testEntry5.CriticalTimestamp == temp5.AdjustedTimestamp);
    //    }

        [TestMethod()]
        public void TryGetNearestBeforeTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void TryGetNearestAfterTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void TryGetNearestBeforeOrAfterTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void TryGetSimpleAverageTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void TryGetWeightedAverageTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void TryGetInterpolatedTest()
        {
            Assert.Fail();
        }
    }
}