﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using XmlDataModelUtility;

namespace TechneauxDataSyncService.ConfigFileSyncing
{
    public interface IPointSetOperationStatus
    {
        int NumTotalPoints { get; }
        int NumPointsInAllowedStates { get; }
        int NumPointsModified { get; }

        int NumRowsInserted { get; }
        int NumRowsDeleted { get; }
        int NumProcessingPasses { get; }

        bool HasAnyPointsToProcess { get; }

        void Start(int numPointsInStates);

        Task RecordPointProcessed(string tag, int numRowsInserted, int numRowsDeleted);

        void End();

        TimeSpan OperationDuration { get; }
    }

    public class PointSetOperationStatus : NotifyModelBase, IPointSetOperationStatus
    {
        private SemaphoreSlim mySem = new SemaphoreSlim(1, 1);

        public PointSetOperationStatus(int numTotalPoints)
        {
            _NumTotalPoints = numTotalPoints;
            _HasAnyPointsToProcess = false;
        }

        private int _NumTotalPoints;
        public int NumTotalPoints
        {
            get => GetPropertyValue<int>();
            private set => SetPropertyValue(value);
        }

        private bool _HasAnyPointsToProcess;
        public bool HasAnyPointsToProcess
        {
            get => GetPropertyValue<bool>();
            private set => SetPropertyValue(value);
        }

        private int _NumPointsInAllowedStates;
        public int NumPointsInAllowedStates
        {
            get => GetPropertyValue<int>();
            private set => SetPropertyValue(value);
        }

        private int _NumPointsProcessed;
        public int NumPointsProcessed
        {
            get => GetPropertyValue<int>();
            private set => SetPropertyValue(value);
        }

        private int _NumPointsModified;
        public int NumPointsModified
        {
            get => GetPropertyValue<int>();
            private set => SetPropertyValue(value);
        }

        private int _NumRowsInserted;
        public int NumRowsInserted
        {
            get => GetPropertyValue<int>();
            private set => SetPropertyValue(value);
        }

        private int _NumRowsDeleted;
        public int NumRowsDeleted
        {
            get => GetPropertyValue<int>();
            private set => SetPropertyValue(value);
        }

        private int _NumProcessingPasses;
        public int NumProcessingPasses
        {
            get => GetPropertyValue<int>();
            private set => SetPropertyValue(value);
        }

        private double _PointsPerMinute;
        public double PointsPerMinute
        {
            get => GetPropertyValue<double>();
            private set => SetPropertyValue(value);
        }

        private TimeSpan _OperationDuration;
        public TimeSpan OperationDuration
        {
            get => GetPropertyValue<TimeSpan>();
            private set => SetPropertyValue(value);
        }

        private Stopwatch _sw = new Stopwatch();

        private Lazy<HashSet<string>> _uniquePointTagsProcessed = new Lazy<HashSet<string>>(() => new HashSet<string>());
        private Lazy<HashSet<string>> _uniquePointTagsModified = new Lazy<HashSet<string>>(() => new HashSet<string>());

        public void End()
        {
            _sw.Stop();
            OperationDuration = _sw.Elapsed;

            _uniquePointTagsModified = null;
            _uniquePointTagsProcessed = null;
            _sw = null;

            UpdateObservableProperties();
        }


        private Stopwatch UpdateTimer = new Stopwatch();

        private void UpdateObservableProperties()
        {
            try
            {
                //mySem.Wait();

                NumTotalPoints = _NumTotalPoints;
                HasAnyPointsToProcess = _HasAnyPointsToProcess;
                NumPointsInAllowedStates = _NumPointsInAllowedStates;
                NumPointsProcessed = _NumPointsProcessed;
                NumPointsModified = _NumPointsModified;
                NumRowsInserted = _NumRowsInserted;
                NumRowsDeleted = _NumRowsDeleted;
                NumProcessingPasses = _NumProcessingPasses;
                PointsPerMinute = _PointsPerMinute;
                OperationDuration = _OperationDuration;
            }
            finally
            {
                //mySem.Release();
            }
        }


        public async Task RecordPointProcessed(string tag, int numRowsInserted, int numRowsDeleted)
        {
            try
            {
                await mySem.WaitAsync();

                _OperationDuration = _sw.Elapsed;

                // Processed
                _uniquePointTagsProcessed.Value.Add(tag);
                _NumPointsProcessed = _uniquePointTagsProcessed.Value.Count;

                // Modified
                if (numRowsInserted + numRowsDeleted > 0)
                {
                    _uniquePointTagsModified.Value.Add(tag);
                    _NumPointsModified = _uniquePointTagsModified.Value.Count;
                }

                // Rows
                _NumProcessingPasses += 1;
                _NumRowsDeleted += numRowsDeleted;
                _NumRowsInserted += numRowsInserted;

                // PPM
                if (_sw.Elapsed.TotalMinutes > 0)
                {
                    _PointsPerMinute = Math.Round(_NumProcessingPasses / _sw.Elapsed.TotalMinutes * 100, 1);
                }

                if (UpdateTimer.ElapsedMilliseconds > 200)
                {
                    UpdateTimer.Restart();
                    UpdateObservableProperties();
                }
            }
            finally
            {
                mySem.Release();
            }
        }

        public void Start(int numPointsInStates)
        {
            _NumPointsInAllowedStates = numPointsInStates;

            _HasAnyPointsToProcess = numPointsInStates > 0;

            _sw.Start();
            UpdateTimer.Start();
        }
    }


    public interface IConfigSyncOperationResults
    {
        void EndOperation();
        PointSetOperationStatus MaintenanceOpStats { get; }
        PointSetOperationStatus BackfillOpStats { get; }
        PointSetOperationStatus DeleteAllOpStats { get; }
        PointSetOperationStatus DeleteOldOpStats { get; }
        DateTime StartTime { get; }
        DateTime EndTime { get; }
        int NumTotalPoints { get; }
        Guid Guid { get; }
        string LastPropertyUpdated { get; }
        event PropertyChangedEventHandler PropertyChanged;
    }

    public class ConfigSyncOperationResults : NotifyModelBase, IConfigSyncOperationResults
    {
        public ConfigSyncOperationResults(
            int totalPointCount)
        {
            OperationStopwatch.Start();

            NumTotalPoints = totalPointCount;

            StartTime = DateTime.Now;

            MaintenanceOpStats = new PointSetOperationStatus(NumTotalPoints);
            BackfillOpStats = new PointSetOperationStatus(NumTotalPoints);
            DeleteAllOpStats = new PointSetOperationStatus(NumTotalPoints);
            DeleteOldOpStats = new PointSetOperationStatus(NumTotalPoints);

            IsRunning = true;
        }

        public void EndOperation()
        {
            OperationStopwatch.Stop();
            EndTime = DateTime.Now;

            ElapsedMinutes = ElapsedTime.TotalMinutes;

            IsRunning = false;
        }

        public bool IsRunning
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        public PointSetOperationStatus MaintenanceOpStats { get; }
        public PointSetOperationStatus BackfillOpStats { get; }
        public PointSetOperationStatus DeleteAllOpStats { get; }
        public PointSetOperationStatus DeleteOldOpStats { get; }


        public Stopwatch OperationStopwatch { get; } = new Stopwatch();

        public TimeSpan ElapsedTime => OperationStopwatch.Elapsed;

        public double MaxAllowedMinutes { get; } = 5;

        public double ElapsedMinutes
        {
            get => GetPropertyValue<double>();
            private set => SetPropertyValue(value);
        }

        public DateTime StartTime
        {
            get => GetPropertyValue<DateTime>();
            private set => SetPropertyValue(value);
        }

        public DateTime EndTime
        {
            get => GetPropertyValue<DateTime>();
            private set => SetPropertyValue(value);
        }

        public int NumTotalPoints
        {
            get => GetPropertyValue<int>();
            private set => SetPropertyValue(value);
        }
    }
}

