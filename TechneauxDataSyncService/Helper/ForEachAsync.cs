﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace TechneauxDataSyncService.Helper
{
    static class MultiTaskAwaitHelper
    {
        public static Task ForEachAsync<T>(
            this IEnumerable<T> source,
            int dop,
            Func<T, Task> body)
        {
            return Task.WhenAll(
                from partition in Partitioner.Create(source).GetPartitions(dop)
                select Task.Run(async delegate
                {
                    using (partition)
                        while (partition.MoveNext())
                            await body(partition.Current).ContinueWith(t =>
                            {
                                //observe exceptions
                            });

                }));
        }

        public static Task ForEachAsync<T>(
            this IEnumerable<T> source,
            int dop,
            TimeSpan timeout,
            Func<T, Task> body)
        {
            var sw = new Stopwatch();
            sw.Start();
               
            return Task.WhenAll(
                from partition in Partitioner.Create(source).GetPartitions(dop)
                select Task.Run(async delegate
                {
                    using (partition)
                        while (partition.MoveNext() && sw.Elapsed < timeout)
                            await body(partition.Current).ContinueWith(t =>
                            {

                                //observe exceptions
                            });

                }));
        }

        public static Task ForEachAsync<T>(
            this IEnumerable<T> source,
            int dop,
            Stopwatch sw,
            TimeSpan timeout,
            Func<T, Task> body)
        {
            if (!sw.IsRunning)
                sw.Start();

            return Task.WhenAll(
                from partition in Partitioner.Create(source).GetPartitions(dop)
                select Task.Run(async delegate
                {
                    using (partition)
                        while (partition.MoveNext() && sw.Elapsed < timeout)
                            await body(partition.Current).ContinueWith(t =>
                            {

                                //observe exceptions
                            });

                }));
        }
    }
}
