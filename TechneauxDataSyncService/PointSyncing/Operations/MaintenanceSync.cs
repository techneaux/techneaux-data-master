﻿using CygNetRuleModel.Resolvers;
using CygNetRuleModel.Resolvers.History;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog.Context;
using TechneauxDataSyncService.PointSyncing.Operations.Base;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.Helper;
using TechneauxUtility;
using XmlDataModelUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    public class MaintenanceSync : ISqlSyncOperation
    {
        private CachedCygNetPointWithRule SrcPoint { get; }
        private DateTime LastCygNetTimestamp { get; }
        private DateTime LastNormTimestamp { get; }
        private SimpleCombinedTableSchema TableSchema { get; }
        private IDictionary<string, IConvertibleValue> OldCachedFixedValues { get; }
        private DateTime NewNormTimestamp { get; set; }
        private DateTime EarliestDate { get; set; }
        private DateTime LatestDate { get; }

        public SimpleSqlSyncResults Results { get; private set; } = new SimpleSqlSyncResults(nameof(MaintenanceSync));
        public ISqlSyncDetailedResults DetailedHistoryResults { get; } = new SqlSyncDetailedResults();

        public MaintenanceSync(
            SimpleCombinedTableSchema newSchema,
            CachedCygNetPointWithRule newSrcPoint,
            IDictionary<string, IConvertibleValue> oldCachedFixedValues,
            DateTime lastCygNetRawHist,
            DateTime lastValidNormTimestamp,
            DateTime? lastCriticalTimestamp)
        {
            TableSchema = newSchema;
            SrcPoint = newSrcPoint;
            OldCachedFixedValues = oldCachedFixedValues;
            LastCygNetTimestamp = lastCygNetRawHist;
            LastNormTimestamp = lastValidNormTimestamp;

            if (IsHistoryNormalized)
            {
                //rollup types use a different window of one single window before
                //if lastnormtimestamp = lastcygnet timestamp and history is norm then there was either no sql or no norm history
                if (SrcPoint.SrcRule.HistoryNormalizationOptions.SelectedSampleType == TechneauxReportingDataModel.CygNet.FacilityPointOptions.PointHistoryNormalizationOptions.SamplingType.Rollup_Simple_Average_Extended
                    || LastNormTimestamp == LastCygNetTimestamp)
                {
                    EarliestDate = LastNormTimestamp.AddMilliseconds(1);
                }
                else
                {
                    EarliestDate = LastNormTimestamp.Add(
                        SrcPoint.SrcRule.HistoryNormalizationOptions.TimespanBetweenNormEntries)
                    .AddSeconds(-5);
                }

            }
            else
            {
                EarliestDate = LastCygNetTimestamp.AddSeconds(1);
            }
            LatestDate = DateTime.Now;
            Results.OperationInterval = (EarliestDate, LatestDate);
            Results.LastCriticalTimestamp = lastCriticalTimestamp;
        }

        private bool IsHistoryNormalized => SrcPoint.SrcRule.HistoryNormalizationOptions.EnableNormalization;
        private bool IsExtendedNorm => SrcPoint.SrcRule.HistoryNormalizationOptions.IsExtendedNormType;

        private bool IsRollupNorm => SrcPoint.SrcRule.HistoryNormalizationOptions.IsRollupNormType;

        public bool NewHistFound { get; private set; }

        private SqlOperation _ThisSqlOp;

        public async Task DoOperationAsync(
            SqlOperation sqlOp,
            IProgress<(int, string)> progress,
            CancellationToken ct)
        {
            _ThisSqlOp = sqlOp;

            if (HarnessForcedTestingOptions.IsDeleteAllTestForced)
            {
                Results.OpResult = OpStatuses.Fail;
                //Results.LogAuditMessage($"New fixed values are : {string.Join(",", newCachedValues.Values.Select(x => x.StringValue))}");
                //Results.LogAuditMessage($"Old fixed values are: {string.Join(",", newCachedValues.Values.Select(x => x.StringValue))}");

                Results.StatusMessage = "Delete All Testing forced fail on this point";

                Results.OpResult = OpStatuses.Fail;

                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: Sql timeout. MaintSync: {this.ToString()}");
                return;
            }
            bool partialResyncNeeded = false;
            NewHistFound = false;
            var sw = new Stopwatch();
            sw.Start();
            if (IsHistoryNormalized)
            {
                //partialResyncNeeded = await CheckForNewTrendHistory(ct);
                NewHistFound = await CheckNewNormalizedHistory();
            }
            else
            {
                NewHistFound = await CheckNewRawHistory();
            }
            if (NewHistFound == false)
            {
                Console.WriteLine("xx");
            }
            sw.Stop();
            Log.Debug("Check before maintenance sync took " + sw.Elapsed + " for pnt :" + SrcPoint.LongId);
            sw.Reset();
            sw.Start();
            if (partialResyncNeeded)
            {
                
                partialResyncNeeded = await UpdateRecentHistoryDataAsync(ct);
            }
            sw.Stop();
            sw.Reset();
            Log.Debug("UpdateRecentHistoryDataAsync took " + sw.Elapsed + " for pnt :" + SrcPoint.LongId);
            if (NewHistFound)
            {
                NewHistFound = await AddNewHistoryToSqlAsync(ct);

            }
            sw.Stop();
            Log.Debug("AddNewHistoryToSqlAsync took " + sw.Elapsed + " for pnt :" + SrcPoint.LongId);
            sw.Reset();
        }
        public override string ToString()
        {
            return $"{nameof(IsRollupNorm)}=[{IsRollupNorm}], " +
                   $"{nameof(LastCygNetTimestamp)}=[{LastCygNetTimestamp}], " +
                   $"{nameof(LastNormTimestamp)}=[{LastNormTimestamp}], " +
                   $"{nameof(LastCygNetTimestamp)}=[{LastCygNetTimestamp}], " +
                   $"{nameof(EarliestDate)}=[{EarliestDate}], " +
                   $"{nameof(LatestDate)}=[{LatestDate}], " +
                   $"{nameof(IsHistoryNormalized)}=[{IsHistoryNormalized}], " +
                   $"{nameof(LastNormTimestamp)}=[{LastNormTimestamp}], " +
                   $"{nameof(NewNormTimestamp)}=[{NewNormTimestamp}], " +
                   $"{nameof(IsHistoryNormalized)}=[{IsHistoryNormalized}], " +
                   $"{nameof(IsExtendedNorm)}=[{IsExtendedNorm}], " +
                   $"{nameof(NewHistFound)}=[{NewHistFound}], " +
                   $"{nameof(Results)}=[{Results}]";
        }
        private async Task<bool> CheckNewRawHistory()
        {
            if (!await SrcPoint.HasNewValueAfter(LastCygNetTimestamp))
            {
                Results.MostRecentNormTimestamp = LastNormTimestamp;
                Results.MostRecentRawHistTimestamp = LastCygNetTimestamp;
                Results.OpResult = OpStatuses.Pass;
                Results.StatusMessage = "No new history.";
                return false;
            }
            return true;
        }

        private async Task<bool> CheckForNewTrendHistory(CancellationToken ct)
        {
            try
            {
                if (Results.LastCriticalTimestamp == null)
                {
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: No new trend history due to no last critical timestamp. MaintSync: {this.ToString()}");
                    return false;
                }
                var includeNumericOnly = PointSync.CheckForNumeric(SrcPoint.SrcRule.HistoryNormalizationOptions);
                var compareModel = new CygNetSqlDataCompareModel(SrcPoint, ServiceOps.CriticalTimestampTimeoutMins);
                var cygHist = await NormalizedPointHistory.GetPointHistory(
                    SrcPoint,
                    LastCygNetTimestamp,
                    Results.LastCriticalTimestamp.Value,
                    null,
                    SrcPoint.SrcRule.GeneralHistoryOptions.IncludeUnreliable,
                    includeNumericOnly,
                    null,
                    ct);

                if (cygHist.Where(x => x.AdjustedTimestamp > LastCygNetTimestamp && x.AdjustedTimestamp <= Results.LastCriticalTimestamp).Any())
                {
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: Trend history appeared between last Cygnet timestamp and last crtitical timestamp. MaintSync: {this.ToString()}");
                    return true;
                }
                else
                {
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: No new trend history due to no new timestamps between last cygnettimestamp and lastcriticaltimestamp. MaintSync: {this.ToString()}");
                    return false;
                }

            }
            catch (Exception ex)
            {
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: Trend history check unhandled exception. MaintSync: {this.ToString()}");

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Failure while trying to retrieve new cygnet history for point {pnt}", SrcPoint.Tag);

                return false;
            }
        }


        private async Task<bool> CheckNewNormalizedHistory()
        {
            // Is there a new norm timestamp possible between last written NORM TIMESTAMP (that was inserted into SQL) and NOW
            // -- i.e. Has the span of a single norm interval passed since the list NORM TIMESTAMP which was inserted into SQL
            if (IsRollupNorm)
            {
                if (!SrcPoint.SrcRule.HistoryNormalizationOptions
                .GetNormalizedRollupTimestampsInWindow(LastNormTimestamp.AddMilliseconds(1), DateTime.Now).Any())
                {
                    Results.MostRecentNormTimestamp = LastNormTimestamp;
                    Results.MostRecentRawHistTimestamp = LastCygNetTimestamp;
                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage = "Can't fit in new norm timestamp.";
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: No new norm hist found for rollup between last norm timestamp and now. MaintSync: {this.ToString()}");
                    return false;
                }
            }
            else
            {
                if (!SrcPoint.SrcRule.HistoryNormalizationOptions
                .GetNormalizedTimestampsInWindow(LastNormTimestamp, DateTime.Now).Any())
                {
                    Results.MostRecentNormTimestamp = LastNormTimestamp;
                    Results.MostRecentRawHistTimestamp = LastCygNetTimestamp;
                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage = "Can't fit in new norm timestamp.";
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: No new norm hist found between last norm timestamp and now. MaintSync: {this.ToString()}");
                    return false;
                }
            }


            // EXTENDED ASSUMES: No new history will ever be found in the VHS that is dated earlier than the last time we _LOOKED_
            if (!IsExtendedNorm)
            {
                // REGULAR NORM ASSUMES: No new history will be found in VHS that is dated earlier than the last history timestamp we read
                if (!await SrcPoint.HasNewValueAfter(LastCygNetTimestamp)) //are there any new raw values after last cygnet value retrieved.
                {
                    Results.MostRecentNormTimestamp = LastNormTimestamp;
                    Results.MostRecentRawHistTimestamp = LastCygNetTimestamp;
                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage = "No new history.";
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: No new norm hist found for non extended after last cygnet timestamp. MaintSync: {this.ToString()}");
                    return false;
                }
            }
            return true;
            //do maintenance sync
        }

        private async Task<bool> AddNewHistoryToSqlAsync(CancellationToken ct)
        {
            try
            {
                var swAddNew = new Stopwatch();

                var newCachedValues = await TableSchema.GetFixedColumnsAndValues(SrcPoint, ct);
                var (isEqual, keyNames, failureReason) = newCachedValues.IsEqualToCommonKeysVerbose(OldCachedFixedValues);
                if (isEqual == false)
                {
                    Results.OpResult = OpStatuses.Fail;
                    Results.LogAuditMessage($"New fixed values are : {string.Join(",", newCachedValues.Values.Select(x => x.StringValue))}");
                    Results.LogAuditMessage($"Old fixed values are: {string.Join(",", newCachedValues.Values.Select(x => x.StringValue))}");

                    Results.StatusMessage = "Fixed values do not match";
                    using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                        Log.Error($"Delete all triggered due to maintenance sync failure for fixed values comparison failure.[{SrcPoint.Tag.GetTagPointIdFull()}] ");
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: Add sql hist failed due to fixed values not matching. MaintSync: {this.ToString()}");
                    return false;
                }

                swAddNew.Stop();

                Log.Debug("Maint sync fixed vals check took " + swAddNew.Elapsed + " for pnt :" + SrcPoint.LongId);
                swAddNew.Reset();
                ct.ThrowIfCancellationRequested();
                //checks for new hist first before sync
                //if (!await SrcPoint.HistoryAvailableInInterval(EarliestDate, LatestDate))
                //{
                //    Results.OpResult = OpStatuses.Pass;
                //    Results.MostRecentNormTimestamp = LastNormTimestamp;
                //    Results.MostRecentRawHistTimestamp = LastCygNetTimestamp;
                //    Results.StatusMessage = "No new history found";
                //    return;
                //}
                swAddNew.Start();
                var compareModel = new CygNetSqlDataCompareModel(SrcPoint, ServiceOps.CriticalTimestampTimeoutMins);
                ct.ThrowIfCancellationRequested();
                //if(EarliestDate == DateTime.MinValue)
                //{
                //    Console.WriteLine("minvalue maintenance");
                //}
                //if(EarliestDate < LatestDate.AddYears(-10))
                //{
                //    Console.WriteLine("toosmall earliest");
                //}
                var includeNumericOnly = PointSync.CheckForNumeric(SrcPoint.SrcRule.HistoryNormalizationOptions);
           
                var cygHist = await Task.Run(() => compareModel.GetCygNetHistory(EarliestDate, LatestDate, SrcPoint.SrcRule, null, includeNumericOnly, ct), ct);
               
  
                if (!cygHist.IsAnyRawHistory)
                {
                    Results.MostRecentNormTimestamp = LastNormTimestamp;
                    Results.MostRecentRawHistTimestamp = LastCygNetTimestamp;
                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage = "Didn't find any history on second attempt";
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: Add sql hist passed but no new rows added due to no raw history. MaintSync: {this.ToString()}");
                    return false;
                }
                swAddNew.Stop();
                Log.Debug("Maint sync get history and history check took " + swAddNew.Elapsed + " for pnt :" + SrcPoint.LongId);
                swAddNew.Reset();
                swAddNew.Start();
                Results.NumRawCygNetEntriesFound = cygHist.RawHistoryEntries.Count;
                Results.MostRecentRawHistTimestamp = cygHist.RawHistoryEntries.Max(entry => entry.AdjustedTimestamp);
                DetailedHistoryResults.CygNetSrcRawHistory = cygHist.RawHistoryEntries;

                if (cygHist.NormalizedHistoryEntries.IsAny())
                {
                    var validNormEntries = cygHist
                        .NormalizedHistoryEntries
                        .Where(entry => entry.IsValid && !entry.IsValueUncertain)
                        .ToList();

                    if (validNormEntries.Any())
                    {
                        Results.MostRecentNormTimestamp =
                                            validNormEntries.Max(entry => entry.NormalizedHistoryValue.AdjustedTimestamp);
                    }
                    else
                    {
                        Results.MostRecentNormTimestamp = LastNormTimestamp;
                    }

                    Results.NumNormEntriesFound = cygHist.NormalizedHistoryEntries.Count;
                    Results.NumValidNormEntriesFound = cygHist.NormalizedHistoryEntries.Count(entry => entry.IsValid & !entry.IsValueUncertain);
                    Results.NumCertainNormEntriesFound =
                        cygHist.NormalizedHistoryEntries.Count(entry => entry.IsValid && !entry.IsValueUncertain);

                    DetailedHistoryResults.CygNetSrcNormalizedHistory = cygHist.NormalizedHistoryEntries;
                }
                else
                {
                    Results.MostRecentNormTimestamp = Results.MostRecentRawHistTimestamp;
                }

                ct.ThrowIfCancellationRequested();
                List<CygNetHistoryRow> cygRows = await compareModel.GetCygNetRows(cygHist, TableSchema.CygNetMappingRules, ct);
                swAddNew.Stop();
                Log.Debug("Maint sync normalized entry check and resolving took " + swAddNew.Elapsed + " for pnt :" + SrcPoint.LongId);
                swAddNew.Reset();
                swAddNew.Start();
                //if (!cygRows.IsAny() && cygHist.NormalizedHistoryEntries.IsAny())
                //    Console.WriteLine("dd");
                if (cygRows.IsAny())
                {
                    Results.NumCygNetRows = cygRows.Count;

                    //await Task.Run(() => compareModel.UpdateSqlTableFast(TableSchema, cygRows.Cast<ValueSet>().ToList(), ct), ct);
                    var insertCount = await Task.Run(() => _ThisSqlOp.AddNewRows(cygRows.Cast<ValueSet>().ToList(), ct), ct);

                    Results.NumSqlRowsInserted = insertCount;

                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage = "New rows inserted";
                    if(IsHistoryNormalized && cygHist.NormalizedHistoryEntries.IsAny())
                    {
                        Results.LastCriticalTimestamp = cygHist.NormalizedHistoryEntries.OrderByDescending(x => x.NormalizedTimestamp).First().CriticalTimestamp;
                    }
                    else
                    {
                        Results.LastCriticalTimestamp = null;
                    }                    
                    
                    swAddNew.Stop();
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: Add sql hist passed with new rows added. MaintSync: {this.ToString()}");
                    Log.Debug("Maint sync update sql took " + swAddNew.Elapsed + " for pnt :" + SrcPoint.LongId);
                    return true;
                }
                else
                {
                    Results.MostRecentNormTimestamp = LastNormTimestamp;
                    Results.MostRecentRawHistTimestamp = LastCygNetTimestamp;
                    Results.OpResult = OpStatuses.Pass;

                    Results.StatusMessage = "No new rows inserted.";
                    swAddNew.Stop();
                    Log.Debug("Main sync no new rows inserted took " + swAddNew.Elapsed + " for pnt :" + SrcPoint.LongId);
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: Add sql hist passed with no rows added. MaintSync: {this.ToString()}");
                    return false;
                }
            }
            catch (SqlException exS)
            {
                Results.FailedException = exS;
                Results.OpResult = OpStatuses.Exception;
                if (exS.Number == -2)
                {
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: Sql timeout. MaintSync: {this.ToString()}");
                    Results.StatusMessage = $"Sql timeout while running insert query for MaintenanceSync. Insert query timeout was set to {ServiceOps.InsertTimeout} seconds.";
                }
                else
                {
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: Sql query failed. MaintSync: {this.ToString()}");
                    Results.StatusMessage = $"Sql Error - Insert query failed for MaintenanceSync.";

                }
                return false;
            }
            catch (Exception ex)
            {
                Results.FailedException = ex;
                Results.MostRecentNormTimestamp = LastNormTimestamp;
                Results.MostRecentRawHistTimestamp = LastCygNetTimestamp;
                Results.OpResult = OpStatuses.Exception;
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: Unhandled Exception. MaintSync: {this.ToString()}");
                Results.StatusMessage = "General operation failure - MaintenanceSync";

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Failure adding new sql data during maintenance operations.");

                return false;
            }
            finally
            {
                //Results.LoggingMessages = _pointSyncAuditTrailBuilder.ToString();
                //Results.TimeTakenPerformingOperation = GetTotalTimeTaken();
            }
        }

        private async Task<bool> UpdateRecentHistoryDataAsync(CancellationToken ct)
        {
            try
            {
                var swAddNew = new Stopwatch();

                swAddNew.Start();
                var compareModel = new CygNetSqlDataCompareModel(SrcPoint, ServiceOps.CriticalTimestampTimeoutMins);
                ct.ThrowIfCancellationRequested();

                var includeNumericOnly = PointSync.CheckForNumeric(SrcPoint.SrcRule.HistoryNormalizationOptions);
                var cygHist = await Task.Run(() => compareModel.GetCygNetHistory(LastCygNetTimestamp, Results.LastCriticalTimestamp.Value, SrcPoint.SrcRule, null, includeNumericOnly, ct), ct);

                if (!cygHist.IsAnyRawHistory)
                {
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: Partial resync did not add new rows due to no raw history. MaintSync: {this.ToString()}");
                    return false;
                }
                swAddNew.Stop();
                Log.Debug("Update recent history get history and history check took " + swAddNew.Elapsed + " for pnt :" + SrcPoint.LongId);
                swAddNew.Reset();
                swAddNew.Start();

                if (cygHist.NormalizedHistoryEntries.IsAny())
                {
                    var validNormEntries = cygHist
                        .NormalizedHistoryEntries
                        .Where(entry => entry.IsValid && !entry.IsValueUncertain)
                        .ToList();
                }

                ct.ThrowIfCancellationRequested();
                List<CygNetHistoryRow> cygRows = await compareModel.GetCygNetRows(cygHist, TableSchema.CygNetMappingRules, ct);
                swAddNew.Stop();
                Log.Debug("Update recent history resolving took " + swAddNew.Elapsed + " for pnt :" + SrcPoint.LongId);
                swAddNew.Reset();
                swAddNew.Start();
                if (cygRows.IsAny())
                {
                    Results.NumCygNetRows = cygRows.Count;

                    //await Task.Run(() => compareModel.UpdateSqlTableFast(TableSchema, cygRows.Cast<ValueSet>().ToList(), ct), ct);
                    var updateCount = await Task.Run(() => _ThisSqlOp.UpdateRecentRows(cygRows.Cast<ValueSet>().ToList(), ct), ct);

                    Results.NumSqlRowsUpdated = updateCount;

                    swAddNew.Stop();

                    Log.Debug("Update recent history sql update took " + swAddNew.Elapsed + " for pnt :" + SrcPoint.LongId);
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync:Partial resync added new rows. MaintSync: {this.ToString()}");
                    return true;
                }
                else
                {

                    Results.StatusMessage = "No new rows inserted.";
                    swAddNew.Stop();
                    Log.Debug("Update recent history no new rows inserted took " + swAddNew.Elapsed + " for pnt :" + SrcPoint.LongId);
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: Partial resync did not add any new rows due to no cygrows being resolved. MaintSync: {this.ToString()}");
                    return false;
                }
            }
            catch (SqlException exS)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error("New data sync failed during maintenance operations for point {pnt}. New data may not be syncing for this point.", SrcPoint.Tag);

                Results.FailedException = exS;
                Results.OpResult = OpStatuses.Exception;
                if (exS.Number == -2)
                {
                    Results.StatusMessage = $"Sql timeout while running update query for MaintenanceSync. Update query timeout uses the insert query timeout option and was set to {ServiceOps.InsertTimeout} seconds.";
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: Sql timeout. MaintSync: {this.ToString()}");
                }
                else
                {
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: Sql query failed. MaintSync: {this.ToString()}");
                    Results.StatusMessage = $"Sql Error - Update query failed for MaintenanceSync.";

                }
                return false;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error("Unhandled exception, new data sync failed during maintenance operations for point {pnt}. New data may not be syncing for this point.", SrcPoint.Tag);

                Results.FailedException = ex;
                Results.OpResult = OpStatuses.Exception;
                Results.StatusMessage = "General operation failure - MaintenanceSync";

                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: MaintSync: Sql query failed. MaintSync: {this.ToString()}");
                return false;
            }
            finally
            {
                //Results.LoggingMessages = _pointSyncAuditTrailBuilder.ToString();
                //Results.TimeTakenPerformingOperation = GetTotalTimeTaken();
            }
        }
        // Possible results
        // - Not normalized, and no new raw history
        // - Isn't possible to fit new norms timestamp between last one and current datetime (don't care new history exists or not)
        // - New norm timestamp can fit, but regular normalization enabled, but no new raw history
        // - New norm timestamp can fit, but regular normalization enabled, and new history doesn't lead to new norm value
        // - New norm timestamp can fit, but regular normalization enabled, and new history does lead to new norm value 
        // - Got through all the above, found history, but failed to make cygnet rows because of a runtime data collection or sql table issue

    }
}
