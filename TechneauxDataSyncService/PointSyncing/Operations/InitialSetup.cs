﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Serilog.Context;
using Techneaux.CygNetWrapper;
using TechneauxDataSyncService.Helper;
using TechneauxDataSyncService.PointSyncing.Operations.Base;
using TechneauxHistorySynchronization;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.Helper;
using TechneauxUtility;
using XmlDataModelUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    public class InitialSetup : TimedOperation, ISqlSyncOperation
    {
        public InitialSetup(
            SimpleCombinedTableSchema simpleSchema,
            CachedCygNetPointWithRule srcPnt,
            int numberRows,
            bool verifiedNoHistoryInSql)
        {
            _verifiedNoHistoryInSql = verifiedNoHistoryInSql;
            if (simpleSchema == null || simpleSchema.IsEmptySqlSchema)
            {

                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: Schema is null or empty. InitialSetup: {this.ToString()}");
                throw new ArgumentNullException(nameof(simpleSchema), "Schema must not be null or empty");
            }
                
            if (srcPnt == null)
            {
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: Source point is null. InitialSetup: {this.ToString()}");
                throw new ArgumentNullException(nameof(srcPnt), "Src Point must not be empty");
            }               

            if (srcPnt.SrcRule == null || !srcPnt.SrcRule.IsRuleValid)
            {
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: Source rule is null or invalid. InitialSetup: {this.ToString()}");
                throw new ArgumentException("Src point rule must be non-null and valid");
            }
                
            Schema = simpleSchema;

            SrcPoint = srcPnt;
            NumRows = numberRows;
        }

        #region LocalVars

        private SimpleCombinedTableSchema Schema { get; }
        private CachedCygNetPointWithRule SrcPoint { get; }

        public DateTime EarliestCommonBackfillTime
        {
            get;
            private set;
        }

        private DateTime EarliestDate { get; set; }
        private DateTime LatestDate { get; set; }

        private string TimeColId => Schema.TimeColumn.SqlTableFieldName;
        private int NumRows { get; }

        private double RetentionDays => SrcPoint.SrcRule.PollingOptions.RetentionDays;
        private DateTime RetentionPeriodEarliestDate => DateTime.Now.AddDays(-RetentionDays);
        private bool IsHistoryNormalized => SrcPoint.SrcRule.HistoryNormalizationOptions.EnableNormalization;
        private bool IsHistoryNormalizedExtended => SrcPoint.SrcRule.HistoryNormalizationOptions.IsExtendedNormType;
        private TimeSpan NormInterval => SrcPoint.SrcRule.HistoryNormalizationOptions.TimespanBetweenNormEntries;

        #endregion

        private enum InitialSetupStatus
        {
            [Description("Tried to get recent rows, but no matching rows found in SQL at all")]
            NoHistoryFoundAtAllWhenGettingRecentRows,

            [Description("The most recent rows have invalid timestamp column values")]
            RecentRowsBadTimestampValues,

            [Description("There was no CygNet history in the same range as most recent SQL rows")]
            NoCygNetHistInRangeOfRecentSqlRows
        }

        

        public SimpleSqlSyncResults Results { get; private set; } = new SimpleSqlSyncResults(nameof(InitialSetup));
        public ISqlSyncDetailedResults DetailedHistoryResults { get; } = new SqlSyncDetailedResults();
        public bool IsDeleteAllNeeded { get; private set; }
        public bool IsBackfillNeeded { get; private set; }
        public bool IsDeleteOldNeeded { get; private set; }

        private bool _verifiedNoHistoryInSql { get; set; }
        
        public bool IsAnySqlRows { get; private set; }
   
        public override string ToString()
        {
            return $"{nameof(IsBackfillNeeded)}=[{IsBackfillNeeded}], " +
                   $"{nameof(IsDeleteAllNeeded)}=[{IsDeleteAllNeeded}], " +
                   $"{nameof(IsDeleteOldNeeded)}=[{IsDeleteOldNeeded}], " +
                   $"{nameof(EarliestCommonBackfillTime)}=[{EarliestCommonBackfillTime}], " +
                   $"{nameof(EarliestDate)}=[{EarliestDate}], " +
                   $"{nameof(LatestDate)}=[{LatestDate}], " +
                   $"{nameof(IsHistoryNormalized)}=[{IsHistoryNormalized}], " +
                   $"{nameof(IsHistoryNormalizedExtended)}=[{IsHistoryNormalizedExtended}], " +
                   $"{nameof(NormInterval)}=[{NormInterval}], " +
                   $"{nameof(NumRows)}=[{NumRows}], " +
                   $"{nameof(RetentionPeriodEarliestDate)}=[{RetentionPeriodEarliestDate}], " +
                   $"{nameof(TimeColId)}=[{TimeColId}], " +
                   $"{nameof(RetentionDays)}=[{RetentionDays}], " +
                   $"{nameof(Results)}=[{Results}]";
        }


        public async Task DoOperationAsync(
            SqlOperation sqlOp,
            IProgress<(int, string)> progress,
            CancellationToken ct)
        {
            try
            {
                

                // Start timing operation
                StartTiming();

                DetailedHistoryResults.FixedCygNetValues = await Schema.GetFixedColumnsAndValues(SrcPoint, ct);

                if (!await IsRecentSqlRowsValidAndMatchingCygNet(sqlOp, ct))
                {
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: Recent rows are not valid and matching. InitialSetup: {this.ToString()}");
                    return;
                }
                    

                IsBackfillNeeded = await IsBackfillRequired(sqlOp, ct);
            }
            catch (SqlException exS)
            {
                Results.FailedException = exS;
                Results.OpResult = OpStatuses.Exception;

                if (exS.Number == -2)
                {
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: Sql timeout. InitialSetup: {this.ToString()}");
                    Results.StatusMessage = $"Sql timeout while running select query for InitialSetup. Select query timeout was set to {ServiceOps.SelectTimeout} seconds.";
                }
                else
                {
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: Sql query failed. InitialSetup: {this.ToString()}");
                    Results.StatusMessage = $"Sql Error - Select query failed for InitialSetup.";

                }
            }
            catch (Exception ex)
            {
                Results.FailedException = ex;
                Results.OpResult = OpStatuses.Exception;
                Results.StatusMessage = "General operation failure - First Check";
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: Unhandled exception. InitialSetup: {this.ToString()}");
                
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception while attempting to initialize point ({tag}) for service sync", SrcPoint.Tag.ToString());

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error(ex, "Failed to initialize point ({tag}) for service sync. This point won't be synced unless future init retries succeed.", SrcPoint.Tag.ToString());
            }
            finally
            {
                Results.TimeTakenPerformingOperation = GetTotalTimeTaken();
            }
        }

        private async Task<bool> IsRecentSqlRowsValidAndMatchingCygNet(SqlOperation sqlOp, CancellationToken ct)
        {
            var compareModel = new CygNetSqlDataCompareModel(SrcPoint, ServiceOps.CriticalTimestampTimeoutMins);

            // -- Get sql rows
            var latestSqlRows = _verifiedNoHistoryInSql ? new List<SqlHistoryRow>() : await GetRecentSqlHistoryRows(sqlOp, ct);
            if(!latestSqlRows.IsAny())
            {
                IsAnySqlRows = false;
            }

            Results.NumTotalSqlRows = latestSqlRows?.Count;
            
            // Is there any sql data at all?
            if (!latestSqlRows.IsAny())
            {
                // No history is a Pass condition
                Results.OpResult = OpStatuses.Pass;
                Results.MostRecentRawHistTimestamp = DateTime.Now.AddHours(-1);
                Results.MostRecentNormTimestamp = Results.MostRecentRawHistTimestamp;
                Results.StatusMessage = InitialSetupStatus.NoHistoryFoundAtAllWhenGettingRecentRows.GetErrorDescription();
                EarliestCommonBackfillTime = Results.MostRecentRawHistTimestamp;
                IsBackfillNeeded = true;
                IsDeleteAllNeeded = false;
                IsDeleteOldNeeded = false;
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: No recent sql rows at all. InitialSetup: {this.ToString()}");
                return false;
            }

            // Do the rows have valid timestamps?
            var rowsWithInvalidTimestamps = latestSqlRows.Where(row =>
                !row[TimeColId].IsDateTime).ToList();

            if (rowsWithInvalidTimestamps.Any())
            {
                IsDeleteAllNeeded = true;
                IsDeleteOldNeeded = false;
                Results.OpResult = OpStatuses.Fail;
                Results.StatusMessage = InitialSetupStatus.RecentRowsBadTimestampValues.GetErrorDescription();
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error($"Delete all triggered due to initialsetup failure with invalid timestamps.[{SrcPoint.Tag.GetTagPointIdFull()}] ");
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: Sql rows had invalid timestamps. InitialSetup: {this.ToString()}");
                return false;
            }

            var sqlTs = latestSqlRows.Select(row => row[TimeColId]).ToList();

            // Check for cancellation
            ct.ThrowIfCancellationRequested();

            // Get earliest and latest dates
            EarliestDate = latestSqlRows.Min(row => row[TimeColId].ToDateTime()).AddSeconds(-1);
            LatestDate = latestSqlRows.Max(row => row[TimeColId].ToDateTime()).AddSeconds(1);

            Results.LogAuditMessage($"Latest: {LatestDate}");
            Results.LogAuditMessage($"Earliest: {EarliestDate}");

            // Get dates in local time (what is this for Sean?)
            var early = new DateTime(EarliestDate.Ticks, DateTimeKind.Local);
            var late = new DateTime(LatestDate.Ticks, DateTimeKind.Local);

            Results.LogAuditMessage($"newEarliest: {early}");
            Results.LogAuditMessage($"newLatest: {late}");

            // Collect CygNet history in the same timeframe as the SQL rows
            var includeNumericOnly = PointSync.CheckForNumeric(SrcPoint.SrcRule.HistoryNormalizationOptions);
            var cygHist = await compareModel.GetCygNetHistory(
                early,
                late,
                SrcPoint.SrcRule.GeneralHistoryOptions,
                SrcPoint.SrcRule.HistoryNormalizationOptions,
                null,
                null,
                SrcPoint.SrcRule.PollingOptions.RetentionDays,
                includeNumericOnly,
                ct);

            // If no CygNet history, there can't be any SQL rows (watch out, if we try to fill in gaps with 0's for normalized data this won't be true)
            if (!cygHist.IsAnyRawHistory)
            {
                IsDeleteAllNeeded = true;
                IsDeleteOldNeeded = false;
                Results.OpResult = OpStatuses.Fail;
                Results.StatusMessage = InitialSetupStatus.NoCygNetHistInRangeOfRecentSqlRows.GetErrorDescription();
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error($"Delete all triggered due to initialsetup failure for no cygnet history.[{SrcPoint.Tag.GetTagPointIdFull()}] ");
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: No raw history in cygnet returned. InitialSetup: {this.ToString()}");
                return false;
            }

            //note on this. need to explain this.
            Results.NumRawCygNetEntriesFound = cygHist.RawHistoryEntries.Count;
            if (Results.NumRawCygNetEntriesFound > 1)
            {
                Results.MostRecentRawHistTimestamp = cygHist
                    .RawHistoryEntries
                    .Select(entry => entry.AdjustedTimestamp)
                    .Max();
            }
            else
            {
                //Results.MostRecentRawHistTimestamp = cygHist
                //    .RawHistoryEntries
                //    .Select(entry => entry.AdjustedTimestamp)
                //    .Max()
                //    .Subtract(SrcPoint.SrcRule.HistoryNormalizationOptions.TimespanBetweenNormEntries);
                Results.MostRecentRawHistTimestamp = cygHist
                 .RawHistoryEntries
                .Select(entry => entry.AdjustedTimestamp)
                .Max();
            }

            if (cygHist.NormalizedHistoryEntries.IsAny())
            {
                var validNormEntries = cygHist.NormalizedHistoryEntries.Where(entry => entry.IsValid && !entry.IsValueUncertain).ToList();
                Results.LogAuditMessage($"validNormEntries Count: {validNormEntries.Count}");
                Results.NumNormEntriesFound = cygHist.NormalizedHistoryEntries.Count;
                Results.NumValidNormEntriesFound = cygHist.NormalizedHistoryEntries.Count(entry => entry.IsValid && !entry.IsValueUncertain);
                Results.NumCertainNormEntriesFound =
                    cygHist.NormalizedHistoryEntries.Count(entry => entry.IsValid && !entry.IsValueUncertain);
                if (validNormEntries.Any())
                    Results.MostRecentNormTimestamp = validNormEntries.Max(entry => entry.NormalizedHistoryValue.AdjustedTimestamp);
                else
                    Results.MostRecentNormTimestamp = Results.MostRecentRawHistTimestamp;
                Results.LastCriticalTimestamp = cygHist.NormalizedHistoryEntries?.OrderByDescending(x => x.NormalizedTimestamp).First().CriticalTimestamp;
            }
            else
            {
                Results.MostRecentNormTimestamp = Results.MostRecentRawHistTimestamp;
               
            }

            DetailedHistoryResults.CygNetSrcRawHistory = cygHist.RawHistoryEntries;
            DetailedHistoryResults.CygNetSrcNormalizedHistory = cygHist.NormalizedHistoryEntries;
            
            var cygHistRows = await compareModel.GetCygNetRows(cygHist, Schema.CygNetMappingRules, ct);
            var cygTs = cygHistRows.Select(x => x[Schema.TimeColumn.SqlTableFieldName].StringValue);
            var rawTs = cygHist.RawHistoryEntries.Select(x => x.AdjustedTimestamp);

            Results.LogAuditMessage($"cygRawTS: {string.Join(" , ", rawTs)}");

            if (!cygHistRows.IsAny())
            {
                AuditCompareResultData(sqlTs, cygHist, cygTs);

                EarliestCommonBackfillTime = DateTime.Now.AddHours(-1);

                Results.OpResult = OpStatuses.Fail;
                IsDeleteAllNeeded = true;
                IsDeleteOldNeeded = false;
                Results.StatusMessage = "No cygnet rows resolved from history";
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error($"Delete all triggered due to initialsetup failure for having no cygnet history resolved.[{SrcPoint.Tag.GetTagPointIdFull()}] ");
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: Cyg hist rows not resolved from history. InitialSetup: {this.ToString()}");
                return false;
            }

            Results.NumCygNetRows = cygHistRows.Count;

            StartSubtask(nameof(compareModel.GetComparedRowsCygRounded));
            //comparing cyg resolved and sql to determine if audit is valid

            var latestComparedRows = compareModel.GetComparedRowsCygRounded(Schema.TimeColumn.SqlTableFieldName, cygHistRows, latestSqlRows);

            Results.LogAuditMessage(GetSubTaskDuration(nameof(compareModel.GetComparedRowsCygRounded)).ToString());

            Results.LogAuditMessage($"cyg TS: {string.Join(" , ", cygTs)}");
            Results.LogAuditMessage($"sql TS: {string.Join(" , ", sqlTs)}");

            // All sql rows should match, so no "sql only" rows should be found

            bool compareIsEqual = !(latestComparedRows.Any(elm => elm.ComparisonValue != MappedSqlRowCompare.CygSqlCompare.Equal));


            DetailedHistoryResults.ComparedRows = latestComparedRows;
            DetailedHistoryResults.SqlSrcRows = latestSqlRows;

            Results.NumMatchingSqlRows =
                latestComparedRows.Count(row => row.ComparisonValue == MappedSqlRowCompare.CygSqlCompare.Equal);

            if (compareIsEqual)
            {
                Results.OpResult = OpStatuses.Pass;
                Results.StatusMessage = "Data matches";
                IsDeleteAllNeeded = false;
                IsDeleteOldNeeded = false;
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: Data passed comparison. InitialSetup: {this.ToString()}");
                return true;
            }
            else
            {
                if (!(latestComparedRows.Any(x => x.ComparisonValue != MappedSqlRowCompare.CygSqlCompare.SqlOnly))
                    && latestComparedRows.Any(y => y.ComparisonValue == MappedSqlRowCompare.CygSqlCompare.Equal)
                    && latestComparedRows.Any(x => x.ComparisonValue == MappedSqlRowCompare.CygSqlCompare.CygOnly)
                    && latestComparedRows.Where(z => z.ComparisonValue == MappedSqlRowCompare.CygSqlCompare.Equal).Select(x => x[TimeColId].ToDateTime()).Max()
                    < latestComparedRows.Where(z => z.ComparisonValue == MappedSqlRowCompare.CygSqlCompare.CygOnly).Select(x => x[TimeColId].ToDateTime()).Min())
                {
                    Results.MostRecentNormTimestamp = latestComparedRows.Where(z => z.ComparisonValue == MappedSqlRowCompare.CygSqlCompare.Equal).Select(x => x[TimeColId].ToDateTime()).Max();
                    Results.MostRecentRawHistTimestamp = Results.MostRecentNormTimestamp;
                    RecordSqlAndCygRowsToAuditLog(latestSqlRows, cygHistRows);
                    IsDeleteAllNeeded = false;
                    IsDeleteOldNeeded = false;
                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage = "Data matches except for newer Cygnet data.";
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: Data matches but some newer Cygnet data. InitialSetup: {this.ToString()}");
                    return true;
                }
                else
                {
                    RecordSqlAndCygRowsToAuditLog(latestSqlRows, cygHistRows);
                    IsDeleteAllNeeded = true;
                    IsDeleteOldNeeded = false;
                    Results.OpResult = OpStatuses.Fail;
                    Results.StatusMessage = "Comparison failed";

                    var normHistCount = cygHist.NormalizedHistoryEntries.IsAny() ? cygHist.NormalizedHistoryEntries.Count() : 0;
                    var reliableNormCount = normHistCount > 0 ? cygHist.NormalizedHistoryEntries.Count(hist => !hist.IsValueUncertain) : 0;
                    var unreliableNormCount = normHistCount > 0 ? cygHist.NormalizedHistoryEntries.Count(hist => hist.IsValueUncertain) : 0;

                    using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                        Log.Warning($"Values in CygNet do not match existing data in SQL Table for point [{SrcPoint.Tag.GetTagPointIdFull()}]. " +
                            $"Values in SQL will be deleted, and fresh data will be written. Technical data (" +
                            $"CygNet row count: {Results.NumCygNetRows} / " +
                            $"Sql row count: {DetailedHistoryResults.SqlSrcRows.Count} / " +
                            $"Cygnet Hist Raw Count: {cygHist.RawHistoryEntries.Count} / " +
                            $"Normalized value count: {normHistCount} / " +
                            $"Reliable Normalized value count: {reliableNormCount} / " +
                            $"Uncertain Normalized value count: {unreliableNormCount})");
                                       
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: Data failed comparison. InitialSetup: {this.ToString()}");
                    return false;
                }
            }
        }

        private void AuditCompareResultData(List<IConvertibleValue> sqlTs, CygNetSqlDataCompareModel.CygNetHistoryResults cygHist, IEnumerable<string> cygTs)
        {
            Results.LogAuditMessage($"sql TS: {string.Join(" , ", sqlTs)}");
            Results.LogAuditMessage($"CygHistLatest: {cygHist.LatestTime}");
            Results.LogAuditMessage($"CygHistEarliest: {cygHist.EarliestTime}");
            Results.LogAuditMessage($"isnormalizesd: {cygHist.IsNormalized}");
            Results.LogAuditMessage($"cyghistrawcount: {cygHist.RawHistoryEntries?.Count}");
            Results.LogAuditMessage($"cyg TS: {string.Join(" , ", cygTs)}");
        }

        private async Task<bool> IsBackfillRequired(SqlOperation sqlOp, CancellationToken ct)
        {
            // -- Get sql rows
            StartSubtask(nameof(sqlOp.GetEarliestXRowsFromDb));

            List<SqlHistoryRow> earliestSqlRows = _verifiedNoHistoryInSql ? new List<SqlHistoryRow>() : await sqlOp.GetEarliestXRowsFromDb(SrcPoint, ct);

            ct.ThrowIfCancellationRequested();
            Results.LogAuditMessage(GetSubTaskDuration(nameof(sqlOp.GetEarliestXRowsFromDb)).ToString());

            if (earliestSqlRows.IsAny() && (earliestSqlRows.Any(row => !row.ContainsKey(TimeColId)) ||
               earliestSqlRows.Any(row => !row[TimeColId].TryGetDateTime(out var dt))))
            {
                IsDeleteAllNeeded = true;
                IsDeleteOldNeeded = false;
                Results.OpResult = OpStatuses.Fail;
                Results.StatusMessage = "Some sql rows didn't have values for time column, or had invalid values";
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: Backfill required. Sql rows missing time column values or invalid values . InitialSetup: {this.ToString()}");
                return true;
            }

            if (!earliestSqlRows.IsAny())
            {
                
                IsDeleteOldNeeded = false;
                EarliestCommonBackfillTime = DateTime.Now.AddDays(-RetentionDays).AddSeconds(-1);
                // If no sql data, backfill def needed
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: Backfill required. No sql data. InitialSetup: {this.ToString()}");
                return true;
            }

            // Next get earliest SQL entry
            var earliestSqlDate = earliestSqlRows.Min(row => row[TimeColId].ToDateTime());

            // Check if earliest SQL entry is within one norm window of the retention period
            if (earliestSqlDate <= RetentionPeriodEarliestDate.Add(NormInterval))
            {
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: Backfill not required. Old sql data exists but delete old needed. InitialSetup: {this.ToString()}");
                IsDeleteOldNeeded = true;
                return false;
            }
                
            ct.ThrowIfCancellationRequested();

            EarliestCommonBackfillTime = earliestSqlDate.AddSeconds(-1);

            switch (IsHistoryNormalized)
            {
                case true:
                    var normLatestSearch = earliestSqlDate
                        .AddSeconds(-1)
                        .Add(NormInterval)
                        .Add(NormInterval);

                    switch (IsHistoryNormalizedExtended)
                    {
                        case true:
                            var extNormEarliestSearch = RetentionPeriodEarliestDate.AddYears(-5);
                            if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                                PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: Backfill required check for norm-extended history. InitialSetup: {this.ToString()}");
                            return await SrcPoint.HasHistoryInInterval(
                                extNormEarliestSearch,
                                normLatestSearch,
                                SrcPoint.SrcRule.GeneralHistoryOptions.IncludeUnreliable, includeNumericOnly: true);

                        default:
                            var regNormEarliestSearch = RetentionPeriodEarliestDate
                                .Subtract(NormInterval)
                                .Subtract(NormInterval);
                            if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                                PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: Backfill required check for normalized non-extended history. InitialSetup: {this.ToString()}");
                            return await SrcPoint.HasHistoryInInterval(
                                regNormEarliestSearch,
                                normLatestSearch,
                                SrcPoint.SrcRule.GeneralHistoryOptions.IncludeUnreliable, includeNumericOnly: true);
                    }

                default:
                    var rawEarliestTimestampSearch = RetentionPeriodEarliestDate;
                    var rawLatestTimestampSearch = earliestSqlDate.AddSeconds(-1);
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: InitialSetup: Backfill required check for non normalized history. InitialSetup: {this.ToString()}");
                    return await SrcPoint.HasHistoryInInterval(
                        rawEarliestTimestampSearch,
                        rawLatestTimestampSearch,
                        SrcPoint.SrcRule.GeneralHistoryOptions.IncludeUnreliable, includeNumericOnly: true);
            }
        }



        private void RecordSqlAndCygRowsToAuditLog(List<SqlHistoryRow> latestSqlRows, List<CygNetRuleModel.Resolvers.CygNetHistoryRow> cygHistRows)
        {
            Results.LogAuditMessage("cygHistRows: ");
            foreach (var row in cygHistRows)
            {
                Results.LogAuditMessage($"{string.Join(" , ", row.Values.ToList().Select(x => x.StringValue))}");
            }

            Results.LogAuditMessage("latestSqlRows: ");
            foreach (var row in latestSqlRows)
            {
                Results.LogAuditMessage($"{string.Join(" , ", row.Values.ToList().Select(x => x.StringValue))}");
            }
        }

        private async Task<List<SqlHistoryRow>> GetRecentSqlHistoryRows(
            SqlOperation sqlOp,
            CancellationToken ct)
        {
            StartSubtask(nameof(sqlOp.GetLatestXRowsFromDb));

            // Collect recent SQL history
            var latestSqlRows = await sqlOp.GetLatestXRowsFromDb(SrcPoint, ct);

            Results.LogAuditMessage(GetSubTaskDuration(nameof(sqlOp.GetLatestXRowsFromDb)).ToString());
            Results.NumTotalSqlRows = latestSqlRows.Count;

            return latestSqlRows;
        }
    }
}
