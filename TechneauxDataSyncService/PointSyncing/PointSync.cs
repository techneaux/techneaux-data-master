﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ReactiveUI;
using Serilog;
using Serilog.Context;
using TechneauxDataSyncService.ConfigFileSyncing;
using TechneauxDataSyncService.Helper;
using TechneauxDataSyncService.PointSyncing.Operations;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using TechneauxReportingDataModel.Helper;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;
using XmlDataModelUtility;
using Microsoft.VisualBasic.CompilerServices;

namespace TechneauxDataSyncService.PointSyncing
{
    public partial class PointSync : NotifyModelBase, ICancellable, IPointSync
    {
        #region Constants-Opts

        private const int MaxOpsPerSync = 5;
        private const int FirstCheckRowCount = 5;

        #endregion

        private DateTime _nextHistoryClean;

        #region LocalProperties

        private SyncFileUtility ParentSyncClass { get; }

        private SqlPointSelection ThisPointOpts
        {
            get;
        }

        private PointHistoryNormalizationOptions NormalizationOpts { get; }
        private double RetentionDays { get; }
        private bool IsNormalized { get; }

        private TimeSpan NormalizationInterval { get; }

        private DateTime? LastCriticalTimestamp { get; set; }

        public CachedCygNetPointWithRule SrcPoint { get; set; }
        //{
        //    get => GetPropertyValue<CachedCygNetPointWithRule>();
        //    set => SetPropertyValue(value);
        //}

        public Dictionary<string, IConvertibleValue> CachedColVals
        {
            get; set;
        }

        private readonly CancellationTokenSource _cts;

        #endregion

        #region InterfaceVars

        public TimeSpan PredictionInterval { get; set; }
        //{
        //    get => GetPropertyValue<TimeSpan>();
        //    private set => SetPropertyValue(value);
        //}

        public bool LastSyncInException { get; }

        private bool VerifiedNoHistoryInSql = false;

        public string CurrentStatus
        {
            get => GetPropertyValue<string>();
            private set => SetPropertyValue(value);
        }

        public string LastOperationType { get; }

        public DateTime LastSyncRunTime
        {
            get => GetPropertyValue<DateTime>();
            private set => SetPropertyValue(value);
        }

        public string LastSyncDuration
        {
            get => GetPropertyValue<string>();
            private set => SetPropertyValue(value);
        }

        public DateTime NextOperationScheduledTime
        { get; set; }
        //{
        //    get => GetPropertyValue<DateTime>();
        //    private set => SetPropertyValue(value);
        //}

        public double MinsUntilNextSync
        { get; set; }
        //{
        //    get => GetPropertyValue<double>();
        //    private set => SetPropertyValue(value);
        //}

        public double AveMinsDelayScheduleVsActual
        { get; set; }
        //{
        //    get => GetPropertyValue<double>();
        //    private set => SetPropertyValue(value);
        //}

        public int NumSyncsRun
        {
            get => GetPropertyValue<int>();
            private set => SetPropertyValue(value);
        }

        public bool TrackAllOps { get; set; }

        public List<ISqlSyncResults> OpsRecords { get; } = new List<ISqlSyncResults>();

        public List<ISqlSyncResults> OpsRecordsHarness
        {
            get => GetPropertyValue<List<ISqlSyncResults>>();
            private set => SetPropertyValue(value);
        }


        //public DateTime LatestSqlDateTimeForFirstSync
        //{
        //    get => GetPropertyValue<DateTime>();
        //    private set => SetPropertyValue(value);
        //}

        public DateTime LastCygNetTimestamp
        {
            get => GetPropertyValue<DateTime>();
            private set => SetPropertyValue(value);
        }

        public DateTime LastNormalizedTimestamp
        {
            get => GetPropertyValue<DateTime>();
            private set => SetPropertyValue(value);
        }

        public DateTime EarliestCygNetRawBackfillTimestamp
        {
            get => GetPropertyValue<DateTime>();
            private set => SetPropertyValue(value);
        }

        public DateTime EarliestCygNetNormalizedBackfillTimestamp
        {
            get => GetPropertyValue<DateTime>();
            private set => SetPropertyValue(value);
        }

        //public string LastExceptionMessage
        //{
        //    get => GetPropertyValue<string>();
        //    private set => SetPropertyValue(value);
        //}

        #endregion

        #region Logging

        private readonly ILogger _configFileLog;

        #endregion       

        public PointSync(
            SyncFileUtility prntSync,
            CachedCygNetPointWithRule newSrcPoint,
            ILogger parentConfigFileLog,
            CancellationToken parentCt)
        {
            ParentSyncClass = prntSync;
            if(ParentSyncClass.SrcDataModel.SqlConfigModel.SourcePointRules.IsAny())
            {
                ThisPointOpts = newSrcPoint.SrcRule;
            }
            else
            {
                ThisPointOpts = new SqlPointSelection();
            }
            NormalizationOpts = ThisPointOpts.HistoryNormalizationOptions;
            RetentionDays = ThisPointOpts.PollingOptions.RetentionDays;
            IsNormalized = NormalizationOpts.EnableNormalization;
            NormalizationInterval = NormalizationOpts.TimespanBetweenNormEntries;               
      
            SrcPoint = newSrcPoint;
            CurrentStatus = "Constructed - Waiting";

            _configFileLog = parentConfigFileLog;

            var randMin = new Random();

            _nextFastAudit = DateTime.Now.AddHours(2).AddMinutes(randMin.Next(0, 60 * 24));
            //_nextHistoryDelete = DateTime.Now.AddHours(1).AddMinutes(randMin.Next(0, 60 * 24));
            //if (_nextFastAudit == _nextHistoryDelete)
            //{
            //    _nextHistoryDelete.AddHours(1);
            //}
            NeedsBackfill = false;
            NeedsDeleteOld = false;
            LastCriticalTimestamp = null;
            _nextHistoryClean = DateTime.Now;
            MinsSinceLastClean = 0;
            MinsOnDeleteAllHold = 0;
            CachedColVals = new Dictionary<string, IConvertibleValue>();

            _cts = CancellationTokenSource.CreateLinkedTokenSource(parentCt);

            SyncState = SyncStates.HistoryCheck;
        }

        public enum SyncStates
        {
            HistoryCheck,
            InitialSetup,
            MaintenanceSync,
            Backfill,
            FastAudit,
            DeleteAllSQLHistory,
            DeleteOldHistory
        }

        //public static bool CheckForNumeric(SqlOperation sqlOp)
        //{
        //    foreach (var rule in sqlOp.TableSchema.CygNetMappingRules)
        //    {
        //        if ((rule.CygNetElementRule.DataElement == TechneauxReportingDataModel.CygNet.Rules.CygNetRuleBase.DataElements.PointHistory
        //            || rule.CygNetElementRule.DataElement == TechneauxReportingDataModel.CygNet.Rules.CygNetRuleBase.DataElements.PointCurrentValue)
        //            && (rule.CygNetElementRule.PointHistoryOptions.PointValueType == TechneauxReportingDataModel.CygNet.FacilityPointOptions.PointHistoryGeneralOptions.PointValueTypesNew.Value
        //            || rule.CygNetElementRule.PointHistoryOptions.PointValueType == TechneauxReportingDataModel.CygNet.FacilityPointOptions.PointHistoryGeneralOptions.PointValueTypesNew.AltValue))
        //        {
        //            foreach (var col in sqlOp.TableSchema.MatchedCols)
        //            {
        //                if (rule.SqlTableFieldName == col.Name && col.IsStringType)
        //                {
        //                    return false;
        //                }
        //            }
        //        }
        //    }
        //    return true;
        //}
        public static bool CheckForNumeric(PointHistoryNormalizationOptions historyNormOptions)
        {
            if (!historyNormOptions.EnableNormalization)
                return false;
            if (historyNormOptions.IsCalculatedNormType)
            {
                return true;
            }
            else
            {
                return false;
            }



            //foreach (var rule in sqlOp.TableSchema.CygNetMappingRules)
            //{
            //    if ((rule.CygNetElementRule.DataElement == TechneauxReportingDataModel.CygNet.Rules.CygNetRuleBase.DataElements.PointHistory
            //        || rule.CygNetElementRule.DataElement == TechneauxReportingDataModel.CygNet.Rules.CygNetRuleBase.DataElements.PointCurrentValue)
            //        && (rule.CygNetElementRule.PointHistoryOptions.PointValueType == TechneauxReportingDataModel.CygNet.FacilityPointOptions.PointHistoryGeneralOptions.PointValueTypesNew.Value
            //        || rule.CygNetElementRule.PointHistoryOptions.PointValueType == TechneauxReportingDataModel.CygNet.FacilityPointOptions.PointHistoryGeneralOptions.PointValueTypesNew.AltValue))
            //    {
            //        foreach (var col in sqlOp.TableSchema.MatchedCols)
            //        {
            //            if (rule.SqlTableFieldName == col.Name && col.IsStringType)
            //            {
            //                return false;
            //            }
            //        }
            //    }
            //}
        }
        public override string ToString()
        {
            return $"{nameof(NeedsBackfill)}=[{NeedsBackfill}], " +
                   $"{nameof(NeedsDeleteAll)}=[{NeedsDeleteAll}], " +
                   $"{nameof(NeedsDeleteOld)}=[{NeedsDeleteOld}], " +
                   $"{nameof(MinsOnDeleteAllHold)}=[{MinsOnDeleteAllHold}], " +
                   $"{nameof(MinsSinceLastClean)}=[{MinsSinceLastClean}], " +
                   $"{nameof(MinsUntilNextSync)}=[{MinsUntilNextSync}]" +
                   $"{nameof(FillPercent)}=[{FillPercent}]" +
                   $"{nameof(VerifiedNoHistoryInSql)}=[{VerifiedNoHistoryInSql}]" +
                   $"{nameof(LastCriticalTimestamp)}=[{LastCriticalTimestamp}]" +
                   $"{nameof(LastCygNetTimestamp)}=[{LastCygNetTimestamp}]" +
                   $"{nameof(LastNormalizedTimestamp)}=[{LastNormalizedTimestamp}]" +
                   $"{nameof(EarliestCygNetNormalizedBackfillTimestamp)}=[{EarliestCygNetNormalizedBackfillTimestamp}]" +
                   $"{nameof(EarliestCygNetRawBackfillTimestamp)}=[{EarliestCygNetRawBackfillTimestamp}]" +
                   $"{nameof(SyncState)}=[{SyncState}]" +
                   $"{nameof(IsNormalized)}=[{IsNormalized}]" +
                   $"{nameof(NormalizationInterval)}=[{NormalizationInterval}]" +
                   $"{nameof(RetentionDays)}=[{RetentionDays}]" +
                   $"{nameof(NumSyncsRun)}=[{NumSyncsRun}]" +
                   $"{nameof(NextOperationScheduledTime)}=[{NextOperationScheduledTime}]" +
                   $"{nameof(CurrentStatus)}=[{CurrentStatus}]" +
                   $"{nameof(LastSyncRunTime)}=[{LastSyncRunTime}]" +
                   $"{nameof(MinsUntilNextSync)}=[{MinsUntilNextSync}]";
        }

        public SyncStates SyncState
        {
            get => GetPropertyValue<SyncStates>();
            private set => SetPropertyValue(value);
        }

        public void CancelAll()
        {
            _cts.Cancel();
        }

        //public async Task SyncNewData()
        //{
        //    await SyncData(_cts.Token);
        //}

        int _taskCount = 0;
        public bool IsBusy => _taskCount > 0;

        List<ISqlSyncResults> IPointSync.OpsRecords { get; }

        private bool _firstRun = true;

        public async Task<(int numRowsInserted, int numRowsDeleted)> SyncData(
            SqlOperation sqlOp,
            SimpleCombinedTableSchema tableSchema,
            bool backfill,
            bool deleteOld,
            bool deleteAll,
            CancellationToken ct)
        {
            CurrentStatus = "Syncing";
            LastSyncRunTime = DateTime.Now;

            var sw = new Stopwatch();
            sw.Start();
            var newRowsTotal = 0;
            var deletedRowsTotal = 0;

            // If we are in maintenance state and backfill is requested, change to backfill state
            if (SyncState == SyncStates.MaintenanceSync && backfill)
                SyncState = SyncStates.Backfill;
            if (SyncState == SyncStates.MaintenanceSync && deleteOld)
            {
                SyncState = SyncStates.DeleteOldHistory;
            }
            if (deleteAll)
            {
                SyncState = SyncStates.DeleteAllSQLHistory;
            }
            try
            {
                using (LogContext.PushProperty("Point Sync Tag", SrcPoint.Tag.ToString()))
                {
                    var opCount = 0;
                    bool processNextState;

                    do
                    {
                        int thisOpRowsInsertedCount, thisOpRowsDeletedCount;
                        (processNextState, thisOpRowsInsertedCount, thisOpRowsDeletedCount) =
                            await ExecuteSingleOperation(sqlOp, tableSchema, ct);

                        newRowsTotal += thisOpRowsInsertedCount;
                        deletedRowsTotal += thisOpRowsDeletedCount;

                        opCount++;
                    } while (opCount <= MaxOpsPerSync && processNextState);
                }

                return (newRowsTotal, deletedRowsTotal);

                // -- Lock task
                //Interlocked.Increment(ref _taskCount);

                //if (NextOperationScheduledTime != default)
                //{
                //    double totalDelayMins = AveMinsDelayScheduleVsActual * NumSyncsRun + (DateTime.Now - NextOperationScheduledTime).TotalMinutes;

                //    AveMinsDelayScheduleVsActual = totalDelayMins == 0 ? 0 : totalDelayMins / (NumSyncsRun + 1);
                //}
                // -- Schema
            }
            catch (OperationCanceledException)
            {
                _configFileLog.Warning($"{SrcPoint.Tag.GetTagPointIdFull()}: Sync cancelled");
                return (newRowsTotal, deletedRowsTotal);
            }
            catch (Exception ex)
            {
                _configFileLog.Error(ex, $"{SrcPoint.Tag.GetTagPointIdFull()}: Point sync failed");

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "General failure in point sync for point {pnt}", SrcPoint.Tag);

                return (newRowsTotal, deletedRowsTotal);
            }
            finally
            {
                Interlocked.Decrement(ref _taskCount);
                CurrentStatus = "Waiting for next sync";
                NumSyncsRun++;

                sw.Stop();

                if (sw.Elapsed < TimeSpan.FromMilliseconds(100))
                {
                    LastSyncDuration = sw.ElapsedMilliseconds.ToString("0") + " msec";
                }
                else
                {
                    LastSyncDuration = sw.Elapsed.TotalSeconds.ToString("0.00") + " secs";
                }
            }
        }

        private DateTime _nextFastAudit;

        public override bool Equals(object obj)
        {
            if (obj is PointSync ps)
            {
                return SrcPoint == ps.SrcPoint;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return SrcPoint.GetHashCode();
        }
    }
}
