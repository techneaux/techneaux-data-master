﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using System.Threading.Tasks;
using Serilog;
using Serilog.Context;
using Serilog.Core;
using Serilog.Filters;
using Serilog.Formatting.Json;
using TechneauxUtility;
using XmlDataModelUtility;
using Bytes = TechneauxUtility.Bytes;

namespace TechneauxDataSyncService.Main
{
    public partial class DataSync : ServiceBase
    {
        public DataSync()
        {
            var LoggingLevelAttr = ConfigurationManager.AppSettings.Get("LogLevelSync");
            Serilog.Events.LogEventLevel newLevel = (Serilog.Events.LogEventLevel)Enum.Parse(typeof(Serilog.Events.LogEventLevel), LoggingLevelAttr);
            var logLevelSwitch = new LoggingLevelSwitch(newLevel);


            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.ControlledBy(logLevelSwitch)
                .Enrich.FromLogContext()
                .WriteTo
                .File(
                    //new JsonFormatter(),
                    $@"{AppDomain.CurrentDomain.BaseDirectory}\Logs\General Sync Service Log (started on {DateTime.Now:MMMM dd, yyyy, hh-mm}).txt",
                    rollOnFileSizeLimit: true,
                    fileSizeLimitBytes: Bytes.FromMegabytes(10),
                    retainedFileCountLimit: 7,
                    shared: true)
                .CreateLogger();

            Log.Logger = new LoggerConfiguration()
               .MinimumLevel.ControlledBy(logLevelSwitch)
               .Enrich.FromLogContext()
               .Enrich.WithCaller()
               .WriteTo
               .File(
                   //new JsonFormatter(),
                   path: $@"{AppDomain.CurrentDomain.BaseDirectory}\Logs\General Sync Service Log (started on {DateTime.Now:MMMM dd, yyyy, hh-mm}).txt",
                   outputTemplate: "[{Timestamp:M-d-yy hh:mm:ss} {Level:u3}] {Message:lj} (at {Caller}){NewLine}{Exception}",
                   rollOnFileSizeLimit: true,
                   fileSizeLimitBytes: Bytes.FromMegabytes(10),
                   retainedFileCountLimit: 7
                   /*shared: true*/)
               .WriteTo.Logger(adminLoggerConfig => adminLoggerConfig
                   .Filter.ByIncludingOnly(Matching.WithProperty(nameof(GlobalLoggers.SpecialLogTypes), GlobalLoggers.SpecialLogTypes.AdminEvent))
                   .WriteTo.File(
                       outputTemplate: "[{Timestamp:M-d-yy hh:mm:ss} ADMIN {Level:u3}] {Message:lj} {NewLine} ========Exception Details========= {NewLine}{Exception}{NewLine}",
                       path: $@"{AppDomain.CurrentDomain.BaseDirectory}\Logs\Service Admin Events (started on {DateTime.Now:MMMM dd, yyyy, hh-mm}).txt",
                       rollOnFileSizeLimit: true,
                       fileSizeLimitBytes: Bytes.FromMegabytes(10),
                       retainedFileCountLimit: 3,
                       shared: true))
               .WriteTo.Logger(generalExceptionLoggerConfig => generalExceptionLoggerConfig
                   .Filter.ByIncludingOnly(Matching.WithProperty(nameof(GlobalLoggers.SpecialLogTypes), GlobalLoggers.SpecialLogTypes.UnhandledException))
                   .WriteTo.File(
                       path: $@"{AppDomain.CurrentDomain.BaseDirectory}\Logs\Service Unhandled Exceptions (started on {DateTime.Now:MMMM dd, yyyy, hh-mm}).txt",
                       outputTemplate: "[{Timestamp:M-d-yy hh:mm:ss} UNHANDLED {Level:u3}] {Message:lj} (at {Caller}){NewLine}{Exception}",
                       rollOnFileSizeLimit: true,
                       fileSizeLimitBytes: Bytes.FromMegabytes(10),
                       retainedFileCountLimit: 3,
                       shared: true))
               .CreateLogger();


            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(GlobalLoggers.SpecialLogTypes), GlobalLoggers.SpecialLogTypes.UnhandledException))
                using (LogContext.PushProperty(nameof(GlobalLoggers.SpecialLogTypes), GlobalLoggers.SpecialLogTypes.AdminEvent))
                    Log.Error(ex, "Unhandled exception, general service init failed. Sync will not proceed.");

            }
        }

        NewFileCheckService _fileCheckService;

        protected override async void OnStart(string[] args)
        {
            try
            {
                var thisExePath = Assembly.GetExecutingAssembly().Location;
                var thisConfigPath = $@"{Path.GetDirectoryName(thisExePath)}\ServiceConfigs\";

                if (await CheckLicensing())
                {
                    _fileCheckService = new NewFileCheckService(thisConfigPath);
                    await _fileCheckService.RunNewFileCheck();
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(GlobalLoggers.SpecialLogTypes), GlobalLoggers.SpecialLogTypes.UnhandledException))
                using (LogContext.PushProperty(nameof(GlobalLoggers.SpecialLogTypes), GlobalLoggers.SpecialLogTypes.AdminEvent))
                    Log.Error(ex, "Unhandled exception, New File Check Service init failed for file. Sync is cancelled.");

                _fileCheckService?.CancelAll();
            }
            finally
            {

            }
        }

        public static async Task<bool> CheckLicensing()
        {

            // Check if in developer mode
            if (CvsSubscriptionLicensing.InDeveloperMode)
            {
                Log.Warning("IN DEVELOPER MODE");
                return true;
            }

            var publicKeyString =
                CvsSubscriptionLicensing.ReadPublicKeyStringFromAssembly(Assembly.GetExecutingAssembly(),
                    "DM_public.key");

            var licenseStatus = CvsSubscriptionLicensing.TryReadingLicense(publicKeyString);

            switch (licenseStatus)
            {
                case CvsSubscriptionLicensing.LicenseReadStatus.NoLicenseFileFound:
                    Log.Error("Could not find a license file, please put a valid license file in the <License> subfolder for the application.");
                    return false;
                case CvsSubscriptionLicensing.LicenseReadStatus.PublicKeyMissing:
                    Log.Error("Decryption error, please contact Techneaux.");
                    return false;
                case CvsSubscriptionLicensing.LicenseReadStatus.CouldNotReadLicense:
                    Log.Error("License file was found but could not be read or was invalid. Please contact Techneaux.");
                    return false;
                case CvsSubscriptionLicensing.LicenseReadStatus.MultipleLicensesFound:
                    Log.Error("Multiple license files were found, please make sure there is only one license file in the <License> subfolder for the application.");
                    return false;
                case CvsSubscriptionLicensing.LicenseReadStatus.ValidLicenseFound:
                    // Check if domain is authorized first
                    if (!CvsSubscriptionLicensing.IsCurrentNetworkDomainAuthorized)
                    {
                        Log.Error(
                            $"Your current network domain ({Environment.UserDomainName}) is not authorized in the current license. Authorized domains are {string.Join(",", CvsSubscriptionLicensing.AllowedNetworkDomains)}. Please contact Techneaux for an updated license.");
                        return false;
                    }

                    // Check for expiration 
                    if (CvsSubscriptionLicensing.IsCurrentlyExpired)
                    {
                        // Regular subscription is expired, now check for grace period
                        if (CvsSubscriptionLicensing.IsGracePeriodExpired)
                        {
                            Log.Error(
                                $"Your license expired on {CvsSubscriptionLicensing.ExpirationDate:MM/dd/yyyy}, " +
                                $"and your grace period of {CvsSubscriptionLicensing.GracePeriodDays} days for renewal has also passed. " +
                                Environment.NewLine + Environment.NewLine +
                                $"This client and the service will not operate until a new license is provided. Please contact Techneaux.");
                            return false;
                        }
                        else
                        {
                            Log.Error(
                                $"Your license expired on {CvsSubscriptionLicensing.ExpirationDate:MM/dd/yyyy}, " +
                                $"but you have a {CvsSubscriptionLicensing.GracePeriodDays} day grace period after expiration to renew before the Data Master client and service stop operating." +
                                Environment.NewLine + Environment.NewLine +
                                $"You have {CvsSubscriptionLicensing.DaysLeftInGracePeriod} days left in this grace period. Please contact Techneaux to renew or cancel.");
                            return false;
                        }
                    }
                    else if (CvsSubscriptionLicensing.DaysBeforeRegularExpiration < 90)
                    {
                        Log.Warning(
                            $"Your license will expire on {CvsSubscriptionLicensing.ExpirationDate:MM/dd/yyyy}, {CvsSubscriptionLicensing.DaysBeforeRegularExpiration} days from now." +
                            Environment.NewLine + Environment.NewLine +
                            $"After the license expires, you have a {CvsSubscriptionLicensing.GracePeriodDays} day grace period to renew before the Data Master client and service stop operating." +
                            $" Please contact Techneaux to renew or cancel.");
                        return true;
                    }

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return true;
        }

        protected override void OnStop()
        {
            _fileCheckService?.CancelAll();
        }
    }
}
