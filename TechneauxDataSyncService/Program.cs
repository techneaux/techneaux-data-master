﻿using System;
using System.ServiceProcess;
using GenericRuleModel.Rules;
using TechneauxDataSyncService.Main;
using TechneauxUtility;

namespace TechneauxDataSyncService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        
        static void Main()
        {
            EnumerationTableHandler.AutoLoadNew = true;

            ServiceBase[] servicesToRun;
            ServiceOps.LogAllOpResults = false;
            servicesToRun = new ServiceBase[]
            {
                new DataSync()
            };
            
            ServiceBase.Run(servicesToRun); 
        }
    }
}
