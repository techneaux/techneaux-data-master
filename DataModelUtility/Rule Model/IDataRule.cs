﻿namespace XmlDataModelUtility.Rule_Model
{
    public interface IDataRule
    {
        //IConvertableValue

        RuleValueTypes RuleValueType { get; }

        bool IsRuleValid { get; }

        //object ResolveSingleValue();

        //object ResolveHistoryValues();
    }
}
