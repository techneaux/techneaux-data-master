﻿using System.Runtime.CompilerServices;
using System.Xml.Serialization;

namespace XmlDataModelUtility
{
    public interface IValidatedRule
    {
        [XmlIgnore]
        bool IsRuleValid { get; }

        [XmlIgnore]
        string ValidationErrorMessage { get; }
    }
}
