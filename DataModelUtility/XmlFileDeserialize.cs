﻿using CygNet.Data.Points;
using Serilog;
using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.IO.Compression;
using System.Security.Policy;
using System.Xml.Serialization;
using Serilog.Context;
using static XmlDataModelUtility.GlobalLoggers;

namespace XmlDataModelUtility
{
    public static class XmlFileSerialization<T>
    {
        public static T LoadModelFromFile(string strPath)
        {
            try
            {
                using (var sr = new StreamReader(strPath))
                {
                    var xmlSerial = new XmlSerializer(typeof(T), NotifyCopyDataModel.DerivedTypes);
                    var readConfig = (T)xmlSerial.Deserialize(sr);

                    return readConfig;
                }
            }
            catch (IOException ex)
            {
                Log.Error(ex, "Failed to load config file {path}. Please check that the file exists and is not locked by another user.", strPath);
                CustomerLogger.Error(ex, "Failed to load config file {path}. Please check that the file exists and is not locked by another user.", strPath);

                return default;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Deserialization failed during loading model from file: {path}", strPath);

                return default;
            }
        }

        public static T LoadModelFromGzipFile(string strPath)
        {
            try
            {
                using (FileStream outFile = File.Open(strPath, FileMode.Open))
                using (GZipStream decompress = new GZipStream(outFile, CompressionMode.Decompress))
                using (StreamReader sr = new StreamReader(decompress))
                //using (var sr = new StreamReader(strPath))
                {
                    var xmlSerial = new XmlSerializer(typeof(T), NotifyCopyDataModel.DerivedTypes);
                    var readConfig = (T)xmlSerial.Deserialize(sr);

                    return readConfig;
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex,
                        "Unhandled exception while loading data model from compressed gzip file format at path {path}. Please contact Techneaux.",
                        strPath);

                return default;
            }
        }

        public static string GetXmlString(T srcModel, XmlRootAttribute root, XmlSerializerNamespaces defaultNamespace)
        {
            try
            {
                using (var sw = new StringWriter())
                {
                    var derivedTypeList = NotifyCopyDataModel.DerivedTypes;

                    XmlAttributeOverrides overrides = new XmlAttributeOverrides();
                    XmlAttributes attributes = new XmlAttributes();
                    attributes.XmlIgnore = true;
                    overrides.Add(typeof(PointConfigRecord), "Tag", attributes);

                    var xmlSerial = new XmlSerializer(typeof(T), derivedTypeList);

                    xmlSerial.Serialize(sw, srcModel, defaultNamespace);

                    return sw.ToString();
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, $"Unhandled exception while serializing datamodel to XML. Please contact Techneaux");
                
                return null;
            }
        }

        public static string GetXmlString(T srcModel)
        {
            try
            {
                using (var sw = new StringWriter())
                {
                    var derivedTypeList = NotifyCopyDataModel.DerivedTypes;

                    var xmlSerial = new XmlSerializer(typeof(T), derivedTypeList);
                    xmlSerial.Serialize(sw, srcModel);

                    return sw.ToString();
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, $"Serializing model failed");

                return null;
            }
        }


        public static bool SaveModelToFile(T srcModel, string strPath)
        {
            try
            {
                using (var sw = new StreamWriter(strPath))
                {
                    var derivedTypeList = NotifyCopyDataModel.DerivedTypes;
                    var xmlSerial = new XmlSerializer(typeof(T), derivedTypeList);
                    xmlSerial.Serialize(sw, srcModel);

                    return true;
                }
            }
            catch (DirectoryNotFoundException)
            {
                Log.Error("Destination folder in the path ({path}) was not found", strPath);
            }
            catch (PathTooLongException)
            {
                Log.Error("Path ({path}) was too long", strPath);
            }
            catch (UnauthorizedAccessException)
            {
                Log.Error("Access denied while attempting to write to ({path})", strPath);
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, $"Unhandled exception while saving file to path [{strPath}]");
            }

            return false;
        }

        public static bool TryWriteObjectToFile(T src, string fileName)
        {
            var path = $@"{Environment.CurrentDirectory}\{fileName}";

            try
            {
                using (var sw = new StreamWriter(path))
                {
                    var xmlSerial = new XmlSerializer(typeof(T));
                    xmlSerial.Serialize(sw, src);

                    return true;
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, $"Unhandled exception while saving object to path [{path}]");
            }

            return false;
        }


        public enum FileOperationResult
        {
            Successful
        }
    }
}
