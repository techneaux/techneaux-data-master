﻿using System;
using System.Reflection;

namespace XmlDataModelUtility
{
    public class HelperClasses
    {        
        [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
        public class ControlTitle : Attribute
        {
            public ControlTitle(string title, string shortTitle = "")
            {
                Title = title;
                ShortTitle = shortTitle;
            }

            public string Title { get; set; }
            public string ShortTitle { get; set; }
        }

        //********************************** Attribute Helper function **********************************
        public static ControlTitle GetAttribute(Type classType, string attributeName)
        {
            var properties = classType.GetProperties();
            foreach (var prop in properties)
            {
                if (prop.Name == attributeName)
                {
                    foreach (var props in prop.GetCustomAttributes<ControlTitle>())
                    {
                        return props;
                    }

                }
            }

            //else no applicable attribute found
            return null;
        }
    }

}
