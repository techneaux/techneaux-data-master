﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace XmlDataModelUtility
{
    public class NotifyModelBase : INotifyPropertyChanged
    {
        [XmlIgnore]
        [JsonIgnore]
        public Guid Guid { get; } = Guid.NewGuid();
        #region "Interface Implementation"

        [NonSerialized]
        [JsonIgnore]
        protected readonly Dictionary<string, object> PropertyBackingDictionary = new Dictionary<string, object>();

        protected readonly Dictionary<IBindingList, string> ListNames = new Dictionary<IBindingList, string>();

        protected T GetPropertyValue<T>([CallerMemberName] string propertyName = null)
        {
            if (propertyName == null) throw new ArgumentNullException(nameof(propertyName));

            if (PropertyBackingDictionary.TryGetValue(propertyName, out var value))
            {
                return (T)value;
            }

            return default;
        }

        private readonly Dictionary<Guid, string> _complexObjectName = new Dictionary<Guid, string>();
        protected bool SetPropertyValue<T>(T newValue, [CallerMemberName] string propertyName = null)
        {
            if (propertyName == null)
                throw new ArgumentNullException(nameof(propertyName));

            if (PropertyBackingDictionary.TryGetValue(propertyName, out var value) && EqualityComparer<T>.Default.Equals(newValue, (T)value))
            {
                return false;
            }
            else
            {
                PropertyBackingDictionary[propertyName] = newValue;

                if (newValue is INotifyPropertyChanged changed)
                {
                    if (changed is NotifyModelBase complexObj)
                        _complexObjectName[complexObj.Guid] = propertyName;

                    changed.PropertyChanged += PropObj_PropertyChanged;
                }
                else if (newValue is IBindingList thisBindingList)
                {
                    ListNames[thisBindingList] = propertyName;
                    thisBindingList.ListChanged += PropObj_ListChanged;
                }

                OnPropertyChanged(propertyName);

                if (this is IValidatedRule)
                {
                    OnPropertyChanged(nameof(IValidatedRule.IsRuleValid));
                    OnPropertyChanged(nameof(IValidatedRule.ValidationErrorMessage));
                }

                return true;
            }
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        protected internal void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            LastPropertyUpdated = propertyName;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            //PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(LastPropertyUpdated)));
        }

        private void PropObj_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (!ListNames.TryGetValue(sender as IBindingList, out var propName))
            {
                propName = e.ListChangedType.ToString();
            }

            OnPropertyChanged(propName);
        }

        private void PropObj_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (sender is NotifyModelBase complexObj)
                OnPropertyChanged(_complexObjectName[complexObj.Guid]);
            else            
                OnPropertyChanged(e.PropertyName);

            if (this is IValidatedRule)
            {
                OnPropertyChanged(nameof(IValidatedRule.IsRuleValid));
                OnPropertyChanged(nameof(IValidatedRule.ValidationErrorMessage));
            }
        }

        [XmlIgnore]
        [JsonIgnore]
        public string LastPropertyUpdated { get; private set; }

        #endregion
    }
}
