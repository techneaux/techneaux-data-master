﻿using System;

namespace XmlDataModelUtility
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public sealed class AffectedByOtherPropertyChangeAttribute : Attribute
    {
        public AffectedByOtherPropertyChangeAttribute(
        string otherPropertyName)
        {
            TriggerProperty = otherPropertyName;
        }

        public string TriggerProperty
        {
            get;
            private set;
        }
    }
}
 