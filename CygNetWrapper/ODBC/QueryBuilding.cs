﻿using System.Collections.Generic;
using System.Linq;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Facilities.Filtering;

namespace Techneaux.CygNetWrapper.ODBC
{
    internal static class QueryBuilding
    {
        public static List<string> BuildSqlConditionList(List<IFacilityFilterCondition> facFilters, string srcTableAlias = "")
        {
            // If table name is not empty, append a period
            if (!string.IsNullOrEmpty(srcTableAlias))
                srcTableAlias += ".";

            var conditions = new List<string>();

            foreach (var filterTemp in facFilters.Where(filter => filter.AttributeId != ""))
            {
                //filterTemp.ConditionalValue = filterTemp.ConditionalValue.Replace('*', '%');

                var thisOperator = filterTemp.ConditionalOperator.ToString().ToUpper().Replace("NOT", "NOT ");

                if (filterTemp.ConditionalOperator == SqlConditionalOperators.In
                    || filterTemp.ConditionalOperator == SqlConditionalOperators.NotIn)
                {
                    //conditions.Add($"{srcTableAlias}{filterTemp.AttributeId} {thisOperator} ('{string.Join("','", filterTemp.InItemList)}')");
                    var subConditions = new List<string>();
                    foreach (var compVal in filterTemp.InItemList)
                    {
                        subConditions.Add(compVal.Contains("*")
                            ? $"{srcTableAlias}{filterTemp.AttributeId} LIKE '{compVal.Replace('*', '%')}'"
                            : $"{srcTableAlias}{filterTemp.AttributeId} = '{compVal}'");
                    }

                    conditions.Add(filterTemp.ConditionalOperator == SqlConditionalOperators.NotIn
                        ? $"NOT ({string.Join(" OR ", subConditions)})"
                        : $"({string.Join(" OR ", subConditions)})");
                }
                else
                {
                    conditions.Add($"{srcTableAlias}{filterTemp.AttributeId} {thisOperator} '{filterTemp.ConditionalValue.Replace('*', '%')}'");
                }
            }

            return conditions;
        }
    }
}
