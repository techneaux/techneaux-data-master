﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Techneaux.CygNetWrapper
{
    public static class ExtensionMethods
    { 
        public static bool HasProperty(this object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName) != null;
        }

        //public static bool IsAny<T>(this IEnumerable<T> srcEnum)
        //{
        //    return (srcEnum != null && srcEnum.Any());
        //}      

        public static string GetErrorDescription(this Enum value)
        {
            var field = value.GetType().GetField(value.ToString());

            return !(Attribute.GetCustomAttribute(field,
                typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
                ? value.ToString()
                : attribute.Description;
        }

        internal static string GetDescription(this Enum value)
        {
            var field = value.GetType().GetField(value.ToString());

            return !(Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
                ? value.ToString()
                : attribute.Description;
        }
    }
}