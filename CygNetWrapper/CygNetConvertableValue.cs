﻿using System.ComponentModel;
using TechneauxUtility;

namespace Techneaux.CygNetWrapper
{
    public class CygNetConvertableValue : ConvertibleValue
    {
        public CygNetConvertableValue(object newVal) : base(newVal)
        {
            ErrorState = ErrorType.None;
        }
        public CygNetConvertableValue(ErrorType errorTypeRef, string errorDescriptionRef) : base(null)
        {
            ErrorState = errorTypeRef;
            ErrorDescription = errorDescriptionRef;
        }

        public enum ErrorType
        {
            [Description("No Error")]
            None,
            [Description("Facility Error")]
            FacError,
            [Description("Fms Error")]
            FmsError,
            [Description("Attribute Error")]
            AttrError,
            [Description("Point Error")]
            PointErr,
            [Description("Formatting Error")]
            FormattingErr,
            [Description("Expression Error")]
            ExpressionErr,
            [Description("Unknown Error")]
            UnknownErr,
            [Description("Blank Error")]
            BlankErr
        }
        
        public ErrorType ErrorState { get; private set; }

        public string ErrorDescription { get; private set; }

        public bool HasError => ErrorState != ErrorType.None;
    }
}
