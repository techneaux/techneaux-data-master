﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CygNet.Data.Core;
using CygNet.Data.Points;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Services;
using System;
using CygNet.Data.Historian;
using Techneaux.CygNetWrapper.Services.PNT;
using Techneaux.CygNetWrapper.Services.VHS;

namespace Techneaux.CygNetWrapper.Points
{
    public interface ICygNetPoint
    {
        CurrentValueService CvsService { get; }
        ICygNetFacility Facility { get; }
        bool HasCurrentValue { get; }
        string LongId { get; }
        PointService PntService { get; }
        string ShortId { get; }
        PointTag Tag { get; }
        string TagFacilityUdc { get; }

        bool Equals(object obj);
        PointValue GetCurrentValue();
        Task<DateTime?> GetEarliestHistoryEntryDate(bool refresh);
        Task<List<CygNetHistoryEntry>> GetEarliestHistoryInInterval(DateTime earliestDate, DateTime latestDate, CancellationToken ct, int numEntries, bool includeUnreliable, bool includeNumericOnly);
        int GetHashCode();
        Task<List<CygNetHistoryEntry>> GetHistory(DateTime earliestDate, DateTime latestDate, IProgress<int> progress, CancellationToken ct);
        Task<List<CygNetHistoryEntry>> GetHistory(DateTime earliestDate, DateTime latestDate, IProgress<int> progress, bool includeUnreliable ,bool includeNumericOnly, CancellationToken ct);
        Task<List<CygNetHistoryEntry>> GetHistoryRollup(DateTime earliestDate, DateTime latestDate, CancellationToken ct);
        Task<NameStatistics> GetHistoryStatistics(bool refresh = false);
        Task<DateTime?> GetLatestHistoryEntryDate(bool refresh);
        Task<List<CygNetHistoryEntry>> GetLatestHistoryInInterval(DateTime earliestDate, DateTime latestDate,
            int? numEntries, bool includeUnreliable, bool includeNumericOnly, CancellationToken ct);

        Task<PointConfigRecord> GetPointConfigRecord(bool refresh = false);
        Task<bool> HasAnyHistory(bool refresh = false);
        Task<bool> HasHistory(bool includeUnreliable, bool includeNumericOnly, bool refresh = false);
        Task<bool> HasHistoryInInterval(DateTime earliestDate, DateTime latestDate, bool includeUnreliable, bool includeNumericOnly);
        void RefreshPointConfigRecord();
        string ToString();
        bool TryGetAttributeValue(string attrId, out PointAttributeValue attributeValue, bool refresh = false);
        bool TryGetCurrentValue(out PointValue result);
        void WriteValue(object newValue, DateTime? newTimestamp);
        void WriteValue(object value, DateTime? timeStamp, BaseStatusFlags baseStatusMask, BaseStatusFlags baseStatus, UserStatusFlags userStatusMask, UserStatusFlags userStatus);
    }

    //public interface ICygNetPoint
    //{
    //    CurrentValueService CvsService { get; }
    //    ICygNetFacility Facility { get; }
    //    PointService PntService { get; }
    //    string ShortId { get; }
    //    string LongId { get; }
    //    PointTag Tag { get; }
    //    string TagFacilityUdc { get; }
    //    Task<PointConfigRecord> GetPointConfigRecord(bool refresh = false);

    //    bool TryGetAttributeValue(string attrId, out PointAttributeValue attributeValue, bool refresh = false);

    //    Task<bool> HistoryAvailableInInterval(DateTime earliestDate, DateTime latestDate);

    //    Task<List<CygNetHistoryEntry>> GetHistory(
    //        DateTime earliestDate, 
    //        DateTime latestDate,
    //        IProgress<int> progress,
    //        CancellationToken ct);

    //    Task<List<CygNetHistoryEntry>> GetHistoryRollup(
    //        DateTime earliestDate, 
    //        DateTime latestDate,
    //        CancellationToken ct);

    //    Task<List<CygNetHistoryEntry>> GetEarliestHistoryInInterval(
    //        DateTime earliestDate, 
    //        DateTime latestDate,
    //        CancellationToken ct, 
    //        int numEntries);

    //    Task<List<CygNetHistoryEntry>> GetLatestHistoryInInterval(
    //        DateTime earliestDate, 
    //        DateTime latestDate,
    //        CancellationToken ct, 
    //        int numEntries);

    //    Task<NameStatistics> GetHistoryStatistics(bool refresh = false);

    //    Task<bool> HasAnyHistory(bool refresh = false);

    //    Task<DateTime?> GetEarliestHistoryEntryDate(bool refresh);

    //    Task<DateTime?> GetLatestHistoryEntryDate(bool refresh);
    //}

    public interface ICachedCygNetPoint : ICygNetPoint
    {

        Task<List<PointAttributeValue>> GetAllAttributeValuesAsync(CancellationToken ct);

        Task<PointAttributeValue> GetAttributeValueAsync(string attrId, CancellationToken ct, bool refresh);

        Task<List<PointAttributeValue>> GetAttributeValuesAsync(IEnumerable<string> attrIds, CancellationToken ct,
            bool refresh);

        new ICachedCygNetFacility Facility { get; }
    }
}