﻿using System;
using System.Collections.Generic;
using CygNet.Data.Core;

namespace Techneaux.CygNetWrapper.Points.PointCaching
{
    public class PointAttributeValueSet
    {
        private const int MinsToExpire = 5;
        
        public PointAttribute PointAttr { get; }

        public DateTime LastRequest { get; private set; }
        //public DateTime RequestsPerMinute { get; private set; }

        public DateTime LastSingleUpdate { get; private set; }
        public DateTime LastFullUpdate { get; private set; }

        public bool IsFullyCached => (DateTime.Now - LastFullUpdate).TotalMinutes > MinsToExpire;
        public bool IsExpired => (DateTime.Now - LastFullUpdate).TotalMinutes > MinsToExpire;

        public PointAttributeValueSet(
            PointAttribute srcAttr)
        {
            PointAttr = srcAttr;
        }

        private Dictionary<PointTag, PointAttributeValue> _cachedValues =
            new Dictionary<PointTag, PointAttributeValue>();

        public bool TryGetAttributeValue(
            PointTag key,
            out PointAttributeValue value)
        {
            LastRequest = DateTime.Now;
            
            if (IsExpired)
            {
                lock (_cachedValues)
                {
                    _cachedValues.Clear();
                }

                value = null;
                return false;
            }

            lock (_cachedValues)
            {
                return _cachedValues.TryGetValue(key, out value);
            }
        }

        public void UpdateAll(Dictionary<PointTag, PointAttributeValue> newValues)
        {
            LastFullUpdate = DateTime.Now;
            
            lock (_cachedValues)
            {
                _cachedValues = newValues;
                LastFullUpdate = DateTime.Now;
            }
        }

        public void UpdateSingle(PointTag tag, PointAttributeValue newValue)
        {
            LastSingleUpdate = DateTime.Now;
            lock (_cachedValues)
            {
                _cachedValues[tag] = newValue;
                LastFullUpdate = DateTime.Now;
            }
        }
    }

}
