﻿using System.Collections.Generic;
using System.Linq;
using CygNet.Data.Core;
using Techneaux.CygNetWrapper.Facilities;
using TechneauxUtility;

namespace Techneaux.CygNetWrapper.Points.PointCaching
{
    // What do we need to know
    //  1) Have we attempted to pull data on a single UDC before and found it doesn't exist
    //  2) Have we attempted to pull all udcs on a particular facility


    public class CachedFac
    {
        private readonly Dictionary<string, CachedUdc> _udcPointLookupDict = new Dictionary<string, CachedUdc>();

        public ICachedCygNetFacility MyFac { get; }
        public CachedFac(ICachedCygNetFacility thisFac)
        {
            MyFac = thisFac;
        }

        public FacilityTag Tag => MyFac.Tag;

        public CachedUdc GetCachedUdc(string udc)
        {
            return _udcPointLookupDict[udc];
        }
        
        public bool TryGetCachedUdc(string udc, out CachedUdc result)
        {
            if (AllPointsFound && !_udcPointLookupDict.ContainsKey(udc))
            {
                // If all points found and this udc not in the cache, must not exist, add empty entry
                AddFailedUdc(udc);
            }

            return _udcPointLookupDict.TryGetValue(udc, out result);
        }

        public bool HasCachedUdc(string udc)
        {
            return TryGetCachedUdc(udc, out var _);
        }

        public IEnumerable<ICachedCygNetPoint> GetAllCachedPoints()
        {
            return _udcPointLookupDict.Values.Where(cudc => cudc.UdcExistsOnFacility).Select(cudc => cudc.ThisPoint);
        }

        public IEnumerable<ICachedCygNetPoint> GetCachedPoints(IEnumerable<string> udcs)
        {
            if (!udcs.IsAny())
            {
                return GetAllCachedPoints();
            }
            else
            {
                var udcSet = new HashSet<string>(udcs);

                return _udcPointLookupDict.Values.Where(cudc => cudc.UdcExistsOnFacility && udcSet.Contains(cudc.Udc)).Select(cudc => cudc.ThisPoint);
            }
        }

        public void AddPoint(ICachedCygNetPoint thisPnt)
        {
            var newPointRecord = new CachedUdc(thisPnt);

            lock(_udcPointLookupDict)
            {
                _udcPointLookupDict[newPointRecord.Udc] = newPointRecord;
            }            
        }

        public void AddPoints(IEnumerable<ICachedCygNetPoint> pnts)
        {
            foreach (var tag in pnts)
            {
                AddPoint(tag);
            }
        }

        public bool AllPointsFound { get; set; }
        public void AddAllAvailablePointsForFac(IEnumerable<CachedCygNetPoint> allPoints)
        {
            AddPoints(allPoints);

            AllPointsFound = true;
        }

        public void AddFailedUdc(string udcNotFoundOnFac)
        {
            var newUdcRecord = new CachedUdc(udcNotFoundOnFac);

            lock (_udcPointLookupDict)
            {
                _udcPointLookupDict[newUdcRecord.Udc] = newUdcRecord;
            }
        }

        public override bool Equals(object obj)
        {
            var compareFac = (CachedFac)obj;
            return MyFac.Equals(compareFac.MyFac);
        }

        public override int GetHashCode()
        {
            return MyFac.GetHashCode();
        }

        public override string ToString()
        {
            return MyFac.ToString();
        }
    }
}
