﻿using CygNet.Data.Core;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog.Context;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.PNT;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace Techneaux.CygNetWrapper.Points.PointCaching
{
    public class PointValueTimestampCache
    {
        private static TimeSpan RetentionTime { get; } = TimeSpan.FromSeconds(30);
        private readonly ExpiringCache<string, Dictionary<string, DateTime?>> _cachedValueTimestamps;
        private CurrentValueService ParentCvsService { get; }

        public PointValueTimestampCache(CurrentValueService srcCvs)
        {
            ParentCvsService = srcCvs;
            CancellationTokenSource cts = new CancellationTokenSource();

            _cachedValueTimestamps = new ExpiringCache<string, Dictionary<string, DateTime?>>
                (CacheAttributeValueAllPointsWithUdc, TimeSpan.FromMinutes(CurrentValueService.MaxCVRefreshRate));

        }

        public async Task<DateTime?> GetLatestValueTimestampForPoint(
            PointTag srcTag,
            CancellationToken ct,
            bool forceRefresh = false)
        {
            try
            {
                // Check if fac service is correct
                if (srcTag.SiteService.ToString() != ParentCvsService.SiteService.ToString())
                    throw new InvalidOperationException($"Facility tag site.service [{srcTag.SiteService}] does not match the current value service [{ParentCvsService.SiteService}] in the cache");

                var facTsDict = await _cachedValueTimestamps.GetCachedItem(srcTag.UDC, ct);

                if (facTsDict.TryGetValue(srcTag.FacilityId, out var ts))
                {
                    return ts;
                }

                return null;
            }
            catch (OperationCanceledException)
            {
                return null;
                // Cancellations ignored
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception while getting latest point timestamp for tag ({tag}).", srcTag.ToString());

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error(ex, "Unable to get latest timestamp for point ({tag}). Sync may stop working for this point.", srcTag.ToString());

                return null;
            }
        }

        private async Task<Dictionary<string, DateTime?>> CacheAttributeValueAllPointsWithUdc(
            string udc,
            CancellationToken ct)
        {
            if (!CygNetPoint.IsUdcValid(udc))
                throw new ArgumentException(nameof(udc));
            try
            {
                var myPointService = await ParentCvsService.GetAssociatedPnt();

                var dsn = "DSN=CygNet;ServiceFilter=" + myPointService.SiteService;

                using (var dbConnection = new OdbcConnection(dsn))
                {
                    await dbConnection.OpenAsync(ct);

                    using (var dbCommand = dbConnection.CreateCommand())
                    {
                        dbCommand.CommandTimeout = 300;

                        dbCommand.CommandText = GetValueTimestampCachingSqlCmd(udc, ParentCvsService);
                        var dbReader = await dbCommand.ExecuteReaderAsync(ct);

                        var newAttrCache = new Dictionary<string, DateTime?>();
                        while (dbReader.Read())
                        {
                            ct.ThrowIfCancellationRequested();
                            var facId = Convert.IsDBNull(dbReader["FacilityId"])
                                ? string.Empty
                                : dbReader["FacilityId"].ToString();
                            if (string.IsNullOrWhiteSpace(facId))
                                continue;
                            var sqlVal = dbReader["Time"];
                            DateTime? thisTimeVal;
                            if (Convert.IsDBNull(sqlVal))
                            {
                                thisTimeVal = null;
                            }
                            else
                            {
                                thisTimeVal = DateTime.Parse(sqlVal.ToString());
                            }
                            newAttrCache[facId] = thisTimeVal;
                        }
                        return newAttrCache;
                    }
                }
            }
            catch (TaskCanceledException)
            {
                throw;
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (OdbcException ex) when (ex.HResult == -2146232009)
            {
                return null;
            } // Odbc cancellation exception
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception caching all point fac ids for UDC {udc}", udc);
                
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error(ex, "General exception caching all point fac ids for UDC {udc}. Sync and client may fail for this udc.", udc);

                return null;
            }
        }



        private static string GetValueTimestampCachingSqlCmd(
            string udc,
            CurrentValueService cvsServ)
        {
            var sqlCvsService = $@"""{cvsServ}""";

            // Build start of query string
            var sql = $@"SELECT FacilityId, [Time] 
                            FROM {sqlCvsService}.RealtimeValues
                            WHERE UDC='{udc}';";

            return sql;
        }
    }
}
