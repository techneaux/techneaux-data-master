﻿using System;
using CygNet.Data.Core;

namespace Techneaux.CygNetWrapper.Points.PointCaching
{
    public class PointAttrIndex : IEquatable<PointAttrIndex>
    {
        public PointAttrIndex(PointTag pntTag, PointAttribute pntAttr)
        {
            PointTag = pntTag;
            PointAttr = pntAttr;
        }

        public PointTag PointTag { get; }
        public PointAttribute PointAttr { get; }


        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
                return true;

            if (obj != null && obj is PointAttrIndex compAttr)
            {
                return Equals(compAttr);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return PointTag.GetHashCode() ^ PointAttr.GetHashCode();
        }

        public override string ToString()
        {
            return $"{PointTag}-{PointAttr}";
        }

        public bool Equals(PointAttrIndex other)
        {
            if (ReferenceEquals(this, other))
                return true;

            if (other != null)
                return PointAttr == other.PointAttr
                    && PointTag == other.PointTag;
            else
                return false;
        }

        public static bool operator ==(PointAttrIndex attr1, PointAttrIndex attr2)
        {
            if (ReferenceEquals(attr1, attr2))
            {
                return true;
            }

            if (attr1 is null || attr2 is null)
            {
                return false;
            }

            return (attr1.PointAttr == attr2.PointAttr);
        }

        public static bool operator !=(PointAttrIndex attr1, PointAttrIndex attr2)
        {
            return !(attr1 == attr2);
        }
    }
}
