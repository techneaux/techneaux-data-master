﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Runtime.Caching;
using System.Threading;
using System.Threading.Tasks;
using CygNet.Data.Core;
using Serilog;
using Serilog.Context;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.PNT;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace Techneaux.CygNetWrapper.Points.PointCaching
{
    public class PointAttributeCache
    {
        private static TimeSpan RetentionTime { get; } = TimeSpan.FromMinutes(5);

        //private Dictionary<(PointAttribute attr, string udc), (DateTime dateAdded, Dictionary<string, PointAttributeValue> attrVals)> _cachedAttributeVals =
        //    new Dictionary<(PointAttribute attr, string udc), (DateTime dateAdded, Dictionary<string, PointAttributeValue> attrVals)>();

        //private Dictionary<PointTag, (DateTime dateAdded, Dictionary<PointAttribute, PointAttributeValue> attrVals)> _fullyCachedAttributes =
        //    new Dictionary<PointTag, (DateTime dateAdded, Dictionary<PointAttribute, PointAttributeValue> attrVals)>();

        private readonly ExpiringCache<(PointAttribute attr, string udc), Dictionary<string, PointAttributeValue>> _cachedAttributeVals;
        private readonly ExpiringCache<string, Dictionary<PointAttribute, PointAttributeValue>> _fullyCachedAttributes;

        private CurrentValueService ParentCvsService { get; }

        public PointAttributeCache(CurrentValueService srcCvs)
        {
            ParentCvsService = srcCvs;

            _cachedAttributeVals = new ExpiringCache<(PointAttribute attr, string udc), Dictionary<string, PointAttributeValue>>
                (CacheAttributeValueAllPointsWithUdc, TimeSpan.FromMinutes(5));

            _fullyCachedAttributes = new ExpiringCache<string, Dictionary<PointAttribute, PointAttributeValue>>
                (CacheAllPointAttributes, TimeSpan.FromMinutes(5));
        }

        //private bool TryGetFullyCachedAttrValues(
        //    PointTag pntTag,
        //    out Dictionary<PointAttribute, PointAttributeValue> cachedAttrVals)
        //{
        //    cachedAttrVals = null;
        //    if (!_fullyCachedAttributes.TryGetValue(pntTag, out var cachedAttrEntry)) return false;

        //    if (DateTime.Now - cachedAttrEntry.dateAdded <= RetentionTime)
        //    {
        //        cachedAttrVals = cachedAttrEntry.attrVals;
        //        return true;
        //    }

        //    _fullyCachedAttributes.Remove(pntTag);

        //    return false;
        //}

        //private bool TryGetAttributeValuesForTag(
        //    PointAttribute attrId,
        //    string udc,
        //    out Dictionary<string, PointAttributeValue> attrVals)
        //{
        //    attrVals = null;
        //    if (_cachedAttributeVals.TryGetValue((attrId, udc), out var cachedTagEntry)) return false;

        //    if (DateTime.Now - cachedTagEntry.dateAdded <= RetentionTime)
        //    {
        //        attrVals = cachedTagEntry.attrVals;
        //        return true;
        //    }

        //    _cachedAttributeVals.Remove((attrId, udc));

        //    return false;
        //}


        public async Task<List<PointAttributeValue>> GetAllAttributeValuesForPoint(
            PointTag srcTag,
            CancellationToken ct,
            bool forceRefresh = false)
        {
            try
            {
                // Check if fac service is correct
                if (srcTag.SiteService.ToString() != ParentCvsService.SiteService.ToString())
                    throw new InvalidOperationException($"Facility tag site.service [{srcTag.SiteService}] does not match the current value service [{ParentCvsService.SiteService}] in the cache");

                var allAttrVals = await _fullyCachedAttributes.GetCachedItem(srcTag.PointId, ct);

                var tagVals = PointAttribute.GetTagAttributes(srcTag);
                foreach (var item in tagVals)
                {
                    allAttrVals.Add(item.Key, item.Value);
                }

                return allAttrVals.Values.ToList();
            }
            catch (OperationCanceledException)
            {
                return null;
                // Cancellations ignored
            }
        }

        public async Task<PointAttributeValue> GetAttributeValueForPoint(
            PointTag srcTag,
            PointAttribute attrId,
            CancellationToken ct,
            bool forceRefresh = false)
        {
            try
            {
                // Check if fac service is correct
                if (srcTag.SiteService.ToString() != ParentCvsService.SiteService.ToString())
                    throw new InvalidOperationException($"Facility tag site.service [{srcTag.SiteService}] does not match the current value service [{ParentCvsService.SiteService}] in the cache");

                if (attrId.Category == PointAttribute.Categories.Tag)
                {
                    var tagVals = PointAttribute.GetTagAttributes(srcTag);

                    return tagVals[attrId];
                }

                var attrDict = await _cachedAttributeVals.GetCachedItem((attrId, srcTag.UDC), ct);

                if (attrDict.TryGetValue(srcTag.PointId, out var attrVal))
                {
                    return attrVal;
                }

                return null;

            }
            catch (OperationCanceledException)
            {
                return null;
                // Cancellations ignored
            }
        }

        private async Task<Dictionary<string, PointAttributeValue>> CacheAttributeValueAllPointsWithUdc(
            (PointAttribute attr, string udc) opts,
            CancellationToken ct)
        {
            if (!CygNetPoint.IsUdcValid(opts.udc))
                throw new ArgumentException(nameof(opts.udc));

            try
            {
                var myPointService = await ParentCvsService.GetAssociatedPnt();

                var dsn = "DSN=CygNet;ServiceFilter=" + myPointService.SiteService;

                using (var dbConnection = new OdbcConnection(dsn))
                {
                    await dbConnection.OpenAsync(ct);

                    using (var dbCommand = dbConnection.CreateCommand())
                    {
                        dbCommand.CommandTimeout = 300;

                        dbCommand.CommandText = GetAttrCachingSqlCmd(opts.attr, opts.udc, myPointService);
                        var dbReader = await dbCommand.ExecuteReaderAsync(ct);

                        var newAttrCache = new Dictionary<string, PointAttributeValue>();


                        while (dbReader.Read())
                        {
                            ct.ThrowIfCancellationRequested();

                            var pointId = Convert.IsDBNull(dbReader["pointid"])
                                ? string.Empty
                                : dbReader["pointid"].ToString();

                            if (string.IsNullOrWhiteSpace(pointId))
                                continue;


                            var sqlVal = dbReader[opts.attr.SqlColumnName];
                            PointAttributeValue thisAttrVal;

                            if (Convert.IsDBNull(sqlVal))
                            {
                                thisAttrVal = new PointAttributeValue(opts.attr, null);
                            }
                            else
                            {
                                thisAttrVal = PointAttribute.ConvertSqlToStandardValue(opts.attr.SqlColumnName, sqlVal);
                            }

                            newAttrCache.Add(pointId, thisAttrVal);

                        }

                        return newAttrCache;
                    }
                }
            }
            catch (TaskCanceledException)
            {
                throw;
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (OdbcException ex) when (ex.HResult == -2146232009)
            {
                return null;
            } // Odbc cancellation exception
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception while exception caching point attributes");

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error(ex, "Failed to cache point attributes. Sync will not proceed as expected.");

                return null;
            }
        }

        private async Task<Dictionary<PointAttribute, PointAttributeValue>> CacheAllPointAttributes(
            string pointId,
            CancellationToken ct)
        {
            try
            {
                var myPointService = await ParentCvsService.GetAssociatedPnt();

                var dsn = "DSN=CygNet;ServiceFilter=" + myPointService.SiteService;

                using (var dbConnection = new OdbcConnection(dsn))
                {
                    await dbConnection.OpenAsync(ct);

                    using (var dbCommand = dbConnection.CreateCommand())
                    {
                        dbCommand.CommandTimeout = 300;

                        dbCommand.CommandText = GetAllAttrCachingSqlCmd(pointId, myPointService);
                        var dbReader = await dbCommand.ExecuteReaderAsync(ct);

                        var newAttrCache = new Dictionary<PointAttribute, PointAttributeValue>();
                        var validAttrDefs = PointAttribute.AllPointAttributes.Values
                            .Where(attr => attr.Category != PointAttribute.Categories.Tag).ToList();

                        while (dbReader.Read())
                        {
                            ct.ThrowIfCancellationRequested();

                            foreach (var attr in validAttrDefs)
                            {
                                var sqlVal = dbReader[attr.SqlColumnName];
                                PointAttributeValue thisAttrVal;

                                if (Convert.IsDBNull(sqlVal))
                                {
                                    thisAttrVal = new PointAttributeValue(attr, null);
                                }
                                else
                                {
                                    thisAttrVal = PointAttribute.ConvertSqlToStandardValue(attr.SqlColumnName, sqlVal);
                                }

                                newAttrCache.Add(attr, thisAttrVal);
                            }

                        }

                        return newAttrCache;
                    }
                }
            }
            catch (TaskCanceledException)
            {
                throw;
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (OdbcException ex) when (ex.HResult == -2146232009)
            {
                return new Dictionary<PointAttribute, PointAttributeValue>();
            } // Odbc cancellation exception
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception caching point attributes");

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error(ex, "Failed to cache point attributes. Service Sync and Client may not work properly");

                return new Dictionary<PointAttribute, PointAttributeValue>();
            }
        }

        private static string GetAllAttrCachingSqlCmd(
            string pntId,
            PointService pntServ)
        {
            var sqlPntService = $@"""{pntServ}""";  //pntServ.ToString().Replace('.', '_');


            var validAttrDefs = PointAttribute.AllPointAttributes.Values.Where(attr => attr.Category != PointAttribute.Categories.Tag);

            // Build start of query string
            var sqlAttrIds = validAttrDefs.Select(attr => attr.SqlColumnName);
            var sql = $@"SELECT {string.Join(",", sqlAttrIds)} 
                            FROM {sqlPntService}.pnt_header_record 
                            WHERE pointid='{pntId}';";

            return sql;
        }

        private static string GetAttrCachingSqlCmd(
            PointAttribute pntAttr,
            string udc,
            PointService pntServ)
        {
            var sqlPntService = $@"""{pntServ}""";  //pntServ.ToString().Replace('.', '_');

            // Build start of query string
            var sql = $@"SELECT pointid, {pntAttr.SqlColumnName} 
                            FROM {sqlPntService}.pnt_header_record 
                            WHERE uniformdatacode='{udc}';";

            return sql;
        }

        //private async Task CachePointAttributesUpdate(CancellationToken ct)
        //{
        //    foreach (var myPointService in _includedPointServices)
        //    {
        //        try
        //        {
        //            var dsn = "DSN=CygNet;ServiceFilter=" + myPointService.SiteService;
        //            using (var dbConnection = new OdbcConnection(dsn))
        //            {
        //                await dbConnection.OpenAsync(ct);

        //                using (var dbCommand = dbConnection.CreateCommand())
        //                {
        //                    dbCommand.CommandTimeout = 300;

        //                    FacilityTag filterFacTag = null;

        //                    dbCommand.CommandText = GetAttrCachingSqlCmd(filterFacTag, myPointService.SiteService,
        //                        udcToCache, attrsToCache);
        //                    var dbReader = await dbCommand.ExecuteReaderAsync(ct);

        //                    _attributeUpdateLock.Reset();

        //                    while (dbReader.Read())
        //                    {
        //                        ct.ThrowIfCancellationRequested();

        //                        var site = Convert.IsDBNull(dbReader["site"])
        //                            ? string.Empty
        //                            : dbReader["site"].ToString();
        //                        var service = Convert.IsDBNull(dbReader["service"])
        //                            ? string.Empty
        //                            : dbReader["service"].ToString();
        //                        var facId = Convert.IsDBNull(dbReader["facilityid"])
        //                            ? string.Empty
        //                            : dbReader["facilityid"].ToString();
        //                        var udc = Convert.IsDBNull(dbReader["uniformdatacode"])
        //                            ? string.Empty
        //                            : dbReader["uniformdatacode"].ToString();
        //                        var pointId = Convert.IsDBNull(dbReader["pointid"])
        //                            ? string.Empty
        //                            : dbReader["pointid"].ToString();
        //                        var pointIdLong = Convert.IsDBNull(dbReader["pointidlong"])
        //                            ? string.Empty
        //                            : dbReader["pointidlong"].ToString();

        //                        if (string.IsNullOrEmpty(site) ||
        //                            string.IsNullOrEmpty(service) ||
        //                            string.IsNullOrEmpty(facId) ||
        //                            string.IsNullOrEmpty(udc))
        //                        {
        //                            continue;
        //                        }

        //                        var thisSiteServ = new SiteService(site, service);
        //                        var thisTag = new PointTag(thisSiteServ, pointId, pointIdLong, facId, udc);
        //                        var facTag = thisTag.GetFacilityTag();

        //                        // Try to get cached fac
        //                        if (_registeredFacs.TryGetValue(facTag, out var thisFac))
        //                        {
        //                            // Get cached UDC
        //                            if (thisFac.TryGetCachedUdc(udcToCache, out var thisCachedUdc) &&
        //                                thisCachedUdc.Tag == thisTag)
        //                            {
        //                                // Must match udc and also tag, in case of duplicate points with same udc

        //                                foreach (var attr in attrsToCache)
        //                                {
        //                                    var sqlVal = dbReader[attr.SqlColumnName];
        //                                    PointAttributeValue thisAttrVal;

        //                                    if (Convert.IsDBNull(sqlVal))
        //                                    {
        //                                        thisAttrVal = new PointAttributeValue(attr, null);
        //                                    }
        //                                    else
        //                                    {
        //                                        thisAttrVal =
        //                                            PointAttribute.ConvertSqlToStandardValue(attr.SqlColumnName,
        //                                                sqlVal);
        //                                    }

        //                                    thisCachedUdc.SetAttributeValue(attr.Id, thisAttrVal);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        catch (TaskCanceledException)
        //        {
        //        }
        //        catch (OperationCanceledException)
        //        {
        //            throw;
        //        }
        //        catch (OdbcException ex) when (ex.HResult == -2146232009)
        //        {
        //        } // Odbc cancellation exception
        //        catch (Exception)
        //        {
        //            // General exception
        //        }
        //        finally
        //        {
        //            _attributeUpdateLock.Set();
        //        }
        //    }
        //}

        //private string GetAttrUpdateCachingSqlCmd(
        //    SiteService pntSiteServ,
        //    IEnumerable<PointAttribute> attrsToCache)
        //{
        //    var sqlAttrIds = attrsToCache.Select(attr => attr.SqlColumnName);
        //    var pntService = pntSiteServ.ToString().Replace('.', '_');

        //    var sql = $@"SELECT site, service, facilityid, uniformdatacode, pointid, pointidlong, {string.Join(",", sqlAttrIds)} 
        //                    FROM {pntService}.pnt_header_record 
        //                    WHERE ";

        //    var sqlConditions = new List<string>();

        //    if (_includedCvsServices.Any())
        //    {
        //        var sites = _includedCvsServices.Select(cvs => $"'{cvs.SiteService.Site}'");
        //        var services = _includedCvsServices.Select(cvs => $"'{cvs.SiteService.Service}'");

        //        sqlConditions.Add($"site IN ({string.Join(",", sites)})");
        //        sqlConditions.Add($"service IN ({string.Join(",", services)})");
        //    }



        //    sql += string.Join(" AND ", sqlConditions);

        //    sql += ";";

        //    return sql;
        //}






    }
}
