﻿using CygNet.Data.Core;
using System;
using TechneauxUtility;

namespace Techneaux.CygNetWrapper.Points
{
    public class PointValue
    {
        public PointValue(PointValueRecord newRecord)
        {
            Value = new ConvertibleValue(newRecord.Value);
            AlternateValue = new ConvertibleValue(newRecord.AlternateValue);

            Timestamp = newRecord.Timestamp;
            PointStatus = newRecord.PointStatus;
            Status = newRecord.Status;
            UserStatus = newRecord.UserStatus;
        }

        public ConvertibleValue Value { get; }
        public ConvertibleValue AlternateValue { get; }
        public DateTime Timestamp { get; }
        public PointStatus PointStatus { get; }
        public BaseStatusFlags Status { get; }
        public UserStatusFlags UserStatus { get; }
    }
}
