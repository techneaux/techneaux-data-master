﻿using CygNet.Data.Facilities;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog.Context;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace Techneaux.CygNetWrapper.Services.FAC
{
    public partial class FacilityService : CygNetService
    {
        Dictionary<string, AttributeDefinition> _attributeDefinitionsCached;

        public bool HasAttributeData => _attributeDefinitionsCached != null;

        public Dictionary<string, AttributeDefinition> AttributeDefinitions
        {
            get
            {
                if (HasAttributeData)
                {
                    return _attributeDefinitionsCached;
                }

                throw new InvalidOperationException("No attribute information was retrieved for this service");
            }
        }

        public bool IsValidAttributeName(string attrName) => AttributeDefinitions.ContainsKey(attrName);

        public Dictionary<string, string> AttributeNames(bool refresh = false) => AttributeDefinitions
            .ToDictionary(attrDef => attrDef.Key, attrDef => attrDef.Value.Description);

        public Dictionary<string, string> EnabledAttributeNames => AttributeDefinitions
                .ToDictionary(attrDef => attrDef.Key, attrDef => attrDef.Value.Description);

        public bool IsValidActiveAttributeName(string attrName) => AttributeDefinitions.ContainsKey(attrName.ToLower());

        public Dictionary<string, AttributeDefinition> EnabledAttributes => AttributeDefinitions.
                Where(attrItem => attrItem.Value.InUse).
                ToDictionary(attrItem => attrItem.Key, attrItem => attrItem.Value);

        private readonly SemaphoreSlim _attrSemaphore = new SemaphoreSlim(1, 1);

        public async Task<Dictionary<string, AttributeDefinition>> GetAttributeDefinitions(bool refresh)
        {
            await RefreshFacServiceAttributesAsync();

            return _attributeDefinitionsCached;
        }

        private DateTime _lastAttrRefresh = DateTime.MinValue;

        public async Task<bool> RefreshFacServiceAttributesAsync(TimeSpan refreshInterval)
        {
            var mustRefresh = (DateTime.Now - _lastAttrRefresh) > refreshInterval;

            if (mustRefresh)
            {
                _lastAttrRefresh = DateTime.Now;
            }

            return await RefreshFacServiceAttributesAsync(mustRefresh);
        }

        public async Task<bool> RefreshFacServiceAttributesAsync(bool refresh = false)
        {
            await _attrSemaphore.WaitAsync();

            try
            {
                if (_attributeDefinitionsCached == null || refresh)
                {
                    var facAttrs = ServiceClient.GetAttributeDefinitions().ToList();

                    _attributeDefinitionsCached = new Dictionary<string, AttributeDefinition>();

                    if (facAttrs.Any())
                    {
                        foreach (var attrDef in facAttrs)
                        {
                            _attributeDefinitionsCached.Add(attrDef.ColumnName, attrDef);

                            if (attrDef.ColumnName.ToLower().StartsWith("facility_table"))
                            {
                                var newAttrDef = attrDef.DeepCopy();
                                newAttrDef.ColumnName = $"{newAttrDef.ColumnName}_desc";
                                newAttrDef.Description = $"{newAttrDef.Description} (Description)";
                                _attributeDefinitionsCached.Add(newAttrDef.ColumnName, newAttrDef);
                            }

                            if (attrDef.ColumnName == "facility_type")
                            {
                                var newAttrDef = attrDef.DeepCopy();
                                newAttrDef.ColumnName = $"{newAttrDef.ColumnName}_desc";
                                newAttrDef.Description = $"{newAttrDef.Description} (Description)";
                                _attributeDefinitionsCached.Add(newAttrDef.ColumnName, newAttrDef);
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, $"Failed to read facility attributes from {DomainSiteService}");
                

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error(ex, $"Failed to read facility attributes from {DomainSiteService}");

                return false;
            } // Error handling
            finally
            {
                _attrSemaphore.Release();
            }
        }




    }
}
