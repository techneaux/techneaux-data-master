﻿using CygNet.Data.Core;
using CygNet.Data.Facilities;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Odbc;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Facilities.Attributes;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using Techneaux.CygNetWrapper.Points;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace Techneaux.CygNetWrapper.Services.FAC
{
    internal class CachedFacilityOdbcQuery
    {
        private readonly FacilityService _srcFacService;
        private readonly Dictionary<string, AttributeDefinition> _srcValidAttributes;
        private readonly Dictionary<SiteService, CurrentValueService> _srcLinkedCvsServices;

        // Query results
        public bool IsQuerySuccessful { get; private set; }

        public Dictionary<FacilityTag, SimpleCygnetFacility> AllSimpleFacs { get; private set; }
        public List<CachedCygNetFacility> BaseFacs { get; private set; }
        public List<CachedCygNetFacility> ParentFacs { get; private set; }
        public List<CachedCygNetFacility> ChildFacs { get; private set; }

        // Constructor
        public CachedFacilityOdbcQuery(
            FacilityService srcFacService,
            Dictionary<string, AttributeDefinition> srcValidAttributes,
            Dictionary<SiteService, CurrentValueService> srcLinkedCvsServices)
        {
            _srcFacService = srcFacService;
            _srcValidAttributes = srcValidAttributes;
            _srcLinkedCvsServices = srcLinkedCvsServices;

            ResetQueryResults();
        }

        public void ResetQueryResults()
        {
            IsQuerySuccessful = false;

            AllSimpleFacs = new Dictionary<FacilityTag, SimpleCygnetFacility>();
            BaseFacs = new List<CachedCygNetFacility>();
            ParentFacs = new List<CachedCygNetFacility>();
            ChildFacs = new List<CachedCygNetFacility>();
        }

        public List<IFacilityFilterCondition> FacilityFilterConditions { get; private set; } = new List<IFacilityFilterCondition>();
        public void SetFacilityConditions(List<IFacilityFilterCondition> conds)
        {
            FacilityFilterConditions = conds.IsAny() ? conds : new List<IFacilityFilterCondition>();
        }

        public string FacilityLinkingAttributeName { get; private set; }
        public void SetLinkingAttr(string linkingAttrName)
        {
            FacilityLinkingAttributeName = linkingAttrName;
        }

        public List<string> ListOfAttributesToPreCache { get; private set; } = new List<string>();
        public void SetListOfAttributesToPreCache(List<string> preCachedAttrList)
        {
            if (preCachedAttrList.IsAny())
            {
                ListOfAttributesToPreCache = preCachedAttrList;
            }
            else
            {
                ListOfAttributesToPreCache = new List<string>();
            }
        }

        public void SetListOfAttributesToPreCache(List<AttributeDefinition> preCachedAttrList)
        {
            if (preCachedAttrList.IsAny())
            {
                SetListOfAttributesToPreCache(preCachedAttrList.Select(attrDef => attrDef.ColumnName).ToList());
            }
        }

        public bool IsValidActiveAttributeName(string attrNameStr)
        {
            return _srcValidAttributes.ContainsKey(attrNameStr);
        }

        public async Task QueryFacilities(
          CancellationToken ct)
        {
            ResetQueryResults();
        }


        private async Task<List<CachedCygNetFacility>> GetCachedFacilitiesWithChildrenAndParents(
            IEnumerable<IFacilityFilterCondition> baseRules,
            string linkingAttribute,
            CancellationToken ct)
        {
            try
            {
                var ws = new Stopwatch();
                ws.Start();

                var facilityFilterConditions = baseRules.ToList();
                Log.Information("Base params {@BaseRules}", facilityFilterConditions);

                // Check if there are valid filters available
                var validBaseFilters = GetValidFacilityFilters(facilityFilterConditions);
                if (!validBaseFilters.Any())
                {
                    return new List<CachedCygNetFacility>();
                }

                AllSimpleFacs = await GetAllSimpleFacilitiesAsync(
                    _srcLinkedCvsServices.Keys.ToList(),
                    facilityFilterConditions.ToList(),
                    linkingAttribute, ct);

                Log.Information($"After query {ws.Elapsed.TotalSeconds}");
                ws.Restart();

                var baseFacsSimple = AllSimpleFacs.Values.Where(fac => FacilityFilterConditionBase.DoAttributesMatchFilters(fac, validBaseFilters)).ToList();


                // Check if there are valid filters available
                var validChildFilters = new List<IFacilityFilterCondition>(); // GetValidFacilityFilters(ChildRules);

                var getChildFacs = (!string.IsNullOrWhiteSpace(linkingAttribute)
                    && IsValidActiveAttributeName(linkingAttribute));

                var allCachedFacs =
                    new Dictionary<CachedCygNetFacility, Dictionary<string, FacilityAttributeValue>>();

                Dictionary<string, List<SimpleCygnetFacility>> refDict = null;

                if (getChildFacs)
                    refDict = SimpleCygnetFacility.BuildReferenceDictionary(AllSimpleFacs.Values, linkingAttribute);

                Log.Information($"After dictionary build {ws.Elapsed.TotalSeconds}");
                ws.Restart();

                foreach (var baseFacSimple in baseFacsSimple)
                {
                    //Log.Information($"Base fac = {BaseFacSimple.Tag.ToString()}, {BaseFacSimple.AttributeValues["facility_desc"]}");

                    var thisCachedBaseFac = new CachedCygNetFacility(_srcFacService, _srcLinkedCvsServices[baseFacSimple.Tag.SiteService], baseFacSimple.Tag);
                    allCachedFacs[thisCachedBaseFac] = baseFacSimple.FullAttributeValues;
                    BaseFacs.Add(thisCachedBaseFac);

                    // Get parent facilities
                    if (getChildFacs)
                    {
                        CachedCygNetFacility thisCachedParentFac = null;
                        var theseChildFacs = new List<ICachedCygNetFacility>();

                        if (baseFacSimple.TryGetParentFac(AllSimpleFacs, linkingAttribute, out var simpleParentFac))
                        {
                            thisCachedParentFac = new CachedCygNetFacility(
                                _srcFacService,
                                _srcLinkedCvsServices[simpleParentFac.Tag.SiteService],
                                simpleParentFac.Tag);

                            allCachedFacs[thisCachedParentFac] = simpleParentFac.FullAttributeValues;

                            ParentFacs.Add(thisCachedParentFac);
                        }

                        var childFacsSimple = baseFacSimple.FindChildren(refDict, linkingAttribute, validChildFilters);

                        foreach (var simpleChildFac in childFacsSimple)
                        {
                            var thisCachedChildFac = new CachedCygNetFacility(
                                _srcFacService,
                                _srcLinkedCvsServices[simpleChildFac.Tag.SiteService],
                                simpleChildFac.Tag);

                            allCachedFacs[thisCachedChildFac] = simpleChildFac.FullAttributeValues;
                            theseChildFacs.Add(thisCachedChildFac);
                            ChildFacs.Add(thisCachedChildFac);
                        }

                        thisCachedBaseFac.SetLinkedFacilities(linkingAttribute, thisCachedParentFac, theseChildFacs);
                    }

                    ct.ThrowIfCancellationRequested();
                }

                Log.Information($"After finding parents and children {ws.Elapsed.TotalSeconds}");
                ws.Restart();

                var basePointCache = new PointAttributeCachingService(BaseFacs);
                var parentPointCache = new PointAttributeCachingService(ParentFacs);
               // var childPointCache = new PointAttributeCachingService(ChildFacs);

               //.RegisterFacilities(allCachedFacs);

                Log.Information($"After linking points to cache {ws.Elapsed.TotalSeconds}");
                ws.Stop();

                return BaseFacs;
            }
            catch (OperationCanceledException ex)
            {
                Log.Information($"{nameof(GetCachedFacilitiesWithChildrenAndParents)} - Cancelled");
                throw;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "General exception getting cached facilities");
                GeneralExceptionLogger.Here().Error(ex, "General exception getting cached facilities");
                throw;
            }
        }

        private DateTime _lastSimpleFacQuery = DateTime.MinValue;
        private Dictionary<FacilityTag, SimpleCygnetFacility> _allSimpleFacsSaved;

        private async Task<Dictionary<FacilityTag, SimpleCygnetFacility>> GetAllSimpleFacilitiesAsync(
            List<SiteService> allowedCvsServs,
            List<IFacilityFilterCondition> baseConditions,
            string childAttrId,
            CancellationToken ct)
        {
            if ((DateTime.Now - _lastSimpleFacQuery).TotalMinutes < 10 &&
                _allSimpleFacsSaved != null)
            {
                return _allSimpleFacsSaved;
            }

            var facsFound = new Dictionary<FacilityTag, SimpleCygnetFacility>();

            try
            {
                var dsn = "DSN=CygNet;ServiceFilter=" + _srcFacService.SiteService;

                using (var dbConnection = new OdbcConnection(dsn))
                {
                    await dbConnection.OpenAsync(ct);

                    using (var dbCommand = dbConnection.CreateCommand())
                    {
                        dbCommand.CommandTimeout = 300;

                        var sqlQuery = GetFastSqlCmdForSimpleFacilities(baseConditions, childAttrId);
                        dbCommand.CommandText = sqlQuery;

                        var dbReader = await dbCommand.ExecuteReaderAsync(ct);

                        // Get columns which are attribute values
                        var sqlAttributesInUse = ODBC.DataReaderTools.GetEnabledAttributesFromDbReader(dbReader, _srcValidAttributes);
                        var relatedTableDescAttrDefs = sqlAttributesInUse
                            .Where(attr => attr.AttributeType() == FacilityAttributeTypes.Table)
                            .ToDictionary(attr => attr, attr => _srcValidAttributes[attr.ColumnName + "_desc"]);

                        while (dbReader.Read())
                        {
                            ct.ThrowIfCancellationRequested();

                            var thisSimpleFac = GetSimpleFacilityFromRow(dbReader, sqlAttributesInUse, relatedTableDescAttrDefs, ct);

                            if (allowedCvsServs.Contains(thisSimpleFac.Tag.SiteService))
                                facsFound[thisSimpleFac.Tag] = thisSimpleFac;
                        }

                        _lastSimpleFacQuery = DateTime.Now;

                        if (!sqlQuery.Contains("WHERE"))
                            _allSimpleFacsSaved = facsFound;

                        return facsFound;
                    }
                }
            }
            catch (OperationCanceledException) { throw; }
            catch (OdbcException ex) when (ex.HResult == -2146232009) { } // Odbc cancellation exception
            catch (Exception ex)
            {
                Log.Error(ex, "General exception getting simple facs from ODBC");
                GeneralExceptionLogger.Here().Error(ex, "General exception getting simple facs from ODBC");
            }

            return new Dictionary<FacilityTag, SimpleCygnetFacility>();
        }

        private SimpleCygnetFacility GetSimpleFacilityFromRow(
           DbDataReader dbReader,
           List<AttributeDefinition> sqlAttrsInUse,
           Dictionary<AttributeDefinition, AttributeDefinition> relatedTableDescAttrs,
           CancellationToken ct)
        {
            // Attribute col names list must include at least site, service, and id
            Debug.Assert(sqlAttrsInUse.Any(attr => attr.ColumnName == "facility_site"));
            Debug.Assert(sqlAttrsInUse.Any(attr => attr.ColumnName == "facility_service"));
            Debug.Assert(sqlAttrsInUse.Any(attr => attr.ColumnName == "facility_id"));

            // Build facility tag
            var facTag = new FacilityTag(
                dbReader["facility_site"] as string,
                dbReader["facility_service"] as string,
                dbReader["facility_id"] as string);

            // Create new facility
            var newFac = new SimpleCygnetFacility(facTag);

            // Cache facility attributes
            foreach (var attr in sqlAttrsInUse)
            {
                var attrId = attr.ColumnName;

                ct.ThrowIfCancellationRequested();

                var thisCellValue = dbReader[attrId];
                if (Convert.IsDBNull(thisCellValue))
                {
                    newFac.AddAttributeValue(new FacilityAttributeValue(attr, null));
                }
                else
                {
                    newFac.AddAttributeValue(new FacilityAttributeValue(attr, Convert.ToString(thisCellValue ?? "")));
                }

                if (attr.AttributeType() == FacilityAttributeTypes.Table)
                {
                    var tableDescAttr = relatedTableDescAttrs[attr];

                    int index = Convert.ToInt16(attrId.Replace("facility_table", ""));
                    var attrValueDesc = _srcFacService.GetTableAttrValueDesc(index, thisCellValue.ToString());

                    newFac.AddAttributeValue(new FacilityAttributeValue(tableDescAttr, attrValueDesc));
                }
            }

            return newFac;
        }

        private List<IFacilityFilterCondition> GetValidFacilityFilters(IEnumerable<IFacilityFilterCondition> facFilters)
        {
            var validFilters = facFilters.Where(filter => _srcValidAttributes.ContainsKey(filter.AttributeId));

            return validFilters.ToList();
        }

        private string GetFastSqlCmdForSimpleFacilities(List<IFacilityFilterCondition> baseFacFilters, string childAttrId)
        {
            var attrNameWithoutTableDescAttrs = _srcValidAttributes.Keys
              .Where(attr => !(attr.StartsWith("facility_table")
              && attr.EndsWith("_desc")));

            var attrNameOnlyTableDescAttrs = _srcValidAttributes.Keys
                .Where(attr => (attr.StartsWith("facility_table")
                && attr.EndsWith("_desc")));

            var facService = _srcFacService.SiteService.ToString().Replace(".", "_");

            var nameWithoutTableDescAttrs = attrNameWithoutTableDescAttrs.ToList();
            var sqlCommand = $@"SELECT {string.Join(",", nameWithoutTableDescAttrs)} FROM {facService}.fac_header";

            // Handle ref facs           
            if (nameWithoutTableDescAttrs.Contains(childAttrId))
            {
                //SqlCommand += $" OR {childAttrId} <> ''";
            }
            else
            {
                var validfacFilters = GetValidFacilityFilters(baseFacFilters);
                var sqlConditions = ODBC.QueryBuilding.BuildSqlConditionList(validfacFilters);

                sqlCommand += $" WHERE ({string.Join(" AND ", sqlConditions)})";
            }

            sqlCommand += ";";

            return sqlCommand;
        }
    }
}

