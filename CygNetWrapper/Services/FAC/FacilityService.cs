﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Odbc;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CygNet.Data.Core;
using CygNet.Data.Facilities;
using CygNet.API.Core;
using CygNet.API.Facilities;
using System.Diagnostics;
using System.Collections.Concurrent;
using Serilog;
using Serilog.Context;
using Techneaux.CygNetWrapper.Services.PNT;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Facilities.Attributes;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using static XmlDataModelUtility.GlobalLoggers;

namespace Techneaux.CygNetWrapper.Services.FAC
{
    public partial class FacilityService : CygNetService
    {
        const ServiceType ThisServiceType = ServiceType.FAC;

        private Client _serviceClient;
        public Client ServiceClient
        {
            get
            {
                if (_serviceClient == null)
                {
                    try
                    {
                        _serviceClient = new Client(DomainSiteService);
                    }
                    catch (Exception ex)
                    {
                        using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                            Log.Error(ex, $"Failed to create service client for {DomainSiteService}. This is usually due to security or connection issues. All operations using this service will fail.");
                    }
                }
                return _serviceClient;
            }
        }

        public override bool IsServiceAvailable => ServiceClient != null;

        public FacilityService(CygNetDomain parentDomain, ServiceDefinition facServiceDef)
            : base(parentDomain, facServiceDef)
        {
            if (ServiceDefinition == null)
                throw new ArgumentException("Service cannot be null");
            if (ServiceDefinition.ServiceType != ThisServiceType)
                throw new ArgumentException("Service is wrong type");

            AttrCache = new FacilityAttributeCache(this);

            //Task.Run(() => ReadFacServiceAttributesAsync());
        }

        public FacilityService(CygNetDomain parentDomain, DomainSiteService facServiceDef)
            : base(parentDomain, facServiceDef, ServiceType.FAC)
        {
            if (facServiceDef == null)
                throw new ArgumentException("Service cannot be null");

            // CODE HERE => Check service type
            if (ServiceDefinition.ServiceType != ServiceType.FAC)
                throw new ArgumentException("Service is wrong type");

            AttrCache = new FacilityAttributeCache(this);

            //Task.Run(() => ReadFacServiceAttributesAsync());
        }

        //public CygNetService AssociatedTrsService
        //{
        //    get
        //    {
        //        return ParentDomain.GetAssociatedService(this, ServiceType.TRS);
        //    }
        //}

        //public CygNetService AssociatedAudService
        //{
        //    get
        //    {
        //        return ParentDomain.GetAssociatedService(this, ServiceType.AUD);
        //    }
        //}

        //private AsyncLazy<FacilityService> AssocFac;
        //public CygNetService AssociatedMssService
        //{
        //    get
        //    {
        //        return ParentDomain.GetAssociatedService(this, ServiceType.MSS);
        //    }
        //}

        //public CygNetService AssociatedMssService
        //{
        //    get
        //    {
        //        return ParentDomain.GetAssociatedService(this, ServiceType.MSS);
        //    }
        //}

        public (bool, string) AddOrUpdateFacilities(FacilityRecord srcRec)
        {
            try
            {
                if (srcRec == null)
                    return (false, "Null fac");
                srcRec.Category = "DDSFAC";
                srcRec.IsRefDDS = true;
                srcRec.IsRefPNT = true;
                if (ServiceClient.FacilityExists(srcRec.Tag))
                {
                    ServiceClient.UpdateFacility(srcRec);
                }
                else
                {
                    ServiceClient.AddFacility(srcRec);
                }

                return (true, "");
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Error adding facility. This code is untested and not yet supported.");

                return (false, ex.Message);
            }

        }

        public async Task<(bool, string)> AddOrUpdateFacilities(IEnumerable<FacilityRecord> srcRec)
        {
            try
            {
                var errorMessage = "";
                foreach (var facilityRecord in srcRec)
                {
                    var result = AddOrUpdateFacilities(facilityRecord);
                    if (!result.Item1)
                    {
                        errorMessage = result.Item2;
                        return (false, errorMessage);
                    }
                }
                return (true, "");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return (false, e.Message);
            }

        }
        public async Task<Dictionary<SiteService, PointService>> GetAssociatedPntServices()
        {
            Dictionary<SiteService, PointService> pntServs = new Dictionary<SiteService, PointService>();

            var associatedCvsList = await GetAssociatedCvsServices();

            foreach (var cvs in associatedCvsList.Values)
            {
                var pnt = await cvs.GetAssociatedPnt();
                if (!pntServs.ContainsKey(pnt.SiteService))
                {
                    pntServs.Add(pnt.SiteService, pnt);
                }

            }
            return pntServs;

        }


        public async Task<Dictionary<SiteService, CurrentValueService>> GetAssociatedCvsServices()
        {
            return (await ParentDomain.GetAssociatedCvsServices(this)).ToDictionary(serv => serv.SiteService, serv => serv);
        }

        private readonly ConcurrentDictionary<int, Dictionary<string, string>> _cachedTables = new ConcurrentDictionary<int, Dictionary<string, string>>();
        public string GetTableAttrValueDesc(int tableAttrOrdinal, string tableAttrKey, bool refresh = false)
        {
            if (!_cachedTables.ContainsKey(tableAttrOrdinal))
            {
                var servInfo = new ServiceInformation();

                var tableName = $"SYSFCT{tableAttrOrdinal.ToString("00")}";
                var thisTable = servInfo.GetEnumerationTable(DomainSiteService, tableName);

                var dictTable = thisTable.ToDictionary(item => item.Key, item => item.Value);
                _cachedTables.TryAdd(tableAttrOrdinal, dictTable);
            }

            if (_cachedTables.TryGetValue(tableAttrOrdinal, out var thisValueTable))
            {
                if (thisValueTable.TryGetValue(tableAttrKey, out var desc))
                {
                    return desc;
                }
            }
            return null;
        }

        public Dictionary<string, string> GetFacilityTypes(bool refresh = false)
        {
            if (!_cachedTables.ContainsKey(-1))
            {
                var servInfo = new ServiceInformation();

                var tableName = "SYSFCTYP";
                var thisTable = servInfo.GetEnumerationTable(DomainSiteService, tableName);

                var dictTable = thisTable.ToDictionary(item => item.Key, item => item.Value);
                _cachedTables.TryAdd(-1, dictTable);
            }

            if (_cachedTables.TryGetValue(-1, out var thisValueTable))
            {
                return thisValueTable;
            }
            return null;
        }

        public async Task<List<CachedCygNetFacility>> GetCachedFacilitiesWithChildrenAndParents(
            IEnumerable<IFacilityFilterCondition> baseRules,
            string linkingAttribute,
            CancellationToken ct)
        {
            try
            {
                //LibraryLogging.StartTimingProcess();

                var facilityFilterConditions = baseRules.ToList(); // .Where(rule => rule.AttributeId != ).ToList();
                Log.Debug("Base params {@BaseRules}", facilityFilterConditions);

                // Check if there are valid filters available
                var validBaseFilters = GetValidFacilityFilters(facilityFilterConditions);
                if (!validBaseFilters.Any())
                {
                    return new List<CachedCygNetFacility>();

                }
                //LibraryLogging.StartTimingProcess("ODBC Query");

                var allowedCvsSiteServs = await GetAssociatedCvsServices();
                var allSimpleFacs = await GetAllSimpleFacilitiesAsync(allowedCvsSiteServs.Keys.ToList(), facilityFilterConditions.ToList(), linkingAttribute, ct);

                //AllSimpleFacs = AllSimpleFacs.Where(item)

                //LibraryLogging.EndTimingProcess("ODBC Query");

                var baseFacsSimple = allSimpleFacs.Values.Where(fac => FacilityFilterConditionBase.DoAttributesMatchFilters(fac.AttributeValues, validBaseFilters)).ToList();

                var baseFacs = new List<CachedCygNetFacility>();
                var parentFacs = new List<CachedCygNetFacility>();
                var childFacs = new List<CachedCygNetFacility>();

                // Check if there are valid filters available
                var validChildFilters = new List<IFacilityFilterCondition>(); // GetValidFacilityFilters(ChildRules);

                var getChildFacs = !string.IsNullOrWhiteSpace(linkingAttribute)
                    && IsValidActiveAttributeName(linkingAttribute);

                var allCachedFacs =
                    new Dictionary<CachedCygNetFacility, Dictionary<string, FacilityAttributeValue>>();

                Dictionary<string, List<SimpleCygnetFacility>> refDict = null;

                //LibraryLogging.StartTimingProcess("Facility Dictionary Build");
                if (getChildFacs)
                    refDict = SimpleCygnetFacility.BuildReferenceDictionary(allSimpleFacs.Values, linkingAttribute);

                //LibraryLogging.EndTimingProcess("Facility Dictionary Build");

                //LibraryLogging.StartTimingProcess("Finding parents and children");

                foreach (var baseFacSimple in baseFacsSimple)
                {
                    //Logger.LogInfo($"Base fac = {BaseFacSimple.Tag.ToString()}, {BaseFacSimple.AttributeValues["facility_desc"]}");

                    var thisCachedBaseFac = new CachedCygNetFacility(
                        this,
                        allowedCvsSiteServs[baseFacSimple.Tag.SiteService],
                        baseFacSimple.Tag);

                    allCachedFacs[thisCachedBaseFac] = baseFacSimple.FullAttributeValues;
                    baseFacs.Add(thisCachedBaseFac);

                    // Get parent facilities
                    if (getChildFacs)
                    {
                        CachedCygNetFacility thisCachedParentFac = null;
                        var theseChildFacs = new List<ICachedCygNetFacility>();

                        if (baseFacSimple.TryGetParentFac(allSimpleFacs, linkingAttribute, out var simpleParentFac))
                        {
                            thisCachedParentFac = new CachedCygNetFacility(
                                this,
                                allowedCvsSiteServs[simpleParentFac.Tag.SiteService],
                                simpleParentFac.Tag);

                            allCachedFacs[thisCachedParentFac] = simpleParentFac.FullAttributeValues;
                            parentFacs.Add(thisCachedParentFac);
                        }

                        var childFacsSimple = baseFacSimple.FindChildren(refDict, linkingAttribute, validChildFilters);

                        foreach (var simpleChildFac in childFacsSimple)
                        {
                            var thisCachedChildFac = new CachedCygNetFacility(
                                this,
                                allowedCvsSiteServs[simpleChildFac.Tag.SiteService],
                                simpleChildFac.Tag);

                            allCachedFacs[thisCachedChildFac] = simpleChildFac.FullAttributeValues;

                            theseChildFacs.Add(thisCachedChildFac);
                            childFacs.Add(thisCachedChildFac);
                        }

                        thisCachedBaseFac.SetLinkedFacilities(linkingAttribute, thisCachedParentFac, theseChildFacs);
                    }

                    ct.ThrowIfCancellationRequested();
                }

                AttrCache.RegisterFacilities(allCachedFacs);

                var baseFacsMatchingFmsConditions = new List<CachedCygNetFacility>();

                foreach (var fac in baseFacs)
                {
                    if (await fac.IsMatchingFmsFilters(facilityFilterConditions))
                        baseFacsMatchingFmsConditions.Add(fac);
                }

                return baseFacsMatchingFmsConditions;
            }
            catch (OperationCanceledException)
            {
                Log.Verbose($"{nameof(GetCachedFacilitiesWithChildrenAndParents)} - Cancelled");
                throw;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception while getting cached facilities");

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error(ex, "Failed to retrieve facilities from the facility service. Facility results in the client and syncing in the service will be unavailable until this is resolved.");

                throw;
            }
        }

        private DateTime _lastSimpleFacQuery = DateTime.MinValue;
        private Dictionary<FacilityTag, SimpleCygnetFacility> _allSimpleFacsSaved;

        public async Task<Dictionary<FacilityTag, SimpleCygnetFacility>> GetAllSimpleFacilitiesAsync(
            ICollection<SiteService> allowedCvsServs,
            List<IFacilityFilterCondition> baseConditions,
            string childAttrId,
            CancellationToken ct)
        {
            await RefreshFacServiceAttributesAsync();

            if ((DateTime.Now - _lastSimpleFacQuery).TotalMinutes < 10 &&
                _allSimpleFacsSaved != null)
            {
                Console.WriteLine("Returning cached simple facs");

                return _allSimpleFacsSaved;
            }

            var facsFound = new Dictionary<FacilityTag, SimpleCygnetFacility>();

            string sqlQuery = "";

            try
            {
                Console.WriteLine($"Starting simple fac query on thread {Thread.CurrentThread.ManagedThreadId} at {DateTime.Now}");
                var st = new Stopwatch();
                st.Start();

                var dsn = "DSN=CygNet;ServiceFilter=" + DomainSiteService.SiteService;

                using (var dbConnection = new OdbcConnection(dsn))
                {
                    await dbConnection.OpenAsync(ct);

                    using (var dbCommand = dbConnection.CreateCommand())
                    {
                        dbCommand.CommandTimeout = 300;

                        sqlQuery = GetFastSqlCmdForSimpleFacilities(baseConditions, childAttrId);
                        dbCommand.CommandText = sqlQuery;

                        var dbReader = await dbCommand.ExecuteReaderAsync(ct);

                        // Get columns which are attribute values
                        var enabledAttributes = EnabledAttributes;
                        var sqlAttributesInUse = ODBC.DataReaderTools.GetEnabledAttributesFromDbReader(dbReader, enabledAttributes);
                        var relatedTableDescAttrDefs = sqlAttributesInUse
                            .Where(attr => attr.AttributeType() == FacilityAttributeTypes.Table)
                            .ToDictionary(attr => attr, attr => enabledAttributes[attr.ColumnName + "_desc"]);

                        while (dbReader.Read())
                        {
                            ct.ThrowIfCancellationRequested();

                            var thisSimpleFac = GetSimpleFacilityFromRow(dbReader, sqlAttributesInUse, relatedTableDescAttrDefs, ct);

                            if (allowedCvsServs.Contains(thisSimpleFac.Tag.SiteService))
                                facsFound[thisSimpleFac.Tag] = thisSimpleFac;
                        }

                        _lastSimpleFacQuery = DateTime.Now;

                        if (!sqlQuery.Contains("WHERE"))
                            _allSimpleFacsSaved = facsFound;

                        st.Stop();
                        Console.WriteLine($"Ending simple fac query on thread {Thread.CurrentThread.ManagedThreadId}, duration = {st.ElapsedMilliseconds}");

                        return facsFound;
                    }
                }

            }
            catch (OperationCanceledException) { throw; }
            catch (OdbcException ex) when (ex.Message.Contains("Query is cancelled"))
            {
                Log.Debug(ex, "ODBC query cancelled");
            } // Odbc cancellation exception
            catch (Exception ex)
            {
                if (ct.IsCancellationRequested)
                {
                    Log.Debug(ex, "ODBC query cancelled");
                }
                else
                {
                    using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                        Log.Error(ex, "Unhandled exception while getting simple facility list with query: {sqlQuery}", sqlQuery);

                    using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                        Log.Error(ex, "Error collecting facility data with query: {sqlQuery}. Please check the ODBC driver, security settings, and client connection.", sqlQuery);
                }
            }

            return new Dictionary<FacilityTag, SimpleCygnetFacility>();
        }

        public async Task<(DateTime LatestDate, Dictionary<FacilityTag, SimpleCygnetFacility> FacResults)> GetSimpleFacilitiesRecentlyUpdatedAsync(
            DateTime timeRestriction,
            CancellationToken ct)
        {
            await RefreshFacServiceAttributesAsync();

            var facsFound = new Dictionary<FacilityTag, SimpleCygnetFacility>();

            try
            {
                Console.WriteLine($"Starting simple fac query on thread {Thread.CurrentThread.ManagedThreadId} at {DateTime.Now}");
                var st = new Stopwatch();
                st.Start();

                var dsn = "DSN=CygNet;ServiceFilter=" + DomainSiteService.SiteService;

                DateTime latestNewDate = default;

                using (var dbConnection = new OdbcConnection(dsn))
                {
                    await dbConnection.OpenAsync(ct);

                    using (var dbCommand = dbConnection.CreateCommand())
                    {
                        dbCommand.CommandTimeout = 300;

                        var sqlQuery = GetFastSqlCmdForSimpleFacilitiesWithTimeRestriction(timeRestriction);
                        dbCommand.CommandText = sqlQuery;

                        var dbReader = await dbCommand.ExecuteReaderAsync(ct);

                        // Get columns which are attribute values
                        var enabledAttributes = EnabledAttributes;
                        var sqlAttributesInUse = ODBC.DataReaderTools.GetEnabledAttributesFromDbReader(dbReader, enabledAttributes);
                        var relatedTableDescAttrDefs = sqlAttributesInUse
                            .Where(attr => attr.AttributeType() == FacilityAttributeTypes.Table)
                            .ToDictionary(attr => attr, attr => enabledAttributes[attr.ColumnName + "_desc"]);

                        while (dbReader.Read())
                        {
                            ct.ThrowIfCancellationRequested();

                            var thisSimpleFac = GetSimpleFacilityFromRow(dbReader, sqlAttributesInUse, relatedTableDescAttrDefs, ct);

                            var thisUpdateDate = (DateTime)dbReader["update_datetime"];

                            if (thisUpdateDate > latestNewDate)
                                latestNewDate = thisUpdateDate;

                            facsFound[thisSimpleFac.Tag] = thisSimpleFac;
                        }

                        st.Stop();
                        Console.WriteLine($"Ending simple fac query on thread {Thread.CurrentThread.ManagedThreadId}, duration = {st.ElapsedMilliseconds}");

                        return (latestNewDate, facsFound);
                    }
                }


            }
            catch (OperationCanceledException) { throw; }
            catch (OdbcException ex) when (ex.Message.Contains("Query is cancelled"))
            {
                Log.Debug(ex, "ODBC Operation cancelled");
            } // Odbc cancellation exception
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception while collecting latest updated simple facility list");

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error(ex, "Failed to collect information about recently created or updated facilities. New facility data is missing and will NOT be included in the service sync or the client");
            }

            return (default, new Dictionary<FacilityTag, SimpleCygnetFacility>());
        }




        private SimpleCygnetFacility GetSimpleFacilityFromRow(
           DbDataReader dbReader,
           List<AttributeDefinition> sqlAttrsInUse,
           Dictionary<AttributeDefinition, AttributeDefinition> relatedTableDescAttrs,
           CancellationToken ct)
        {
            // Attribute col names list must include at least site, service, and id
            Debug.Assert(sqlAttrsInUse.Any(attr => attr.ColumnName == "facility_site"));
            Debug.Assert(sqlAttrsInUse.Any(attr => attr.ColumnName == "facility_service"));
            Debug.Assert(sqlAttrsInUse.Any(attr => attr.ColumnName == "facility_id"));

            // Build facility tag
            var facTag = new FacilityTag(
                dbReader["facility_site"] as string,
                dbReader["facility_service"] as string,
                dbReader["facility_id"] as string);

            // Create new facility
            var newFac = new SimpleCygnetFacility(facTag);

            // Cache facility attributes
            foreach (var attr in sqlAttrsInUse)
            {
                var attrId = attr.ColumnName;

                ct.ThrowIfCancellationRequested();

                var thisCellValue = dbReader[attrId];

                newFac.AddAttributeValue(Convert.IsDBNull(thisCellValue)
                    ? new FacilityAttributeValue(attr, null)
                    : new FacilityAttributeValue(attr, Convert.ToString(thisCellValue ?? "")));

                if (attr.AttributeType() == FacilityAttributeTypes.Table)
                {
                    var tableDescAttr = relatedTableDescAttrs[attr];

                    int index = Convert.ToInt16(attrId.Replace("facility_table", ""));
                    var attrValueDesc = GetTableAttrValueDesc(index, thisCellValue.ToString());

                    newFac.AddAttributeValue(new FacilityAttributeValue(tableDescAttr, attrValueDesc));
                }

                if (attr.ColumnName == "facility_type")
                {
                    var facTypes = GetFacilityTypes();

                    var typeDescAttr = attr.DeepCopy();
                    typeDescAttr.ColumnName = "facility_type_desc";

                    if (facTypes.TryGetValue(thisCellValue?.ToString() ?? "", out var typeDesc))
                    {
                        newFac.AddAttributeValue(new FacilityAttributeValue(typeDescAttr, typeDesc));
                    }
                    else
                    {
                        newFac.AddAttributeValue(new FacilityAttributeValue(typeDescAttr, thisCellValue?.ToString() ?? ""));
                    }
                }
            }
            return newFac;
        }

        public List<IFacilityFilterCondition> GetValidFacilityFilters(IEnumerable<IFacilityFilterCondition> facFilters)
        {
            var validFilters = facFilters
                .Where(filter => !FacilityFmsFilterCondition.FmsFilteringAttrs.Any(attr => attr.ColumnName == filter.AttributeId))
                .Where(filter => EnabledAttributes.ContainsKey(filter.AttributeId));

            return validFilters.ToList();
        }

        private string GetFastSqlCmdForSimpleFacilitiesWithTimeRestriction(DateTime timeRestriction)
        {
            var attrNameWithoutTableDescAttrs = EnabledAttributeNames.Keys
              .Where(attr => !(attr.StartsWith("facility_table")
              && attr.EndsWith("_desc")) && attr != "facility_type_desc");

            var attrNameOnlyTableDescAttrs = EnabledAttributeNames.Keys
                .Where(attr => (attr.StartsWith("facility_table")
                && attr.EndsWith("_desc")));

            var facService = DomainSiteService.SiteService.ToString().Replace(".", "_");

            var sqlCommand = $@"SELECT {string.Join(",", attrNameWithoutTableDescAttrs)}, update_datetime " +
                                $@" FROM {facService}.fac_header " +
                                $@" WHERE update_datetime > '{timeRestriction.ToString("yyyy-MM-dd HH:mm:ss")}'";

            sqlCommand += ";";

            return sqlCommand;
        }

        private string GetFastSqlCmdForSimpleFacilities(List<IFacilityFilterCondition> baseFacFilters, string childAttrId)
        {
            var attrNameWithoutTableDescAttrs = EnabledAttributeNames.Keys
                .Where(attr => !(attr.StartsWith("facility_table")
                                 && attr.EndsWith("_desc")) && attr != "facility_type_desc");

            var attrNameOnlyTableDescAttrs = EnabledAttributeNames.Keys
                .Where(attr => (attr.StartsWith("facility_table")
                                && attr.EndsWith("_desc")));

            var facService = DomainSiteService.SiteService.ToString();/*.Replace(".", "_");*/

            var sqlCommand = $@"SELECT {string.Join(",", attrNameWithoutTableDescAttrs)} FROM ""[{DomainId}]{facService}"".fac_header";

            // Handle ref facs           
            if (attrNameWithoutTableDescAttrs.Contains(childAttrId))
            {
                //SqlCommand += $" OR {childAttrId} <> ''";
            }
            else
            {
                var validfacFilters = GetValidFacilityFilters(baseFacFilters);
                var sqlConditions = ODBC.QueryBuilding.BuildSqlConditionList(validfacFilters);

                sqlCommand += $" WHERE ({string.Join(" AND ", sqlConditions)})";
            }

            sqlCommand += ";";

            return sqlCommand;
        }

        private FacilityAttributeCache AttrCache { get; }


        public FacilityAttributeValue GetCachedAttributeValue(FacilityTag facTag, string attrId)
        {
            return AttrCache.GetCachedAttributeValue(facTag, attrId);
        }

        public List<string> GetCachedAttributeIds()
        {
            return AttrCache.GetCachedAttributeIds();
        }

    }
}
