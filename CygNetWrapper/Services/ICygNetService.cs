﻿using CygNet.Data.Core;

namespace Techneaux.CygNetWrapper.Services
{
    public interface ICygNetService
    {
        ushort DomainId { get; }
        DomainSiteService DomainSiteService { get; }
        bool IsRunning { get; }
        bool IsServiceAvailable { get; }
        CygNetDomain ParentDomain { get; }
        ServiceDefinition ServiceDefinition { get; }
        SiteService SiteService { get; }
        ServiceStatus Status { get; }
        ServiceType Type { get; }
    }
}