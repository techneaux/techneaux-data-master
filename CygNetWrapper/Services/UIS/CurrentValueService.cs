﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CygNet.Data.Core;
using CygNet.API.Points;
using Techneaux.CygNetWrapper.Services.FAC;
using Techneaux.CygNetWrapper.Services.VHS;
using Nito.AsyncEx;
using Serilog;
using Techneaux.CygNetWrapper.Points.PointCaching;
using Techneaux.CygNetWrapper.Services.FMS;
using Techneaux.CygNetWrapper.Services.PNT;
using Techneaux.CygNetWrapper.Services.TRS;
using System.Threading;
using Serilog.Context;
using Techneaux.CygNetWrapper.ODBC;
using Techneaux.CygNetWrapper.Points;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace Techneaux.CygNetWrapper.Services
{
    public class CurrentValueService : CygNetService
    {
        public static double MaxCVRefreshRate { get; set; } = 1;

        private RealtimeClient _serviceClient;

        private Points.PointCaching.ExpiringCache<SiteService, PointServicePointTagCache> UdcCache;


        public RealtimeClient ServiceClient
        {
            get
            {
                if (_serviceClient == null)
                {
                    try
                    {
                        _serviceClient = new RealtimeClient(DomainSiteService);
                    }
                    catch (Exception ex)
                    {
                        using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                            Log.Error(ex, $"Failed to create service client for {DomainSiteService}");
                    }
                }

                return _serviceClient;
            }
        }

        public override bool IsServiceAvailable => ServiceClient != null;

        public CurrentValueService(CygNetDomain myDomain, ServiceDefinition newCvsServiceDef) : base(myDomain,
            newCvsServiceDef)
        {
            if (ServiceDefinition == null)
                throw new ArgumentException("Service cannot be null");

            _assocPnt = new AsyncLazy<PointService>(async () =>
                await Task.Run(() => ParentDomain.GetAssociatedService(this, ServiceType.PNT)) as PointService);
            _assocFac = new AsyncLazy<FacilityService>(async () =>
                await Task.Run(() => ParentDomain.GetAssociatedService(this, ServiceType.FAC)) as FacilityService);
            _assocVhs = new AsyncLazy<ValueHistoryService>(async () =>
                await Task.Run(() => ParentDomain.GetAssociatedService(this, ServiceType.VHS)) as ValueHistoryService);
            _assocTrs = new AsyncLazy<TableReferenceService>(async () =>
                await Task.Run(() => ParentDomain.GetAssociatedService(this, ServiceType.TRS)) as TableReferenceService);

            CurrentValueTimestampCache = new PointValueTimestampCache(this);

            //PointTagCache = new PointTagCache(this);
            PointAttributeValueCache = new PointAttributeCache(this);
        }

        public async Task<PointTag> GetPointTagAsync(FacilityTag facTag, string udc, CancellationToken ct)
        {
            var assocPnt = await GetAssociatedPnt();

            return await assocPnt.PointCache.GetPointTagAsync(facTag, udc, ct);
        }

        public async Task<List<PointTag>> GetPointTagsAsync(FacilityTag facTag, List<string> udcs, CancellationToken ct)
        {
            var assocPnt = await GetAssociatedPnt();

            return await assocPnt.PointCache.GetPointTagsAsync(facTag, udcs, ct);
        }


        public async Task<List<PointTag>> GetPointTagsFromOdbcAsync(
           CancellationToken ct)
        {
            return await GetPointTagsFromOdbcAsync(
                SiteService,
                null,
                null,
                null,
                this.DomainId,
                ct);
        }

        public async Task<List<PointTag>> GetPointTagsFromOdbcAsync(
            SiteService cvsSiteService,
            string facId,
            CancellationToken ct)
        {
            return await GetPointTagsFromOdbcAsync(
                SiteService,
                facId,
                null,
                new List<SiteService> { cvsSiteService },
                DomainId,
                ct);
        }

        public async Task<List<PointTag>> GetPointTagsFromOdbcAsync(
            FacilityTag facTag,
            List<string> allowedUdcs,
            CancellationToken ct)
        {
            return await GetPointTagsFromOdbcAsync(
                SiteService,
                facTag.FacilityId,
                allowedUdcs,
                new List<SiteService> { facTag.SiteService },
                DomainId,
                ct);
        }

        public async Task<List<PointTag>> GetPointTagsFromOdbcAsync(
            SiteService cvsService,
            CancellationToken ct)
        {
            return await GetPointTagsFromOdbcAsync(
                SiteService,
                null,
                null,
                new List<SiteService> { cvsService },
                DomainId,
                ct);
        }

        public async Task<List<PointTag>> GetPointTagsFromOdbcByUdcAsync(
            SiteService cvsService,
            string allowedUdc,
            CancellationToken ct)
        {
            return await GetPointTagsFromOdbcAsync(
                SiteService,
                null,
                new List<string> { allowedUdc },
                new List<SiteService> { cvsService },
                DomainId,
                ct);
        }

        public async Task<List<PointTag>> GetPointTagsFromOdbcAsync(
            SiteService cvsService,
            List<string> allowedUdcs,
            CancellationToken ct)
        {
            return await GetPointTagsFromOdbcAsync(
                SiteService,
                null,
                allowedUdcs,
                new List<SiteService> { cvsService },
                DomainId,
                ct);
        }

        public async Task<List<PointTag>> GetPointTagsFromOdbcAsync(
            List<SiteService> cvsServices,
            CancellationToken ct)
        {
            return await GetPointTagsFromOdbcAsync(
                SiteService,
                null,
                null,
                cvsServices,
                DomainId,
                ct);
        }

        public async Task<List<PointTag>> GetPointTagsFromOdbcAsync(
            List<SiteService> cvsServices,
            List<string> allowedUdcs,
            CancellationToken ct)
        {
            return await GetPointTagsFromOdbcAsync(
                SiteService,
                null,
                allowedUdcs,
                cvsServices,
                DomainId,
                ct);
        }

        private async Task<List<string>> GetUniqueUDCsFromOdbcAsync(
            string filler,
            CancellationToken ct)
        {
            try
            {

                using (var dbConnection = new OdbcConnection(CygNetOdbcConnection.DsnString))
                {
                    await dbConnection.OpenAsync(ct);

                    using (var dbCommand = dbConnection.CreateCommand())
                    {
                        dbCommand.CommandTimeout = 300;

                        dbCommand.CommandText = GetAllUdcCmd(
                            this.SiteService, this.DomainId);

                        var dbReader = await dbCommand.ExecuteReaderAsync(ct);

                        var allUdcs = new HashSet<string>();
                        while (dbReader.Read())
                        {
                            ct.ThrowIfCancellationRequested();

                            string udc;
                            udc = Convert.IsDBNull(dbReader["uniformdatacode"]) ? string.Empty : dbReader["uniformdatacode"].ToString();

                            if (string.IsNullOrEmpty(udc))
                            {
                                continue;
                            }

                            allUdcs.Add(udc);
                        }
                        return allUdcs.Distinct().ToList();
                    }
                }
            }
            catch (TaskCanceledException) { }
            catch (OperationCanceledException) { throw; }
            catch (OdbcException ex) when (ex.HResult == -2146232009) { } // Odbc cancellation exception
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "General exception getting udcs from point ");
                // General exception
            }

            return new List<string>();
        }

        private static async Task<List<PointTag>> GetPointTagsFromOdbcAsync(
            SiteService pntService,
            string filterFacId,
            IEnumerable<string> udcs,
            IEnumerable<SiteService> cvsServiceFilter,
            ushort domainId,
            CancellationToken ct)
        {
            var siteServFilterList = (cvsServiceFilter?.ToList() ?? new List<SiteService>())
                .Where(siteserv => siteserv != null)
                .ToList();

            var allowedSites = siteServFilterList.Select(siteserv => siteserv.Site).ToList();
            var siteIsKnown = allowedSites.Count == 1;
            var knownSite = allowedSites.FirstOrDefault() ?? "";

            var allowedServs = siteServFilterList.Select(siteserv => siteserv.Service).ToList();
            var serviceIsKnown = allowedServs.Count == 1;
            var knownService = allowedServs.FirstOrDefault() ?? "";

            var facIdIsKnown = !string.IsNullOrWhiteSpace(filterFacId);
            var knownFacId = filterFacId?.Trim();

            var udcFilterList = CygNetPoint.GetValidUdcsFromList(udcs);
            var udcIsKnown = udcFilterList.Count == 1;
            var knownUdc = udcFilterList.FirstOrDefault() ?? "";

            try
            {
                using (var dbConnection = new OdbcConnection(CygNetOdbcConnection.DsnString))
                {
                    await dbConnection.OpenAsync(ct);

                    using (var dbCommand = dbConnection.CreateCommand())
                    {
                        dbCommand.CommandTimeout = 300;

                        dbCommand.CommandText = GetSqlAllPointCmd(
                            pntService,
                            (facIdIsKnown, knownFacId),
                            (udcFilterList.Count > 0, udcFilterList),
                            (allowedSites.Count > 0, allowedSites),
                            (allowedServs.Count > 0, allowedServs), domainId);

                        var dbReader = await dbCommand.ExecuteReaderAsync(ct);

                        var allPointTags = new HashSet<PointTag>();
                        while (dbReader.Read())
                        {
                            ct.ThrowIfCancellationRequested();

                            string site, service, facId, udc;

                            if (siteIsKnown)
                                site = knownSite;
                            else
                                site = Convert.IsDBNull(dbReader["site"]) ? string.Empty : dbReader["site"].ToString();

                            if (serviceIsKnown)
                                service = knownService;
                            else
                                service = Convert.IsDBNull(dbReader["service"]) ? string.Empty : dbReader["service"].ToString();

                            if (facIdIsKnown)
                                facId = knownFacId;
                            else
                                facId = Convert.IsDBNull(dbReader["facilityid"]) ? string.Empty : dbReader["facilityid"].ToString();

                            if (udcIsKnown)
                                udc = knownUdc;
                            else
                                udc = Convert.IsDBNull(dbReader["uniformdatacode"]) ? string.Empty : dbReader["uniformdatacode"].ToString();

                            var pointId = Convert.IsDBNull(dbReader["pointid"]) ? string.Empty : dbReader["pointid"].ToString();
                            var pointIdLong = Convert.IsDBNull(dbReader["pointidlong"]) ? string.Empty : dbReader["pointidlong"].ToString();

                            if (string.IsNullOrEmpty(site) ||
                                string.IsNullOrEmpty(service) ||
                                string.IsNullOrEmpty(facId) ||
                                string.IsNullOrEmpty(udc))
                            {
                                continue;
                            }

                            var newSiteServ = new SiteService(site, service);
                            var newTag = new PointTag(newSiteServ, pointId, pointIdLong, facId, udc);
                            allPointTags.Add(newTag);
                        }

                        return allPointTags.ToList();
                    }
                }
            }
            catch (TaskCanceledException) { }
            catch (OperationCanceledException) { throw; }
            catch (OdbcException ex) when (ex.HResult == -2146232009) { } // Odbc cancellation exception
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "General exception getting point tags");


                // General exception
            }
            return new List<PointTag>();
        }

        private static string GetAllUdcCmd(SiteService pntService, ushort domain)
        {
            // Build query string
            var sql = new StringBuilder();


            sql.AppendLine("SELECT DISTINCT uniformdatacode");
            sql.AppendLine($@"  FROM ""[{domain}]{pntService.ToString()}"".pnt_header_record");
            return sql.ToString();
        }

        private static string GetSqlAllPointCmd(
            SiteService pntService,
            (bool isActive, string facId) facCondition,
            (bool isActive, List<string> allowedUdcs) udcCondition,
            (bool isActive, List<string> allowedSites) siteCondition,
            (bool isActive, List<string> allowedServices) serviceCondition, ushort domain)
        {
            var columnsInSelect = new List<string> { "pointid", "pointidlong", "uniformdatacode" };
            var whereConditionList = new List<string>();

            if (facCondition.isActive)
                whereConditionList.Add($"facilityid='{facCondition.facId}'");
            else
                columnsInSelect.Add("facilityid");

            if (udcCondition.isActive)
                whereConditionList.Add($"uniformdatacode IN ({string.Join(", ", udcCondition.allowedUdcs.Select(udc => $"'{udc}'"))})");
            else
                if (udcCondition.allowedUdcs.Count > 1)
                columnsInSelect.Add("uniformdatacode");

            if (siteCondition.isActive)
                whereConditionList.Add($"site IN ({string.Join(", ", siteCondition.allowedSites.Select(site => $"'{site}'"))})");
            else
                if (siteCondition.allowedSites.Count > 1)
                columnsInSelect.Add("site");

            if (serviceCondition.isActive)
                whereConditionList.Add($"service IN ({string.Join(", ", serviceCondition.allowedServices.Select(serv => $"'{serv}'"))})");
            else
                if (serviceCondition.allowedServices.Count > 1)
                columnsInSelect.Add("service");

            // Build query string
            var sql = new StringBuilder();
            sql.AppendLine($@"SELECT {string.Join(",", columnsInSelect)}");
            sql.AppendLine($@"  FROM ""[{domain}]{pntService.ToString()}"".pnt_header_record");

            if (whereConditionList.IsAny())
            {
                sql.AppendLine($@" WHERE {string.Join(" AND ", whereConditionList)}");
            }

            sql.Append(";");

            return sql.ToString();
        }


        //public PointTagCache PointTagCache { get; }

        public PointAttributeCache PointAttributeValueCache { get; }

        private PointValueTimestampCache CurrentValueTimestampCache { get; }

        private readonly AsyncLazy<PointService> _assocPnt;

        public async Task<PointService> GetAssociatedPnt()
        {
            return await _assocPnt;
        }

        private readonly AsyncLazy<FacilityService> _assocFac;

        public async Task<FacilityService> GetAssociatedFac()
        {
            return await _assocFac;
        }

        private readonly AsyncLazy<ValueHistoryService> _assocVhs;

        public async Task<ValueHistoryService> GetAssociatedVhs()
        {
            return await _assocVhs;
        }

        private readonly AsyncLazy<TableReferenceService> _assocTrs;

        public async Task<TableReferenceService> GetAssociatedTRS()
        {
            return await _assocTrs;
        }

        public bool PointExists(PointTag tag)
        {
            return ServiceClient.PointExists(tag);
        }

        public PointValueRecord GetPointValue(PointTag tag)
        {
            return ServiceClient.GetPointValueRecord(tag);
        }

        public PointValueRecord TryGetPointValue(PointTag tag)
        {
            throw new NotImplementedException();
        }

        public double GetPointVersion(PointTag tag)
        {
            return ServiceClient.GetPointVersion(tag);
        }

        public void WriteValue(
            PointTag tag,
            object newValue,
            DateTime? newTimestamp
        )
        {
            ServiceClient.SetPointValue(tag, newValue, newTimestamp);
        }

        public async Task<DateTime?> GetLatestCurrentValueDate(PointTag srcTag)
        {
            return await CurrentValueTimestampCache.GetLatestValueTimestampForPoint(srcTag, CancellationToken.None);
        }

        public async Task<DateTime?> GetLatestCurrentValueDate(PointTag srcTag, CancellationToken ct)
        {
            return await CurrentValueTimestampCache.GetLatestValueTimestampForPoint(srcTag, ct);
        }


        public void WriteValue(
            PointTag tag,
            object value,
            DateTime? timeStamp,
            BaseStatusFlags baseStatusMask,
            BaseStatusFlags baseStatus,
            UserStatusFlags userStatusMask,
            UserStatusFlags userStatus
        )
        {
            ServiceClient.SetPointValue(tag, value, timeStamp, baseStatusMask, baseStatus, userStatusMask, userStatus);
        }
    }
}