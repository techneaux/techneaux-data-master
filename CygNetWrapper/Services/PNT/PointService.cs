﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CygNet.API.Points;
using CygNet.Data.Core;
using CygNet.Data.Points;
using Nito.AsyncEx;
using Serilog;
using Serilog.Context;
using Techneaux.CygNetWrapper.ODBC;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services.TRS;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace Techneaux.CygNetWrapper.Services.PNT
{
    public class PointService : CygNetService
    {
        private ExpiringCache<string, List<string>> _pntTable;
        private ConfigClient _serviceClient;
        public ConfigClient ServiceClient
        {
            get
            {
                if (_serviceClient == null)
                {
                    try
                    {
                        _serviceClient = new ConfigClient(DomainSiteService);
                    }
                    catch (Exception ex)
                    {
                        using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                            Log.Warning(ex, "Failed to get PNT service client");
                    }
                }
                return _serviceClient;
            }
        }

        public PointServicePointTagCache PointCache { get; }


        public override bool IsServiceAvailable => ServiceClient != null;

        public PointService(CygNetDomain myDomain, ServiceDefinition pntServiceDef) : base(myDomain, pntServiceDef)
        {
            if (ServiceDefinition == null)
                throw new ArgumentException("Service cannot be null");
            if (ServiceDefinition.ServiceType != ServiceType.PNT)
                throw new ArgumentException("Service is wrong type");
            _assocTrs = new AsyncLazy<TableReferenceService>(async () => await Task.Run(() => ParentDomain.GetAssociatedService(this, ServiceType.TRS)) as TableReferenceService);
            _pntTable = new ExpiringCache<string, List<string>>(GetUniqueUDCsFromOdbcAsync, TimeSpan.FromMinutes(30));

            PointCache = new PointServicePointTagCache(this);
        }

        public PointService(CygNetDomain myDomain, DomainSiteService pntServiceDef) : base(myDomain, pntServiceDef, ServiceType.PNT)
        {  
            if (ServiceDefinition == null)
                throw new ArgumentException("Service cannot be null");
            if (ServiceDefinition.ServiceType != ServiceType.PNT)
                throw new ArgumentException("Service is wrong type");
            _assocTrs = new AsyncLazy<TableReferenceService>(async () => await Task.Run(() => ParentDomain.GetAssociatedService(this, ServiceType.TRS)) as TableReferenceService);
            _pntTable = new ExpiringCache<string, List<string>>(GetUniqueUDCsFromOdbcAsync, TimeSpan.FromMinutes(30));

            PointCache = new PointServicePointTagCache(this);
        }


        public async Task<List<string>> GetUniqueUdcList()
        {
            var cts = new CancellationTokenSource();
            return await _pntTable.GetCachedItem(SiteServiceName, cts.Token);
        }

        private readonly AsyncLazy<TableReferenceService> _assocTrs;

        public async Task<TableReferenceService> GetAssociatedTRS()
        {
            return await _assocTrs;
        }

        public async Task<Dictionary<SiteService, CurrentValueService>> GetAssociatedCvsServices()
        {
            return (await ParentDomain.GetAssociatedCvsServices(this)).ToDictionary(serv => serv.SiteService, serv => serv);
        }

        public PointConfigRecord GetPointConfigRecord(string tag)
        {
            return ServiceClient.ReadPointRecord(new PointTag(tag));
        }

        public PointConfigRecord GetPointConfigRecord(PointTag tag)
        {
            return ServiceClient.ReadPointRecord(tag);
        }

        public Dictionary<string, PointAttribute> GetAttributeNames()
        {
            return PointAttribute.AllPointAttributes;
        }


        public bool IsValidAttributeName(string attrName)
        {
            return PointAttribute.AllPointAttributes.ContainsKey(attrName);
        }

        public enum CygNetRecordUpdate
        {
            Created,
            Updated,
        }


        public (bool isSuccess, CygNetRecordUpdate updateType) AddOrUpdatePoints(PointConfigRecord pnt)
        {
            if (pnt == null)
                throw new NullReferenceException(nameof(pnt));

            try
            {
                if (ServiceClient.PointRecordExists(pnt.Tag))
                {
                    pnt.Tag = ServiceClient.ResolveTag(pnt.Tag);
                    ServiceClient.UpdatePointRecord(pnt);
                    return (true, CygNetRecordUpdate.Updated);
                }
                else
                {
                    ServiceClient.AddPointRecord(pnt);
                    return (true, CygNetRecordUpdate.Created);
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception while adding or updating point.");

                throw;
            }
        }

        public async Task<(bool isSuccess, Exception ex, PointConfigRecord pointAffected)> AddOrUpdatePoints(
            IEnumerable<PointConfigRecord> newPoints)
        {
            foreach (var pnt in newPoints)
            {
                try
                {
                    var result = AddOrUpdatePoints(pnt);
                    if (!result.isSuccess)
                    {
                        return (false, null, null);
                    }
                }
                catch (Exception e)
                {
                    using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                        Log.Error(e, "Unhandled exception, failed to create point");

                    return (false, e, pnt);
                }
            }

            return (true, null, null);
        }

        public async Task<List<PointTag>> GetPointTagsFromOdbcAsync(
            CancellationToken ct)
        {
            return await GetPointTagsFromOdbcAsync(
                SiteService,
                null,
                null,
                null,
                this.DomainId,
                ct);
        }

        public async Task<List<PointTag>> GetPointTagsFromOdbcAsync(
            SiteService cvsSiteService,
            string facId,
            CancellationToken ct)
        {
            return await GetPointTagsFromOdbcAsync(
                SiteService,
                facId,
                null,
                new List<SiteService> { cvsSiteService },
                DomainId,
                ct);
        }

        public async Task<List<PointTag>> GetPointTagsFromOdbcAsync(
            FacilityTag facTag,
            List<string> allowedUdcs,
            CancellationToken ct)
        {
            return await GetPointTagsFromOdbcAsync(
                SiteService,
                facTag.FacilityId,
                allowedUdcs,
                new List<SiteService> { facTag.SiteService },
                DomainId,
                ct);
        }

        public async Task<List<PointTag>> GetPointTagsFromOdbcAsync(
            SiteService cvsService,
            CancellationToken ct)
        {
            return await GetPointTagsFromOdbcAsync(
                SiteService,
                null,
                null,
                new List<SiteService> { cvsService },
                DomainId,
                ct);
        }

        public async Task<List<PointTag>> GetPointTagsFromOdbcByUdcAsync(
            SiteService cvsService,
            string allowedUdc,
            CancellationToken ct)
        {
            return await GetPointTagsFromOdbcAsync(
                SiteService,
                null,
                new List<string> { allowedUdc },
                new List<SiteService> { cvsService },
                DomainId,
                ct);
        }

        public async Task<List<PointTag>> GetPointTagsFromOdbcAsync(
            SiteService cvsService,
            List<string> allowedUdcs,
            CancellationToken ct)
        {
            return await GetPointTagsFromOdbcAsync(
                SiteService,
                null,
                allowedUdcs,
                new List<SiteService> { cvsService },
                DomainId,
                ct);
        }

        public async Task<List<PointTag>> GetPointTagsFromOdbcAsync(
            List<SiteService> cvsServices,
            CancellationToken ct)
        {
            return await GetPointTagsFromOdbcAsync(
                SiteService,
                null,
                null,
                cvsServices,
                DomainId,
                ct);
        }

        public async Task<List<PointTag>> GetPointTagsFromOdbcAsync(
            List<SiteService> cvsServices,
            List<string> allowedUdcs,
            CancellationToken ct)
        {
            return await GetPointTagsFromOdbcAsync(
                SiteService,
                null,
                allowedUdcs,
                cvsServices,
                DomainId,
                ct);
        }

        private async Task<List<string>> GetUniqueUDCsFromOdbcAsync(
            string filler,
            CancellationToken ct)
        {
            try
            {

                using (var dbConnection = new OdbcConnection(CygNetOdbcConnection.DsnString))
                {
                    await dbConnection.OpenAsync(ct);

                    using (var dbCommand = dbConnection.CreateCommand())
                    {
                        dbCommand.CommandTimeout = 300;

                        dbCommand.CommandText = GetAllUdcCmd(
                            this.SiteService, this.DomainId);

                        var dbReader = await dbCommand.ExecuteReaderAsync(ct);

                        var allUdcs = new HashSet<string>();
                        while (dbReader.Read())
                        {
                            ct.ThrowIfCancellationRequested();

                            string udc;
                            udc = Convert.IsDBNull(dbReader["uniformdatacode"]) ? string.Empty : dbReader["uniformdatacode"].ToString();

                            if (string.IsNullOrEmpty(udc))
                            {
                                continue;
                            }

                            allUdcs.Add(udc);
                        }
                        return allUdcs.Distinct().ToList();
                    }
                }
            }
            catch (TaskCanceledException) { }
            catch (OperationCanceledException) { throw; }
            catch (OdbcException ex) when (ex.HResult == -2146232009) { } // Odbc cancellation exception
            catch (Exception ex)
            {
                Log.Error(ex, "General exception getting udcs from point ");
                // General exception
            }

            return new List<string>();
        }

       

        private static async Task<List<PointTag>> GetPointTagsFromOdbcAsync(
            SiteService pntService,
            string filterFacId,
            IEnumerable<string> udcs,
            IEnumerable<SiteService> cvsServiceFilter,
            ushort domainId,
            CancellationToken ct)
        {
            var siteServFilterList = (cvsServiceFilter?.ToList() ?? new List<SiteService>())
                .Where(siteserv => siteserv != null)
                .ToList();

            var allowedSites = siteServFilterList.Select(siteserv => siteserv.Site).ToList();
            var siteIsKnown = allowedSites.Count == 1;
            var knownSite = allowedSites.FirstOrDefault() ?? "";

            var allowedServs = siteServFilterList.Select(siteserv => siteserv.Service).ToList();
            var serviceIsKnown = allowedServs.Count == 1;
            var knownService = allowedServs.FirstOrDefault() ?? "";

            var facIdIsKnown = !string.IsNullOrWhiteSpace(filterFacId);
            var knownFacId = filterFacId?.Trim();

            var udcFilterList = CygNetPoint.GetValidUdcsFromList(udcs);
            var udcIsKnown = udcFilterList.Count == 1;
            var knownUdc = udcFilterList.FirstOrDefault() ?? "";

            try
            {
                using (var dbConnection = new OdbcConnection(CygNetOdbcConnection.DsnString))
                {
                    await dbConnection.OpenAsync(ct);

                    using (var dbCommand = dbConnection.CreateCommand())
                    {
                        dbCommand.CommandTimeout = 300;

                        dbCommand.CommandText = GetSqlAllPointCmd(
                            pntService,
                            (facIdIsKnown, knownFacId),
                            (udcFilterList.Count > 0, udcFilterList),
                            (allowedSites.Count > 0, allowedSites),
                            (allowedServs.Count > 0, allowedServs), domainId);

                        var dbReader = await dbCommand.ExecuteReaderAsync(ct);

                        var allPointTags = new HashSet<PointTag>();
                        while (dbReader.Read())
                        {
                            ct.ThrowIfCancellationRequested();

                            string site, service, facId, udc;

                            if (siteIsKnown)
                                site = knownSite;
                            else
                                site = Convert.IsDBNull(dbReader["site"]) ? string.Empty : dbReader["site"].ToString();

                            if (serviceIsKnown)
                                service = knownService;
                            else
                                service = Convert.IsDBNull(dbReader["service"]) ? string.Empty : dbReader["service"].ToString();

                            if (facIdIsKnown)
                                facId = knownFacId;
                            else
                                facId = Convert.IsDBNull(dbReader["facilityid"]) ? string.Empty : dbReader["facilityid"].ToString();

                            if (udcIsKnown)
                                udc = knownUdc;
                            else
                                udc = Convert.IsDBNull(dbReader["uniformdatacode"]) ? string.Empty : dbReader["uniformdatacode"].ToString();

                            var pointId = Convert.IsDBNull(dbReader["pointid"]) ? string.Empty : dbReader["pointid"].ToString();
                            var pointIdLong = Convert.IsDBNull(dbReader["pointidlong"]) ? string.Empty : dbReader["pointidlong"].ToString();

                            if (string.IsNullOrEmpty(site) ||
                                string.IsNullOrEmpty(service) ||
                                string.IsNullOrEmpty(facId) ||
                                string.IsNullOrEmpty(udc))
                            {
                                continue;
                            }

                            var newSiteServ = new SiteService(site, service);
                            var newTag = new PointTag(newSiteServ, pointId, pointIdLong, facId, udc);
                            allPointTags.Add(newTag);
                        }

                        return allPointTags.ToList();
                    }
                }
            }
            catch (TaskCanceledException) { }
            catch (OperationCanceledException) { throw; }
            catch (OdbcException ex) when (ex.HResult == -2146232009) { } // Odbc cancellation exception
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception while getting point tags");

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error(ex,
                        "Failed to retrieve point tags from CygNet using the ODBC driver. Please check the following: " + Environment.NewLine  + 
                        "1) Make sure the 64-bit ODBC driver is installed " + Environment.NewLine  +
                        "2) Make sure that a CygNet client connection is established " + Environment.NewLine  +
                        "3) Make sure that the ACS has ODBC permissions for the Facility, Point, and TRS services for all users running this application");
            }
            return new List<PointTag>();
        }

        private static string GetAllUdcCmd(SiteService pntService, ushort domain)
        {
            // Build query string
            var sql = new StringBuilder();


            sql.AppendLine("SELECT DISTINCT uniformdatacode");
            sql.AppendLine($@"  FROM ""[{domain}]{pntService.ToString()}"".pnt_header_record");
            return sql.ToString();
        }

        private static string GetSqlAllPointCmd(
            SiteService pntService,
            (bool isActive, string facId) facCondition,
            (bool isActive, List<string> allowedUdcs) udcCondition,
            (bool isActive, List<string> allowedSites) siteCondition,
            (bool isActive, List<string> allowedServices) serviceCondition, ushort domain)
        {
            var columnsInSelect = new List<string> { "pointid", "pointidlong", "uniformdatacode"};
            var whereConditionList = new List<string>();

            if (facCondition.isActive)
                whereConditionList.Add($"facilityid='{facCondition.facId}'");
            else
                columnsInSelect.Add("facilityid");

            if (udcCondition.isActive)
                whereConditionList.Add($"uniformdatacode IN ({string.Join(", ", udcCondition.allowedUdcs.Select(udc => $"'{udc}'"))})");
            else
                if (udcCondition.allowedUdcs.Count > 1)
                columnsInSelect.Add("uniformdatacode");

            if (siteCondition.isActive)
                whereConditionList.Add($"site IN ({string.Join(", ", siteCondition.allowedSites.Select(site => $"'{site}'"))})");
            else
                if (siteCondition.allowedSites.Count > 1)
                columnsInSelect.Add("site");

            if (serviceCondition.isActive)
                whereConditionList.Add($"service IN ({string.Join(", ", serviceCondition.allowedServices.Select(serv => $"'{serv}'"))})");
            else
                if (serviceCondition.allowedServices.Count > 1)
                columnsInSelect.Add("service");

            // Build query string
            var sql = new StringBuilder();
            sql.AppendLine($@"SELECT {string.Join(",", columnsInSelect)}");
            sql.AppendLine($@"  FROM ""[{domain}]{pntService.ToString()}"".pnt_header_record");

            if (whereConditionList.IsAny())
            {
                sql.AppendLine($@" WHERE {string.Join(" AND ", whereConditionList)}");
            }

            sql.Append(";");

            return sql.ToString();
        }


    }
}

