﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CygNet.API.Core;
using CygNet.Data.Core;
using System.Runtime.InteropServices;
using Techneaux.CygNetWrapper.Services.FAC;
using Serilog;
using Serilog.Context;
using Techneaux.CygNetWrapper.Services.PNT;
using Techneaux.CygNetWrapper.Services.DDS;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace Techneaux.CygNetWrapper.Services
{
    public class CygNetDomain
    {
        private readonly Dictionary<string, CygNetService> _cachedServices = new Dictionary<string, CygNetService>();
        private readonly Dictionary<string, CygNetService> _inactiveCachedServices = new Dictionary<string, CygNetService>();

        private CygNetDomain(ushort thisDomain)
        {
            DomainId = thisDomain;
        }

        public ushort DomainId { get; }

        private bool _servicesLoaded;
        private readonly SemaphoreSlim _serviceSemaphore = new SemaphoreSlim(1, 1);

        public async Task<bool> RefreshSiteServiceInfoAsync(CancellationToken ct, bool forceRefresh = false)
        {
            Log.Debug("Starting search for services");
            try
            {
                await _serviceSemaphore.WaitAsync();
                ct.ThrowIfCancellationRequested();
                if (!forceRefresh && _servicesLoaded)
                {
                    Log.Debug("Search canceled, services already loaded");
                    return true;
                }

                ct.ThrowIfCancellationRequested();

                var completeSrc = new TaskCompletionSource<IEnumerable<ServiceDefinition>>();
                ct.Register(() => completeSrc.TrySetResult(new List<ServiceDefinition>()));

                Log.Verbose("Creating service client");
                var cygServInfoClient = new ServiceInformation();

                Log.Verbose("Retrieving services");

                var serviceRefreshTask = Task.Run(() => cygServInfoClient.GetServiceList(DomainId, new List<ServiceType>(), new List<ServiceStatus>() { ServiceStatus.OK }), ct);

                var servList = await await Task.WhenAny(completeSrc.Task, serviceRefreshTask);

                ct.ThrowIfCancellationRequested();

                Log.Verbose("Processing services");

                var castServices = servList.Select(serv => ServiceUtility.GetDerivedService(this, serv)).ToList();

                var newActiveServices = new HashSet<CygNetService>(castServices.Where(serv => serv.IsRunning));
                var newInactiveServices = new HashSet<CygNetService>(castServices.Where(serv => !serv.IsRunning));

                try
                {
                    _serviceListLock.EnterWriteLock();

                    foreach (var serv in newActiveServices)
                    {
                        if (_inactiveCachedServices.TryGetValue(serv.DomainSiteServiceName, out CygNetService existingServ))
                        {
                            _cachedServices[serv.DomainSiteServiceName] = existingServ;
                            _inactiveCachedServices.Remove(serv.DomainSiteServiceName);
                        }
                        else if (!_cachedServices.ContainsKey(serv.DomainSiteServiceName))
                        {
                            _cachedServices.Add(serv.DomainSiteServiceName, serv);
                        }
                    }

                    foreach (var serv in newInactiveServices)
                    {
                        if (_cachedServices.TryGetValue(serv.DomainSiteServiceName, out CygNetService existingServ))
                        {
                            _inactiveCachedServices[serv.DomainSiteServiceName] = existingServ;
                            _cachedServices.Remove(serv.DomainSiteServiceName);
                        }
                        else if (!_inactiveCachedServices.ContainsKey(serv.DomainSiteServiceName))
                        {
                            _inactiveCachedServices.Add(serv.DomainSiteServiceName, serv);
                        }
                    }
                }
                finally
                {
                    _serviceListLock.ExitWriteLock();
                }

                // Task.Run(() => GetServiceAssociationsAsync());

                Log.Debug("Service refresh successful");

                _servicesLoaded = true;
            }
            catch (OperationCanceledException) { }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception while getting services from domain ({domain})", DomainId);

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error(ex, "Could not retrieve services from domain ({domain}). Please check the following: /n" + 
                        "1) Check for a fully licensed client connection /n" +
                        "2) Check for proper permissions in the ACS /n" +
                        "3) Check that the ARS is running and the domain ID is correct", DomainId);

                return false;
            }
            finally
            {
                _serviceSemaphore.Release();
                Log.Debug($"Service semaphore release => count: {_serviceSemaphore.CurrentCount}");
            }

            return true;
        }

        public async Task<List<CurrentValueService>> GetAssociatedCvsServices(CygNetService srcService)
        {
            var linkedCvsServices = new List<CurrentValueService>();

            try
            {
                //var theseCvsServices = Services.Where(serv => serv is CurrentValueService && serv.SiteService.Site == srcService.SiteService.Site).ToList();
                var theseCvsServices = Services.Where(serv => serv is CurrentValueService).ToList();

                foreach (var cvsServ in theseCvsServices)
                {
                    var linkedServ = await GetAssociatedService(cvsServ, srcService.Type);

                    if (linkedServ != null && linkedServ.DomainSiteService == srcService.DomainSiteService)
                    {
                        linkedCvsServices.Add(cvsServ as CurrentValueService);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Warning(ex, $"Failed to get associated cvs service for {srcService.DomainSiteService}");
                GeneralExceptionLogger.Here().Warning(ex, $"Failed to get associated cvs service for {srcService.DomainSiteService}");

                return new List<CurrentValueService>();
            }

            return linkedCvsServices;
        }

        public async Task<List<PointService>> GetLinkedPointServices(FacilityService srcFacService)
        {
            var cvsServices = await GetAssociatedCvsServices(srcFacService);

            var pntServs = new HashSet<PointService>();

            foreach (var cvsServ in cvsServices)
            {
                var thisPnt = await cvsServ.GetAssociatedPnt();

                if (thisPnt != null)
                    pntServs.Add(thisPnt);
            }

            return pntServs.ToList();

            //return cvsServices.Select(serv => serv.GetAssociatedPnt()).Distinct().ToList();
        }


        public async Task<CygNetService> GetAssociatedService(CygNetService sourceService, ServiceType associatedType)
        {
            if (sourceService == null)
                throw new ArgumentException("Service must not be null");

            CygNetService associatedService = null;

            try
            {
                var servClient = new ServiceInformation();
                var associatedDomainSiteService = await Task.Run(() => servClient.GetAssociatedService(sourceService.DomainSiteService, associatedType));
                associatedService = FindService(associatedDomainSiteService);
            }
            catch (MessagingException ex)
            {
                Log.Warning(ex, $"Failed to get associated service of type [{associatedType.ToString()}] for service [{sourceService.DomainSiteService}])");
            }
            catch (SEHException ex)
            {
                Log.Warning(ex, $"Failed to get associated service of type [{associatedType.ToString()}] for service [{sourceService.DomainSiteService}])");
            }

            return associatedService;
        }

        private async Task<CygNetService> _GetAssociatedService(ICygNetService sourceService, ServiceType associatedType)
        {
            try
            {
                var servInfoClient = new ServiceInformation();
                var associatedDomainSiteService = await Task.Run(() => servInfoClient.GetAssociatedService(sourceService.DomainSiteService, associatedType));
                var associatedService = FindService(associatedDomainSiteService);

                return associatedService;
            }
            catch (MessagingException ex)
            {
                Log.Warning(ex, $"Failed to get associated service of type [{associatedType.ToString()}] for service [{sourceService.DomainSiteService}])");
            }
            catch (SEHException ex)
            {
                Log.Warning(ex, $"Failed to get associated service of type [{associatedType.ToString()}] for service [{sourceService.DomainSiteService}])");
            }

            return null;
        }

        private readonly ReaderWriterLockSlim _serviceListLock = new ReaderWriterLockSlim();
        public IReadOnlyList<CygNetService> Services
        {
            get
            {
                _serviceListLock.EnterReadLock();
                try
                {
                    IReadOnlyList<CygNetService> servList = _cachedServices.Values.ToList();
                    return servList;
                }
                finally
                {
                    _serviceListLock.ExitReadLock();
                }
            }
        }

        public IEnumerable<FacilityService> FacilityServices =>
            Services.OfType<FacilityService>();

        public IEnumerable<DeviceDefinitionService> DeviceDefinitionServices => Services.OfType<DeviceDefinitionService>();

        public IEnumerable<PointService> PointServices => Services.OfType<PointService>();

        public IEnumerable<CurrentValueService> CvsServices =>
            Services.OfType<CurrentValueService>();

        public IEnumerable<DomainSiteService> DomainSiteServices => Services
            .Select(servDef => servDef.DomainSiteService);

        public IEnumerable<CygNetService> FindServicesByType(ServiceType filter) => Services
            .Where(servDef => servDef.Type == filter);

        private CygNetService FindService(string siteDotService) => Services
                .FirstOrDefault(serv => string.Equals(serv.SiteService.ToString(), siteDotService, StringComparison.CurrentCultureIgnoreCase));

        private CygNetService FindService(DomainSiteService domainSiteService) => Services
               .FirstOrDefault(serv => string.Equals(serv.DomainSiteService.ToString(), domainSiteService.ToString(), StringComparison.CurrentCultureIgnoreCase));

        public static ushort? GetCurrentDomainIdAsync()
        {
            Log.Debug("Searching for current domain ID");
            var cygServInfoClient = new ServiceInformation();

            try
            {
                var curDomain = cygServInfoClient.GetAmbientDomain();

                Log.Debug($"Current domain found: {curDomain}");
                return curDomain;
            }
            catch (Exception ex)
            {
                Log.Warning(ex, $"Failed to get ambient domain");
                GeneralExceptionLogger.Here().Warning(ex, $"Failed to get ambient domain");
                return null;
            }
        }

        private static readonly SemaphoreSlim DomainLock = new SemaphoreSlim(1, 1);
        private static readonly Dictionary<ushort, CygNetDomain> DomainCache = new Dictionary<ushort, CygNetDomain>();

        public static CygNetDomain GetDomainAsync(ushort id)
        {
            try
            {
                DomainLock.Wait();

                Log.Verbose($"Attempting to get domain with id: {id}");
                if (DomainCache.TryGetValue(id, out var cygDomain))
                {
                    Log.Verbose("Domain already found, returning it");
                    return cygDomain;
                }
                else
                {
                    var newDomain = new CygNetDomain(id);

                    DomainCache.Add(id, newDomain);

                    Log.Verbose("New domain required, returning it");
                    return newDomain;
                }
            }
            finally
            {
                DomainLock.Release();
            }
        }

        public static CygNetDomain GetCurrentDomainAsync()
        {
            var thisDomain = GetCurrentDomainIdAsync();

            return thisDomain.HasValue ? GetDomainAsync(thisDomain.Value) : null;
        }

        public override bool Equals(object obj)
        {
            if (obj is CygNetDomain compDomain)
            {
                return DomainId == compDomain.DomainId;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return DomainId.GetHashCode();
        }

        public override string ToString()
        {
            return $@"CygNet Domain [{DomainId}]";
        }

    }
}