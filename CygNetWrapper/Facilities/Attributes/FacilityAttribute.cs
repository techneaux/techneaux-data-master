using CygNet.Data.Facilities;
using System.Collections.Generic;

namespace Techneaux.CygNetWrapper.Facilities.Attributes
{
    public class FacilityAttribute : AttributeDefinition
    {
        public FacilityAttribute()
        {

        }

        public FacilityAttribute(AttributeDefinition newAttribute)
        {
            ColumnId = newAttribute.ColumnId;
            ColumnName = newAttribute.ColumnName;
            Description = newAttribute.Description;
            Category = newAttribute.Category;
            InUse = newAttribute.InUse;
            IsBoolean = newAttribute.IsBoolean;
            AttributeType = newAttribute.AttributeType();
        }

        public uint AttributeIndex { get; set; }


        public FacilityAttributeTypes AttributeType { get; set; }

        
        static FacilityAttribute()
        {
            var allAttrs = new List<FacilityAttribute>();

            allAttrs.Add(new FacilityAttribute
            {
                ColumnName = $"facility_site",
                IsBoolean = false,
                AttributeType = FacilityAttributeTypes.General
            });

            allAttrs.Add(new FacilityAttribute
            {
                AttributeType = FacilityAttributeTypes.General,
            ColumnName = $"facility_service",
                IsBoolean = false
            });

            allAttrs.Add(new FacilityAttribute
            {
                AttributeType = FacilityAttributeTypes.General,
                ColumnName = $"facility_id",
                IsBoolean = false
            });

            allAttrs.Add(new FacilityAttribute
            {
                AttributeType = FacilityAttributeTypes.General,
                ColumnName = $"facility_type",
                IsBoolean = false
            });

            allAttrs.Add(new FacilityAttribute
            {
                AttributeType = FacilityAttributeTypes.General,
                ColumnName = $"facility_description",
                IsBoolean = false
            });

            allAttrs.Add(new FacilityAttribute
            {
                AttributeType = FacilityAttributeTypes.General,
                ColumnName = $"facility_category",
                IsBoolean = false
            });

            for (uint i = 0; i <= 29; i++)
            {
                allAttrs.Add(new FacilityAttribute {
                    AttributeType = FacilityAttributeTypes.Text,
                    AttributeIndex = i,
                    ColumnName=$"facility_attr{i}",
                    IsBoolean = false});
                    
            }

            for (uint i = 0; i <= 29; i++)
            {
                allAttrs.Add(new FacilityAttribute
                {
                    AttributeType = FacilityAttributeTypes.Table,
                    AttributeIndex = i,
                    ColumnName = $"facility_table{i}",
                    IsBoolean = false
                });
            }

            for (uint i = 0; i <= 19; i++)
            {
                allAttrs.Add(new FacilityAttribute
                {
                    AttributeType = FacilityAttributeTypes.YesNo,
                    AttributeIndex = i,
                    ColumnName = $"facility_yesno{i}",
                    IsBoolean = true
                });
            }
            for (uint i = 0; i < 2; i++)
            {
                allAttrs.Add(new FacilityAttribute
                {
                    AttributeType = FacilityAttributeTypes.Info,
                    AttributeIndex = i,
                    ColumnName = $"facility_info{i}",
                    IsBoolean = true

                });
            }
            AllPossibleAttrs = allAttrs;
        }


        public static List<FacilityAttribute> AllPossibleAttrs { get; set; }


    }
}
