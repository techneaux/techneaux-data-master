using System;
using System.Collections.Generic;
using CygNet.Data.Facilities;
using TechneauxUtility;

namespace Techneaux.CygNetWrapper.Facilities.Attributes
{   
    public class FacilityAttributeValue : ConvertibleValue, IAttributeValue
    {
        public AttributeValueResults ValueAvailabilityStatus { get; }
        public Exception ThisValueException { get; }

        public AttributeDefinition ThisFacilityAttribute { get; }

        public string AttributeId => ThisFacilityAttribute?.ColumnName;

        public string AttributeDesc => ThisFacilityAttribute?.Description;

        public FacilityAttributeValue(AttributeDefinition thisAttr, AttributeValueResults thisResult, Exception ex = null) : base(null)
        {
            ThisFacilityAttribute = thisAttr;
            if (thisResult == AttributeValueResults.ValueAvailable)
                throw new ArgumentException("This constructor cannot be called with a <Value Available> status. It is for all other types of statuses.");
            ValueAvailabilityStatus = thisResult;
            ThisValueException = ex;
        }

        public class StringPool
        {
            private readonly Dictionary<string, string> contents =
                new Dictionary<string, string>();

            public string Add(string item)
            {
                string ret;
                if (!contents.TryGetValue(item, out ret))
                {
                    contents[item] = item;
                    ret = item;
                }
                return ret;
            }
        }

        private StringPool AttrStringPool = new StringPool();

        public FacilityAttributeValue(AttributeDefinition thisAttr, object newVal) : base(newVal)
        {
            ThisFacilityAttribute = thisAttr;
            ValueAvailabilityStatus = AttributeValueResults.ValueAvailable;
        }

        public override string ToString()
        {
            if (ValueAvailabilityStatus == AttributeValueResults.ValueAvailable)
            {
                if (RawValue is string s)
                {
                    return s;
                }

                return base.ToString();
            }

            return $"[{ValueAvailabilityStatus.ToString()}]";
        }
    }

}