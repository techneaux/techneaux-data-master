using System.ComponentModel;
using CygNet.Data.Facilities;

namespace Techneaux.CygNetWrapper.Facilities.Attributes
{

    public enum FacilityAttributeTypes
    {
        [Description("General")]
        General,

        [Description("Info")]
        Info,

        [Description("Text")]
        Text,

        [Description("Table")]
        Table,

        [Description("Yes/No")]
        YesNo,

        [Description("Runtime - FMS")]
        ExtendedFms,

        [Description("Runtime - Points")]
        ExtendedPoint
    }

    public static class FacilityExtensionMethods
    {
        public static FacilityAttributeTypes AttributeType(this AttributeDefinition thisAttr)
        {
            if (thisAttr.ColumnName.ToLower().StartsWith("facility_attr"))
                return FacilityAttributeTypes.Text;
            else if (thisAttr.ColumnName.ToLower().StartsWith("facility_table"))
                return FacilityAttributeTypes.Table;
            else if (thisAttr.ColumnName.ToLower().StartsWith("facility_yes_no"))
                return FacilityAttributeTypes.YesNo;
            else if (thisAttr.ColumnName.ToLower().StartsWith("facility_info"))
                return FacilityAttributeTypes.Info;
            else
                return FacilityAttributeTypes.General;
        }
    }
}
