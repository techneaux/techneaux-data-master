﻿using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using Microsoft.VisualBasic.CompilerServices;
using Microsoft.VisualBasic.FileIO;

namespace Techneaux.CygNetWrapper.Facilities.Filtering
{
    public enum SqlConditionalOperators
    {
        [Description("Like")]
        Like,

        [Description("In {list}")]
        In,

        [Description("Not in {list}")]
        NotIn,

  

        [Description("Not Like")]
        NotLike,
    }

    public interface IFacilityFilterCondition
    {
        string AttributeId { get; set; }
        string AttributeDescription { get; set; }

        SqlConditionalOperators ConditionalOperator { get; set; }
        string ConditionalValue { get; set; }

        List<string> InItemList { get; }

        bool IsValid { get; }

        string InvalidReason { get; }
    }

}