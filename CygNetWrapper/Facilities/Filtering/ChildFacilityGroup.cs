using System;
using System.ComponentModel;
using System.Linq;
using TechneauxUtility;
using XmlDataModelUtility;

namespace Techneaux.CygNetWrapper.Facilities.Filtering
{
    [Serializable]
    public class ChildFacilityGroup : NotifyCopyDataModel
    {
        public ChildFacilityGroup()
        {
            GroupKey = new IdDescKey
            {
                Id = "[Group Id]",
                Desc = "[Group Desc]"
            };

            FilterConditions = new BindingList<FacilityAttributeFilterCondition>();
        }

        public IdDescKey GroupKey
        {
            get => GetPropertyValue<IdDescKey>();
            set => SetPropertyValue(value);
        }

        public BindingList<FacilityAttributeFilterCondition> FilterConditions
        {
            get => GetPropertyValue<BindingList<FacilityAttributeFilterCondition>>();
            set => SetPropertyValue(value);
        }

        public string ConditionsString
        {
            get
            {
                var SingleConditionStrings = FilterConditions.Select(cond => cond.ToString());
                var FullString = string.Join(";", SingleConditionStrings);
                SetPropertyValue(FullString);
                return GetPropertyValue<string>();
            }
            set => SetPropertyValue(value);
        }

        //public ChildFacilityGroup DeepCopyObject()
        //{
        //    ChildFacilityGroup childGroupCopy = new ChildFacilityGroup();
        //    childGroupCopy.GroupDescription = this.GroupDescription;
        //    childGroupCopy.GroupId = this.GroupId;
        //    childGroupCopy.FilterConditions = new BindingList<CygFacilityFilterCondition>();

        //    foreach (CygFacilityFilterCondition facCondition in this.FilterConditions)
        //    {
        //        CygFacilityFilterCondition facConditionCopy = facCondition.DeepObjectCopy() as CygFacilityFilterCondition;
        //        childGroupCopy.FilterConditions.Add(facConditionCopy);
        //    }

        //    return childGroupCopy;
        //}
    }
}
