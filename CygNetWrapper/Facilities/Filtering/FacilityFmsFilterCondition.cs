using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using Techneaux.CygNetWrapper.Facilities.Attributes;

namespace Techneaux.CygNetWrapper.Facilities.Filtering
{
    [Serializable]
    public class FacilityFmsFilterCondition : FacilityFilterConditionBase
    {
        public FacilityFmsFilterCondition() : base()
        {
        }

        public override bool IsValid => !string.IsNullOrEmpty(AttributeId);

        public override string InvalidReason => IsValid ? "Rule is valid" : "Attribute or condition ID is blank";

        private static List<FacilityAttribute> _fmsFilteringAttrs = new List<FacilityAttribute>()
        {
            new FacilityAttribute
            {
                AttributeType = FacilityAttributeTypes.ExtendedFms,
                ColumnName = FmsFilterTypes.facility_has_fms_meter_YN.ToString(),
                Description = FmsFilterTypes.facility_has_fms_meter_YN.GetDescription(),
                InUse = true,
                IsBoolean = true
            },

            new FacilityAttribute
            {
                AttributeType = FacilityAttributeTypes.ExtendedFms,
                ColumnName = FmsFilterTypes.facility_fms_meter_in_group.ToString(),
                Description = FmsFilterTypes.facility_fms_meter_in_group.GetDescription(),
                InUse = true,
                IsBoolean = true
            }
        };

        public enum FmsFilterTypes
        {
            [Description("Facility Has FMS Meter (Yes/No)")]
            facility_has_fms_meter_YN,

            [Description("Facility Has FMS Meter in Group")]
            facility_fms_meter_in_group
        }

        public static List<FacilityAttribute> FmsFilteringAttrs => _fmsFilteringAttrs;
    }
}