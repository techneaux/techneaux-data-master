﻿using CygNet.Data.Core;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Techneaux.CygNetWrapper.Facilities.Attributes;
using Techneaux.CygNetWrapper.Services.FAC;

namespace Techneaux.CygNetWrapper.Facilities
{
    public class FacilityAttributeCache
    {
       

        public TimeSpan TimeBetweenUpdateChecks { get; set; } = TimeSpan.FromMinutes(10);

        private readonly ConcurrentDictionary<FacilityTag, Dictionary<string, FacilityAttributeValue>> _internalCache =
            new ConcurrentDictionary<FacilityTag, Dictionary<string, FacilityAttributeValue>>();

        private readonly HashSet<string> _cachedAttributeIds = new HashSet<string>();

        public FacilityService MyFacilityService { get; }

        public DateTime TimeCacheLastUpdated { get; private set; }
        public FacilityAttributeCache(FacilityService thisFacServ)
        {
            MyFacilityService = thisFacServ;

            //Observable.Interval(TimeBetweenUpdateChecks)
            //.Subscribe(async evt => { await UpdateFacilityCache(); });

            TimeCacheLastUpdated = DateTime.Now.AddMinutes(-5);

            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            aTimer.Interval = TimeBetweenUpdateChecks.TotalMilliseconds;
            aTimer.Enabled = true;            
        }

        // Specify what you want to happen when the Elapsed event is raised.
        private async void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            await UpdateFacilityCache();
        }

        public void RegisterFacilities(Dictionary<CachedCygNetFacility, Dictionary<string, FacilityAttributeValue>> registeredFacsWithAttributes)
        {
            foreach (var fac in registeredFacsWithAttributes.Keys)
            {
                _internalCache.TryAdd(fac.Tag, registeredFacsWithAttributes[fac]);

                _cachedAttributeIds.UnionWith(registeredFacsWithAttributes[fac].Keys);
            }
        }

        private SemaphoreSlim LockCacheUpdates = new SemaphoreSlim(1, 1);
        public async Task UpdateFacilityCache()
        {
            try
            {
                await LockCacheUpdates.WaitAsync();

                // Get updated facs
                var (latestDate, facResults) = await Task.Run(() => MyFacilityService.GetSimpleFacilitiesRecentlyUpdatedAsync(TimeCacheLastUpdated,
                    CancellationToken.None));

                foreach (var fac in facResults)
                {
                    _internalCache[fac.Key] = fac.Value.FullAttributeValues;
                    Console.WriteLine($"********* NEW FACS FOUND ************ {facResults.Count}");
                }

                if (latestDate != default)
                {
                    TimeCacheLastUpdated = latestDate;
                }

            }
            finally
            {
                LockCacheUpdates.Release();
            }
        }

        public FacilityAttributeValue GetCachedAttributeValue(FacilityTag facTag, string attrId)
        {
            var attrDef = MyFacilityService.EnabledAttributes[attrId];

            if (_internalCache.TryGetValue(facTag, out var thisAttrCache))
            {
                if (!thisAttrCache.TryGetValue(attrId, out var thisVal))
                {
                    thisVal = new FacilityAttributeValue(attrDef, AttributeValueResults.AttributeNotCached);
                }

                return thisVal;
            }
            else
            {
                // Facility is not in cache, need to handle this appropriately here
                throw new NotImplementedException();
            }
        }

        public List<string> GetCachedAttributeIds()
        {
            return _cachedAttributeIds.ToList();
        }
    }
}
