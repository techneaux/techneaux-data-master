﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper.Points;

namespace Techneaux.CygNetWrapper.Facilities
{
    public class FacilityPointCache
    {
        private readonly ICygNetFacility _myParentFac;
        public FacilityPointCache(ICygNetFacility parentFac)
        {
            _myParentFac = parentFac;
        }

        public List<ICachedCygNetPoint> GetAllPointsForFacility()
        {
            return .GetCachedPointsForFac(_myParentFac, null).ToList();
        }

        public bool TryGetPoint(string udc, out ICachedCygNetPoint result)
        {
            var udcList = new List<string> { udc };
            var pointsFound = MyCachingService.GetCachedPointsForFac(_myParentFac, udcList).ToList();

            if (pointsFound.Any())
            {
                result = pointsFound.First();
                return true;
            }
            else
            {
                result = null;
                return false;
            }
        }

        public async Task<List<ICachedCygNetPoint>> GetAllExistingPointsForFacility(CancellationToken ct)
        {
            return await MyCachingService.GetPointsForFacility(_myParentFac, null, ct);
        }
        

        public async Task<ICachedCygNetPoint> TryGetPointAsync(string udc, CancellationToken ct)
        {
            var udcList = new List<string>() { udc };
            var pointsFound = await MyCachingService.GetPointsForFacility(_myParentFac, udcList, ct);

            return pointsFound.FirstOrDefault();
        }
    }
}
