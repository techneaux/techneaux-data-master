﻿namespace Techneaux.CygNetWrapper.Facilities
{

    public class LinkedFacility
    {
        public string linkingAttribute { get; }
        public ICygNetFacility Facility { get; }

        public int? Ordinal { get; }
        public LinkedFacility(
            string newParentAttr,
            ICygNetFacility newCygFac)
        {
            linkingAttribute = newParentAttr;
            Facility = newCygFac;
        }

        public LinkedFacility(
            string newParentAttr,
            string childOrdAttr,
            ICygNetFacility newCygFac)
        {
            linkingAttribute = newParentAttr;
            Facility = newCygFac;

            // Try to get ordinal
            // -- Get atttribute value if possible
            FacilityAttributeValue OrdAttrVal;
            if (Facility.TryGetAttributeValue(childOrdAttr, out OrdAttrVal))
            {
                int ord;
                if (OrdAttrVal.TryGetInt(out ord))
                {
                    Ordinal = ord;
                }
            }
        }

        public override bool Equals(object obj)
        {
            LinkedFacility ThisLinkedFac = (LinkedFacility)obj;

            return this.linkingAttribute == ThisLinkedFac.linkingAttribute
                && this.Facility.Equals(ThisLinkedFac.Facility);
        }

        public override int GetHashCode()
        {
            return ($"{linkingAttribute}{Facility.Tag.ToString()}").GetHashCode();
        }

        public static bool operator ==(LinkedFacility leftFacility, LinkedFacility rightFacility)
        {
            return (leftFacility.Equals(rightFacility));
        }

        public static bool operator !=(LinkedFacility leftFacility, LinkedFacility rightFacility)
        {
            return (!leftFacility.Equals(rightFacility));
        }
    }


}