﻿using CygNet.Data.Core;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog.Context;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.DDS;
using Techneaux.CygNetWrapper.Services.FAC;
using Techneaux.CygNetWrapper.Services.PNT;
using Techneaux.CygNetWrapper.Services.TRS;
using XmlDataModelUtility;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace CygNetRuleModel.Models
{
    public static class CygNetCachedFacilityPointModel
    {
        public static async Task<CygNetDomain> GetDomain(bool useCurrentDomain, ushort fixedDomainId, CancellationToken ct)
        {
            CygNetDomain thisDomain;
            try
            {
                if (useCurrentDomain)
                {
                    thisDomain = await Task.Run(() => CygNetDomain.GetCurrentDomainAsync());
                }
                else
                {
                    thisDomain = CygNetDomain.GetDomainAsync(fixedDomainId);
                }
                await Task.Run(() => thisDomain.RefreshSiteServiceInfoAsync(ct));
                return thisDomain;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception getting domain");
            }
            return null;
        }

        public static async Task<(FacilityService, PointService, TableReferenceService)> GetAssociatedServices(DeviceDefinitionService ddsService)
        {
            try
            {
                if (ddsService == null)
                    return (null, null, null);
                var myUis = await ddsService.GetAssociatedUis();

                return (await myUis.GetAssociatedFac(), await myUis.GetAssociatedPnt(), await myUis.GetAssociatedTRS());

            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Warning(ex, "Unhandled exception while getting associated services from DDS Service ({dds}).", ddsService?.DomainSiteServiceName);

                return (null, null, null);
            }
        }

        public static async Task<List<CachedCygNetFacility>> GetFacList(FacilityService facService, FacilityFilteringOptions filterOpts, CancellationToken ct)
        {
            //var tempOpts = filterOpts.Copy();

            // Make a copy of the config
            var facFilterOpts = filterOpts.Copy();

            var cachedFacs = new List<CachedCygNetFacility>();
            try
            {
                if (facService != null && facFilterOpts != null && facFilterOpts.BaseRules.Any(filter => !string.IsNullOrWhiteSpace(filter.AttributeId)))
                {
                    ct.ThrowIfCancellationRequested();
                    cachedFacs = await Task.Run(() => facService.GetCachedFacilitiesWithChildrenAndParents(facFilterOpts.BaseRules, facFilterOpts.ChildReferenceAttrKey.Id, ct));
                    return cachedFacs;
                }
            }
            catch (OperationCanceledException)
            {
                Log.Debug($"{nameof(GetFacList)} Cancelled");
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception getting facility list on service ({fac}) with filters: /n {@filter}", facService?.DomainSiteServiceName, filterOpts);
            }

            return cachedFacs;
        }
    }
}
