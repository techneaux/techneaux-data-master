﻿using CygNet.Data.Core;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog.Context;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.DDS;
using Techneaux.CygNetWrapper.Services.FAC;
using Techneaux.CygNetWrapper.Services.PNT;
using Techneaux.CygNetWrapper.Services.TRS;
using XmlDataModelUtility;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;
using TechneauxReportingDataModel.CygNet;
using static TechneauxReportingDataModel.CygNet.StatusBitMetadata;

namespace CygNetRuleModel.Models
{
    public static class CygNetCachedAlarmModel
    {

        public static async Task<List<PointScheme>> GetPointSchemes(CancellationToken ct)
        {
            try
            {
                var PointStat = new CygNet.API.Core.PointMetadata();
                PointStat.LoadAmbientMetadata(out _, errors: out _, warnings: out _);
                var schemeList = PointStat.GetPointSchemes();
                return schemeList.ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Exception getting point schemes.");
                return new List<PointScheme>();
            }

        }
        public static async Task<StatusBitMetadata> GetStatusMetadata(CancellationToken ct)
        {
            try
            {
                var cxPntMetaData = new CXSCRIPTLib.PointMetadata();
                var statusBitArray = new StatusBitMetadata(cxPntMetaData.GetStatusBitMetadata());
                if (statusBitArray == null)
                {
                    return new StatusBitMetadata();
                }
                return statusBitArray;

            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception getting status bit metadata.");

                return new StatusBitMetadata();
            }

        }

        public static async Task<List<BitMetadata>> GetBitIdList(PointScheme pointScheme, PointTypes pointType, StatusBitMetadata statusBitMetadata, CancellationToken ct)
        {
            try
            {
                if (statusBitMetadata == null)
                    return new List<BitMetadata>();

                return await statusBitMetadata.GetBitIdList(pointScheme, pointType);
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception getting status bit Id list.");
               
                return new List<BitMetadata>();
            }

        }
    }
}
