using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using TechneauxUtility;
using static TechneauxReportingDataModel.CygNet.FacilityPointOptions.Helper.HistoryNormalization;

namespace CygNetRuleModel.Resolvers.History
{
    public partial class HistoryNormalization
    {
        private readonly PointHistoryNormalizationOptions _normRules;
        private DateTime _periodStart;
        private readonly DateTime _periodEnd;
        private List<CygNetHistoryEntry> _pointHistoryData;
        private double _critTsTimeoutMins = 60;

        public HistoryNormalization(PointHistoryNormalizationOptions normalizationOpts, DateTime beginning, DateTime end, double critTsTimeoutMins)
        {

            _periodStart = beginning;
            _periodEnd = end;
            _normRules = normalizationOpts;
            _timespanBetweenNormEntries = normalizationOpts.TimespanBetweenNormEntries;
            _critTsTimeoutMins = critTsTimeoutMins;
        }

        public async Task<List<NormalizedHistoryEntry>> Normalize(List<CygNetHistoryEntry> rawValues)
        {
            return await Normalize(rawValues, false, false);
        }

        public async Task<List<NormalizedHistoryEntry>> Normalize(List<CygNetHistoryEntry> rawValues,bool hasHistLaterThanInterval, bool isRollup)
        {
            if(!rawValues.IsAny())
                return new List<NormalizedHistoryEntry>();
            
            _pointHistoryData = rawValues;
 
            var srcData = rawValues.OrderBy(val => val.AdjustedTimestamp).ToList();
            IEnumerable<NormalizedHistDate> normTimestamps;
            if (isRollup)
            {
                normTimestamps = _normRules.GetNormalizedRollupTimestampsInWindow(_periodStart.AddMilliseconds(1), _periodEnd);
            }
            else
            {
                normTimestamps = _normRules.GetNormalizedTimestampsInWindow(_periodStart, _periodEnd);
            }            
            
            var latestLocalTimestamp = srcData.Last().AdjustedTimestamp;

            var normTsWithSourceHist = normTimestamps.Select(ts => new NormDateEntriesInRange(ts)).ToList();

            //await Task.Run(() => GetSourceRawValuesInRangeForNormTimestamps(srcData, normTsWithSourceHist));
            if(isRollup)
            {
                await Task.Run(() => GetSourceRawValuesInRangeForRollupNormTs(srcData, normTsWithSourceHist));
            }
            else
            {
                await Task.Run(() => GetSourceRawValuesInRangeForNormTs(srcData, normTsWithSourceHist));
            }            
            
            var normalizedHistoryData = await Task.Run(() => normTsWithSourceHist
                .Select(nts => new NormalizedHistoryEntry(
                    nts.NormTimestamp.TargetTimestamp,
                    nts.SrcItems,
                    nts.LastPointBeforeValidWindows,
                    (latestLocalTimestamp >= nts.NormTimestamp.LatestTimestampAllowed || hasHistLaterThanInterval),
                    _normRules.SelectedSampleType,
                    _timespanBetweenNormEntries,
                    latestLocalTimestamp, _critTsTimeoutMins))
                .ToList());

            return normalizedHistoryData;
        }

        //public static void GetSourceRawValuesExtendedForNormTimestamps(List<CygNetHistoryEntry> srcData, List<NormDateEntriesInRange> normTsWithSourceHist)
        //{
        //    foreach (var norm in normTsWithSourceHist)
        //    {
        //        var beforeList = new List<CygNetHistoryEntry>();
        //        foreach (var data in srcData)
        //        {
        //            if (data.AdjustedTimestamp < norm.NormTimestamp.EarliestTimestampAllowed)
        //            {
        //                beforeList.Add(data);
        //            }
        //        }
        //        beforeList = beforeList.OrderBy(val => val.AdjustedTimestamp).ToList();
        //        if (beforeList.IsAny())
        //        {
        //            norm.SrcItems.Add(beforeList.Last());
        //        }
        //    }

        //    var startIndex = 0;
        //    foreach (var pnt in srcData)
        //    {
        //        var foundFirst = false;

        //        if (startIndex >= normTsWithSourceHist.Count - 1)
        //            break;

        //        if (pnt.AdjustedTimestamp <= normTsWithSourceHist[startIndex].NormTimestamp.EarliestTimestampAllowed)
        //            continue;

        //        for (var i = startIndex; i < normTsWithSourceHist.Count; i++)
        //        {
        //            var thisNt = normTsWithSourceHist[i];

        //            //thisNt.SrcItemsExtended.Add(pnt);
        //            if (thisNt.TryAdd(pnt))
        //            {
        //                foundFirst = true;
        //            }
        //            else
        //            {
        //                if (foundFirst)
        //                {
        //                    break;
        //                }
        //                else
        //                {
        //                    startIndex += 1;
        //                }
        //            }
        //        }
        //    }

        //}
        
        public static void GetSourceRawValuesInRangeForRollupNormTs(List<CygNetHistoryEntry> srcData, List<NormDateEntriesInRange> normTsWithSourceHist)
        {
            var nextStartingRawIndex = 0;
            for (var ntsIndex = 0; ntsIndex < normTsWithSourceHist.Count; ntsIndex++)
            {
                var thisNts = normTsWithSourceHist[ntsIndex];
                for (var start = nextStartingRawIndex; start < srcData.Count; start++)
                {
                    var thisRaw = srcData[start];
                    if (!thisNts.IsWithinEarlyBoundaryRollup(thisRaw))
                    {
                        nextStartingRawIndex = start;
                        thisNts.LastPointBeforeValidWindows = thisRaw;
                    }
                    else
                    {
                        //if (ntsIndex < normTsWithSourceHist.Count - 1)
                        //{
                        //    var nextNts = normTsWithSourceHist[ntsIndex + 1];
                        //    for (int j = start; j < srcData.Count; j++)
                        //    {
                        //        if (!nextNts.IsWithinEarlyBoundary(thisRaw))
                        //        {
                        //            nextStartingRawIndex = start;
                        //            nextNts.LastPointBeforeValidWindows = thisRaw;
                        //        }

                        //        if (!thisNts.TryAdd(srcData[j]))
                        //            break;
                        //    }
                        //}
                        //else
                        //{
                        for (int j = start; j < srcData.Count; j++)
                        {
                            if (!thisNts.TryAddRollup(srcData[j]))
                                break;
                        }
                        //}

                        break;
                    }
                }
            }
        }

        public static void GetSourceRawValuesInRangeForNormTs(List<CygNetHistoryEntry> srcData, List<NormDateEntriesInRange> normTsWithSourceHist)
        {
            var nextStartingRawIndex = 0;
            for (var ntsIndex = 0; ntsIndex < normTsWithSourceHist.Count; ntsIndex++)
            {
                var thisNts = normTsWithSourceHist[ntsIndex];
                for (var start = nextStartingRawIndex; start < srcData.Count; start++)
                {
                    var thisRaw = srcData[start];
                    if (!thisNts.IsWithinEarlyBoundary(thisRaw))
                    {
                        nextStartingRawIndex = start;
                        thisNts.LastPointBeforeValidWindows = thisRaw;
                    }
                    else
                    {
                        //if (ntsIndex < normTsWithSourceHist.Count - 1)
                        //{
                        //    var nextNts = normTsWithSourceHist[ntsIndex + 1];
                        //    for (int j = start; j < srcData.Count; j++)
                        //    {
                        //        if (!nextNts.IsWithinEarlyBoundary(thisRaw))
                        //        {
                        //            nextStartingRawIndex = start;
                        //            nextNts.LastPointBeforeValidWindows = thisRaw;
                        //        }

                        //        if (!thisNts.TryAdd(srcData[j]))
                        //            break;
                        //    }
                        //}
                        //else
                        //{
                            for (int j = start; j < srcData.Count; j++)
                            {
                                if (!thisNts.TryAdd(srcData[j]))
                                    break;
                            }
                        //}

                        break;
                    }
                }
            }
        }


        private readonly TimeSpan _timespanBetweenNormEntries;        
    }
}

