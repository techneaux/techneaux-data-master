﻿using CygNet.Data.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using TechneauxReportingDataModel.General;

namespace CygNetRuleModel.Resolvers.History
{
    public partial class HistoryNormalization
    {
        public class NormalizedHistoryEntry
        {
            public NormalizedHistoryEntry(
                DateTime normalizedTimestamp,
                IEnumerable<CygNetHistoryEntry> srcRawEntriesInWindow,
                CygNetHistoryEntry firstEntryBeforeValidWindows,
                bool laterValueExists,                
                PointHistoryNormalizationOptions.SamplingType sampleType,
                TimeSpan windowTimespan,
                DateTime latestLocalTimestamp,
                double critTsTimeoutMins)
            {
                LatestRawTimestamp = latestLocalTimestamp;
                NormalizedTimestamp = normalizedTimestamp;
                SourceRawHistoryEntries = srcRawEntriesInWindow.ToList();
                //SourceRawHistoryEntriesExtended = srcRawEntriesExtended.ToList();
                SampleType = sampleType;
                WindowTimespan = windowTimespan;
                CriticalTimestampTimeoutMinutes = critTsTimeoutMins;
                SingleWindowStartTime = NormalizedTimestamp - windowTimespan;
                SingleWindowEndTime = NormalizedTimestamp + windowTimespan;
                DoubleWindowStartTime = NormalizedTimestamp - windowTimespan - windowTimespan;
                DoubleWindowEndTime = NormalizedTimestamp + windowTimespan + windowTimespan;

                var allowExtended =
                    sampleType == PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before_Extended ||
                    sampleType == PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before_After_Extended ||
                    sampleType == PointHistoryNormalizationOptions.SamplingType.Rollup_Simple_Average_Extended;                       

                switch (sampleType)
                {
                    case PointHistoryNormalizationOptions.SamplingType.Rollup_Simple_Average_Extended:
                        if(TryGetRollupSimpleAverageExtended(
                            normalizedTimestamp,
                            SingleRollupWindowEntries,
                            firstEntryBeforeValidWindows,
                            out var normHistSimpleAverageRollupExtended))
                        {
                            IsValid = true;
                            NormalizedHistoryValue = normHistSimpleAverageRollupExtended;
                            if (allowExtended)
                            {
                                if (!laterValueExists)
                                {
                                    CriticalTimestamp = NormalizedTimestamp;
                                }
                            }
                            else
                            {
                                if (!laterValueExists && !SourceRawHistoryEntries.Any(hist => hist.AdjustedTimestamp >= SingleWindowEndTime))
                                {
                                    CriticalTimestamp = NormalizedTimestamp;
                                }
                            }
                        }
                        break;
                            
                    case PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before_After_Extended:
                    case PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before_After:
                        if (TryGetNearestBeforeOrAfter(
                            normalizedTimestamp,
                            SourceRawHistoryEntries,
                            allowExtended ? firstEntryBeforeValidWindows : null,
                            out var normHistResult))
                        {
                            IsValid = true;
                            NormalizedHistoryValue = normHistResult.NewEntry;
                            if(allowExtended)
                            {
                                if (!laterValueExists && normHistResult.SrcEntry.AdjustedTimestamp < NormalizedTimestamp)
                                {
                                    if (NormalizedTimestamp - normHistResult.SrcEntry.AdjustedTimestamp > (windowTimespan + windowTimespan))
                                    {
                                        CriticalTimestamp = DoubleWindowEndTime;
                                    }
                                    else
                                    {
                                        CriticalTimestamp = NormalizedTimestamp.Add(NormalizedTimestamp - normHistResult.SrcEntry.AdjustedTimestamp);
                                    }
                                }
                            }
                            else
                            {
                                if (!laterValueExists && normHistResult.SrcEntry.AdjustedTimestamp < NormalizedTimestamp)
                                {
                                    CriticalTimestamp = NormalizedTimestamp.Add(NormalizedTimestamp - normHistResult.SrcEntry.AdjustedTimestamp);
                                }
                            }                        
                        }
                        break;
                    case PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before_Extended:
                    case PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before:
                        if (TryGetNearestBefore(
                            normalizedTimestamp,
                            SourceRawHistoryEntries,
                            allowExtended ? firstEntryBeforeValidWindows : null,
                            out var normHistBefore))
                        {
                            IsValid = true;
                            NormalizedHistoryValue = normHistBefore;

                            //if(allowExtended)
                            //    break;

                            if (!laterValueExists && normHistBefore.AdjustedTimestamp <= NormalizedTimestamp)
                            {
                                CriticalTimestamp = NormalizedTimestamp;
                            }
                        }
                        break;
                    
                    case PointHistoryNormalizationOptions.SamplingType.Take_Nearest_After:
                        if (TryGetNearestAfter(
                           normalizedTimestamp,
                           SourceRawHistoryEntries,
                           out var normHistAfter))
                        {
                            IsValid = true;
                            NormalizedHistoryValue = normHistAfter;
                        }

                        break;
                    case PointHistoryNormalizationOptions.SamplingType.Simple_Average:
                        if (TryGetSimpleAverage(
                            normalizedTimestamp,
                            SingleWindowEntries,
                            out var normHistSimpleAveSingle))
                        {
                            // Single window interval

                            IsValid = true;
                            NormalizedHistoryValue = normHistSimpleAveSingle;

                            if (!laterValueExists &&
                                !SourceRawHistoryEntries.Any(hist => hist.AdjustedTimestamp >= SingleWindowEndTime))
                            {
                                CriticalTimestamp = SingleWindowEndTime;
                            }
                        }
                        else
                        {
                            // Double window interval

                            if (TryGetSimpleAverage(
                            normalizedTimestamp,
                            SourceRawHistoryEntries,
                            out var normHistSimpleAveDouble))
                            {
                                IsValid = true;
                                NormalizedHistoryValue = normHistSimpleAveDouble;

                                if (!laterValueExists)
                                {
                                    CriticalTimestamp = DoubleWindowEndTime;
                                }
                            }
                        }

                        break;
                    case PointHistoryNormalizationOptions.SamplingType.Weighted_Average:
                        if (TryGetWeightedAverage(
                                normalizedTimestamp,
                                SingleWindowEndTime,
                                SingleWindowEntries,
                                out var normHistWeightedAveSingle))
                        {
                            // Single window interval

                            IsValid = true;
                            NormalizedHistoryValue = normHistWeightedAveSingle;

                            if (!laterValueExists &&
                                !SourceRawHistoryEntries.Any(hist => hist.AdjustedTimestamp >= SingleWindowEndTime))
                            {
                                CriticalTimestamp = SingleWindowEndTime;
                            }
                        }
                        else
                        {
                            // Double window interval

                            if (TryGetWeightedAverage(
                                normalizedTimestamp,
                                DoubleWindowEndTime,
                                SourceRawHistoryEntries,
                            out var normHistWeightedAveDouble))
                            {
                                IsValid = true;
                                NormalizedHistoryValue = normHistWeightedAveDouble;

                                if (!laterValueExists)
                                {
                                    CriticalTimestamp = DoubleWindowEndTime;
                                }
                            }
                        }

                        break;
                    case PointHistoryNormalizationOptions.SamplingType.Linear_Interpolation:
                        var TryGetResults = TryGetInterpolated(
                           normalizedTimestamp,
                           SingleWindowEntries,
                           out var normHistInterpSingle);
                        bool success = TryGetResults.Item1;
                        if(success)
                        {
                            // Single window interval

                            IsValid = true;
                            NormalizedHistoryValue = normHistInterpSingle;
                        }
                        else
                        {
                            // Double window interval
                            var TryGetResultsDouble = TryGetInterpolated(
                            normalizedTimestamp,
                            SourceRawHistoryEntries,
                            out var normHistInterpDouble);
                            success = TryGetResultsDouble.Item1;
                            if (success)
                            {
                                IsValid = true;
                                NormalizedHistoryValue = normHistInterpDouble;
                                if(!laterValueExists && TryGetResultsDouble.Item2 != DateTime.MinValue)
                                {
                                    CriticalTimestamp = TryGetResultsDouble.Item2;
                                }
                            }
                        }
                        break;
                    default:
                        throw new NotImplementedException($"Sampling type [{sampleType.ToString()}] not implemented");
                };

                // If later val exists, then there is never a critical timestamp
                if (laterValueExists)
                    CriticalTimestamp = null;
            }

            //private bool TryGetSingleOrDoubleWindowSimpleAverage()
            //{

            //}

            public DateTime NormalizedTimestamp { get; }

            public bool IsValueUncertain
            {
                get
                {
                    if (CriticalTimestamp == null)
                        return false;
                    if(CriticalTimestamp <= LatestRawTimestamp)                    
                        return false;
                    if (CriticalTimestamp.Value.AddMinutes(CriticalTimestampTimeoutMinutes) <= DateTime.Now)
                        return false;
                    return true;
                }
            }
               

            public DateTime? CriticalTimestamp { get; } = null;

            private double CriticalTimestampTimeoutMinutes { get; set; } = 60;

            public List<CygNetHistoryEntry> SourceRawHistoryEntries { get; }

            public List<CygNetHistoryEntry> SourceRawHistoryEntriesExtended { get; }
            
            public DateTime SingleWindowStartTime { get; }
            public DateTime SingleWindowEndTime { get; }

            public DateTime DoubleWindowStartTime { get; }
            public DateTime DoubleWindowEndTime { get; }

            public IEnumerable<CygNetHistoryEntry> SingleWindowEntries => SourceRawHistoryEntries
                .SkipWhile(hist => hist.AdjustedTimestamp < SingleWindowStartTime)
                .TakeWhile(hist => hist.AdjustedTimestamp < SingleWindowEndTime);


            public IEnumerable<CygNetHistoryEntry> SingleRollupWindowEntries => SourceRawHistoryEntries
                .SkipWhile(hist => hist.AdjustedTimestamp < SingleWindowStartTime)
                .TakeWhile(hist => hist.AdjustedTimestamp <= NormalizedTimestamp);

            public bool IsValid { get; } = false;

            public DateTime LatestRawTimestamp { get; }

            public TimeSpan WindowTimespan { get; }

            public PointHistoryNormalizationOptions.SamplingType SampleType { get; }

            public CygNetHistoryEntry NormalizedHistoryValue { get; } = null;

            // Static methods

            public static bool TryGetNearestBefore(
                DateTime targetTime,
                IEnumerable<CygNetHistoryEntry> srcVals,
                CygNetHistoryEntry mostRecentValBeforeWindows,
                out CygNetHistoryEntry result)
            {
                var last = srcVals
                    .TakeWhile(hist => hist.AdjustedTimestamp <= targetTime)
                    .LastOrDefault();
                
                if (last == null && mostRecentValBeforeWindows != null)
                    last = mostRecentValBeforeWindows;

                if (last != null)
                {
                    result = new CygNetHistoryEntry()
                    {
                        RawValue = last.RawValue,
                        BaseStatus = last.BaseStatus,
                        UserStatus = last.UserStatus,
                        RawTimestamp = targetTime,
                        ServerUtcOffset = last.ServerUtcOffset
                    };
                }
                else
                {
                    result = null;
                }
                return result != null;
            }
     

            public static bool TryGetNearestAfter(
               DateTime targetTime,
               IEnumerable<CygNetHistoryEntry> srcVals,
               out CygNetHistoryEntry result)
            {
                var first = srcVals.FirstOrDefault(hist => hist.AdjustedTimestamp >= targetTime);

                if (first != null)
                {
                    result = new CygNetHistoryEntry()
                    {
                        RawValue = first.RawValue,
                        BaseStatus = first.BaseStatus,
                        UserStatus = first.UserStatus,
                        RawTimestamp = targetTime,
                        ServerUtcOffset = first.ServerUtcOffset
                    };
                }
                else
                {
                    result = null;
                }

                return result != null;
            }

            public static bool TryGetNearestBeforeOrAfter(
                DateTime targetTime,
                IEnumerable<CygNetHistoryEntry> srcVals,
                CygNetHistoryEntry mostRecentValBeforeWindows,
                out (CygNetHistoryEntry NewEntry, CygNetHistoryEntry SrcEntry) normHistResult)
            {
                var temp = srcVals
                    .OrderBy(hist => Math.Abs((targetTime - hist.AdjustedTimestamp).TotalHours));
                var srcHistEntry = temp.FirstOrDefault();

                if (srcHistEntry == null && mostRecentValBeforeWindows != null)
                    srcHistEntry = mostRecentValBeforeWindows;
                
                CygNetHistoryEntry normHistEntry = null;                

                if (srcHistEntry != null)
                {
                    normHistEntry = new CygNetHistoryEntry()
                    {
                        RawValue = srcHistEntry.RawValue,
                        BaseStatus = srcHistEntry.BaseStatus,
                        UserStatus = srcHistEntry.UserStatus,
                        RawTimestamp = targetTime,
                        ServerUtcOffset = srcHistEntry.ServerUtcOffset
                    };
                }

                normHistResult = (normHistEntry, srcHistEntry);

                return normHistEntry != null;
            }

            public static bool TryGetSimpleAverage(
                DateTime targetTime,
                IEnumerable<CygNetHistoryEntry> srcVals,
                out CygNetHistoryEntry result)
            {
                // Check for entry on the timestamp
                var numericVals = srcVals.Where(hist => hist.IsNumeric).ToList();

                var matchingVal = numericVals.FirstOrDefault(hist => hist.AdjustedTimestamp == targetTime);
                if (matchingVal != null)
                {
                    result = matchingVal;
                    return true;
                }

                // Make sure there are values before and after
                if (numericVals.Any(hist => hist.AdjustedTimestamp < targetTime) &&
                    numericVals.Any(hist => hist.AdjustedTimestamp > targetTime))
                {
                    var ave = numericVals.Average(hist => hist.NumericValue);
                    var newBaseStatus = numericVals.Select(hist => hist.BaseStatus).Aggregate((x, y) => x | y);
                    var newUserStatus = numericVals.Select(hist => hist.UserStatus).Aggregate((x, y) => x | y);

                    result = new CygNetHistoryEntry()
                    {
                        RawValue = ave,
                        BaseStatus = newBaseStatus,
                        UserStatus = newUserStatus,
                        RawTimestamp = new DateTime(targetTime.Ticks, DateTimeKind.Local),
                        ServerUtcOffset = numericVals.First().ServerUtcOffset
                    };

                    return true;
                }
                else
                {
                    result = null;
                    return false;
                }
            }

            public static bool TryGetRollupSimpleAverageExtended(
                DateTime targetTime,
                IEnumerable<CygNetHistoryEntry> srcVals,
                CygNetHistoryEntry mostRecentValBeforeWindows,
                out CygNetHistoryEntry result)
            {
                var numericVals = srcVals.Where(hist => hist.IsNumeric).ToList();
                CygNetHistoryEntry mostRecentNumeric = null;
                if(mostRecentValBeforeWindows != null)
                {
                    if(mostRecentValBeforeWindows.IsNumeric)
                    {
                        mostRecentNumeric = mostRecentValBeforeWindows;
                    }
                }
                // Make sure there are values before and after
                if (numericVals.Any(hist => hist.AdjustedTimestamp < targetTime))
                {
                    var ave = numericVals.Average(hist => hist.NumericValue);
                    var newBaseStatus = numericVals.Select(hist => hist.BaseStatus).Aggregate((x, y) => x | y);
                    var newUserStatus = numericVals.Select(hist => hist.UserStatus).Aggregate((x, y) => x | y);
                    result = new CygNetHistoryEntry()
                    {
                        RawValue = ave,
                        BaseStatus = newBaseStatus,
                        UserStatus = newUserStatus,
                        RawTimestamp = new DateTime(targetTime.Ticks, DateTimeKind.Local),
                        ServerUtcOffset = numericVals.First().ServerUtcOffset
                    };

                    return true;
                }
                else
                {
                    if (mostRecentNumeric!= null)
                    {
                        //result = mostRecentValBeforeWindows;
                        result = new CygNetHistoryEntry()
                        {
                            RawValue = mostRecentNumeric.RawValue,
                            BaseStatus = mostRecentNumeric.BaseStatus,
                            UserStatus = mostRecentNumeric.UserStatus,
                            RawTimestamp = new DateTime(targetTime.Ticks, DateTimeKind.Local),
                            ServerUtcOffset = mostRecentNumeric.ServerUtcOffset
                        };
                        return true;
                    }
                    else
                    {
                        result = null;
                        return false;
                    }
                        
                    
                    return false;
                }
            }

            public static bool TryGetWeightedAverage(
               DateTime targetTime,
               DateTime endTime,
               IEnumerable<CygNetHistoryEntry> srcVals,
               out CygNetHistoryEntry result)
            {
                // Check for entry on the timestamp
                var numericVals = srcVals.Where(hist => hist.IsNumeric).ToList();

                var matchingVal = numericVals.FirstOrDefault(hist => hist.AdjustedTimestamp == targetTime);
                if (matchingVal != null)
                {
                    result = matchingVal;
                    return true;
                }

                // Make sure there are values before and after
                if (numericVals.Any(hist => hist.AdjustedTimestamp < targetTime) &&
                    numericVals.Any(hist => hist.AdjustedTimestamp > targetTime))
                {
                    double runningWeightedSum = 0;

                    var valList = numericVals.ToList();
                    for (var i = 0; i < valList.Count; i++)
                    {
                        if (i == valList.Count - 1)
                        {
                            runningWeightedSum += valList[i].NumericValue * (endTime - valList[i].AdjustedTimestamp).TotalHours;
                        }
                        else
                        {
                            runningWeightedSum += valList[i].NumericValue * (valList[i + 1].AdjustedTimestamp - valList[i].AdjustedTimestamp).TotalHours;
                        }
                    }

                    var ave = runningWeightedSum / (endTime - valList.First().AdjustedTimestamp).TotalHours;

                    var newBaseStatus = numericVals.Select(hist => hist.BaseStatus).Aggregate((x, y) => x | y);
                    var newUserStatus = numericVals.Select(hist => hist.UserStatus).Aggregate((x, y) => x | y);

                    result = new CygNetHistoryEntry()
                    {
                        RawValue = ave,
                        BaseStatus = newBaseStatus,
                        UserStatus = newUserStatus,
                        RawTimestamp = new DateTime(targetTime.Ticks, DateTimeKind.Local),
                        ServerUtcOffset = numericVals.First().ServerUtcOffset
                    };

                    return true;
                }
                else
                {
                    result = null;
                    return false;
                }
            }

            public static (bool, DateTime) TryGetInterpolated(
              DateTime targetTime,
              IEnumerable<CygNetHistoryEntry> srcVals,
              out CygNetHistoryEntry result)
            {
                // Check for entry on the timestamp
                var numericVals = srcVals.Where(hist => hist.IsNumeric).ToList();

                var matchingVal = numericVals.FirstOrDefault(hist => hist.AdjustedTimestamp == targetTime);
                if (matchingVal != null)
                {
                    result = matchingVal;
                    return (true, DateTime.MinValue);
                }

                // Make sure there are values before and after
                var firstBefore = numericVals.FirstOrDefault(hist => hist.AdjustedTimestamp < targetTime);
                var firstAfter = numericVals.FirstOrDefault(hist => hist.AdjustedTimestamp > targetTime);

                if (firstBefore != null &&
                    firstAfter != null)
                {
                    var valBefore = firstBefore.NumericValue;
                    var valAfter = firstAfter.NumericValue;

                    var slope = (valAfter - valBefore) /
                        (firstAfter.AdjustedTimestamp - firstBefore.AdjustedTimestamp).TotalHours;

                    var interpVal = slope * (targetTime - firstBefore.AdjustedTimestamp).TotalHours + valBefore;

                    var newBaseStatus = firstBefore.BaseStatus | firstAfter.BaseStatus;
                    var newUserStatus = firstBefore.UserStatus | firstAfter.UserStatus;

                    result = new CygNetHistoryEntry()
                    {
                        RawValue = interpVal,
                        BaseStatus = newBaseStatus,
                        UserStatus = newUserStatus,
                        RawTimestamp = targetTime,
                        ServerUtcOffset = numericVals.First().ServerUtcOffset
                    };

                    return (true, firstAfter.AdjustedTimestamp);
                }
                else
                {
                    DateTime CritTimeStamp = new DateTime();
                    if (firstAfter == null)
                    {
                        CritTimeStamp = targetTime;
                    }
                    else
                    {
                        CritTimeStamp = firstAfter.AdjustedTimestamp;
                    }
                    result = null;
                    return (false, CritTimeStamp); 
                }
            }
        }
    }
}

