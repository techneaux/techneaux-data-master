﻿using System.Collections.Generic;
using Techneaux.CygNetWrapper.Services.VHS;
using static TechneauxReportingDataModel.CygNet.FacilityPointOptions.Helper.HistoryNormalization;

namespace CygNetRuleModel.Resolvers.History
{
    public partial class HistoryNormalization
    {
        public class NormDateEntriesInRange
        {
            public NormDateEntriesInRange(NormalizedHistDate srcTs)
            {
                NormTimestamp = srcTs;
            }

            public NormalizedHistDate NormTimestamp { get; }
            public List<CygNetHistoryEntry> SrcItems { get; } = new List<CygNetHistoryEntry>();
            public CygNetHistoryEntry LastPointBeforeValidWindows { get; set; }

           // public List<CygNetHistoryEntry> SrcItemsExtended { get; } = new List<CygNetHistoryEntry>();

            public bool IsWithinEarlyBoundary(CygNetHistoryEntry pnt) => 
                pnt.AdjustedTimestamp > NormTimestamp.EarliestTimestampAllowed;

            public bool IsWithinEarlyBoundaryRollup(CygNetHistoryEntry pnt) =>
                pnt.AdjustedTimestamp >= NormTimestamp.EarliestTimestampAllowed;
      
            public bool IsWithinLaterBoundary(CygNetHistoryEntry pnt) =>
                pnt.AdjustedTimestamp <= NormTimestamp.LatestTimestampAllowed;

            public bool TryAdd(CygNetHistoryEntry pnt)
            {
                if (IsWithinEarlyBoundary(pnt)
                    && IsWithinLaterBoundary(pnt))
                {
                    SrcItems.Add(pnt);
                    return true;
                }

                //if (!IsWithinEarlyBoundary(pnt))
                //{
                //    LastPointBeforeValidWindows = pnt;
                //}

                return false;
            }
            public bool TryAddRollup(CygNetHistoryEntry pnt)
            {
                if (IsWithinEarlyBoundaryRollup(pnt)
                    && IsWithinLaterBoundary(pnt))
                {
                    SrcItems.Add(pnt);
                    return true;
                }

                //if (!IsWithinEarlyBoundary(pnt))
                //{
                //    LastPointBeforeValidWindows = pnt;
                //}

                return false;
            }
        }
    }
}

