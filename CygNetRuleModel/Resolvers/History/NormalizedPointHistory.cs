using CygNet.Data.Core;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog.Context;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace CygNetRuleModel.Resolvers.History
{
    public static class NormalizedPointHistory
    {
        // Static methods

        public static async Task<List<CygNetHistoryEntry>> GetPointHistory(ICachedCygNetPoint srcPoint,
            DateTime earliestDate,
            DateTime latestDate,
            int? maxEntries,
            bool includeUnreliable,
            bool includeNumericOnly,
            IProgress<int> progress,
            CancellationToken ct)
        {
            // Read history and return
            try
            {
                var cygHistory = maxEntries.HasValue ?
                    await srcPoint.GetLatestHistoryInInterval(earliestDate, latestDate, maxEntries, includeUnreliable, includeNumericOnly, ct) :
                    await srcPoint.GetHistory(earliestDate, latestDate, progress, includeUnreliable, includeNumericOnly, ct);

                if (cygHistory == null)
                    return new List<CygNetHistoryEntry>();

                //if (!includeUnreliable)
                //{
                //    cygHistory = cygHistory.Where(entry => !entry.BaseStatus.HasFlag(BaseStatusFlags.Unreliable)).ToList();
                //}
                //if(includeNumericOnly)
                //{
                //    cygHistory = cygHistory.Where(entry => entry.IsNumeric).ToList();
                //}                

                return cygHistory;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception trying to get point history from normalization methods");

                throw;
            }
        }

        public static async Task<List<CygNetHistoryEntry>> GetPointHistoryExtended(
          ICachedCygNetPoint srcPoint,
          DateTime earliestDate,
          DateTime latestDate,
          bool includeUnreliable,
          bool includeNumericOnly,
          int numEntries,
          CancellationToken ct)
        {
            // Read history and return
            try
            {
                var cygHistory = (await
                    srcPoint.GetLatestHistoryInInterval(
                        earliestDate,
                        latestDate,
                        numEntries,
                        includeUnreliable: includeUnreliable,
                        includeNumericOnly: includeNumericOnly,
                        ct));

                if (cygHistory == null)
                    return new List<CygNetHistoryEntry>();
                if (!includeUnreliable)
                {
                    cygHistory = cygHistory.Where(entry => !entry.BaseStatus.HasFlag(BaseStatusFlags.Unreliable)).ToList();
                }
                //cygHistory = cygHistory.Where(entry => entry.IsNumeric).ToList();
                return cygHistory;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception trying to get point history from normalization methods for reverse extended.");

                throw;
            }
        }
        //public static async Task<List<CygNetHistoryEntry>> GetNormalizedHistoryValuesAsync(
        //    List<CygNetHistoryEntry> rawHist,
        //    DateTime earliestDate,
        //    DateTime latestDate,
        //    PointHistoryNormalizationOptions normalizationOpts
        //    )
        //{
        //    List<CygNetHistoryEntry> normHist;
        //    if (rawHist.IsAny() && normalizationOpts.EnableNormalization != false)
        //    {
        //        var normalizer = new HistoryNormalization(normalizationOpts, earliestDate, latestDate);
        //        normHist = (await normalizer.Normalize(rawHist)).Where(hist => hist.IsValid && !hist.IsValueUncertain).Select(hist => hist.NormalizedHistoryValue).ToList();
        //    }
        //    else
        //    {
        //        normHist = null;
        //    }

        //    return normHist;
        //}
    }
}

// get on demand here



