﻿using System.Collections.Generic;
using CygNetRuleModel.Resolvers.History;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxUtility;

namespace CygNetRuleModel.Resolvers
{
    public class CygNetHistoryRow : ValueSet
    {
        public bool IsFromNormalized => SrcNormalizedHistoryEntry != null;

        public HistoryNormalization.NormalizedHistoryEntry SrcNormalizedHistoryEntry { get; set; }

        public CygNetHistoryEntry SrcRawHistoryEntry { get; set; }
    }
}
