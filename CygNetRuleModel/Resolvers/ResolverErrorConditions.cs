﻿using System.ComponentModel;

namespace CygNetRuleModel.Resolvers
{
    public static partial class CygNetResolver
    {
        public enum ResolverErrorConditions
        {
            [Description("Succeeded, no errors")] None,

            [Description("Parent facility was not found")]
            ParentFacNotAvailable,

            [Description("Validation Error: Child facility group id blank")]
            ChildGroupIdBlank,

            [Description("Child facility group id is invalid or not defined")]
            ChildInvalidGroupId,

            [Description("Facility does not have a linked FMS meter")]
            NoLinkedFmsMeter,

            [Description("Child facility not available because chosen ordinal was not found")]
            ChildFacOrdinalNotFound,

            [Description("Udc is blank or whitespace")]
            UdcBlank,

            [Description("UDC not found")] UdcNotFound,

            [Description("Point attribute not valid")]
            InvalidPointAttribute,

            [Description("Error retrieving point attribute value")]
            PointAttrValueRetrievalError,

            [Description("Error retrieving point current value")]
            PointCurrentValueRetrievalError,

            [Description("Facility attribute invalid or disabled")]
            InvalidFacAttribute,

            [Description("Error retrieving point attribute value")]
            FacAttrValueRetrievalError
        }
    }
}