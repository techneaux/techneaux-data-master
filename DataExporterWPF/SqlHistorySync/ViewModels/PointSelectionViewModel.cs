﻿using DataMasterClient.SqlHistorySync.ViewModels.Sub;
using DataMasterClient.ViewModels;
using DataMasterClient.ViewModels.CygNet.Reactive;
using ReactiveUI;
using ReactiveUI.Legacy;
using Serilog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Windows;
using CygNet.Data.Core;
using Techneaux.CygNetWrapper.Facilities;
using TechneauxHistorySynchronization.Models;
using TechneauxReportingDataModel.Helper;
using TechneauxUtility;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using DataMasterClient.Forms;

namespace DataMasterClient.SqlHistorySync.ViewModels
{
    public class PointSelectionViewModel : ReactiveObject
    {
        private readonly CygNetFacilityPointSearchViewModel _srcCygNetModel;


        public BindingList<SqlPointSelection> PointSelectionConfigRuleList { get; }

        private bool _firstRun;
        public bool FirstRun
        {
            get => _firstRun;
            set => this.RaiseAndSetIfChanged(ref _firstRun, value);
        }

        public PointSelectionViewModel(
            CygNetFacilityPointSearchViewModel srcCygNetModel,
            BindingList<SqlPointSelection> srcPointRules)
        {
            var sqlRuleComparer = new CompareRuleOnPointSelection();

            UniquePointSelectionRules = new HashSet<SqlPointSelection>(srcPointRules
                                .Where(rule => rule.PointSelection.IsRuleValid)).ToList();

            PointSelectionConfigRuleList = srcPointRules;

            RuleList = new RuleGridViewModel<SqlPointSelection, SqlPointSelectionGridViewModel>(srcPointRules);
            _srcCygNetModel = srcCygNetModel;

            Observable.FromEventPattern<ListChangedEventHandler, ListChangedEventArgs>
                (h => srcPointRules.ListChanged += h, h => srcPointRules.ListChanged -= h)
                .Where(args => args.EventArgs.ListChangedType == ListChangedType.ItemAdded ||
                        args.EventArgs.ListChangedType == ListChangedType.ItemDeleted ||
                        args.EventArgs.ListChangedType == ListChangedType.ItemChanged)
                .Select(args => (args.Sender as BindingList<SqlPointSelection>)
                                .Where(rule => rule.PointSelection.IsRuleValid))
                .Select(rules => new HashSet<SqlPointSelection>(rules, sqlRuleComparer))
                .DistinctUntilChanged()
                .Throttle(TimeSpan.FromSeconds(1), RxApp.MainThreadScheduler)
                .ObserveOnDispatcher()
                .Select(set => set.ToList())
                .Subscribe(res => { UniquePointSelectionRules = res; });

            var pointSrcDataChanged = this.WhenAnyValue(me => me.UniquePointSelectionRules, me => me._srcCygNetModel.CachedFacilities)
                .Select(args => (srcFacs: args.Item1, udcList: args.Item2))
                .Throttle(TimeSpan.FromSeconds(1), RxApp.MainThreadScheduler)
                .ObserveOnDispatcher()
                .Publish().RefCount();                            
           
            PointListUpdateCmd = ReactiveCommand
                .CreateFromObservable<(IEnumerable<SqlPointSelection> pointSelectRules, IEnumerable<CachedCygNetFacility> srcFacs),
                                        (List<CachedCygNetPointWithRule> Points, List<FacilityUdcResult> UdcResults, List<CachedCygNetPointWithRule> Duplicates)>
                    (args => Observable.StartAsync(ct => CygNetPointResolver.GetPointsAsync(UniquePointSelectionRules,
                        _srcCygNetModel.CachedFacilities,
                        _srcCygNetModel.CygNetOptsModel.FacilityFilteringRules.ChildGroups.ToList(),
                        _srcCygNetModel.CurrentFacilityService,
                        ct))
                .TakeUntil(pointSrcDataChanged));

            PointListUpdateCmd.ThrownExceptions.Subscribe(ex =>
            {
                Log.Error(ex, "Unhandled UI Exception while getting point list");
                MessageBox.Show(
                    "Unhandled UI Exception while getting point list, please contact Techneaux support");
            });

            pointSrcDataChanged.Subscribe(_ => PointListUpdateCmd.Execute().Subscribe());
           
            _duplicateList = PointListUpdateCmd
                .Select(results => results.Duplicates)
                .ToProperty(this, x=> x.DuplicateList, new List<CachedCygNetPointWithRule>());
            var duplicatesUpdated = this.WhenAnyValue(x => x.DuplicateList)
             .Publish()
             .RefCount();

            duplicatesUpdated.Subscribe(_ => UpdateDuplicates());
            
            _pointList = PointListUpdateCmd
                .Select(results => results.Points)
                .ToProperty(this, x => x.PointList, new List<CachedCygNetPointWithRule>());

            _udcList = this.WhenAnyValue(me => me.PointList)
                .Select(rules => new HashSet<string>(rules.Select(item => item.Tag.UDC)))
                .DistinctUntilChanged()
                .Take(50)
                .Select(set => set.ToList())
                .ObserveOnDispatcher()
                .ToProperty(this, x => x.UdcList, new List<string>());

            //        var pointSrcDataChanged = this.WhenAnyValue(me => me.UniquePointSelectionRules, me => me._srcCygNetModel.CachedFacilities)
            //.Select(args => (srcFacs: args.Item1, udcList: args.Item2))
            //.Throttle(TimeSpan.FromSeconds(1), RxApp.MainThreadScheduler)
            //.ObserveOnDispatcher()
            //.Publish().RefCount();

            //selectedRuleChanged.Subscribe(_ => );
            _selectedRuleUdcList = this.WhenAnyValue(me => me.PointList, me => me.RuleList.SelectedRule)
                .Where(x => x.Item2 != null)               
                .Throttle(TimeSpan.FromSeconds(1), RxApp.MainThreadScheduler)
                .Select(x => x.Item1.Where(y => y.SrcRule.PointSelection.Equals(x.Item2.MyDataModel.PointSelection)).Select(z => z.Tag.UDC).Distinct().OrderBy(q => q).ToList())
                .ToProperty(this, s => s.SelectedRuleUdcList);

            _facilityUdcGrid = PointListUpdateCmd
                .Select(results => results
                    .UdcResults
                    .ToObservable()
                    .Delay(TimeSpan.FromMilliseconds(10), RxApp.MainThreadScheduler)
                    .CreateCollection())
                .ToProperty(this, x => x.FacilityUdcGrid);

            _duplicatesExist = PointListUpdateCmd
                .Select(x => x.Duplicates.Count != 0)
                .ToProperty(this, x => x.DuplicatesExist, false);
            List<string> temp = new List<string>();

            _duplicatesMessage = PointListUpdateCmd
                .Select(x => x.Duplicates.Select(y => y.LongId).GetDuplicatesMessage(100))
                .ToProperty(this, x => x.DuplicatesMessage, "");

            // Build list of point selection view models here
            // Init point selection datagrid here
            UdcChooser.StaticDataContext = this;
        }

        public class CompareRuleOnPointSelection : IEqualityComparer<SqlPointSelection>
        {
            public bool Equals(SqlPointSelection x, SqlPointSelection y)
            {
                return x.PointSelection == y.PointSelection;
            }

            public int GetHashCode(SqlPointSelection obj)
            {
                return obj.PointSelection.GetHashCode();
            }
        }

        public ReactiveCommand<(IEnumerable<SqlPointSelection> pointSelectRules, IEnumerable<CachedCygNetFacility>
                srcFacs), (List<CachedCygNetPointWithRule> Points, List<FacilityUdcResult> UdcResults, List<CachedCygNetPointWithRule> Duplicates)>
            PointListUpdateCmd
        { get; set; }

   
        // Expose list of point selection viewmodels to data grid

        private List<SqlPointSelection> _uniquePointSelectionRules;
        public List<SqlPointSelection> UniquePointSelectionRules
        {
            get => _uniquePointSelectionRules;
            set => this.RaiseAndSetIfChanged(ref _uniquePointSelectionRules, value);
        }

        public void UpdateDuplicates()
        {
            Console.WriteLine("UpdateDups");
            foreach(var rule in RuleList.Rows)
            {
                var ruleDuplicates = new List<PointTag>();
                foreach(var dup in DuplicateList)
                {
                    if (rule.ConfiguredRuleView.MyDataModel.Equals(dup.SrcRule.PointSelection))
                    {
                        ruleDuplicates.Add(dup.Tag);
                    }
                }

                var udcs = ruleDuplicates.Select(tg => tg.UDC).Distinct().ToList();

                rule.ConfiguredRuleView.DuplicatesExist = ruleDuplicates.Count != 0 ? Visibility.Visible : Visibility.Collapsed;
                rule.ConfiguredRuleView.DuplicatesMessage =
                    "Some of the UDCs matching this point rule also match other point rules. \n Please adjust your point rules to eliminate duplicates. \n\n" +
                    $"There are {udcs.Count} duplicated UDCs found, covering {ruleDuplicates.Count} points. \n" +
                    $"Duplicate UDCs include: {string.Join(", ",udcs.Take(50))}";

                rule.ConfiguredRuleView.DuplicateUdcList = udcs;

                //ruleDuplicates.GetDuplicatesMessage(100);
            }

        }

        private readonly ObservableAsPropertyHelper<List<string>> _udcList;
        public List<string> UdcList => _udcList.Value;

        private readonly ObservableAsPropertyHelper<IReactiveDerivedList<FacilityUdcResult>> _facilityUdcGrid;
        public IReactiveDerivedList<FacilityUdcResult> FacilityUdcGrid => _facilityUdcGrid.Value;

        private readonly ObservableAsPropertyHelper<List<CachedCygNetPointWithRule>> _pointList;
        public List<CachedCygNetPointWithRule> PointList => _pointList.Value;

        private readonly ObservableAsPropertyHelper<bool> _duplicatesExist;

        public bool DuplicatesExist => _duplicatesExist.Value;

        private readonly ObservableAsPropertyHelper<List<string>> _selectedRuleUdcList;

        public List<string> SelectedRuleUdcList => _selectedRuleUdcList.Value;

        private readonly ObservableAsPropertyHelper<string> _duplicatesMessage;
        public string DuplicatesMessage => _duplicatesMessage.Value;

        private readonly ObservableAsPropertyHelper<List<CachedCygNetPointWithRule>> _duplicateList;
        public List<CachedCygNetPointWithRule> DuplicateList => _duplicateList.Value;

        private RuleGridViewModel<SqlPointSelection, SqlPointSelectionGridViewModel> _ruleList;
        public RuleGridViewModel<SqlPointSelection, SqlPointSelectionGridViewModel> RuleList
        {
            get => _ruleList;
            set => this.RaiseAndSetIfChanged(ref _ruleList, value);
        }

        //public async Task<List<FacilityUdcResult>> UpdatePointGridResults(List<CachedCygNetPointWithRule> srcPoints)
        //{
        //    var ThisFacPointResult = new FacilityUdcResult(fac);
        //    facResultList.Add(ThisFacPointResult);

        //    var fac = new CachedCygNetFacility();

        //    fac.PointCache.

        //    FacilityUdcGrid = facResultList;
        //}
    }
}
