using DataExporter.ExtensionMethods;
using DataExporter.Forms;
using DataExporter.View_Models;
using DataExporter.View_Models.Reactive;
using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Techneaux.CygNetWrapper.Points;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory.DatabaseOperations;
using TechneauxReportingDataModel.General;
using TechneauxReportingDataModel.Helper;
using TechneauxReportingDataModel.SqlHistory;
using XmlDataModelUtility;

namespace DataExporterWPF.SqlHistorySync.ViewModels
{
    public class MainSqlViewModel : NotifyDynamicBase
    {
        public SqlHistorySyncConfigModel MyDataModel { get; }
        public CygNetFacilityPointSearchViewModel CygNetDataViewModel { get; }

        public List<ICachedCygNetPoint> PointList
        {
            get { return GetPropertyValue<List<ICachedCygNetPoint>>(); }
            set { SetPropertyValue(value); }
        }

        public CygNetFacilityPointSearchViewModel ReactiveCygNetViewModel
        {
            get { return GetPropertyValue<CygNetFacilityPointSearchViewModel>(); }
            set { SetPropertyValue(value); }
        }

        public MainSqlViewModel(SqlHistorySyncConfigModel srcConfigModel, CygNetFacilityPointSearchViewModel cygViewModel) : base(srcConfigModel)
        {            
            MyDataModel = srcConfigModel;
            RuleList = new BindingList<RuleViewModel>();
            SqlFieldList = new BindingList<RuleViewModel>();

            for (int i = 0; i < srcConfigModel.SourcePointRules.Count(); i++)
            {
                RuleViewModel RuleModel = new RuleViewModel(srcConfigModel.ReportElements[i]);
                RuleList.Add(RuleModel);
            }

            for (int i = 0; i < srcConfigModel.SQLFieldElements.Count(); i++)
            {
                RuleViewModel SqlModel = new RuleViewModel(srcConfigModel.SQLFieldElements[i]);
                SqlFieldList.Add(SqlModel);
            }

            SqlDatabaseTableChooser.StaticDataContext = this;

            SqlGeneralSubViewModel = new SqlGeneralViewModel(srcConfigModel.SqlGeneralOpts);
            GeneralHistoryViewModel = new HistorySyncViewModel(srcConfigModel.HistorySyncOpts);
            //CygNetGeneralSubViewModel = new CygNetGeneralViewModel(srcModel.CygNetGeneral);

            ReactiveCygNetViewModel = new CygNetFacilityPointSearchViewModel(srcConfigModel.CygNetGeneral);

            // ---
            FacilityPointPreviewViewModel = new PointPreviewSubViewModel(SqlGeneralSubViewModel, MyDataModel);
            var udcList = MyDataModel.ReportElements.Where(rule => rule.ConfiguredRule is CygNetRollupRuleBase)
                                    .Select(rule => (rule.ConfiguredRule as CygNetRollupRuleBase).UDC);
            FacilityPointPreviewViewModel.UdcPreviewList = udcList.ToList();

            //FacilityPointPreviewCompareViewModel = new PointPreviewCompareViewModel(new CygNetSqlDataCompareModel(MyDataModel));

            HookUpPointPreviewTriggers();

            //MyDataModel.SqlGeneralOpts.PropertyChanged += SqlGeneralOpts_PropertyChanged;
            //  FacilityPointPreviewViewModel.PropertyChanged += VM_PropertyChanged;
            MyDataModel.SQLFieldElements.ListChanged += SqlFieldElements_ListChanged;
            //CygNetGeneralSubViewModel.PropertyChanged += CygNetGeneralSubViewModel_PropertyChanged;

            //TestNormHistory.TestAsync();
        }

        public async Task LoadData()
        {
            await SqlGeneralSubViewModel.LoadData();
        }

        private async void CygNetGeneralSubViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

            if (e.PropertyName != "CachedFacilities")
            {
                FacilityPointPreviewViewModel.IsPointPreviewBusy = true;
                await UpdatePointResults();
                FacilityPointPreviewViewModel.IsPointPreviewBusy = false;
            }
        }

        private void SqlFieldElements_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.PropertyDescriptor != null)
            {
                if (e.PropertyDescriptor.Name == "TimeColumn")
                {
                    if (MyDataModel.SQLFieldElements[e.NewIndex].LastPropertyUpdated == "TimeColumn")
                    {
                        if (MyDataModel.SQLFieldElements[e.NewIndex].TimeColumn == true)
                        {
                            for (int i = 0; i < MyDataModel.SQLFieldElements.Count; i++)
                            {
                                if (i != e.NewIndex)
                                {
                                    MyDataModel.SQLFieldElements[i].TimeColumn = false;
                                }
                            }
                        }
                    }
                }
            }
        }
        private BindingList<Column> GetAvailableFieldIdList()
        {
            BindingList<Column> FieldIdList = new BindingList<Column>();
            if (SqlGeneralSubViewModel.DbList != null)
            {
                foreach (var sqlDB in SqlGeneralSubViewModel.DbList)
                {
                    if (sqlDB.MyDataModel != null)
                    {
                        if (sqlDB.MyDataModel.Name == MyDataModel.SqlGeneralOpts.DatabaseName)
                        {
                            foreach (Microsoft.SqlServer.Management.Smo.Table sqlTable in sqlDB.MyDataModel.Tables)
                            {
                                if (sqlTable.Name == MyDataModel.SqlGeneralOpts.TableName)
                                {
                                    BindingList<Column> tempBinding = new BindingList<Column>();
                                    ColumnCollection tempCollection = sqlTable.Columns;
                                    for (int i = 0; i < tempCollection.Count; i++)
                                    {
                                        tempBinding.Add(tempCollection[i]);
                                    }
                                    return tempBinding;
                                }
                            }
                        }
                    }
                }
            }
            return FieldIdList;
        }
        
        //
        public void HookUpPointPreviewTriggers()
        {
            //var CachedFacilitiesUpdated = Observable
            //    .FromEventPattern<PropertyChangedEventArgs>(CygNetGeneralSubViewModel, nameof(CygNetGeneralSubViewModel.PropertyChanged))
            //    .Where(evt =>
            //         evt.EventArgs.PropertyName == nameof(CygNetGeneralViewModel.CachedFacilities))
            //    .Subscribe(async evt =>
            //    {
            //        await UpdatePointResults();
            //    });

            var PointCollection = MyDataModel.ReportElements;

            var PointSelectionUpdated = Observable
                .FromEventPattern<ListChangedEventArgs>(PointCollection, nameof(PointCollection.ListChanged))
                .Where(evt =>
                     evt.EventArgs.ListChangedType == ListChangedType.ItemChanged &&
                     PointCollection[evt.EventArgs.NewIndex].ConfiguredRule.LastPropertyUpdated == "UDC")
                .Subscribe(async evt =>
                {
                    var udcList = MyDataModel.ReportElements.Where(rule => rule.ConfiguredRule is CygNetRollupRuleBase)
                                    .Select(rule => (rule.ConfiguredRule as CygNetRollupRuleBase).UDC);

                    FacilityPointPreviewViewModel.UdcPreviewList = udcList.ToList();
                    FacilityPointPreviewViewModel.IsPointPreviewBusy = true;
                    await UpdatePointResults();
                    FacilityPointPreviewViewModel.IsPointPreviewBusy = false;
                });
        }
        Task<(List<FacilityUdcResult> facList, List<ICachedCygNetPoint> pointList)> PointResultUpdateTask = null;
        CancellationTokenSource PointResultUpdateTaskCts = null;

        public async Task UpdatePointResults()
        {
            // SpellcheckAsync can be re-entered
            FacilityPointPreviewViewModel.IsSummaryPreviewBusy = true;
            FacilityPointPreviewViewModel.IsPointPreviewBusy = true;
            var tempSchema = await SqlServerConnectionUtility.GetTableSchema(MyDataModel.SqlGeneralOpts, MyDataModel.SQLFieldElements.ToList());

            var previousCts = this.PointResultUpdateTaskCts;
            var newCts = new CancellationTokenSource();
            this.PointResultUpdateTaskCts = newCts;

            if (previousCts != null)
            {
                // cancel the previous session and wait for its termination
                previousCts.Cancel();
                try { await this.PointResultUpdateTask; } catch { }
            }

            FacilityPointPreviewViewModel.FacilityUdcGrid = null; // = new BindingList<FacilityUdcResult>();

            var UdcListCopy = FacilityPointPreviewViewModel.UdcPreviewList.DeepCopy();
            try
            {
                newCts.Token.ThrowIfCancellationRequested();
                this.PointResultUpdateTask = Task.Run(() => UpdatePointResultsHelper(UdcListCopy, newCts.Token));
                var TaskResult = await this.PointResultUpdateTask;
                newCts.Token.ThrowIfCancellationRequested();
                var newList = new BindingList<FacilityUdcResult>();

                FacilityPointPreviewViewModel.FacilityUdcGrid = TaskResult.facList;
                //validates rules and before updating pointlist
                if (tempSchema == null || !CygNetSqlDataCompareModel.ValidateRulesDataOnly(tempSchema, this.MyDataModel.SQLFieldElements.ToList()))
                {
                    FacilityPointPreviewViewModel.IsSummaryPreviewBusy = false;
                    FacilityPointPreviewViewModel.IsPointPreviewBusy = false;
                    return;
                }
                PointList = TaskResult.pointList;

                FacilityPointPreviewViewModel.UpdatePointList(PointList);
                FacilityPointPreviewViewModel.IsSummaryPreviewBusy = false;
                FacilityPointPreviewViewModel.IsPointPreviewBusy = false;
                ////if (FacilityPointPreviewViewModel.SelectedPoint != null)
                ////{
                ////    await UpdatePointList();
                ////    await SqlCompareAsync();
                ////    //  SyncLivePreview.UpdatePointList(FacilityPointPreviewViewModel.SelectedPoint));
                ////}

                //  SyncLivePreview.UpdatePointSummary();

                //FacilityUdcGrid = newList;
            }            
            catch
            {
                FacilityPointPreviewViewModel.IsSummaryPreviewBusy = false;
                FacilityPointPreviewViewModel.IsPointPreviewBusy = false;
                return;
            }
        }

        public async Task<(List<FacilityUdcResult> facList, List<ICachedCygNetPoint> pointList)>
            UpdatePointResultsHelper(List<string> udcList, CancellationToken ct)
        {
            var dataModelCopy = MyDataModel; //clone// .Clone() as ReportConfigModel;
            CancellationTokenSource cts = new CancellationTokenSource();
            var udcs = udcList;
            var pointList = new List<ICachedCygNetPoint>();
            // SyncLivePreview.PointList = new List<BaseCygNetPoint>();
            var facResultList = new List<FacilityUdcResult>();
            if (CygNetGeneralSubViewModel.CachedFacilities != null)
            {
                foreach (var fac in CygNetGeneralSubViewModel.CachedFacilities)
                {
                    var ThisFacPointResult = new FacilityUdcResult(fac);
                    facResultList.Add(ThisFacPointResult);

                    foreach (var udc in udcs)
                    {
                        ICachedCygNetPoint point = (ICachedCygNetPoint)(await fac.PointCache.TryGetPointAsync(udc, cts.Token));
                        if (!string.IsNullOrWhiteSpace(udc) && point != null)
                        {
                            ThisFacPointResult.AddUdc(udc);
                            pointList.Add(point);
                        }
                    }
                }
            }
            return (facResultList, pointList);
        }

        public PointPreviewSubViewModel FacilityPointPreviewViewModel
        {
            get { return GetPropertyValue<PointPreviewSubViewModel>(); }
            set { SetPropertyValue(value); }
        }

        public ICommand AddRow
        {
            get
            {
                return new DelegateCommand(AddNewRow);
            }
        }

        public ICommand AddSqlFieldRow
        {
            get
            {
                return new DelegateCommand(AddNewSqlFieldRow);
            }
        }

        public void AddNewRow(object inputObject)
        {
            if (SelectedRule == null)
            {
                RuleViewModel NewRuleView = new RuleViewModel(MyDataModel.AddNewRule(RuleList.Count));
                RuleList.Add(NewRuleView);
                SelectedRule = RuleList[RuleList.Count - 1];
                return;
            }
            int index = RuleList.IndexOf(SelectedRule);
            if (index < 0)
                index = 0;
            else if (index >= RuleList.Count)
                index = RuleList.Count - 1;
            RuleViewModel RuleView = new RuleViewModel(MyDataModel.AddNewRule(index));
            RuleList.Insert(index, RuleView);
            if (index < 0)
                index = 0;
            else if (index >= RuleList.Count)
                index = RuleList.Count - 1;
            SelectedRule = RuleList[index];
        }

        public void AddNewSqlFieldRow(object inputObject)
        {
            if (SelectedField == null)
            {
                RuleViewModel NewRuleView = new RuleViewModel(MyDataModel.AddNewSqlField(SqlFieldList.Count));
                // SqlFieldList.Add(NewRuleView);
                SqlFieldList.Add(NewRuleView);
                SelectedField = SqlFieldList[SqlFieldList.Count - 1];
                return;
            }

            int index = SqlFieldList.IndexOf(SelectedField);
            if (index < 0)
                index = 0;
            else if (index >= SqlFieldList.Count)
                index = SqlFieldList.Count - 1;

            RuleViewModel RuleView = new RuleViewModel(MyDataModel.AddNewSqlField(index));
            SqlFieldList.Insert(index, RuleView);
            if (index < 0)
                index = 0;
            else if (index >= SqlFieldList.Count)
                index = SqlFieldList.Count - 1;

            SelectedField = SqlFieldList[index];

        }
        public void DragAndDrop(int DragIndex, int DropIndex)
        {

            if (RuleList.FirstOrDefault() == null)
                return;
            RuleViewModel draggedRule = RuleList[DragIndex];
            RuleList.RemoveAt(DragIndex);
            MyDataModel.DeleteRule(DragIndex);
            if (DragIndex < 0)
                DragIndex = 0;
            else if (DragIndex >= RuleList.Count)
                DragIndex = RuleList.Count - 1;

            //SelectedField = SqlFieldList[DragIndex];

            //if (SelectedRule == null)
            //{
            //    RuleViewModel NewRuleView = new RuleViewModel(MyDataModel.AddNewRule(RuleList.Count));
            //    RuleList.Add(NewRuleView);
            //    SelectedRule = RuleList[RuleList.Count - 1];
            //    return;
            //}        

            MyDataModel.ReportElements.Insert(DropIndex, draggedRule.MyDataModel);
            RuleList.Insert(DropIndex, draggedRule);
            SelectedRule = RuleList[DragIndex];
        }

        public void DragAndDropSQL(int DragIndex, int DropIndex)
        {

            if (SqlFieldList.FirstOrDefault() == null)
                return;
            RuleViewModel draggedRule = SqlFieldList[DragIndex];
            SqlFieldList.RemoveAt(DragIndex);
            MyDataModel.DeleteSqlField(DragIndex);
            if (DragIndex < 0)
                DragIndex = 0;
            else if (DragIndex >= SqlFieldList.Count)
                DragIndex = SqlFieldList.Count - 1;

            //SelectedField = SqlFieldList[DragIndex];

            //if (SelectedRule == null)
            //{
            //    RuleViewModel NewRuleView = new RuleViewModel(MyDataModel.AddNewRule(RuleList.Count));
            //    RuleList.Add(NewRuleView);
            //    SelectedRule = RuleList[RuleList.Count - 1];
            //    return;
            //}   

            MyDataModel.SQLFieldElements.Insert(DropIndex, draggedRule.MyDataModel);
            SqlFieldList.Insert(DropIndex, draggedRule);
            SelectedField = SqlFieldList[DragIndex];
        }

       

        public Database GetSelectedDb()
        {
            Database CurrentDb = null;
            foreach (var db in SqlGeneralSubViewModel.DbList)
            {
                if (db.MyDataModel != null)
                {
                    if (db.MyDataModel.Name == MyDataModel.SqlGeneralOpts.DatabaseName)
                        CurrentDb = db.MyDataModel;
                }
            }
            return CurrentDb;
        }

        public Microsoft.SqlServer.Management.Smo.Table GetSelectedTable()
        {
            Microsoft.SqlServer.Management.Smo.Table CurrentTable = null;
            Database CurrentDb = null;
            foreach (var db in SqlGeneralSubViewModel.DbList)
            {
                if (db.MyDataModel != null)
                {
                    if (db.MyDataModel.Name == MyDataModel.SqlGeneralOpts.DatabaseName)
                    {
                        CurrentDb = db.MyDataModel;
                    }
                }
            }
            foreach (Microsoft.SqlServer.Management.Smo.Table tbl in CurrentDb.Tables)
            {
                if (tbl.Name == MyDataModel.SqlGeneralOpts.TableName)
                {
                    CurrentTable = tbl;
                }
            }
            return CurrentTable;
        }

        public ICommand DeleteRow
        {
            get
            {
                return new DelegateCommand(DeleteSelectedRow);
            }
        }
        public ICommand DeleteSqlFieldRow
        {
            get
            {
                return new DelegateCommand(DeleteSelectedSqlFieldRow);
            }
        }
        public void DeleteSelectedRow(object inputObject)
        {
            int index = RuleList.IndexOf(SelectedRule);
            RuleList.Remove(SelectedRule);
            MyDataModel.DeleteRule(index);

            if (RuleList.FirstOrDefault() == null)
                return;

            if (index < 0)
                index = 0;
            else if (index >= RuleList.Count)
                index = RuleList.Count - 1;

            SelectedRule = RuleList[index];
            return;
        }



        public void DeleteSelectedSqlFieldRow(object inputObject)
        {
            int index = SqlFieldList.IndexOf(SelectedField);
            SqlFieldList.Remove(SelectedField);
            MyDataModel.DeleteSqlField(index);

            if (SqlFieldList.FirstOrDefault() == null)
                return;

            if (index < 0)
                index = 0;
            else if (index >= SqlFieldList.Count)
                index = SqlFieldList.Count - 1;

            SelectedField = SqlFieldList[index];
            return;
        }

        public BindingList<RuleViewModel> RuleList
        {
            get { return GetPropertyValue<BindingList<RuleViewModel>>(); }
            set { SetPropertyValue(value); }
        }
        public BindingList<RuleViewModel> SqlFieldList
        {
            get { return GetPropertyValue<BindingList<RuleViewModel>>(); }
            set { SetPropertyValue(value); }
        }

        public RuleViewModel SelectedRule
        {
            get { return GetPropertyValue<RuleViewModel>(); }
            set { SetPropertyValue(value); }
        }

        public CygNetGeneralViewModel CygNetGeneralSubViewModel
        {
            get { return GetPropertyValue<CygNetGeneralViewModel>(); }
            set { SetPropertyValue(value); }
        }

        public SqlGeneralViewModel SqlGeneralSubViewModel
        {
            get { return GetPropertyValue<SqlGeneralViewModel>(); }
            set { SetPropertyValue(value); }
        }

        public RuleViewModel SelectedField
        {
            get { return GetPropertyValue<RuleViewModel>(); }
            set { SetPropertyValue(value); }
        }
        public HistorySyncViewModel GeneralHistoryViewModel
        {
            get { return GetPropertyValue<HistorySyncViewModel>(); }
            set { SetPropertyValue(value); }
        }

        public class FilterConditionEventViewModel
        {

        }
    }
}