﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CygNet.Data.Core;
using CygNet.Data.Historian;
using CygNetRuleModel.Resolvers.History;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;

namespace DataMasterClient.Testing
{
    public static class TestNormHistory
    {
        static TestNormHistory()
        {

        }
        public static async Task TestAsync()
        {
            var testRaw = new List<HistoricalEntry>();
            var test1 = new HistoricalEntry();
            var test2 = new HistoricalEntry();
            var test3 = new HistoricalEntry();
            var test4 = new HistoricalEntry();
            var test5 = new HistoricalEntry();
            var test6 = new HistoricalEntry();
            var test7 = new HistoricalEntry();
            var test8 = new HistoricalEntry();
            var test9 = new HistoricalEntry();
            var test10 = new HistoricalEntry();
            test1.Timestamp = new DateTime(1, 2, 3, 4, 5, 6, DateTimeKind.Local);
            double num = 1;
            test1.SetValue(num);
            test1.BaseStatus = BaseStatusFlags.Initialized;
            test2.Timestamp = new DateTime(1, 2, 3, 4, 15, 6, DateTimeKind.Local);
            num = 3;
            test2.SetValue(num);
            test2.BaseStatus = BaseStatusFlags.OutputData;
            test3.Timestamp = new DateTime(1, 2, 3, 4, 25, 6, DateTimeKind.Local);
            num = 5;
            test3.SetValue(num);
            test3.BaseStatus = BaseStatusFlags.Initialized;
            test4.Timestamp = new DateTime(1, 2, 3, 4, 35, 6, DateTimeKind.Local);
            num = 7;
            test4.SetValue(num);
            test4.BaseStatus = BaseStatusFlags.OutputData;
            test5.Timestamp = new DateTime(1, 2, 3, 4, 45, 6, DateTimeKind.Local);
            num = 9;
            test5.SetValue(num);
            test5.BaseStatus = BaseStatusFlags.Initialized;
            test6.Timestamp = new DateTime(1, 2, 3, 4, 55, 6, DateTimeKind.Local);
            num = 11;
            test6.SetValue(num);
            test6.BaseStatus = BaseStatusFlags.OutputData;
            test7.Timestamp = new DateTime(1, 2, 3, 5, 5, 6, DateTimeKind.Local);
            num = 13;
            test7.SetValue(num);
            test7.BaseStatus = BaseStatusFlags.Initialized;
            test8.Timestamp = new DateTime(1, 2, 3, 5, 7, 6, DateTimeKind.Local);
            num = 15;
            test8.SetValue(num);
            test8.BaseStatus = BaseStatusFlags.OutputData;
            test9.Timestamp = new DateTime(1, 2, 3, 5, 25, 6, DateTimeKind.Local);
            num = 16;
            test9.SetValue(num);
            test9.BaseStatus = BaseStatusFlags.Initialized;
            test10.Timestamp = new DateTime(1, 2, 3, 5, 27, 6, DateTimeKind.Local);
            num = 19;
            test10.SetValue(num);
            test10.BaseStatus = BaseStatusFlags.OutputData;
            testRaw.Add(test1);
            testRaw.Add(test2);
            testRaw.Add(test3);
            testRaw.Add(test4);
            testRaw.Add(test5);
            testRaw.Add(test6);
            testRaw.Add(test7);
            testRaw.Add(test8);
            testRaw.Add(test9);
            testRaw.Add(test10);

            var tempRule = new PointHistoryNormalizationOptions
            {
                EnableNormalization = true,

                NumWindowHistEntries = 2,
                SelectedSampleType = PointHistoryNormalizationOptions.SamplingType.Weighted_Average,
                NormalizeWindowStartTime = 3,
                NormalizeWindowIntervalLength = 0.50m
            };

            var testR = new List<CygNetHistoryEntry>();
            foreach (var raw in testRaw)
            {
                testR.Add(new CygNetHistoryEntry(raw));
            }

            var normClass = new HistoryNormalization(tempRule, TestDates.TestBeg, TestDates.TestEnd);

            var testNorm = await normClass.Normalize(testR);

            var testOut = new List<string>();
            var testFlags = new List<BaseStatusFlags>();
            //for (int i = 0; i < testNorm.Count; i++)
            //{
            //    testOut.Add(testNorm[i].Value.StringValue);
            //}
            //for (int j = 0; j < testNorm.Count; j++)
            //{
            //    testFlags.Add(testNorm[j].BaseStatus);
            //}
        }
    }
}
