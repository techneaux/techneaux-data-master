﻿using DataMasterClient.Utility;
using DataMasterClient.ViewModels;
using DataMasterClient.ViewModels.General;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using ReactiveUI;
using Serilog;
using Serilog.Context;
using Serilog.Core;
using Serilog.Filters;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using TechneauxReportingDataModel.General;
using TechneauxUtility;
using TechneauxWpfControls;
using XmlDataModelUtility;
using static XmlDataModelUtility.GlobalLoggers;
using Bytes = TechneauxUtility.Bytes;

namespace DataMasterClient
{
    /// <summary>
    /// 
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        //ReportConfigModel CurrentModel = new ReportConfigModel();
        //MainViewModel myViewModel;

        readonly ConfigModelFileService _configFile;

        public MainWindow()
        {
           
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += MyHandler;
            //var fileName = $"client_Log_" + "{Date}.txt";
            var loggingLevelAttr = ConfigurationManager.AppSettings.Get("LogLevelConfig");
            var newLevel = (Serilog.Events.LogEventLevel)Enum.Parse(typeof(Serilog.Events.LogEventLevel), loggingLevelAttr);
            var logLevelSwitch = new LoggingLevelSwitch(newLevel);

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.ControlledBy(logLevelSwitch)
                .Enrich.FromLogContext()
                .Enrich.WithCaller()
                .WriteTo
                .File(
                    //new JsonFormatter(),
                    path: $@"{AppDomain.CurrentDomain.BaseDirectory}\Logs\General Client Log (started on {DateTime.Now:MMMM dd, yyyy, hh-mm}).txt",
                    outputTemplate: "[{Timestamp:M-d-yy hh:mm:ss} {Level:u3}] {Message:lj} (at {Caller}){NewLine}{Exception}",
                    rollOnFileSizeLimit: true,
                    fileSizeLimitBytes: Bytes.FromMegabytes(10),
                    retainedFileCountLimit: 7
                    /*shared: true*/)
                .WriteTo.Logger(adminLoggerConfig => adminLoggerConfig
                    .Filter.ByIncludingOnly(Matching.WithProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    .WriteTo.File(
                        outputTemplate: "[{Timestamp:M-d-yy hh:mm:ss} ADMIN {Level:u3}] {Message:lj} {NewLine} ========Exception Details========= {NewLine}{Exception}{NewLine}",
                        path: $@"{AppDomain.CurrentDomain.BaseDirectory}\Logs\Client Admin Events (started on {DateTime.Now:MMMM dd, yyyy, hh-mm}).txt",
                        rollOnFileSizeLimit: true,
                        fileSizeLimitBytes: Bytes.FromMegabytes(10),
                        retainedFileCountLimit: 3,
                        shared: true))
                .WriteTo.Logger(generalExceptionLoggerConfig => generalExceptionLoggerConfig
                    .Filter.ByIncludingOnly(Matching.WithProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    .WriteTo.File(
                        path: $@"{AppDomain.CurrentDomain.BaseDirectory}\Logs\Client Unhandled Exceptions (started on {DateTime.Now:MMMM dd, yyyy, hh-mm}).txt",
                        outputTemplate: "[{Timestamp:M-d-yy hh:mm:ss} UNHANDLED {Level:u3}] {Message:lj} (at {Caller}){NewLine}{Exception}",
                        rollOnFileSizeLimit: true,
                        fileSizeLimitBytes: Bytes.FromMegabytes(10),
                        retainedFileCountLimit: 3,
                        shared: true))
                .CreateLogger();

            //.MinimumLevel.ControlledBy(logLevelSwitch)
            //.WriteTo
            //.RollingFile($@"{AppDomain.CurrentDomain.BaseDirectory}Logs\{fileName}", shared: true)
            //.CreateLogger();

            Log.Debug("Launching client.");
            //load gui config settings.


            try
            {
                var deleteSeconds = ConfigurationManager.AppSettings["SqlDeleteTimeoutSeconds"];
                var selectSeconds = ConfigurationManager.AppSettings["SqlSelectTimeoutSeconds"];
                var insertSeconds = ConfigurationManager.AppSettings["SqlInsertTimeoutSeconds"];

                if (insertSeconds != null)
                {
                    ServiceOps.InsertTimeout = Convert.ToDouble(insertSeconds);
                }

                if (deleteSeconds != null)
                {
                    ServiceOps.DeleteTimeout = Convert.ToDouble(deleteSeconds);
                }

                if (selectSeconds != null)
                {
                    ServiceOps.SelectTimeout = Convert.ToDouble(selectSeconds); ;
                }

                ServiceOps.CriticalTimestampTimeoutMins = Convert.ToDouble(ConfigurationManager.AppSettings.Get("CriticalTimestampTimeoutMinutes"));
            }
            catch (ConfigurationErrorsException ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Warning(ex, "Not all SQL Timeout settings were found in the app config file. Using defaults instead.");

            }


            InitializeComponent();

            RxApp.MainThreadScheduler = DispatcherScheduler.Current;
            RxApp.TaskpoolScheduler = TaskPoolScheduler.Default;


            //MessageBox.Show(Assembly.GetExecutingAssembly().GetName().Version.ToString());

            Title = $"Techneaux Data Master (Version {Assembly.GetExecutingAssembly().GetName().Version})";

            try
            {
                //   TechneauxHistorySynchronization.LibraryLogging.InitLogging("SQL Sync UI", Serilog.Events.LogEventLevel.Debug);



                //MessageBox.Show(AppDomain.CurrentDomain.BaseDirectory);

                var commandLineParams = Environment.GetCommandLineArgs().ToList();
                _configFile = new ConfigModelFileService(this);
                _configFile.ResetModified();
                FileOpsPanel.DataContext = _configFile;
                if (commandLineParams.Count > 0 && commandLineParams.Exists(item => item.ToLower().Contains(@"/autorun")))
                {
                    SetupAutoRun();
                
                }
                else
                {
                    var myViewModel = new MainViewModel(_configFile.ThisConfigModel);
                    if (AvailableOptions.MainType == AvailableOptions.SyncOptions.SqlAndSmartSheet)
                    {
                        SqlSyncTab.Visibility = Visibility.Visible;
                        SmartSheetTab.Visibility = Visibility.Visible;
                    }
                    else if (AvailableOptions.MainType == AvailableOptions.SyncOptions.SqlHistory)
                    {
                        SmartSheetTab.Visibility = Visibility.Collapsed;
                        SqlSyncTab.Visibility = Visibility.Visible;
                    }
                    else if (AvailableOptions.MainType == AvailableOptions.SyncOptions.SmartSheet)
                    {
                        SqlSyncTab.Visibility = Visibility.Collapsed;
                        SmartSheetTab.Visibility = Visibility.Visible;
                    }
                    _configFile.ResetModified();
                    DataContext = myViewModel;

                    Closing += MainWindow_Closing;
                    BtnNewFile.Click += Button_New;
                    BtnOpenFile.Click += Button_Open;
                    BtnSaveFile.Click += Button_Save;
                    BtnSaveFileAsNew.Click += Button_SaveAsNew;
                    Loaded += MainWindow_Loaded;
                    ContentRendered += MainWindow_ContentRendered;
                }
                //var newConfigModel = new ReportConfigModel();


            }
            catch (Exception e)
            {
                Log.Error(e, "Fatal error");
                MessageBox.Show(e.ToString());
            }
        }


        private async Task SetupAutoRun()
        {
            CancellationTokenSource ctSource = new CancellationTokenSource();
            await AutoOperation.RunAutoOperationAsync(DateTime.Today, Environment.GetCommandLineArgs().ToList(), _configFile, ctSource.Token);

            Close();
        }


        static void MyHandler(object sender, UnhandledExceptionEventArgs args)
        {
            var e = (Exception)args.ExceptionObject;

            using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
            {
                if (e.InnerException != null)
                    Log.Error("Unhandled exception InnerExceptionMessage: " + e.InnerException.Message);
                Log.Error("Unhandled exception StackTrace: " + e.StackTrace);
                Log.Error("Unhandled exception Message: " + e.Message);
                Log.Error("Unhandled exception Source: " + e.Source);
            }

            Console.WriteLine($@"MyHandler caught : {e.Message}");
            Console.WriteLine(@"Runtime terminating: {0}", args.IsTerminating);
            MessageBox.Show(@"An unhandled exception occurred and the program will exit. See Log for details.", @"Error", MessageBoxButton.OK);
            Environment.Exit(-1);

        }

        private bool _licenseFailed = false;
        private async void MainWindow_ContentRendered(object sender, EventArgs e)
        {
            try
            {
                // Check if in developer mode
                if (CvsSubscriptionLicensing.InDeveloperMode)
                {
                    DeveloperBanner.Visibility = Visibility.Visible;
                }

                if (!await WpfLicenseCheck.CheckLicensing())
                {
                    _licenseFailed = true;
                    Close();
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception while checking license");

                _licenseFailed = true;
                Close();
            }



            //if (TrialPeriod.LicenseStatus.approvedToRun)
            //    return;

            //var lcForm = new LicenseForm { Owner = this };

            //if (lcForm.IsTrialExpired())
            //{
            //    LicenseFailed = true;
            //    Close();
            //}
        }

        private async void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var check = TestLoadLibraries.AreDllsLoaded();

            if (!check)
            {
                await this.ShowMessageAsync("Missing PreRequisite", "CxFMS64.dll is misisng or not registered. Please register it before continuing");
                Close();
            }

            var vm = DataContext as MainViewModel;

            //var chart = new TestCharts();
            //var chartVm = new TestVm();
            //chart.DataContext = chartVm;
            //chart.ShowDialog();
            try
            {
                await vm.LoadData();
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception while initializing the main view. Please contact Techneaux.");

                MessageBox.Show($"Unhandled exception while initializing the main view. Please contact Techneaux. Details: {ex}");
            }

        }



        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (_licenseFailed) return;

            if (!_configFile.TryClose())
            {
                e.Cancel = true;
            }
        }

        private async void Button_New(object sender, RoutedEventArgs e)
        {
            //InitializeComponent();

            if (_configFile.CreateNewConfig())
            {
                var myViewModel = new MainViewModel(_configFile.ThisConfigModel);


                DataContext = null;
                DataContext = myViewModel;
                _configFile.ResetModified();

                await myViewModel.LoadData();
            }
        }

        private async void Button_Open(object sender, RoutedEventArgs e)
        {
            if (_configFile.OpenConfigFile())
            {
                try
                {
                    var myViewModel = new MainViewModel(_configFile.ThisConfigModel);

                    DataContext = null;
                    DataContext = myViewModel;
                    _configFile.ResetModified();

                    await myViewModel.LoadData();
                }
                catch (Exception ex)
                {
                    using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                        Log.Error(ex, "Unhandled exception while loading a config file. Please contact Techneaux.");

                    MessageBox.Show( $"Unhandled exception while loading a config file. Please contact Techneaux. Details: {ex}");
                }

            }
        }

        private void Button_Save(object sender, RoutedEventArgs e)
        {
            _configFile.SaveConfigFile();
        }

        private void Button_SaveAsNew(object sender, RoutedEventArgs e)
        {
            _configFile.SaveConfigFile(true);
        }

        private void HistoryPointSelection_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}

