﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic;
using Serilog;

using GenericRuleModel.Rules;
using XmlDataModelUtility;

namespace CygNetRuleModel.Rules
{
    public class EnumerationTables : NotifyCopyDataModel
    {
        public BindingList<EnumerationTable> EnumTables { get; set; }

        public EnumerationTables()
        {
            EnumTables = new BindingList<EnumerationTable>();
        }

        // Classes (enum)
        
        public EnumerationTable GetTable(string tableName)
        {
            foreach (EnumerationTable table in this.EnumTables)
            {
                if (table.Name.Equals(tableName))
                    return table;
            }
            return null;
        }

        public static EnumerationTables LoadEnumTables(string strFilePath, string strFileName = null)
        {
            try
            {
                if (strFileName == null)
                {
                    strFileName = "EnumTables.XML";
                }

                System.IO.Directory.CreateDirectory(strFilePath);

                if (Strings.Right(strFilePath, 1) != @"\")
                {
                    strFilePath = strFilePath + @"\";
                }

                using (StreamReader sr = new StreamReader(strFilePath + strFileName))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(EnumerationTables), new XmlRootAttribute("EnumRoot"));
                    EnumerationTables LoadedTables = (EnumerationTables)serializer.Deserialize(sr);
                    if (LoadedTables.EnumTables == null)
                    {
                        BindingList<EnumerationTable> EnumTableList = new BindingList<EnumerationTable>();
                        LoadedTables.EnumTables = EnumTableList;
                    }
                    return LoadedTables;
                }
            }
            catch (Exception ex)
            {
                EnumerationTables EnumObject = new EnumerationTables();
                
                Log.Error("Unable to open EnumTables file: " + ex.Message);
                return EnumObject;
            }
        }

        //public EnumerationTables DeepObjectCopy()
        //{
        //    EnumerationTables tablesCopy = new EnumerationTables();
        //    tablesCopy.EnumTables = new BindingList<EnumerationTable>();
        //    foreach (EnumerationTable tableRef in this.EnumTables)
        //    {
        //        EnumerationTable copyTable = tableRef.DeepObjectCopy();
        //        tablesCopy.EnumTables.Add(copyTable);
        //    }
        //    return tablesCopy;
        //}
    }


}
