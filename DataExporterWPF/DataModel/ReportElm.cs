﻿namespace DataExporter.DataModel
{
    using DataExporter.DataModel;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using XmlDataModelUtility;
    using static CygNetRuleModel.Rules.CygNetElementHistoryOptions;

    //Subclass containing column definition information
    public class ReportElm : INotifyPropertyChanged
    {
        public ReportElm()
        {
            CalculationCommand = "";
            ConversionType = ConversionTypes.String;
            CustomStringFormat = "";
            DataType = DataTypes.UDC;
            IsHidden = false;

            // init note enums
            NoteHistoryType = NoteHistoryTypes.OngoingL;
            NoteValueType = NoteValueTypes.Body;

            HistoryType = HistoryTypes.First_AfterRollupPeriodBegins;
            PointValueType = PointValueTypes.Value;
            AllowUnreliable = true;
            Item = "";
            LinkedAttr = "";
            Name = "";
            IsGrouped = false;
            NoValue = "";
            NumPart = PartsOfNumber.FullNumber;
            SummaryType = SummaryTypes.None;
            SortType = SortTypes.None;
            SortingOrderNumber = 0;
        }

        private readonly Dictionary<string, object> _propertyBackingDictionary = new Dictionary<string, object>();

        protected T GetPropertyValue<T>([CallerMemberName] string propertyName = null)
        {
            if (propertyName == null) throw new ArgumentNullException("propertyName");

            object value;
            if (_propertyBackingDictionary.TryGetValue(propertyName, out value))
            {
                return (T)value;
            }

            return default(T);
        }

        protected bool SetPropertyValue<T>(T newValue, [CallerMemberName] string propertyName = null)
        {
            if (propertyName == null) throw new ArgumentNullException("propertyName");

            if (EqualityComparer<T>.Default.Equals(newValue, GetPropertyValue<T>(propertyName))) return false;

            _propertyBackingDictionary[propertyName] = newValue;
            OnPropertyChanged(propertyName);
            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string CalculationCommand
        {
            get { return GetPropertyValue<string>(); }
            set { SetPropertyValue(value); }
        }

        public enum ConversionTypes
        {
            [Description("String")]
            String,

            [Description("String => DateTime")]
            String_toDateTime,

            [Description("String => Decimal")]
            DecimalNumber,

            [Description("Boolean => Y/N")]
            BoolToYN,

            [Description("Boolean => Y only for True")]
            BoolToY,

            [Description("Boolean => 0/1")]
            BoolTo01,

            [Description("Feet => Inches")]
            Feet_toInches,

            [Description("Feet => Quarter Inches")]
            Feet_toQuarterInches,

            [Description("Feet => Frac. Quarter Inches")]
            Feet_toFracQuarterInches,

            [Description("Feet => Dec. Quarter Inches")]
            Feet_toDecQuarterInches,

            [Description("m => mm")]
            Meters_toMM,

            [Description("cm => mm")]
            Centimeters_toMM,

            [Description("Inches => Feet")]
            Inches_toFeet,

            [Description("Inches => Excess Inches")]
            Inches_toExcessInches,

            [Description("Y/N to 1/-1")]
            YesNo_toPlusMinusOne,

            [Description("Alternate Unit Value")]
            AltUnits
        }
        public ConversionTypes ConversionType
        {
            get { return GetPropertyValue<ConversionTypes>(); }
            set { SetPropertyValue(value); }
        }

        public string CustomStringFormat
        {
            get { return GetPropertyValue<string>(); }
            set { SetPropertyValue(value); }
        }

        public enum NoteHistoryTypes
        {
            [Description("Most Recent Note (by Start Date)")]
            MostRecentPiTNote,
            [Description("Ongoing in Period (Last)")]
            OngoingL,
            [Description("Ongoing in Period (First)")]
            OngoingF,
            [Description("Starts Inside (Last)")]
            StartInsideL,
            [Description("Starts Inside (First)")]
            StartInsideF,
            [Description("Ends Inside (Last)")]
            EndinsideL,
            [Description("Ends Inside (First)")]
            EndinsideF,
            [Description("Start/End Overlaps Contract (Last)")]
            OverlapContractL,
            [Description("Start/End Overlaps Contract (First)")]
            OverlapContractF
        }
        public NoteHistoryTypes NoteHistoryType
        {
            get { return GetPropertyValue<NoteHistoryTypes>(); }
            set { SetPropertyValue(value); }
        }

        public enum NoteValueTypes
        {
            [Description("Type")]
            Type,
            [Description("Body")]
            Body,
            [Description("Start Date")]
            StartDate,
            [Description("End Date")]
            EndDate
        }
        public NoteValueTypes NoteValueType
        {
            get { return GetPropertyValue<NoteValueTypes>(); }
            set { SetPropertyValue(value); }
        }

        public enum DataTypes
        {
            [Description("Point Value (UDC)")]
            UDC,

            [Description("Facility Attribute")]
            FacilityAttr,

            [Description("Fixed Text")]
            FixedText,

            [Description("Start Date")]
            StartDate,

            [Description("End Date")]
            EndDate,

            [Description("Calculation")]
            Calculation,

            [Description("Facility Note")]
            FacilityNote,

            // -- Add back in database options when code is supported
            //[Description("Datebase Entry")]
            //Database
        }
        public DataTypes DataType
        {
            get { return GetPropertyValue<DataTypes>(); }
            set { SetPropertyValue(value); }
        }

        public bool IsHidden
        {
            get { return GetPropertyValue<bool>(); }
            set { SetPropertyValue(value); }
        }

        public enum HistoryTypes
        {
            [Description("[First] After Contract Begins")]
            First_AfterRollupPeriodBegins,

            [Description("[First] Before Contract Ends")]
            First_BeforeRollupPeriodEnds,

            [Description("[First] Before Contract Ends (extend hist)")]
            First_BeforeRollupPeriodEndsExtend,

            [Description("[First] After Contract Ends")]
            First_AfterRollupPeriodEnds,

            [Description("[Value] Current")]
            Value_Current,

            [Description("[Value] at This Contract Hour")]
            Value_AtRollupPeriodHour,

            [Description("[Value] at Last Contract Hour")]
            Value_AtLastRollupPeriodHour,

            [Description("[Value] at Next Contract Hour")]
            Value_AtNextRollupPeriodHour,

            [Description("[Contract Period] Weighted Average")]
            RollupPeriod_CalcWeightedAvg,

            [Description("[Contract Period] Mean")]
            RollupPeriod_CalcMean,

            [Description("[Contract Period] Min")]
            RollupPeriod_CalcMin,

            [Description("[Contract Period] Max")]
            RollupPeriod_CalcMax,

            [Description("[Contract Period] Delta")]
            RollupPeriod_CalcDelta
        }
        public HistoryTypes HistoryType
        {
            get { return GetPropertyValue<HistoryTypes>(); }
            set { SetPropertyValue(value); }
        }

        public enum PointValueTypes
        {
            [Description("Value")]
            Value,

            [Description("Timestamp")]
            Timestamp,

            [Description("[Flag] IsInitialized")]
            IsInitialized,

            [Description("[Flag] IsUnreliable")]
            IsUnreliable,

            [Description("[Flag] IsUpdated")]
            IsUpdated,

            [Description("[Flag] IsDigitalAlarm")]
            IsDigitalAlarm,

            [Description("[Flag] IsDigitalChattering")]
            IsDigitalChattering,

            [Description("[Flag] IsDigitalWarning")]
            IsDigitalWarning,

            [Description("[Flag] IsHighAlarm")]
            IsHighAlarm,

            [Description("[Flag] IsHighOutOfRange")]
            IsHighOutOfRange,

            [Description("[Flag] IsHighWarning")]
            IsHighWarning,

            [Description("[Flag] IsLowAlarm")]
            IsLowAlarm,

            [Description("[Flag] IsLowOutOfRange")]
            IsLowOutOfRange,

            [Description("[Flag] IsLowWarning")]
            IsLowWarning,

            [Description("[Flag] IsHighDeviation")]
            IsHighDeviation,

            [Description("[Flag] IsStringAlarm1")]
            IsStringAlarm1,

            [Description("[Flag] IsStringAlarm2")]
            IsStringAlarm2,

            [Description("[Flag] IsStringAlarm3")]
            IsStringAlarm3,

            [Description("[Flag] IsStringAlarm4")]
            IsStringAlarm4,

            [Description("[Flag] IsStringAlarm5")]
            IsStringAlarm5,

            [Description("[Flag] IsStringAlarm6")]
            IsStringAlarm6,
        }
        public PointValueTypes PointValueType
        {
            get { return GetPropertyValue<PointValueTypes>(); }
            set { SetPropertyValue(value); }
        }

        public bool AllowUnreliable
        {
            get { return GetPropertyValue<bool>(); }
            set { SetPropertyValue(value); }
        }

        public string Item
        {
            get { return GetPropertyValue<string>(); }
            set { SetPropertyValue(value); }
        }

        public string LinkedAttr
        {
            get { return GetPropertyValue<string>(); }
            set { SetPropertyValue(value); }
        }

        public string Name
        {
            get { return GetPropertyValue<string>(); }
            set
            {
                SetPropertyValue(value);
            }
        }

        public bool IsGrouped
        {
            get { return GetPropertyValue<bool>(); }
            set { SetPropertyValue(value); }
        }

        public string NoValue
        {
            get { return GetPropertyValue<string>(); }
            set { SetPropertyValue(value); }
        }

        public enum PartsOfNumber
        {
            [Description("(Not Numeric)")]
            NotNumeric,

            [Description("Full Number")]
            FullNumber,

            [Description("Integer Part")]
            IntegerPart,

            [Description("Floor")]
            Floor,

            [Description("Ceiling")]
            Ceiling,

            [Description("Decimal Part")]
            DecimalPart
        }
        public PartsOfNumber NumPart
        {
            get { return GetPropertyValue<PartsOfNumber>(); }
            set { SetPropertyValue(value); }
        }

        public enum SummaryTypes
        {
            None,
            Sum,
            Mean,
            Max,
            Min,
            Count
        }
        public SummaryTypes SummaryType
        {
            get { return GetPropertyValue<SummaryTypes>(); }
            set { SetPropertyValue(value); }
        }

        public enum SortTypes
        {
            [Description("None")]
            None,

            [Description("Ascend")]
            Ascending,

            [Description("Descend")]
            Descending
        }

        public enum SortTypesLimited
        {
            [Description("Ascending")]
            Ascending,

            [Description("Descending")]
            Descending
        }
        public SortTypes SortType
        {
            get { return GetPropertyValue<SortTypes>(); }
            set { SetPropertyValue(value); }
        }

        public int SortingOrderNumber
        {
            get { return GetPropertyValue<int>(); }
            set { SetPropertyValue(value); }
        }

        public bool IsTableHistoryValue
        {
            get { return GetPropertyValue<bool>(); }
            set { SetPropertyValue(value); }
        }

        //[XmlIgnore()]
        //public string RowConditionStr
        //{
        //    get
        //    {

        //    }
        //    set
        //    {

        //    }
        //}

        //public BindingList<RowCondition> RowConditions
        //    {
        //    get {}
        //    set {}

        //    }

        public enum ValueTypes
        {
            String,
            Number,
            DateTime
        }
        public ValueTypes ValType
        {
            get
            {
                switch (ConversionType)
                {
                    case ConversionTypes.String:
                        return ValueTypes.String;

                    case ConversionTypes.String_toDateTime:
                        return ValueTypes.DateTime;

                    case ConversionTypes.DecimalNumber:
                    case ConversionTypes.Feet_toInches:
                    case ConversionTypes.Feet_toQuarterInches:
                    case ConversionTypes.Feet_toFracQuarterInches:
                    case ConversionTypes.Feet_toDecQuarterInches:
                    case ConversionTypes.Meters_toMM:
                    case ConversionTypes.Centimeters_toMM:
                    case ConversionTypes.Inches_toFeet:
                    case ConversionTypes.Inches_toExcessInches:
                        return ValueTypes.Number;

                    default:
                        return ValueTypes.String;
                }
            }
        }
    }
}