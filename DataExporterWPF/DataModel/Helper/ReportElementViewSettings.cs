﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataExporter.DataModel.Helper
{
    using System;
    using static DataExporter.DataModel.ReportConfig;

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class ReportElementViewSettings : Attribute
    {       

        public ReportElementViewSettings(ReportTypes thisType, int colIndex)
        {
            NotifyType = thisType;
            ColumnIndex = colIndex;
        }

        public ReportTypes NotifyType { get; set; }
        public int ColumnIndex {get;set;}
    }
}
