﻿using System;
using System.Windows;
using System.Windows.Input;
using DataMasterClient.ViewModels.CygNet.Sub;
using GenericRuleModel.Helper;
using MahApps.Metro.Controls;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using TechneauxWpfControls.DataConverters;

namespace DataMasterClient.Forms
{
    /// <summary>
    /// Interaction logic for FacilityLinkingAttributeWindow.xaml
    /// </summary>
    public partial class CygNetChildFacilityWindow : MetroWindow
    {
        public CygNetChildFacilityWindow()
        {
            InitializeComponent();

            //throw new NotImplementedException("Add event listener for bound list changes by reading the context object at runtime");
            AttributeListView.MouseDoubleClick += AttributeListView_MouseDoubleClick;


            //throw new NotImplementedException("Add event listener for bound list changes by reading the context object at runtime");
        }

        private void AttributeListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (!(AttributeListView.SelectedItem is ChildFacilityGroup selectedAttributeItem))
            {
                return;
            }

            _thisChooserResult.Value = selectedAttributeItem.GroupKey.Id;
            _thisChooserResult.Description = selectedAttributeItem.GroupKey.Desc;

            _wasCancelled = false;

            Close();
        }
        private void OnCloseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Hide();
        }

        public static object StaticDataContext;
        public static CygNetChildFacilityWindow Instance()
        {
            if (StaticDataContext == null)
            {
                throw new InvalidOperationException("Static Data Context must not be null");
            }

            var instance = new CygNetChildFacilityWindow
            {
                DataContext = StaticDataContext
            };

            return instance;
        }

        public bool TryGetKeyDescId(out ValueDescription result)
        {
            Owner = Application.Current.MainWindow;
            ShowDialog();

            result = _thisChooserResult;
            return !_wasCancelled;
        }

        private bool _wasCancelled;
        private readonly ValueDescription _thisChooserResult = new ValueDescription();

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            if (!(AttributeListView.SelectedItem is ChildFacilityGroupViewModel selectedAttributeItem))
            {
                return;
            }

            _thisChooserResult.Value = ((dynamic)selectedAttributeItem).GroupKey.Id;
            _thisChooserResult.Description = ((dynamic)selectedAttributeItem).GroupKey.Desc;

            _wasCancelled = false;

            Close();
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            _wasCancelled = true;
            Close();
        }
    }
}
