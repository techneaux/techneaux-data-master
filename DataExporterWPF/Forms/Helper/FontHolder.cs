﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataExporter.Forms.Helper
{
    public class FontHolder
    {
        public Font font;
        private static FontHolder instance;

        private FontHolder() { }

        public static FontHolder Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new FontHolder();
                }
                return instance;
            }
        }
    }
}
