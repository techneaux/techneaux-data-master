﻿using System;
using System.Windows;
using System.Windows.Input;
using GenericRuleModel.Helper;
using MahApps.Metro.Controls;
using Techneaux.CygNetWrapper.Facilities.Attributes;
using TechneauxWpfControls.DataConverters;

namespace DataMasterClient.Forms
{
    /// <summary>
    /// Interaction logic for FacilityLinkingAttributeWindow.xaml
    /// </summary>
    public partial class AlarmBitChooser : MetroWindow
    {
        public static object StaticDataContext;

        public AlarmBitChooser()
        {
            InitializeComponent();

            AlarmBitListView.MouseDoubleClick += AlarmBitListView_MouseDoubleClick;

            //throw new NotImplementedException("Add event listener for bound list changes by reading the context object at runtime");
        }

        private void AlarmBitListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (!(AlarmBitListView.SelectedItem is FacilityAttribute selectedAttributeItem))
            {
                return;
            }

            _thisChooserResult.Value = selectedAttributeItem.ColumnName;
            _thisChooserResult.Description = selectedAttributeItem.Description;

            _wasCancelled = false;

            Close();
        }
        
        private void OnCloseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Hide();
        }

        public static AlarmBitChooser Instance()
        {
            if(StaticDataContext == null)
                {
                    throw new InvalidOperationException("Static Data Context must not be null");
                }

            var instance = new AlarmBitChooser
            {
                DataContext = StaticDataContext
            };

            return instance;
        }

        public bool TryGetKeyDescId(out ValueDescription result)
        {
            Owner = Application.Current.MainWindow;
            ShowDialog();

            result = _thisChooserResult;
            return !_wasCancelled;
        }

        private bool _wasCancelled = false;
        private readonly ValueDescription _thisChooserResult = new ValueDescription();

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            if (!(AlarmBitListView.SelectedItem is FacilityAttribute selectedAttributeItem))
            {
                return;
            }

            _thisChooserResult.Value = selectedAttributeItem.ColumnName;
            _thisChooserResult.Description = selectedAttributeItem.Description;

            _wasCancelled = false;

            Close();
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {

            _wasCancelled = true;        
            Close();
        }
    }
}
