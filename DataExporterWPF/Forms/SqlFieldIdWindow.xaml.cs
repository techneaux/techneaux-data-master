﻿using DataMasterClient.ViewModels;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using MahApps.Metro.Controls;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using TechneauxWpfControls.DataConverters;
using Application = System.Windows.Application;
using GenericRuleModel.Helper;

namespace DataMasterClient.Forms
{
    /// <summary>
    /// Interaction logic for FacilityLinkingAttributeWindow.xaml
    /// </summary>
    public partial class SqlFieldIdWindow : MetroWindow
    {
        public static object StaticDataContext { get; set; }
        public static BindingList<SqlTableMapping> SqlRuleMappingList { get; set; }

        public SqlFieldIdWindow()
        {
            InitializeComponent();

            FieldIdListView.MouseDoubleClick += FieldIdListView_MouseDoubleClick;
        }

        private void FieldIdListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var selectedRowItem = FieldIdListView.SelectedItem as SqlColumnViewModel;
            if (selectedRowItem == null)
            {
                return;
            }

            if(SqlRuleMappingList.Any(rule => rule.SqlTableFieldName == selectedRowItem.Name))
            {
                System.Windows.MessageBox.Show("This field is already in use, please choose another one");
                return;
            }

            ThisChooserResult.Value = selectedRowItem.Name;
            ThisChooserResult.Description = selectedRowItem.Id;

            WasCancelled = false;

            Close();
        }

        private void OnCloseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Hide();
        }

        public static SqlFieldIdWindow Instance()
        {
            if (StaticDataContext == null)
            {
                throw new InvalidOperationException("Static Data Context must not be null");
            }

            var instance = new SqlFieldIdWindow
            {
                DataContext = StaticDataContext,
                Owner = Application.Current.MainWindow
            };

            return instance;
        }

        public bool TryGetKeyDescId(out ValueDescription result)
        {
            ShowDialog();

            result = ThisChooserResult;
            return !WasCancelled;
        }

        public bool WasCancelled = false;
        public ValueDescription ThisChooserResult { get; private set; } = new ValueDescription();
        

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedRowItem = FieldIdListView.SelectedItem as SqlColumnViewModel;
            if (selectedRowItem == null)
            {
                return;
            }

            if (SqlRuleMappingList.Any(rule => rule.SqlTableFieldName == selectedRowItem.Name))
            {
                System.Windows.MessageBox.Show("This field is already in use, please choose another one");
                return;
            }

            ThisChooserResult.Value = selectedRowItem.Name;
            ThisChooserResult.Description = selectedRowItem.Id;
            WasCancelled = false;
            Close();
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {

            WasCancelled = true;
            Close();
        }
    }
}
