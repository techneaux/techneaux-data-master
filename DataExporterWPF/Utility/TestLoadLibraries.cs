﻿using CxFmsLib;
using System;
using System.Runtime.InteropServices;
using Serilog;
using Serilog.Context;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace DataMasterClient.Utility
{
    public static class TestLoadLibraries
    {

        [DllImport("kernel32.dll")]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        public static bool IsDllLoaded(string path)
        {
            return GetModuleHandle(path) != IntPtr.Zero;
        }

        public static bool AreDllsLoaded()
        {
            try
            {
                var x = new FmsClient();
                return true;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Failed while checking if DLLs are loaded.");

                return false;
            }
        }

    }
}
