﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DataMasterClient
{
    public static class ServiceOps
    {
        
        public static double DeleteTimeout { get; set; } = 300;

        public static double InsertTimeout { get; set; } = 300;

        public static double SelectTimeout { get; set; } = 300;
        
        public static double CriticalTimestampTimeoutMins { get; set; } = 60;
        
    }
    
}
