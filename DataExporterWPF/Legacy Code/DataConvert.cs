﻿using CXSCRIPTLib;
using CygNet.Core;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DataExporter.DataModel.ReportElm;

namespace DataExporter
{
    public static class DataConvert
    {
        private static Dictionary<string, UnitInfo> abbrevUnits = null;

        public static string ConvertAndFormat(string inputValue, PartsOfNumber numPart, ConversionTypes conversion, string customFormatString, string pointTag)
        {
            switch (conversion)
            {
                case ConversionTypes.String:
                    return inputValue;
                case ConversionTypes.String_toDateTime:
                    if (IsDateTime(inputValue))
                    {
                        return String_toDateTime(inputValue).ToString(customFormatString);
                    }
                    else
                    {
                        return inputValue;
                    }
                case ConversionTypes.BoolToYN:
                    return BoolToYN(inputValue);
                case ConversionTypes.BoolToY:
                    return BoolToY(inputValue);
                case ConversionTypes.BoolTo01:
                    return BoolTo01(inputValue);
                case ConversionTypes.YesNo_toPlusMinusOne:
                    return YesNo_toPlusMinusOne(inputValue);
                default:
                    if (IsNumeric(inputValue))
                    {
                        double DblValue = String_toDouble(inputValue);
                        DblValue = GetPartOfNumber(DblValue, numPart);

                        switch (conversion)
                        {
                            case ConversionTypes.DecimalNumber:
                                return DblValue.ToString(customFormatString);

                            case ConversionTypes.Feet_toInches:
                                return Feet_toInches(DblValue).ToString(customFormatString);

                            case ConversionTypes.Feet_toQuarterInches:
                                return Feet_toQuarterInches(DblValue).ToString(customFormatString);

                            case ConversionTypes.Feet_toFracQuarterInches:
                                return Feet_toFracQuarterInches(DblValue);

                            case ConversionTypes.Feet_toDecQuarterInches:
                                return Feet_toDecQuarterInches(DblValue);

                            case ConversionTypes.Meters_toMM:
                                return Meters_toMM(DblValue).ToString(customFormatString);

                            case ConversionTypes.Centimeters_toMM:
                                return Centimeters_toMM(DblValue).ToString(customFormatString);

                            case ConversionTypes.Inches_toFeet:
                                return Inches_toFeet(DblValue).ToString(customFormatString);

                            case ConversionTypes.Inches_toExcessInches:
                                return Inches_toExcessInches(DblValue).ToString(customFormatString);

                            case ConversionTypes.AltUnits:
                                return DataConvert.AltUnitConversion(DblValue, pointTag).ToString();

                            default:
                                return DblValue.ToString(customFormatString);
                        }
                    }
                    else
                    {
                        return inputValue;
                    }
            }

        }

        public static string AltUnitConversion(double value, string pointTag)
        {
            //Use tag to get a point object and fetch the units and alt units
            Points pts = new Points();
            //string pointTag = factag + "."; //+ ColumnData.Item;
            Point objPoint = pts.Point(pointTag);

            

            //Using these and the conversion library from cygnet, convert and return the value as a string
            UnitConversions uconvers = new UnitConversions();

            if (abbrevUnits == null)
            {
                abbrevUnits = new Dictionary<string, UnitInfo>();
                foreach (UnitInfo unit in UnitConversions.GetAllUnitInfo())
                {
                    abbrevUnits.Add(unit.Abbreviation, unit);
                }
            }
            string primaryUnits = objPoint.PrimaryUnits;
            string alterUnits = objPoint.AlternateUnits;
            return uconvers.ConvertValue(objPoint.PrimaryUnits, objPoint.AlternateUnits, value).ToString();
        }

        public static string YesNo_toPlusMinusOne(string value)
        {
            if (value == "Y")
                return "1";
            else
            {
                return "-1";
            }
        }

        public static string BoolToYN(string value)
        {
            bool val;

            if (bool.TryParse(value, out val))
            {
                if (val)
                {
                    return "Y";
                }
                else
                {
                    return "N";
                }
            }
            else
            {
                return null;
            }
        }

        public static string BoolToY(string value)
        {
            bool val;

            if (bool.TryParse(value, out val))
            {
                if (val)
                {
                    return "Y";
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return null;
            }
        }

        public static string BoolTo01(string value)
        {
            bool val;

            if (bool.TryParse(value, out val))
            {
                return Convert.ToInt16(val).ToString();
            }
            else
            {
                return null;
            }
        }

        public static bool IsNumeric(string value)
        {
            double TestDbl;
            return double.TryParse(value, out TestDbl);
        }
        public static double String_toDouble(string value)
        {
            if (IsNumeric(value))
            {
                return double.Parse(value);
            }
            else
            {
                return double.NaN;
            }

        }

        public static double GetPartOfNumber(double value, PartsOfNumber desiredPart)
        {
            switch (desiredPart)
            {
                case PartsOfNumber.FullNumber:
                    return value;
                case PartsOfNumber.IntegerPart:
                    return Math.Truncate(value);
                case PartsOfNumber.Floor:
                    return Math.Floor(value);
                case PartsOfNumber.Ceiling:
                    return Math.Ceiling(value);
                case PartsOfNumber.DecimalPart:
                    return value - Math.Truncate(value);
                default:
                    return value;
            }
        }

        public static bool IsDateTime(string value)
        {
            DateTime TestDate;
            return DateTime.TryParse(value, out TestDate);
        }

        public static DateTime String_toDateTime(string value)
        {
            if (IsDateTime(value))
            {
                return DateTime.Parse(value);
            }
            else
            {
                return new DateTime();
            }
        }

        public static double Meters_toMM(double value)
        {
            return value * 1000;
        }

        public static double Centimeters_toMM(double value)
        {
            return value * 100;
        }

        public static double Feet_toInches(double value)
        {
            return value * 12;
        }

        public static double Feet_toQuarterInches(double value)
        {
            return Feet_toInches(value) * 4;
        }

        public static string Feet_toDecQuarterInches(double value)
        {
            // First convert to inches, and then take the fractional part of inches, then round to nearest quarter (in decimal)
            value = GetPartOfNumber(Feet_toInches(value), PartsOfNumber.DecimalPart);
            return RoundToNearestQuarterDec(value);
        }

        public static string Feet_toFracQuarterInches(double value)
        {
            // First convert to inches, and then take the fractional part of inches, then round to nearest quarter (in fractions)
            value = GetPartOfNumber(Feet_toInches(value), PartsOfNumber.DecimalPart);
            return RoundToNearestQuarterFrac(value);
        }

        public static string RoundToNearestQuarterDec(double value)
        {
            int numQuarters = Convert.ToInt16(Math.Floor((value - Math.Truncate(value)) / .25));
            double quarters = numQuarters * .25;
            return quarters.ToString(".##");
        }

        public static string RoundToNearestQuarterFrac(double value)
        {
            int numQuarters = Convert.ToInt16(Math.Floor((value - Math.Truncate(value)) / .25));
            switch (numQuarters)
            {
                case 0:
                    return "";
                case 1:
                    return "1/4";
                case 2:
                    return "1/2";
                case 3:
                    return "3/4";
                default:
                    return "";

            }
        }

        public static double Inches_toFeet(double value)
        {
            return Math.Floor(value / 12);
        }

        public static double Inches_toExcessInches(double value)
        {
            return value - Math.Floor(value / 12) * 12;
        }
    }
}

