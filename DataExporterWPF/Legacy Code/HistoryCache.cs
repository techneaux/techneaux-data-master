﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using static DataExporter.PointHistory;

namespace DataExporter
{

    public static class HistoryCache
    {
        private static ConcurrentDictionary<string, CachedHistory> PointValueCache = new ConcurrentDictionary<string, CachedHistory>();
        private static ConcurrentDictionary<DateTime, string> PointTimestampCache = new ConcurrentDictionary<DateTime, string>();

        public static void AddHistoryToCache(
            string vhsService,
            string pointTag,
            DateTime earliestTime,
            DateTime latestTime,
            bool allowUnreliable)
        {
            string cacheKey = GetCacheKey(vhsService, pointTag, allowUnreliable);

            CachedHistory ThisHist;
            if (!PointValueCache.TryGetValue(cacheKey, out ThisHist)
                || ThisHist.EarliestDate > earliestTime
                || ThisHist.LatestDate < latestTime)
            {
                CachedHistory thisHist = new CachedHistory();
                thisHist.HistoryValues = PointHistory.GetHistoryValues(vhsService, pointTag, earliestTime, latestTime, allowUnreliable);
                thisHist.EarliestDate = earliestTime;
                thisHist.LatestDate = latestTime;

                if (thisHist.HistoryValues == null)
                    return;

                PointValueCache.TryAdd(cacheKey, thisHist);
                PointTimestampCache.TryAdd(DateTime.Now, cacheKey);

                if (PointTimestampCache.Count > 20)
                {
                    DateTime OldestTimestamp = PointTimestampCache.Keys.Min();

                    string KeyToRemove;

                    if (PointTimestampCache.TryGetValue(OldestTimestamp, out KeyToRemove))
                    {
                        CachedHistory ThisVal;
                        PointValueCache.TryRemove(KeyToRemove, out ThisVal);

                        string ThisKey;
                        PointTimestampCache.TryRemove(OldestTimestamp, out ThisKey);
                    }
                }
            }
        }

        public static bool TryGetHistoryFromCache(
            string vhsService,
            string pointTag,
            DateTime earliestTime,
            DateTime latestTime,
            bool allowUnreliable,
             out List<PointValue> thisValue)
        {
            string cacheKey = GetCacheKey(vhsService, pointTag, allowUnreliable);

            CachedHistory thisHist;
            if (PointValueCache.TryGetValue(cacheKey, out thisHist))
            {
                if (thisHist.EarliestDate <= earliestTime && thisHist.LatestDate >= latestTime)
                {
                    thisValue = thisHist.HistoryValues.Where(val => val.Timestamp >= earliestTime && val.Timestamp <= latestTime).ToList();
                    return true;
                }
            }

            thisValue = null;
            return false;
        }

        public static string GetCacheKey(
             string vhsService,
             string pointTag,
             bool allowUnreliable)
        {
            return $"vhs={vhsService}|tag={pointTag}|unreliable={allowUnreliable}";
        }

        public struct CachedHistory
        {
            public DateTime EarliestDate;
            public DateTime LatestDate;

            public List<PointValue> HistoryValues;
        }

    }

}