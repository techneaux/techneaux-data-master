using System.Windows.Input;
using DataMasterClient.Forms;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using TechneauxWpfControls.DataConverters;
using XmlDataModelUtility;

namespace DataMasterClient.ViewModels.CygNet.Sub
{
    public class CygNetFacilityFilterConditionViewModel : NotifyDynamicBase<FacilityAttributeFilterCondition>
    {
        public CygNetFacilityFilterConditionViewModel(FacilityAttributeFilterCondition srcModel) : base(srcModel)
        {
        }

        public ICommand OpenFacilityFilterAttributeWindow => new DelegateCommand(OpenSelectFacilityFilterAttributeWindow);

        public void OpenSelectFacilityFilterAttributeWindow(object inputObject)
        {
            var windowInstance = FacilityAttributeChooser.Instance();

            var success = windowInstance.TryGetKeyDescId(out var result);
            if (!success) return;

            if (result.Value != null)
            {
                MyDataModel.AttributeId = result.Value as string;
            }
            if (result.Description != null)
            {
                MyDataModel.AttributeDescription = result.Description as string;

            }
        }
    }
}
