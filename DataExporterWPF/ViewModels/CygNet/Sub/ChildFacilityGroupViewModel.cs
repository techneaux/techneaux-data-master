using System.ComponentModel;
using System.Linq;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using XmlDataModelUtility;

namespace DataMasterClient.ViewModels.CygNet.Sub
{
    public class ChildFacilityGroupViewModel : NotifyDynamicBase<ChildFacilityGroup>
    {
        public ChildFacilityGroupViewModel(ChildFacilityGroup srcModel) : base(srcModel)
        {
            ChildGroupConditionsViewModels = new BindingList<CygNetFacilityFilterConditionViewModel>();
            for (var i = 0; i < srcModel.FilterConditions.Count(); i++)
            {
                var filterModel = new CygNetFacilityFilterConditionViewModel(srcModel.FilterConditions[i]);
                ChildGroupConditionsViewModels.Add(filterModel);
            }
        }

        public BindingList<CygNetFacilityFilterConditionViewModel> ChildGroupConditionsViewModels
        {
            get {

                OnPropertyChanged("ConditionsString");
                return GetPropertyValue<BindingList<CygNetFacilityFilterConditionViewModel>>(); }
            set {
                OnPropertyChanged("ConditionsString");
                SetPropertyValue(value); }
        }

        public bool DeleteCondition(int index)
        {
            if (MyDataModel.FilterConditions.ElementAtOrDefault(index) == null)
                return false;

            MyDataModel.FilterConditions.RemoveAt(index);
            return true;
        }

        public FacilityAttributeFilterCondition AddNewCondition( int index)
        {
            var newCondition = new FacilityAttributeFilterCondition();
            MyDataModel.FilterConditions.Insert(index, newCondition);

            return newCondition;
        }
    }
}