using System.Collections.Generic;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory.DatabaseOperations;
using XmlDataModelUtility;

namespace DataMasterClient.ViewModels
{
    public class SqlTableViewModel : NotifyModelBase
    {     
        public SqlTableViewModel(Table srcModel)
        {
            Name = srcModel.Name;
            SqlColumnSubViewModels = new List<SqlColumnViewModel>();

            return;
            foreach (SimpleSqlColumn col in srcModel.Columns)
            {
                SqlColumnSubViewModels.Add(new SqlColumnViewModel(col));
            }
        }

        public string Name
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
        public List<SqlColumnViewModel> SqlColumnSubViewModels
        {
            get => GetPropertyValue<List<SqlColumnViewModel>>();
            set => SetPropertyValue(value);
        }
    }
}
