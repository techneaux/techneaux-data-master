using System;
using System.Windows;
using Serilog;
using Serilog.Context;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory.DatabaseOperations;
using XmlDataModelUtility;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace DataMasterClient.ViewModels
{
    public class SqlColumnViewModel : NotifyModelBase
    {
        public SqlColumnViewModel(SimpleSqlColumn srcModel)
        {
            Name = srcModel.Name;
            Id = srcModel.ID.ToString();
            try
            {
                DataTypeName = srcModel.DataType.ToString();
                DataTypeSize = srcModel.FieldLength.ToString();

                if (srcModel.InPrimaryKey)
                {
                    IsPrimaryKeyVisible = Visibility.Visible;
                }
                else
                {
                    IsPrimaryKeyVisible = Visibility.Collapsed;
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Debug(ex, "Error creating view modes for SQL columns");

                IsPrimaryKeyVisible = Visibility.Collapsed;
            }
        }

        public Visibility IsPrimaryKeyVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        public string Name
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public string Id
        {
            get => GetPropertyValue<string>();
            set { SetPropertyValue(value); }
        }

        public string DataTypeName
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public string DataTypeSize
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
    }
}
