﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DataMasterClient.Forms;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using TechneauxHistorySynchronization.Helper;
using TechneauxHistorySynchronization.Models;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using static TechneauxHistorySynchronization.Models.CygNetSqlDataCompareModel;
using Serilog;
using XmlDataModelUtility;
using ReactiveUI.Legacy;
using Serilog.Context;
using TechneauxUtility;
using Xceed.Wpf.DataGrid.Markup;
using static XmlDataModelUtility.GlobalLoggers;


namespace DataMasterClient.ViewModels
{
    public class LivePreviewPointSubViewModel : ReactiveObject
    {
        private readonly SqlGeneralViewModelReactive _sqlSelectionVm;

        private readonly IObservable<EventPattern<ListChangedEventArgs>> _mappingRulesChangedObs;

        private readonly ObservableAsPropertyHelper<CygNetSqlDataCompareModel> _sqlCompareModel;
        public CygNetSqlDataCompareModel SqlCompareModel => _sqlCompareModel.Value;

        private bool _firstPointSelectionFlag = false;
        private bool FirstPointSelectionFlag
        {
            get => _firstPointSelectionFlag;
            set => this.RaiseAndSetIfChanged(ref _firstPointSelectionFlag, value);
        }

        private readonly LivePreviewViewModel _parentVm;

        public LivePreviewPointSubViewModel(
            LivePreviewViewModel parentVm,
            SqlGeneralViewModelReactive srcSqlModel,
            BindingList<SqlPointSelection> pntSelectionRules,
            BindingList<SqlTableMapping> sqlMappingRules)
        {
            //_srcPnt = srcPoint ?? throw new ArgumentNullException($"Source point [{nameof(srcPoint)}] must not be null");
            _sqlSelectionVm = srcSqlModel;

            //MappingRules = sqlMappingRules;
            // Point history

            _parentVm = parentVm;

            _srcPntRule = Observable.FromEventPattern<ListChangedEventHandler, ListChangedEventArgs>(
                    h => pntSelectionRules.ListChanged += h,
                    h => pntSelectionRules.ListChanged -= h)
                .Where(evt => evt.EventArgs.ListChangedType == ListChangedType.ItemChanged)
                .Select(evt => pntSelectionRules[evt.EventArgs.NewIndex])
                .Do(arg => Console.WriteLine($"Last prop updated: {arg.LastPropertyUpdated}"))
                .Where(updatedRule =>
                    updatedRule.LastPropertyUpdated == nameof(SqlPointSelection.HistoryNormalizationOptions) ||
                    updatedRule.LastPropertyUpdated == nameof(SqlPointSelection.PollingOptions) ||
                    updatedRule.LastPropertyUpdated == nameof(SqlPointSelection.GeneralHistoryOptions))
                .Do(arg => Console.WriteLine(
                    $"Is Point null: [{_parentVm.SelectedPoint == null}], rule guid=[{arg.Guid}], point rule guid=[{_parentVm.SelectedPoint?.MyDataModel.SrcRule.Guid.ToString()}]"))
                .Select(rule => _parentVm.SelectedPoint != null && rule.Guid == _parentVm?.SelectedPoint?.MyDataModel.SrcRule.Guid
                    ? rule.Copy()
                    : new SqlPointSelection())
                .Throttle(TimeSpan.FromSeconds(.5), RxApp.MainThreadScheduler)
                .ObserveOnDispatcher()
                .ToProperty(this, x => x.SrcPntRule, new SqlPointSelection());

            _sqlCompareModel = this
                .WhenAnyValue(me => me._parentVm.SelectedPoint)
                .Select(pnt => new CygNetSqlDataCompareModel(pnt?.MyDataModel, ServiceOps.CriticalTimestampTimeoutMins))
                .ToProperty(this, x => x.SqlCompareModel, new CygNetSqlDataCompareModel(null, ServiceOps.CriticalTimestampTimeoutMins));

            var pointHistRulesChanged = this
                .WhenAnyValue(me => me._parentVm.SelectedPoint, me => me.SrcPntRule, me => me.FirstPointSelectionFlag)
                .Throttle(TimeSpan.FromSeconds(.8), RxApp.MainThreadScheduler)
                .ObserveOnDispatcher()
                .Publish()
                .RefCount();

            HistoryProgress = new Progress<int>(percent =>
            {
                HistoryProgressPercent = percent;
            });

            // Update CygNet history
            var getNewCygNetHistoryCmd = ReactiveCommand
                .CreateFromObservable(() => Observable
                    .StartAsync(SqlGetHistoryAsync)
                    .TakeUntil(pointHistRulesChanged));

            getNewCygNetHistoryCmd.ThrownExceptions.Subscribe(ex =>
            {
                Log.Error(ex, "Unhandled UI Exception while getting CygNet history");
                MessageBox.Show(
                    "Unhandled UI Exception while getting CygNet history, please contact Techneaux support");
            });

            pointHistRulesChanged.Subscribe(_ => getNewCygNetHistoryCmd.Execute().Subscribe());

            //pointHistRulesChanged.Subscribe(_ => Console.WriteLine("changed src rule"));

            getNewCygNetHistoryCmd.ThrownExceptions.Subscribe(er => Console.WriteLine($"Cmd exception: {er.ToString()}"));

            _cygHistory = getNewCygNetHistoryCmd.Do(cmd => Console.WriteLine($"this cmd: {cmd == null}"))
                .ToProperty(this, x => x.CygHistory, new CygNetHistoryResults());

            // Comparison results update


            //var GetNewCompareCmd = ReactiveCommand
            //    .CreateFromObservable((CygNetHistoryResults cygHist, CancellationToken ct) => Observable
            //        .StartAsync(SqlSyncAsync(cygHist, ct))
            //        .TakeUntil(pointHistRulesChanged));

            var sqlRulesChanged = Observable.FromEventPattern<ListChangedEventHandler, ListChangedEventArgs>(
                h => _sqlSelectionVm.SqlFieldList.SourceRules.ListChanged += h,
                h => _sqlSelectionVm.SqlFieldList.SourceRules.ListChanged -= h);

            //var timeEvt = Observable.Interval(TimeSpan.FromSeconds(5)).Publish().RefCount();

            // sqlRulesChanged.Subscribe(_ => Console.WriteLine(@"SqlHistRulesChanged"));

            _mappingRulesChangedObs = Observable.FromEventPattern<ListChangedEventHandler, ListChangedEventArgs>(
                h => sqlMappingRules.ListChanged += h,
                h => sqlMappingRules.ListChanged -= h)
                .Throttle(TimeSpan.FromSeconds(.5), RxApp.MainThreadScheduler);

            //.Select(evt => evt.Sender as BindingList<SqlTableMapping>);

            _mappingRules = this.WhenAnyObservable(me => me._mappingRulesChangedObs)
                .Throttle(TimeSpan.FromSeconds(.5), RxApp.MainThreadScheduler)
                .Select(evt => new BindingList<SqlTableMapping>((evt.Sender as BindingList<SqlTableMapping>).ToList()))
                .ToProperty(this, x => x.MappingRules, sqlMappingRules);

            _currentTableSchema = this.WhenAnyValue(me => me._sqlSelectionVm.SqlSchema, me => me.MappingRules)
                .Select(arg => arg.Item1.WithCygNetColumns(arg.Item2))
                .Do(_ => Console.WriteLine("List Changed"))
                .ToProperty(this, x => x.CurrentTableSchema, SimpleCombinedTableSchema.Empty());

            _historyTrendVm = this.WhenAnyValue(x => x.CygHistory)
                .Do(x => Console.WriteLine($"Method {nameof(_historyTrendVm)} is on thread {Thread.CurrentThread.ManagedThreadId} vs dispatcher {Application.Current.Dispatcher.Thread.ManagedThreadId}"))
                .ObserveOn(RxApp.MainThreadScheduler)
                .Do(x => Console.WriteLine($"Method {nameof(_historyTrendVm)} is on thread (after ObserveOn) {Thread.CurrentThread.ManagedThreadId} vs dispatcher {Application.Current.Dispatcher.Thread.ManagedThreadId}"))
                .Select(x => new HistoryTrendViewModel(x))

                .ToProperty(this, x => x.HistoryTrendVm);

            var sqlCompareUpdateChanged = this
                .WhenAnyValue(x => x.CygHistory, x => x.CurrentTableSchema, x => x.ButtonClickedFlag, x => x.MappingRules)
                .Publish()
                .RefCount();

            var getNewComparisonResultsCmd = ReactiveCommand
                .CreateFromObservable(() => Observable
                    .StartAsync(SqlCompareUpdateAsync)
                    .TakeUntil(sqlCompareUpdateChanged));

            getNewComparisonResultsCmd.ThrownExceptions.Subscribe(ex =>
            {
                Log.Error(ex, "Unhandled UI Exception while getting new data comparison results");
                MessageBox.Show(
                    "Unhandled UI Exception while getting new data comparison results, please contact Techneaux support");
            });

            sqlCompareUpdateChanged.Subscribe(_ => getNewComparisonResultsCmd.Execute().Subscribe());

            getNewComparisonResultsCmd.ThrownExceptions
                .Do(er => Console.WriteLine($"Cmd exception: {er.ToString()}"));

            // Compare
            _sqlCompare = getNewComparisonResultsCmd.Do(cmd => Console.WriteLine($"this comparehistupdatedcmd: {cmd == null}"))
                .ObserveOn(RxApp.MainThreadScheduler)
                .ToProperty(this, x => x.SqlCompare, new SqlCompareResults());

            //_sqlCompare = GetNewComparisonResultsSchemaUpdatedCmd.Do(cmd => Console.WriteLine($"this compareschemaupdated cmd: {cmd == null}"))
            //    .ToProperty(this, x => x.SqlCompare, new SqlCompareResults());

            //_sqlCompare = GetNewComparisonResultsMappingUpdatedCmd.Do(cmd => Console.WriteLine($"this comparemappingupdated cmd: {cmd == null}"))
            //     .ToProperty(this, x => x.SqlCompare, new SqlCompareResults());

            //_sqlCompare = GetNewComparisonResultsButtonClickedCmd.Do(cmd => Console.WriteLine($"this comparebuttonclicked cmd: {cmd == null}"))
            //     .ToProperty(this, x => x.SqlCompare, new SqlCompareResults());

            var sqlCompareListUpdate = this
                .WhenAnyValue(x => x.SqlCompare)
                .Publish()
                .RefCount();

            _sqlCompareList = sqlCompareListUpdate
                .ObserveOn(RxApp.MainThreadScheduler)
                .Select(x => x.ComparedRows != null
                    ? x.ComparedRows.Select(y => new SqlRowViewModel(y))
                        .ToList()
                    : new List<SqlRowViewModel>()).Select(list => list.ToObservable().Delay(TimeSpan.FromMilliseconds(10)).CreateCollection())
                .ObserveOnDispatcher()
                .ToProperty(this, x => x.SqlCompareList);

            _sqlColumnsList = this.WhenAnyValue(x => x.CurrentTableSchema)
                .Select(x => x.MatchedCols.Select(col => col.Name).ToList())
                .ObserveOnDispatcher()
                .ToProperty(this, x => x.SqlColumnsList, new List<string>());

            //_isCompareViewBusy = GetNewCygNetHistoryCmd.IsExecuting.ToProperty(this, x => x.IsCompareViewBusy, false
        }

        //public PropertyProgress<int> HistoryProgress { get; } = new PropertyProgress<int>(0);

        public IProgress<int> HistoryProgress { get; set; }

        private int _historyProgressPercent;
        public int HistoryProgressPercent
        {
            get => _historyProgressPercent;
            set => this.RaiseAndSetIfChanged(ref _historyProgressPercent, value);
        }

        //private CachedCygNetPointWithRule _srcPnt;
        //public CachedCygNetPointWithRule SrcPoint
        //{
        //    get => _srcPnt;
        //    set => this.RaiseAndSetIfChanged(ref _srcPnt, value);
        //}

        private ObservableAsPropertyHelper<SqlPointSelection> _srcPntRule;
        public SqlPointSelection SrcPntRule => _srcPntRule.Value;

        private ObservableAsPropertyHelper<BindingList<SqlTableMapping>> _mappingRules;
        public BindingList<SqlTableMapping> MappingRules => _mappingRules.Value;

        private bool _buttonClickedFlag;
        public bool ButtonClickedFlag
        {
            get => _buttonClickedFlag;
            set => this.RaiseAndSetIfChanged(ref _buttonClickedFlag, value);
        }

        private readonly ObservableAsPropertyHelper<CygNetHistoryResults> _cygHistory;
        public CygNetHistoryResults CygHistory => _cygHistory.Value;

        private readonly ObservableAsPropertyHelper<SqlCompareResults> _sqlCompare;
        public SqlCompareResults SqlCompare => _sqlCompare.Value;

        private readonly ObservableAsPropertyHelper<List<string>> _sqlColumnsList;
        public List<string> SqlColumnsList => _sqlColumnsList.Value;

        private readonly ObservableAsPropertyHelper<HistoryTrendViewModel> _historyTrendVm;
        public HistoryTrendViewModel HistoryTrendVm => _historyTrendVm.Value;

        private readonly ObservableAsPropertyHelper<SimpleCombinedTableSchema> _currentTableSchema;
        public SimpleCombinedTableSchema CurrentTableSchema => _currentTableSchema.Value;

        private readonly ObservableAsPropertyHelper<IReactiveDerivedList<SqlRowViewModel>> _sqlCompareList;
        public IReactiveDerivedList<SqlRowViewModel> SqlCompareList => _sqlCompareList.Value;

        private string _validationError;
        public string ValidationError
        {
            get => _validationError;
            set => this.RaiseAndSetIfChanged(ref _validationError, value);
        }

        private bool _isGetHistBusy;
        public bool IsGetHistBusy
        {
            get => _isGetHistBusy;
            set => this.RaiseAndSetIfChanged(ref _isGetHistBusy, value);
        }
        private bool _isCompareViewBusy;
        public bool IsCompareViewBusy
        {
            get => _isCompareViewBusy;
            set => this.RaiseAndSetIfChanged(ref _isCompareViewBusy, value);
        }

        public bool InverseIsCompareViewBusy
        {
            get => !_isCompareViewBusy;
        }

        public ICommand SyncButton => new DelegateCommand(SyncTaskAsync);
        public async void SyncTaskAsync(object inputObject)
        {
            //fix later. make command for binding to button . currentcomparemodel.updatesqltable on button click
            //Task.Run(() => SqlSyncAsync());
            var (isValid, failureReason) = ValidateRulesDataOnly(CurrentTableSchema, CygHistory, _parentVm?.SelectedPoint?.MyDataModel);

            //var SqlCompareModel = new CygNetSqlDataCompareModel(SrcPoint);

            if (isValid)
            {
                SinglePointSyncPopup.ShowPopup(
                    SqlCompareModel,
                    CurrentTableSchema,
                    SqlCompare.ComparedRows);
                ButtonClickedFlag = !ButtonClickedFlag;
            }
            ValidationError = failureReason;
        }

        // Methods
        public async Task<CygNetHistoryResults> SqlGetHistoryAsync(
            CancellationToken ct)
        {
            try
            {
                Console.WriteLine("Entered on hist lock");

                await _blockGetHist.WaitAsync(ct);

                HistoryProgress.Report(0);

                var srcPoint = _parentVm.SelectedPoint;
                var pntRule = _parentVm.SelectedPoint?.MyDataModel?.SrcRule;

                IsGetHistBusy = true;

                //var SqlCompareModel = new CygNetSqlDataCompareModel(srcPoint);

                if (srcPoint == null)
                    return new CygNetHistoryResults();
                bool includeNumericOnly = true;
                //foreach (var rule in CurrentTableSchema.CygNetMappingRules)
                //{
                //    if ((rule.CygNetElementRule.DataElement == TechneauxReportingDataModel.CygNet.Rules.CygNetRuleBase.DataElements.PointHistory
                //        || rule.CygNetElementRule.DataElement == TechneauxReportingDataModel.CygNet.Rules.CygNetRuleBase.DataElements.PointCurrentValue)
                //        && (rule.CygNetElementRule.PointHistoryOptions.PointValueType == TechneauxReportingDataModel.CygNet.FacilityPointOptions.PointHistoryGeneralOptions.PointValueTypesNew.Value
                //        || rule.CygNetElementRule.PointHistoryOptions.PointValueType == TechneauxReportingDataModel.CygNet.FacilityPointOptions.PointHistoryGeneralOptions.PointValueTypesNew.AltValue))
                //    {
                //        foreach (var col in CurrentTableSchema.MatchedCols)
                //        {
                //            if (rule.SqlTableFieldName == col.Name && col.IsStringType)
                //            {
                //                includeNumericOnly = false;
                //            }
                //        }
                //    }
                //}
                if (!pntRule.HistoryNormalizationOptions.EnableNormalization)
                    includeNumericOnly = false;
                if (pntRule.HistoryNormalizationOptions.IsCalculatedNormType)
                {
                    includeNumericOnly = true;
                }
                else
                {
                    includeNumericOnly = false;
                }
                var cygHist = await Task.Run(() => SqlCompareModel.GetCygNetHistory(
                    pntRule.PollingOptions.RetentionDays,
                    pntRule.GeneralHistoryOptions,
                    pntRule.HistoryNormalizationOptions,
                    HistoryProgress,
                    includeNumericOnly,
                    ct),
                    ct);

                return cygHist;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Error getting CygNet history");
                
                //System.Windows.Forms.MessageBox.Show(
                //    $@"Failed to get history: {ex.Message} {Environment.NewLine}Operation Failed");

                //if (ex.InnerException != null)
                //    System.Windows.Forms.MessageBox.Show($@"Inner exception = {ex.InnerException.Message}");

                return new CygNetHistoryResults();
            }
            finally
            {
                IsGetHistBusy = false;
                _blockGetHist.Release();
                Console.WriteLine("Release hist lock");
            }
        }

        private SemaphoreSlim _blockGetHist = new SemaphoreSlim(1, 1);
        private SemaphoreSlim _blockCompare = new SemaphoreSlim(1, 1);
        public async Task<SqlCompareResults> SqlCompareUpdateAsync(CancellationToken ct)
        {
            try
            {
                await _blockCompare.WaitAsync();
                IsCompareViewBusy = true;


                var sqlComp = new SqlCompareResults();
                var (isValid, failureReason) = ValidateRulesDataOnly(CurrentTableSchema, CygHistory, _parentVm?.SelectedPoint?.MyDataModel);
                if (isValid)
                {
                    sqlComp = await Task.Run(() => SqlCompareModel.GetNewSqlComparisonResults(
                                 CurrentTableSchema,
                                 CygHistory,
                                 ct));
                }

                ValidationError = failureReason;

                return sqlComp;
            }
            catch (Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show(
                //    $@"Failed to create sql comparison: {ex.Message} {Environment.NewLine}Operation Failed");

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception while getting comparison results.");

                ValidationError = "Sql connection timed out.";

                //if (ex.InnerException != null)
                //    System.Windows.Forms.MessageBox.Show($@"Inner exception = {ex.InnerException.Message}");

                return new SqlCompareResults
                {
                    CompException = ex
                };
            }
            finally
            {
                IsCompareViewBusy = false;
                _blockCompare.Release();
            }
        }

    }
}