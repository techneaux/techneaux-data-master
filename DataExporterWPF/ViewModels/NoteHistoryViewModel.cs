using System.Windows.Input;
using DataMasterClient.Forms;
using TechneauxReportingDataModel.CygNet.NoteSubOptions;
using TechneauxWpfControls.DataConverters;
using XmlDataModelUtility;

namespace DataMasterClient.ViewModels
{
    public class NoteHistoryViewModel : NotifyDynamicBase<NoteOptions>
    {
        public NoteHistoryViewModel(NoteOptions srcModel) : base(srcModel)
        {
        }

        public void ClearControl()
        {
            MyDataModel.AttributeId.Id = "";
            MyDataModel.AttributeId.Desc = "";
        }
        public ICommand OpenRuleFacilityNoteWindow => new DelegateCommand(OpenSelectRuleFacilityNoteWindow);

        public void OpenSelectRuleFacilityNoteWindow(object inputObject)
        {
            var windowInstance = FacilityAttributeChooser.Instance();

            var success = windowInstance.TryGetKeyDescId(out var result);
            if (success)
            {
                if (result.Value != null)
                {
                    MyDataModel.AttributeId.Id = result.Value as string;

                }
                if (result.Description != null)
                {
                    MyDataModel.AttributeId.Desc = result.Description;

                }
            }
        }
        public ICommand OpenRulePointNoteWindow => new DelegateCommand(OpenSelectRulePointNoteWindow);

        public void OpenSelectRulePointNoteWindow(object inputObject)
        {
            var windowInstance = PointAttributeChooser.Instance();

            var success = windowInstance.TryGetKeyDescId(out var result);
            if (!success) return;
            if (result.Value != null)
            {
                MyDataModel.AttributeId.Id = result.Value as string;

            }
            if (result.Description != null)
            {
                MyDataModel.AttributeId.Desc = result.Description;

            }
        }
    }
}