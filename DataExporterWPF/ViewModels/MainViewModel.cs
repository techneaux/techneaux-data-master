using System.Collections.Generic;
using System.Threading.Tasks;
using DataMasterClient.SmartSheetSync.ViewModels;
using DataMasterClient.SqlHistorySync.ViewModels;
using DataMasterClient.Utility;
using DataMasterClient.ViewModels.CygNet.Reactive;
using Techneaux.CygNetWrapper.Points;
using TechneauxReportingDataModel.General;
using XmlDataModelUtility;

namespace DataMasterClient.ViewModels
{
    public class MainViewModel : NotifyDynamicBase<ReportConfigModel>
    {

        public List<ICachedCygNetPoint> PointList
        {
            get => GetPropertyValue<List<ICachedCygNetPoint>>();
            set => SetPropertyValue(value);
        }

        public CygNetFacilityPointSearchViewModel ReactiveCygNetViewModel
        {
            get => GetPropertyValue<CygNetFacilityPointSearchViewModel>();
            set => SetPropertyValue(value);
        }

        public MainSqlViewModel SqlSectionViewModel
        {
            get => GetPropertyValue<MainSqlViewModel>();
            set => SetPropertyValue(value);
        }

        public MainSmartSheetViewModel SmartSheetViewModel
        {
            get => GetPropertyValue<MainSmartSheetViewModel>();
            set => SetPropertyValue(value);
        }

        public async Task LoadData()
        {
            //await SqlSectionViewModel.SqlGeneralSubViewModel.LoadData();
        }
        

        public MainViewModel(ReportConfigModel srcModel) : base(srcModel)
        {
            ReactiveCygNetViewModel = new CygNetFacilityPointSearchViewModel(srcModel.CygNetGeneral);

            if (AvailableOptions.MainType == AvailableOptions.SyncOptions.SqlHistory)
            {
                    SqlSectionViewModel = new MainSqlViewModel(srcModel.SqlConfigModel, ReactiveCygNetViewModel);
            }

            if (AvailableOptions.MainType == AvailableOptions.SyncOptions.SmartSheet)
            {

                    SmartSheetViewModel = new MainSmartSheetViewModel(
                        srcModel.SmartSheetConfigModel,
                        ReactiveCygNetViewModel,
                        srcModel.CygNetGeneral.FacilityFilteringRules.ChildGroups);

               
            }
            if(AvailableOptions.MainType == AvailableOptions.SyncOptions.SqlAndSmartSheet)
            {
                SqlSectionViewModel = new MainSqlViewModel(srcModel.SqlConfigModel, ReactiveCygNetViewModel);
                SmartSheetViewModel = new MainSmartSheetViewModel(
                    srcModel.SmartSheetConfigModel,
                    ReactiveCygNetViewModel,
                    srcModel.CygNetGeneral.FacilityFilteringRules.ChildGroups);
            }
        }
    }
}