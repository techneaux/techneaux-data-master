using System;
using System.ComponentModel;
using Serilog;
using Serilog.Context;
using TechneauxHistorySynchronization.SqlServerHistory.DatabaseOperations;
using XmlDataModelUtility;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace DataMasterClient.ViewModels
{
    public class SqlDatabaseViewModel : NotifyModelBase
    {
        public SqlDatabaseViewModel(Database db, string errorMessage)
        {
            Name = db.Name;
            SqlTableSubViewModel = new BindingList<SqlTableViewModel>();

            if (!db.IsAccessible)
                return;

            try
            {
                foreach (var tbl in db.Tables)
                {
                    SqlTableSubViewModel.Add(new SqlTableViewModel(tbl));
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception reading database");

            }

            ErrorMessage = errorMessage;
        }

        public SqlDatabaseViewModel()
        {
            ErrorMessage = "";

        }

        public string Name
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public BindingList<SqlTableViewModel> SqlTableSubViewModel
        {
            get => GetPropertyValue<BindingList<SqlTableViewModel>>();
            set => SetPropertyValue(value);
        }

        public string ErrorMessage
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

    }
}
