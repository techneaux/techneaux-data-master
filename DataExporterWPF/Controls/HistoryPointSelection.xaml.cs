﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using DataMasterClient.SqlHistorySync.ViewModels;
using MahApps.Metro.Controls;

namespace DataMasterClient.Controls
{
    /// <summary>
    /// Interaction logic for RuleSelection.xaml
    /// </summary>
    public partial class HistoryPointSelection : UserControl
    {
        private Point _startPoint;
        private readonly bool _isDragging = false;
    
        public delegate Point GetPosition(IInputElement element);

        private int _rowIndex = -1;
        public HistoryPointSelection()
        {
            InitializeComponent();

            HistPointRulePicker.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(RulePicker_PreviewMouseLeftButtonDown);
            HistPointRulePicker.PreviewMouseMove += new MouseEventHandler(RulePicker_PreviewMouseMove);
            HistPointRulePicker.Drop += new DragEventHandler(RulesDataGrid_Drop);
        }

        private void RulePicker_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            if (!e.OriginalSource.GetType().Equals(typeof(Rectangle)) && !e.OriginalSource.GetType().Equals(typeof(Path)) && !e.OriginalSource.GetType().Equals(typeof(MetroThumb)) &&
                   !e.OriginalSource.GetType().Equals(typeof(TextBlock)) && !e.OriginalSource.GetType().Equals(typeof(TextBox)) && !e.OriginalSource.GetType().Equals(typeof(Grid)) &&
                   !e.OriginalSource.GetType().Equals(typeof(Button)))
            {
                _rowIndex = GetCurrentRowIndex(e.GetPosition);
                if (_rowIndex < 0)
                    return;

                _startPoint = e.GetPosition(null);
            }
        }

        void RulePicker_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            _rowIndex = GetCurrentRowIndex(e.GetPosition);
            if (_rowIndex < 0)
                return;
            if (e.LeftButton == MouseButtonState.Pressed && !_isDragging && !e.OriginalSource.GetType().Equals(typeof(Rectangle)) && !e.OriginalSource.GetType().Equals(typeof(Path)) && !e.OriginalSource.GetType().Equals(typeof(MetroThumb))
                && !e.OriginalSource.GetType().Equals(typeof(TextBlock)) && !e.OriginalSource.GetType().Equals(typeof(TextBox)) && !e.OriginalSource.GetType().Equals(typeof(Grid)) &&
                !e.OriginalSource.GetType().Equals(typeof(Button)))
            {
                Point position = e.GetPosition(null);

                if (Math.Abs(position.X - _startPoint.X) > SystemParameters.MinimumHorizontalDragDistance*5 ||
                        Math.Abs(position.Y - _startPoint.Y) > SystemParameters.MinimumVerticalDragDistance*5)
                {
                    _rowIndex = GetCurrentRowIndex(e.GetPosition);
                    HistPointRulePicker.SelectedIndex = _rowIndex;
                    var selected = HistPointRulePicker.SelectedItem;
                
                    if (selected == null)
                        return;
                    var dragdropeffects = DragDropEffects.Move;
                    if (DragDrop.DoDragDrop(HistPointRulePicker, selected, dragdropeffects)
                                        != DragDropEffects.None)
                    {
                        HistPointRulePicker.SelectedItem = selected;
                    }

                }
            }
        }


        private void RulesDataGrid_Drop(object sender, DragEventArgs e)
        {
            if (!e.OriginalSource.GetType().Equals(typeof(Rectangle)) && !e.OriginalSource.GetType().Equals(typeof(Path)) && !e.OriginalSource.GetType().Equals(typeof(MetroThumb)) &&
                !e.OriginalSource.GetType().Equals(typeof(TextBlock)) && !e.OriginalSource.GetType().Equals(typeof(TextBox)) && !e.OriginalSource.GetType().Equals(typeof(Grid)) &&
                !e.OriginalSource.GetType().Equals(typeof(Button)))
            {
                if (_rowIndex < 0)
                    return;
                var index = GetCurrentRowIndex(e.GetPosition);
                if (index < 0)
                    return;
                if (index == _rowIndex)
                    return;
                if (index == HistPointRulePicker.Items.Count)
                {
                    return;
                }
            ((PointSelectionViewModel)DataContext).RuleList.DragAndDrop(_rowIndex, index);

            }
        }


        private bool GetMouseTargetRow(Visual theTarget, GetPosition position)
        {
            if (theTarget == null)
                return false;
            Rect rect = VisualTreeHelper.GetDescendantBounds(theTarget);
            var point = position((IInputElement)theTarget);
            return rect.Contains(point);
        }
        private DataGridRow GetRowItem(int index)
        {
            if (HistPointRulePicker.ItemContainerGenerator.Status
                    != GeneratorStatus.ContainersGenerated)
                return null;
            return HistPointRulePicker.ItemContainerGenerator.ContainerFromIndex(index)
                                                            as DataGridRow;
        }

        private int GetCurrentRowIndex(GetPosition pos)
        {
            var curIndex = -1;
            for (var i = 0; i < HistPointRulePicker.Items.Count; i++)
            {
                var itm = GetRowItem(i);
                if (GetMouseTargetRow(itm, pos))
                {
                    curIndex = i;
                    break;
                }
            }
            return curIndex;
        }

        private void RulePicker_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            foreach (var t in HistPointRulePicker.Columns)
            {
                t.Width = 0;
            }

            HistPointRulePicker.UpdateLayout();

            foreach (var t in HistPointRulePicker.Columns)
            {
                t.Width = new DataGridLength(1, DataGridLengthUnitType.Auto);
            }

        }

    }


}
