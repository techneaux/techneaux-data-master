﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using DataMasterClient.SqlHistorySync.ViewModels;
using DataMasterClient.ViewModels;

namespace DataMasterClient.Controls
{
    /// <summary>
    /// Interaction logic for LiveAnalysisPreview.xaml
    /// </summary>
    public partial class LiveAnalysisPreview : UserControl
    {
        public LiveAnalysisPreview()
        {
            InitializeComponent();

            Loaded += UserControl1_Loaded;
        }
        void UserControl1_Loaded(object sender, RoutedEventArgs e)
        {
            if (SummaryPreview.DataContext != null && SummaryPreview.DataContext is INotifyPropertyChanged)
            {
                var myMVm = DataContext as LivePreviewViewModel;
                var selectedPointVm = myMVm as INotifyPropertyChanged;

                AddGridColumns();
                //var myVM = TablePreview.DataContext as INotifyPropertyChanged;
                selectedPointVm.PropertyChanged += MyVM_PropertyChanged;
            }
        }
        private void MyVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //if (e.PropertyName == "SqlColumnsList" || e.PropertyName== "SqlDataPreview" || e.PropertyName == "SqlCompareList") 
            //{
            //    AddGridColumns();
            //}

            if (e.PropertyName == "SqlColumnsList")
            {
                AddGridColumns();
            }
        }
        private void AddGridColumns()
        {
            if (SummaryPreview.DataContext == null)
                throw new ArgumentNullException("Data context must not be null");

            if (!(SummaryPreview.DataContext is LivePreviewViewModel))
                throw new ArgumentException("Data context is wrong type");

            //var myMVm = this.DataContext as MainViewModel;
            //PointPreviewCompareViewModel myVm = myMVm.FacilityPointPreviewViewModel.FacilityPointPreviewCompareViewModel;
            var columnsListVm = SummaryPreview.DataContext as LivePreviewViewModel;

            //List<string> UdcList = myVm.UdcPreviewList;
            //if (!UdcList.IsAny())
            //    return;
            TablePreviewGrid.AllowsColumnReorder = true;

            if (columnsListVm.LivePointCompareVm == null || columnsListVm.LivePointCompareVm.SqlColumnsList == null)
                return;

            if (TablePreviewGrid.Columns.Count > 0)
            {
                for (var k = TablePreviewGrid.Columns.Count - 1; k >= 0; k--)
                {
                    TablePreviewGrid.Columns.RemoveAt(k);
                }
            }

            for (var i = TablePreviewGrid.Columns.Count; i < columnsListVm.LivePointCompareVm.SqlColumnsList.Count; i++)
            {
                TablePreviewGrid.Columns.Add(new GridViewColumn());
            }
            for (var i = 0; i < TablePreviewGrid.Columns.Count; i++)
            {
                var column = columnsListVm.LivePointCompareVm.SqlColumnsList[i];
                //GridViewColumn newColumn = new GridViewColumn();
                //BoolXOConverter HasUdcConverter = new BoolXOConverter();

                var columnBinding = new Binding("SqlValuesList[" + column + "]");
                //Binding columnBinding = new Binding("", i)

                var thisCol = TablePreviewGrid.Columns[i];

                thisCol.DisplayMemberBinding = columnBinding;
                thisCol.Header = column;

                thisCol.Width = thisCol.ActualWidth;
                thisCol.Width = double.NaN;
                //TablePreview.Columns.Add(newColumn);                    
            }

            var refreshDataPreviewBinding = new Binding("SqlCompareList");
            TablePreview.ItemsSource = null;
            //DataPreview.ItemsSource = null;
            BindingOperations.SetBinding(TablePreview, ItemsControl.ItemsSourceProperty, refreshDataPreviewBinding);

        }

        //private void NormHist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{

        //}
    }
}
