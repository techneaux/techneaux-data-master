﻿using System;
using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;
using TechneauxWpfControls.Simple;

namespace DataMasterClient.Controls
{
   
    /// <summary>
    /// Interaction logic for RuPicker.xaml
    /// </summary>
    public partial class RuleNormalizationPicker : UserControl
    {
        public RuleNormalizationPicker()
        {
            InitializeComponent();          
            
        }
       
        private void NumericUpDown_GotFocus(object sender, RoutedEventArgs e)
        {
            var focused = sender as NumericUpDown;
            focused.HideUpDownButtons = false;
        }

        private void NumericUpDown_LostFocus(object sender, RoutedEventArgs e)
        {
            var lostFocused = sender as NumericUpDown;
            lostFocused.HideUpDownButtons = true;
        }       
    }
}
