﻿using System.Windows.Controls;

namespace DataMasterClient.Controls
{
    /// <summary>
    /// Interaction logic for GeneralHistoryOptionsSelection.xaml
    /// </summary>
    public partial class GeneralHistoryOptionsSelection : UserControl
    {
        public GeneralHistoryOptionsSelection()
        {
            InitializeComponent();
        }
    }
}
