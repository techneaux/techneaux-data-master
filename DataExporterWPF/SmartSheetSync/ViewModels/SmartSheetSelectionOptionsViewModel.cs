﻿using DataMasterClient.SmartSheetSync.View;
using System.Windows.Input;
using TechneauxReportingDataModel.CygNet.FmsSubOptions;
using TechneauxReportingDataModel.SmartSheet.SubOptions;
using TechneauxWpfControls.DataConverters;
using XmlDataModelUtility;

namespace DataMasterClient.SmartSheetSync.ViewModels
{
    public class SmartSheetSelectionOptionsViewModel : NotifyDynamicBase<SheetSelectionOptions>
    {
        
        public SmartSheetSelectionOptionsViewModel(SheetSelectionOptions sourceDataModel) : base(sourceDataModel)
        {

        }
        public ICommand OpenKeyColWindow => new DelegateCommand(OpenSelectedKeyColWindow);
        public void OpenSelectedKeyColWindow(object inputObject)
        {
            if (SmartSheetColumnIdWindow.StaticDataContext == null)
                return;

            var windowInstance = SmartSheetColumnIdWindow.Instance();

            var success = windowInstance.TryGetKeyDescId(out var result);
            if (!success) return;
            if (result.Value != null)
            {
                MyDataModel.KeyColumn = result.Value as SmartSheetItemId;
            }
        }
    }
}
