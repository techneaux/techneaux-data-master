﻿using System.Collections.Generic;
using System.Linq;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.SmartSheet.SubOptions;
using TechneauxUtility;
using XmlDataModelUtility;

namespace DataMasterClient.SmartSheetSync.ViewModels
{
    public class ReportPreviewViewModel : NotifyModelBase
    {
        public Dictionary<SmartSheetItemId, IConvertibleValue> MyDataModel
        { get; }
        public ReportPreviewViewModel(Dictionary<SmartSheetItemId, IConvertibleValue> srcModel)
        {
            MyDataModel = srcModel;
            
            SmartSheetValuesList = srcModel.ToDictionary(item => item.Key.IdString, item => item.Value.StringValue);
            SmartSheetItemIdList = srcModel.ToDictionary(item => item.Key, item => item.Value.StringValue);
        }

        public Dictionary<string, string> SmartSheetValuesList
        {
            get => GetPropertyValue<Dictionary<string, string>>();
            set => SetPropertyValue(value);
        }

        public Dictionary<SmartSheetItemId, string> SmartSheetItemIdList
        {
            get => GetPropertyValue<Dictionary<SmartSheetItemId, string>>();
            set => SetPropertyValue(value);

        }
        
    }
}
