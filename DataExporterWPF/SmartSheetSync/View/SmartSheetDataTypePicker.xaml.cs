﻿using System.Windows.Controls;

namespace DataMasterClient.SmartSheetSync.View
{
    /// <summary>
    /// Interaction logic for SQLDataTypePicker.xaml
    /// </summary>
    public partial class SmartSheetDataTypePicker : UserControl
    {
        public SmartSheetDataTypePicker()
        {
            InitializeComponent();
        }
    }
}
