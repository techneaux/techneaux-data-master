﻿using System.Windows.Controls;

namespace DataMasterClient.SmartSheetSync.View
{
    /// <summary>
    /// Interaction logic for SQLFormattingPicker.xaml
    /// </summary>
    public partial class SmartSheetFormattingPicker : UserControl
    {
        public SmartSheetFormattingPicker()
        {
            InitializeComponent();
        }
    }
}
