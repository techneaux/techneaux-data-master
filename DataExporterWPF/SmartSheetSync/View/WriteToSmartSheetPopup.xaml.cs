﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.CygNet;
using TechneauxReportingDataModel.CygNet.Utility;
using TechneauxReportingDataModel.SmartSheet;
using TechneauxSmartSheetSync.Resolver;

namespace DataMasterClient.SmartSheetSync.View
{
    /// <summary>
    /// Interaction logic for WriteToSmartSheetPopup.xaml
    /// </summary>
    public partial class WriteToSmartSheetPopup : MetroWindow
    {
        CygNetGeneralOptions CygGeneral;
        SmartSheetSyncConfigModel CurrentConfig;
        RollupPeriod Rollup;

        public WriteToSmartSheetPopup(
              CygNetGeneralOptions cygModel,
              SmartSheetSyncConfigModel myModel,
              RollupPeriod DefaultRollupPeriod,
              CancellationToken ct)
        {
            InitializeComponent();
            CygGeneral = cygModel;
            CurrentConfig = myModel;
            Rollup = DefaultRollupPeriod;
            Loaded += WriteToSmartSheetPopupWindowSync_Loaded;
        }

        private readonly CancellationTokenSource _cs = new CancellationTokenSource();

        private async void WriteToSmartSheetPopupWindowSync_Loaded(object sender, RoutedEventArgs e)
        {
          
            Results = await WriteReportToSmartSheet.UploadReport(CygGeneral, CurrentConfig, Rollup, CancellationToken.None);
            Close();
        }

        private SmartSheetSyncResults Results;

        public static SmartSheetSyncResults ShowPopup(
            CygNetGeneralOptions srcCompModel,
            SmartSheetSyncConfigModel myModel,
            RollupPeriod DefaultRollupPeriod,
            CancellationToken ct)
        {
            var newWindowInstance = new WriteToSmartSheetPopup(srcCompModel, myModel, DefaultRollupPeriod, ct);

            newWindowInstance.Owner = Application.Current.MainWindow;
            newWindowInstance.ShowDialog();
            return newWindowInstance.Results;
        }


        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            CancelButton.Content = "Cancelling";
            CancelButton.IsEnabled = false;
            _cs.Cancel();

            Close();
        }
    }
}
