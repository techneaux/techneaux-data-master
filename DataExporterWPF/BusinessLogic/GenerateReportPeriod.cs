using System;
using System.Collections.Generic;
using TechneauxReportingDataModel.ExcelTextFile.SubOptions;
using TechneauxReportingDataModel.GenericOptions;
using TechneauxUtility;

namespace DataMasterClient.BusinessLogic
{
    public class GenerateReportPeriod
    {
        private readonly SchedulingOptions _schedOpts;
        private readonly ContractPeriodOptions _contractOpts;

        public GenerateReportPeriod(SchedulingOptions schedOpts, ContractPeriodOptions contractOpts)
        {
            _schedOpts = schedOpts;
            _contractOpts = contractOpts;
        }

        public TimePeriod ReportPeriod
        {
            get
            {
                //// Check if the contract period extends past Now(), and check if that's allowed
                //if (ThisConfig.DontAllowPartialPeriods)
                //{
                //    if (ThisConfig.GetContractEndAtDate(RunDate) > DateTime.Now)
                //    {
                //        // The period extends into the future, which is not allowed, so push RunDate back one day
                //        RunDate = RunDate.AddDays(-1);
                //    }
                //}

                //reportPeriod = new TimePeriod(RunDate.AddDays(-ThisConfig.RunDayCount - .5), RunDate.AddDays(.5));

                // Get start date
                var latestDate = DateTime.Now.Date.Add(_schedOpts.StartTime.TimeOfDay);
                latestDate = latestDate.AddDays(-_schedOpts.RelativeDaysForStart);

                DateTime earliestDate;

                switch (_schedOpts.ReportDurationUnits)
                {
                    case SchedulingOptions.TimeDurationUnits.Weeks:
                        earliestDate = latestDate.AddDays(-_schedOpts.DurationUnitCount * 7);
                        break;
                    case SchedulingOptions.TimeDurationUnits.Days:
                        earliestDate = latestDate.AddDays(-_schedOpts.DurationUnitCount);
                        break;
                    case SchedulingOptions.TimeDurationUnits.Hours:
                        earliestDate = latestDate.AddHours(-_schedOpts.DurationUnitCount);
                        break;
                    case SchedulingOptions.TimeDurationUnits.Minutes:
                        earliestDate = latestDate.AddMinutes(-_schedOpts.DurationUnitCount);
                        break;
                    default:
                        earliestDate = latestDate;
                        break;
                }

                return new TimePeriod(earliestDate, latestDate);
            }
        }

        public bool IsBeginInclusive => _contractOpts.IncludeDataAtStartTime;

        public bool IsEndInclusive => _contractOpts.IncludeDataAtEndTime;

        public List<DateTime> GetReportDateList()
        {
            //// Get list of dates and sort accordingly
            //List<DateTime> DateList = new List<DateTime>();

            //for (int dayIncr = 0; dayIncr < RunDayCount; dayIncr++)
            //{
            //    DateList.Add(runDate.AddDays(-dayIncr));
            //}

            //// Sort by date in the direction indicated in the options
            //if (ReportDateOrder == ReportDateOrders.NewerDatesLower)
            //    DateList.Sort();
            //else
            //{
            //    DateList.Sort();
            //    DateList.Reverse();
            //}

            //return DateList;

            throw new NotImplementedException();
        }

        public DateTime GetContractEndAtDate(DateTime thisDate)
        {
            thisDate = thisDate.Date;

            switch (_contractOpts.ContractPeriodType)
            {
                case ContractPeriodOptions.ContractPeriodTypes.HourDuration:
                    return thisDate
                        .AddDays(-1)
                        .AddHours(_contractOpts.ContractHour);
                case ContractPeriodOptions.ContractPeriodTypes.CustomStartEnd:
                    return thisDate
                        .AddDays(-1)
                        .Add(_contractOpts.FixedStartTime.TimeOfDay);
                default:
                    return thisDate
                        .AddDays(-1)
                        .AddHours(_contractOpts.ContractHour);
            }
        }

        public DateTime GetContractStartAtDate(DateTime thisDate)
        {
            switch (_contractOpts.ContractPeriodType)
            {
                case ContractPeriodOptions.ContractPeriodTypes.HourDuration:
                    return thisDate.AddDays(-1)
                        .AddHours(_contractOpts.ContractHour)
                        .AddHours(_contractOpts.ContractPeriodLengthHours);
                case ContractPeriodOptions.ContractPeriodTypes.CustomStartEnd:
                    return thisDate
                        .AddDays(-1)
                        .Add(_contractOpts.FixedEndTime.TimeOfDay);
                default:
                    return thisDate.AddDays(-1)
                        .AddHours(_contractOpts.ContractHour)
                        .AddHours(_contractOpts.ContractPeriodLengthHours);
            }
        }
    }
}
