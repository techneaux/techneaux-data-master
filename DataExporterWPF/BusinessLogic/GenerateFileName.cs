using Serilog;
using System;
using System.IO;
using System.Text.RegularExpressions;
using Serilog.Context;
using TechneauxReportingDataModel.ExcelTextFile.SubOptions;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace DataMasterClient.BusinessLogic
{
    public static class GenerateFileName
    {
        public static string GetFullFilePath(ExportFileOptions thisConfig, bool getTestFileName = false)
        {
            var srcDate = DateTime.Now;

            var exportFileName = ParseStringForDatePattern(thisConfig.ExportName, srcDate);

            var thisExportPath = thisConfig.ExportPath ?? "";

            if (thisExportPath != "" && thisExportPath.Substring(thisExportPath.Length - 1) != @"\") //used to add a slash if one did not exist
            {
                thisExportPath = thisExportPath + @"\";
                if (!Directory.Exists(thisExportPath))
                    thisExportPath = @"[Bad Path]\";
            }

            if (exportFileName == "")
                exportFileName = @"[No Name]";

            if (getTestFileName)
                exportFileName = $"{exportFileName}_TEST";

            return $"{thisExportPath}{exportFileName}.{thisConfig.FileFormat.ToString().ToLower()}";
        }

        // -- Used to modify the export file name with dates and times if needed. Uses {} to determine what should be replaced
        private static string ParseStringForDatePattern(string srcString, DateTime srcDate)
        {
            var toConvert = Regex.Match(srcString, @"(?<=\{)[^}]*(?=\})").Groups[0].Value;

            var newString = srcString;

            if (toConvert == "") return newString;

            var beforeConvert = toConvert;
            try
            {
                toConvert = toConvert.Replace(toConvert, srcDate.ToString(toConvert));
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Debug(ex, "Failure converting date");
                toConvert = "[Date Formatting Error]";

            }

            newString = srcString.Replace(beforeConvert, toConvert).Replace("{", "").Replace("}", "");

            return newString;
        }

    }
}
