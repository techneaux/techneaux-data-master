﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using CygNetRuleModel.Resolvers;
using Serilog;
using TechneauxUtility;

namespace TechneauxHistorySynchronization.SqlServerHistory
{
    public static class SqlComparison
    {
        public static List<MappedSqlRowCompare> Compare(
            string timeColumnKey, 
            List<CygNetHistoryRow> cyg, 
            List<SqlHistoryRow> sql)
        {
            var comparedRows = new List<MappedSqlRowCompare>();
             
            var sqlDict = sql
                .Where(row => row[timeColumnKey].IsDateTime)
                .ToDictionary(row => row[timeColumnKey].ToDateTime(), row => row);
            
            var cygDict = new Dictionary<DateTime, CygNetHistoryRow>();

            var cygDictRows = cyg
                .Where(row => row[timeColumnKey].IsDateTime);
            
            foreach (var row in cygDictRows)
            {
                if(!cygDict.ContainsKey(row[timeColumnKey].ToSqlDateTime))
                {
                    cygDict.Add(row[timeColumnKey].ToSqlDateTime, row);
                }
                else
                {
                    Log.Verbose($"Duplicate cygnet dictionary timestamp entry : {row[timeColumnKey].ToSqlDateTime}");
                }
            }


            var sqlKeys = new HashSet<DateTime>(sqlDict.Keys);
            var cygKeys = new HashSet<DateTime>(cygDict.Keys);

            var sqlOnlyKeys = sqlKeys.Except(cygKeys).ToList();
            var cygOnlyKeys = cygKeys.Except(sqlKeys).ToList();
            var commonKeys = sqlKeys.Intersect(cygKeys).ToList();
            
            // Start with timestamp compare
            comparedRows.AddRange(sqlOnlyKeys.Select(key => 
                new MappedSqlRowCompare(sqlDict[key], 
                    MappedSqlRowCompare.CygSqlCompare.SqlOnly)));

            comparedRows.AddRange(cygOnlyKeys.Select(key =>
                new MappedSqlRowCompare(cygDict[key], 
                    MappedSqlRowCompare.CygSqlCompare.CygOnly)));
            
            foreach (var key in commonKeys)
            {
                var sqlRow = sqlDict[key];
                var cygRow = cygDict[key];
                
                if (CompareSingleRow(sqlRow, cygRow))
                {
                    var comparisonRow = new MappedSqlRowCompare(cygRow, 
                        MappedSqlRowCompare.CygSqlCompare.Equal);

                    comparedRows.Add(comparisonRow);
                }
                else
                {
                    comparedRows.Add(new MappedSqlRowCompare(sqlRow, MappedSqlRowCompare.CygSqlCompare.SqlOnly));
                    comparedRows.Add(new MappedSqlRowCompare(cygRow, MappedSqlRowCompare.CygSqlCompare.CygOnly));
                }
            }

            comparedRows = comparedRows.OrderBy(obj => obj[timeColumnKey].ToDateTime()).ToList();

            return comparedRows;
        }

        public static List<MappedSqlRowCompare> CompareCygRounded(
         string timeColumnKey,
         List<CygNetHistoryRow> cyg,
         List<SqlHistoryRow> sql
         )
        {
            var comparedRows = new List<MappedSqlRowCompare>();

            var sqlDict = sql
                .Where(row => row[timeColumnKey].IsDateTime)
                .ToDictionary(row => row[timeColumnKey].ToDateTime(), row => row);

            var cygDict = new Dictionary<DateTime, CygNetHistoryRow>();

            var cygDictRows = cyg
                .Where(row => row[timeColumnKey].IsDateTime);  

            //filter cyg only keys that were only brought in because of 1 sec increase in window
            var earliestSql = sqlDict.Keys.Min();
            var latestSql = sqlDict.Keys.Max();

            foreach (var row in cygDictRows)
            {
                if (!cygDict.ContainsKey(row[timeColumnKey].ToSqlDateTime))
                {
                    cygDict.Add(row[timeColumnKey].ToSqlDateTime, row);
                }
                else
                {
                    Log.Verbose($"Duplicate cygnet dictionary timestamp entry : {row[timeColumnKey].ToSqlDateTime}");
                }
            }

            cygDict = cygDict.Where(x => x.Key >= earliestSql && x.Key <= latestSql).ToDictionary(y => y.Key, y=> y.Value);

            
            // .ToDictionary(row => row.CellValues[timeColumnKey].ToSqlDateTime, row => row);

            var sqlKeys = new HashSet<DateTime>(sqlDict.Keys);
            var cygKeys = new HashSet<DateTime>(cygDict.Keys);

            var sqlOnlyKeys = sqlKeys.Except(cygKeys).ToList();
            var cygOnlyKeys = cygKeys.Except(sqlKeys).ToList();
            var commonKeys = sqlKeys.Intersect(cygKeys).ToList();


            // Start with timestamp compare
            comparedRows.AddRange(sqlOnlyKeys.Select(key =>
                new MappedSqlRowCompare(sqlDict[key],
                    MappedSqlRowCompare.CygSqlCompare.SqlOnly)));

            comparedRows.AddRange(cygOnlyKeys.Select(key =>
                new MappedSqlRowCompare(cygDict[key],
                    MappedSqlRowCompare.CygSqlCompare.CygOnly)));

            foreach (var key in commonKeys)
            {
                var sqlRow = sqlDict[key];
                var cygRow = cygDict[key];

                if (CompareSingleRow(sqlRow, cygRow))
                {
                    MappedSqlRowCompare comparisonRow = new MappedSqlRowCompare(cygRow,
            MappedSqlRowCompare.CygSqlCompare.Equal);

                    comparedRows.Add(comparisonRow);
                }
                else
                {
                    comparedRows.Add(new MappedSqlRowCompare(sqlRow, MappedSqlRowCompare.CygSqlCompare.SqlOnly));
                    comparedRows.Add(new MappedSqlRowCompare(cygRow, MappedSqlRowCompare.CygSqlCompare.CygOnly));
                }
            }

            comparedRows = comparedRows.OrderBy(row => row[timeColumnKey].ToDateTime()).ToList();

            return comparedRows;
        }

        private static bool CompareSingleRow(SqlHistoryRow sqlRow, CygNetHistoryRow cygRow)
        {
            var (isEqual, keyNames, failureReason) = sqlRow.IsEqualToCommonKeysVerbose(cygRow);

            if (isEqual == false)
            {
                Log.Verbose($"Comparison failed because of {failureReason.ToString()} for keys {string.Join(" , ", keyNames) }");
            }

            return isEqual;
        }
    }
}
