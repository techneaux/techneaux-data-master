﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace TechneauxHistorySynchronization.SqlServerHistory.DatabaseOperations
{

    public static class OdbcWrapper
    {
        [DllImport("odbc32.dll")]
        public static extern int SQLDataSources(int envHandle, int direction, StringBuilder serverName, int serverNameBufferLenIn,
    ref int serverNameBufferLenOut, StringBuilder driver, int driverBufferLenIn, ref int driverBufferLenOut);

        [DllImport("odbc32.dll")]
        public static extern int SQLAllocEnv(ref int envHandle);
    }

    public class OdbcSystemDsn
    {
        public static List<OdbcDataSource> ListOdbCsources()
        {
            var envHandle = 0;
            const int sqlFetchNext = 1;
            const int sqlFetchFirstSystem = 32;

            var srcList = new List<OdbcDataSource>();

            if (OdbcWrapper.SQLAllocEnv(ref envHandle) == -1) return srcList;

            var serverName = new StringBuilder(1024);
            var driverName = new StringBuilder(1024);
            var snLen = 0;
            var driverLen = 0;                

            var ret = OdbcWrapper.SQLDataSources(envHandle, sqlFetchFirstSystem, serverName, serverName.Capacity, ref snLen,
                driverName, driverName.Capacity, ref driverLen);
            while (ret == 0)
            {
                //System.Windows.Forms.MessageBox.Show(serverName + System.Environment.NewLine + driverName);
                srcList.Add(new OdbcDataSource { ServerName = serverName.ToString(), DriverName = driverName.ToString() });

                ret = OdbcWrapper.SQLDataSources(envHandle, sqlFetchNext, serverName, serverName.Capacity, ref snLen,
                    driverName, driverName.Capacity, ref driverLen);
            }

            return srcList;
        }

        public class OdbcDataSource
        {
            public string ServerName { get; set; }
            public string DriverName { get; set; }
        }
    }
}
