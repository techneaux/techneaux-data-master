﻿using Nito.AsyncEx;
using Serilog;
using SqlKata;
using SqlKata.Compilers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog.Context;
using Techneaux.CygNetWrapper.Points;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory.DatabaseOperations;
using TechneauxUtility;
using Z.BulkOperations;
using static XmlDataModelUtility.GlobalLoggers;

namespace TechneauxHistorySynchronization.SqlServerHistory
{
    public class SqlOperation
    {
        //public SqlConnection SqlConnection { get; private set; }

        //public SqlBulkCopy BulkSqlWriter { get; private set; }
        public BulkOperation BulkSqlWriter { get; private set; }

        public SimpleCombinedTableSchema TableSchema { get; set; }

        // removed static here
        private readonly SemaphoreSlim _sqlOperationSemaphore = new SemaphoreSlim(1, 4);
        private readonly double _selectTimeout;
        private readonly double _deleteTimeout;
        private readonly double _insertTimeout;
        private CancellationToken _operCt;
        public SqlOperation(SimpleCombinedTableSchema thisSchema, double deleteTimeout, double selectTimeout, double insertTimeout, CancellationToken ct)
        {
            _operCt = ct;
            TableSchema = thisSchema;
            _selectTimeout = selectTimeout;
            _insertTimeout = insertTimeout;
            _deleteTimeout = deleteTimeout;

            _allLatestXRows = new AsyncLazy<Dictionary<string, List<SqlHistoryRow>>>(async () =>
                 await GetTopXFromDbTimeRangeGroup(DataRangeBoundary.Latest, CachedRowCountLatest, _operCt));

            _allEarliestXRows = new AsyncLazy<Dictionary<string, List<SqlHistoryRow>>>(async () =>
                await GetTopXFromDbTimeRangeGroup(DataRangeBoundary.Earliest, CachedRowCountEarliest, _operCt));
        }

        public async Task<int> AddNewRowsSlow(
           IEnumerable<ValueSet> newRows,
           CancellationToken ct)
        {
            try
            {
                var wsAll = new Stopwatch();
                wsAll.Start();

                using (var newTable = MakeTableForInsert(newRows.ToList()))
                {
                    try
                    {
                        //await BulkSqlWriter.WriteToServerAsync(newTable, ct);
                        await _sqlOperationSemaphore.WaitAsync(ct);

                        var connStr =
                            SqlServerConnectionUtility.SqlStringConnectionBuilder(TableSchema.SrcSqlGeneralOptions, true);

                        using (var sqlConnection = new SqlConnection(connStr))
                        {
                            await sqlConnection.OpenAsync(ct);
                            BulkSqlWriter.Connection = sqlConnection;
                            BulkSqlWriter.BatchTimeout = Convert.ToInt32(_insertTimeout);
                            await BulkSqlWriter.BulkInsertAsync(newTable, DataRowState.Unchanged, ct);
                        }
                    }
                    catch (Exception ex)
                    {
                        using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                            Log.Error(ex, $"WriteToServer failed. Failed to run insert query. Timeout set to {_insertTimeout} seconds.");
                    }

                    return newTable.Rows.Count;
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception while running insert query");

                return 0;
            }
            finally
            {
                _sqlOperationSemaphore.Release();
            }
        }

        private List<ValueSet> TempHoldingListForInserts { get; set; } = new List<ValueSet>();

        public async Task<int> UpdateRecentRows(
            List<ValueSet> newRows,
            CancellationToken ct)
        {
            try
            {
                var wsAll = new Stopwatch();
                wsAll.Start();

                using (var newTable = MakeTableForInsert(newRows.ToList()))
                {
                    await _sqlOperationSemaphore.WaitAsync(ct);
                    try
                    {
                        //await BulkSqlWriter.WriteToServerAsync(newTable, ct);


                        var connStr =
                            SqlServerConnectionUtility.SqlStringConnectionBuilder(TableSchema.SrcSqlGeneralOptions, true);

                        using (var sqlConnection = new SqlConnection(connStr))
                        {
                            await sqlConnection.OpenAsync(ct);
                            BulkSqlWriter.Connection = sqlConnection;
                            BulkSqlWriter.BatchTimeout = Convert.ToInt32(_insertTimeout);

                            await BulkSqlWriter.BulkUpdateAsync(newTable, DataRowState.Unchanged, ct);
                        }
                    }
                    catch (SqlException ex)
                    {
                        using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                            Log.Debug(ex, "Sql exception while updating DB rows. Some data may not be properly synced");

                        throw;
                    }
                    catch (Exception ex)
                    {
                        using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                            Log.Error(ex, $"Unhandled exception, WriteToServer failed. Failed to run update query. Update query uses insert timeout setting. Timeout set to {_insertTimeout} seconds.");

                        throw;
                    }

                    return newTable.Rows.Count;
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception during insert query");

                throw;
            }
            finally
            {
                _sqlOperationSemaphore.Release();
            }

            return 0;

        }

        public async Task<int> FlushRowsToSql(CancellationToken ct)
        {
            try
            {
                await _sqlOperationSemaphore.WaitAsync(ct);

                using (var newTable = MakeTableForInsert(TempHoldingListForInserts))
                {
                    try
                    {
                        //await BulkSqlWriter.WriteToServerAsync(newTable, ct);

                        var connStr =
                            SqlServerConnectionUtility.SqlStringConnectionBuilder(TableSchema.SrcSqlGeneralOptions, true);
                        using (var sqlConnection = new SqlConnection(connStr))
                        {
                            await sqlConnection.OpenAsync(ct);
                            BulkSqlWriter.Connection = sqlConnection;
                            BulkSqlWriter.BatchTimeout = Convert.ToInt32(_insertTimeout);
                            await BulkSqlWriter.BulkInsertAsync(newTable, DataRowState.Unchanged, ct);
                        }
                    }
                    catch (Exception ex)
                    {
                        

                        using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                            Log.Error(ex, $"Unhandled exception, WriteToServer failed. Failed to run insert query. Timeout is set to {_insertTimeout} seconds.");
                       
                    }

                    return newTable.Rows.Count;
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, $"Unhandled exception trying to flush DataTable rows to sql. Timeout was set to {_insertTimeout} seconds.");

                return 0;
            }
            finally
            {
                TempHoldingListForInserts.Clear();
                _sqlOperationSemaphore.Release();
            }
        }



        public async Task<int> AddNewRows(
            IEnumerable<ValueSet> newRows,
            CancellationToken ct)
        {
            var wsAll = new Stopwatch();
            wsAll.Start();

            var continueInsert = false;

            try
            {
                await _sqlOperationSemaphore.WaitAsync(ct);
                TempHoldingListForInserts.AddRange(newRows);
                if (TempHoldingListForInserts.Count > 105000)
                    continueInsert = true;
            }
            catch (Exception)
            {

            }
            finally
            {
                _sqlOperationSemaphore.Release();
            }

            if (continueInsert)
                await FlushRowsToSql(ct);

            return newRows.Count();
        }

        //public async Task<SqlConnection> GetConnection(CancellationToken ct)
        //{
        //    var connStr =
        //        await SqlServerConnectionUtility.SqlStringConnectionBuilder(TableSchema.SrcSqlGeneralOptions, true);

        //    SqlConnection = new SqlConnection(connStr);

        //    await SqlConnection.OpenAsync(ct);

        //    return SqlConnection;
        //}

        public BulkOperation GetBulkCopier()
        {
            //if (SqlConnection is null)
            //    throw new InvalidOperationException("Sql connection is not initialized");

            //BulkSqlWriter =
            //    new SqlBulkCopy(SqlConnection)
            //    {
            //        DestinationTableName = TableSchema.SrcSqlGeneralOptions.TableName,
            //        BulkCopyTimeout = 60,
            //        BatchSize = 100000
            //    };

            //BulkSqlWriter = new BulkOperation(SqlConnection);
            BulkSqlWriter = new BulkOperation();

            return BulkSqlWriter;
        }

        public void CheckIfInitialized()
        {
            //if (SqlConnection is null)
            //    throw new InvalidOperationException("Sql connection is not initialized");

            if (BulkSqlWriter is null)
                throw new InvalidOperationException("Bulk SQL Copy not initialized");
        }

        public async Task ReplaceAllRows(
            ICachedCygNetPoint srcPoint,
            IEnumerable<ValueSet> newRows,
            CancellationToken ct)
        {
            // Delete all rows first
            await DeleteSqlRows(srcPoint, ct);

            // Add new ones
            await AddNewRows(newRows, ct);
        }

        public async Task UpdateSql(
            ICachedCygNetPoint srcPoint,
            List<MappedSqlRowCompare> srcRows,
            CancellationToken ct)
        {
            var newRows = srcRows.Where(row => row.ComparisonValue == MappedSqlRowCompare.CygSqlCompare.Equal ||
                                               row.ComparisonValue == MappedSqlRowCompare.CygSqlCompare.CygOnly).ToArray();

            var rowsToDelete = srcRows.Where(row => row.ComparisonValue == MappedSqlRowCompare.CygSqlCompare.SqlOnly).ToArray();

            if (rowsToDelete.IsAny())
            {
                // delete all rows and add missing ones
                await ReplaceAllRows(srcPoint, newRows, ct);
            }
            else
            {
                // Add missing rows
                await AddNewRows(newRows, ct);
            }
        }

        public async Task UpdateFixedValues(
            ICachedCygNetPoint srcPoint)
        {

        }

        //private GetPkeyCmdWithConditions

        public async Task<List<SqlHistoryRow>> GetEarliestXRowsFromDb(
            ICachedCygNetPoint basePnt,
            CancellationToken ct)
        {
            var fixedPriKeyVals = await TableSchema.GetUniquePrimaryKeysAndValues(basePnt, ct);
            string pointKey = TableSchema.GetRowLookupKeyFromFixedPrimaryKeys(fixedPriKeyVals);

            //if (CachedRowCountEarliest == 0 && numRows >= 1)
            //    CachedRowCountEarliest = numRows;
            //else if (numRows <= 0)
            //    throw new ArgumentOutOfRangeException(nameof(numRows), "Num records must be >= 1");
            //else if (CachedRowCountEarliest >= 1 && CachedRowCountEarliest != numRows)
            //    throw new InvalidOperationException("Can't change row count in middle of operation");

            var rowDict = await _allEarliestXRows;

            return rowDict.TryGetValue(pointKey, out var rowList) ? rowList : new List<SqlHistoryRow>();
        }

        public async Task<bool> HasAnySqlHistory(
            ICachedCygNetPoint basePnt,
            CancellationToken ct)
        {
            var recentRows = await GetEarliestXRowsFromDb(basePnt, ct);

            return recentRows.IsAny();
        }

        public async Task<List<SqlHistoryRow>> GetLatestXRowsFromDb(
            ICachedCygNetPoint basePnt,
            CancellationToken ct)
        {
            var fixedPriKeyVals = await TableSchema.GetUniquePrimaryKeysAndValues(basePnt, ct);
            string pointKey = TableSchema.GetRowLookupKeyFromFixedPrimaryKeys(fixedPriKeyVals);

            //if (CachedRowCountEarliest == 0 && numRows >= 1)
            //    CachedRowCountEarliest = numRows;
            //else if (numRows <= 0)
            //    throw new ArgumentOutOfRangeException(nameof(numRows), "Num records must be >= 1");
            //else if (CachedRowCountEarliest >= 1 && CachedRowCountEarliest != numRows)
            //    throw new InvalidOperationException("Can't change row count in middle of operation");

            var rowDict = await _allLatestXRows;

            return rowDict.TryGetValue(pointKey, out var rowList) ? rowList : new List<SqlHistoryRow>();
        }

        public int CachedRowCountLatest { get; set; } = 3;
        private readonly AsyncLazy<Dictionary<string, List<SqlHistoryRow>>> _allLatestXRows;
        public int CachedRowCountEarliest { get; set; } = 1;
        private readonly AsyncLazy<Dictionary<string, List<SqlHistoryRow>>> _allEarliestXRows;

        public enum DataRangeBoundary
        {
            Earliest,
            Latest
        }

        private async Task<Dictionary<string, List<SqlHistoryRow>>> GetTopXFromDbTimeRangeGroup(
            DataRangeBoundary rangeBoundary,
            int rowCountPerGroup,
            CancellationToken ct)
        {
            var joinedListOfNonTimestampPrimaryKeys = string.Join(",", TableSchema.FixedPrimaryKeyNamesOrdered);

            var sortDirection = rangeBoundary == DataRangeBoundary.Earliest ? "ASC" : "DESC";

            string cmdStr = $@"WITH dt AS( 
                              SELECT ROW_NUMBER() OVER(PARTITION BY {joinedListOfNonTimestampPrimaryKeys} 
                                ORDER BY {TableSchema.SqlTimeColumn.SafeSqlName} {sortDirection}) AS 'RowNumber',
                                *
                                FROM {TableSchema.SrcSqlGeneralOptions.TableName})
                            SELECT *
                            FROM dt
                            WHERE RowNumber <= {rowCountPerGroup}";

            var rowsFound = await QuerySqlData(cmdStr, ct);


            var resultDict = new Dictionary<string, List<SqlHistoryRow>>();

            foreach (var row in rowsFound)
            {
                var pointKey = TableSchema.GetRowLookupKeyFromFixedPrimaryKeys(row);

                if (!resultDict.TryGetValue(pointKey, out var rowList))
                {
                    rowList = new List<SqlHistoryRow>();
                    resultDict[pointKey] = rowList;
                }

                rowList.Add(row);

                ct.ThrowIfCancellationRequested();
            }

            return resultDict;
        }

        private async Task<List<SqlHistoryRow>> GetFirstXFromDbSingle(
            ICachedCygNetPoint basePnt,
            int numRecords,
            CancellationToken ct)
        {
            var timeColumn = TableSchema.TimeColumn.SqlTableFieldName;
            var fixedPriKeyVals = await TableSchema.GetUniquePrimaryKeysAndValues(basePnt, ct);

            var conditions = fixedPriKeyVals.Keys.ToDictionary(key => key, key => fixedPriKeyVals[key].RawValue);

            var getFirstXRowsQuery = new Query()
                .Select(TableSchema.MatchedCols
                    .Select(item => item.Name).ToArray())
                .From(TableSchema.SrcSqlGeneralOptions.TableName)
                .Where(conditions)
                .OrderBy(timeColumn)
                .Limit(numRecords);

            var compiler = new SqlServerCompiler();
            var queryCompiled = compiler.Compile(getFirstXRowsQuery);

            var rowsFound = await QuerySqlData(queryCompiled, ct);

            return rowsFound.ToList();
        }



        public async Task<List<SqlHistoryRow>> GetLastXFromDbNonCached(
            ICachedCygNetPoint basePnt,
            int numRecords,
            CancellationToken ct)
        {
            var timeColumn = TableSchema.TimeColumn.SqlTableFieldName;
            var fixedPriKeyVals = await TableSchema.GetUniquePrimaryKeysAndValues(basePnt, ct);

            var conditions = fixedPriKeyVals.Keys.ToDictionary(key => key, key => fixedPriKeyVals[key].RawValue);

            var getFirstXRowsQuery = new Query()
                .Select(TableSchema.MatchedCols
                    .Select(item => item.Name).ToArray())
                .From(TableSchema.SrcSqlGeneralOptions.TableName)
                .Where(conditions)
                .OrderByDesc(timeColumn)
                .Limit(numRecords);

            var compiler = new SqlServerCompiler();
            var queryCompiled = compiler.Compile(getFirstXRowsQuery);

            var rowsFound = await QuerySqlData(queryCompiled, ct);

            return rowsFound.ToList();
        }


        private async Task<List<SqlHistoryRow>> QuerySqlData(
            SqlResult query,
            CancellationToken ct)
        {
            var resultSet = new List<SqlHistoryRow>();

            try
            {
                await _sqlOperationSemaphore.WaitAsync(ct);

                var connStr = SqlServerConnectionUtility.SqlStringConnectionBuilder(TableSchema.SrcSqlGeneralOptions, true);

                using (var sqlConnection = new SqlConnection(connStr))
                using (var selectCmd = new SqlCommand(query.Sql, sqlConnection))
                {
                    await sqlConnection.OpenAsync(ct);

                    selectCmd.Parameters.AddRange(
                        query.NamedBindings
                            .Select(x =>
                                new SqlParameter($"@{x.Key}", x.Value))
                            .ToArray());

                    using (var rdr = await selectCmd.ExecuteReaderAsync(ct))
                    {
                        while (await rdr.ReadAsync(ct))
                        {
                            var colsIndexes = Enumerable.Range(0, rdr.FieldCount)
                                .Select(num => (index: num, colName: rdr.GetName(num)))
                                .Where(nameIndex => TableSchema.SqlColumns.Any(col => col.Name == nameIndex.colName))
                                .ToDictionary(nameIndex => TableSchema.SqlColumns.Single(col => col.Name == nameIndex.colName),
                                    nameIndex => nameIndex.index);

                            var sqlRow = new SqlHistoryRow();
                            foreach (var col in colsIndexes.Keys)
                            {
                                switch (col.DataType)
                                {
                                    case SqlDbType.DateTime2:
                                        //DateTime rdrSqlDateTime2 = rdr.GetDateTime(i);
                                        //DateTime.TryParse(rdr.GetValue(i).ToString(), out rdrSqlDateTime2);
                                        //sqlRow.Add(rdr.GetName(i).ToString(), new ConvertibleValue(new ConvertibleValue.SqlDateTime2(rdrSqlDateTime2, sqlColumn.DataType.NumericScale)));
                                        break;
                                    case SqlDbType.DateTime:
                                        {
                                            var rdrSqlDateTime = rdr.GetDateTime(colsIndexes[col]);
                                            sqlRow.Add(col.Name, new ConvertibleValue(new SqlDateTime(rdrSqlDateTime)));
                                            break;
                                        }
                                    default:
                                        sqlRow.Add(col.Name, new ConvertibleValue(rdr[col.Name]));
                                        break;
                                }
                            }
                            resultSet.Add(sqlRow);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Debug(ex, "Sql exception while reading DB rows. Sync may be disrupted.");

                throw;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception while reading DB rows");

                throw;
            }
            finally
            {
                _sqlOperationSemaphore.Release();
            }
            return resultSet;
        }

        private async Task<List<SqlHistoryRow>> QuerySqlData(
            string query,
            CancellationToken ct)
        {
            var resultSet = new List<SqlHistoryRow>();

            try
            {
                await _sqlOperationSemaphore.WaitAsync(ct);

                var connStr = SqlServerConnectionUtility.SqlStringConnectionBuilder(TableSchema.SrcSqlGeneralOptions, true);

                using (var sqlConnection = new SqlConnection(connStr))
                {
                    await sqlConnection.OpenAsync(ct);

                    using (var selectCmd = new SqlCommand(query, sqlConnection)
                    {
                        CommandTimeout = Convert.ToInt32(_selectTimeout)
                    })
                    using (var rdr = await selectCmd.ExecuteReaderAsync(ct))
                    {
                        while (await rdr.ReadAsync(ct))
                        {
                            var colsIndexes = Enumerable.Range(0, rdr.FieldCount)
                                .Select(num => (index: num, colName: rdr.GetName(num)))
                                .Where(nameIndex => TableSchema.SqlColumns.Any(col => col.Name == nameIndex.colName))
                                .ToDictionary(
                                    nameIndex => TableSchema.SqlColumns.Single(col => col.Name == nameIndex.colName),
                                    nameIndex => nameIndex.index);

                            var sqlRow = new SqlHistoryRow();
                            foreach (var col in colsIndexes.Keys)
                            {
                                switch (col.DataType)
                                {
                                    case SqlDbType.DateTime2:
                                        //DateTime rdrSqlDateTime2 = rdr.GetDateTime(i);
                                        //DateTime.TryParse(rdr.GetValue(i).ToString(), out rdrSqlDateTime2);
                                        //sqlRow.Add(rdr.GetName(i).ToString(), new ConvertibleValue(new ConvertibleValue.SqlDateTime2(rdrSqlDateTime2, sqlColumn.DataType.NumericScale)));
                                        break;
                                    case SqlDbType.DateTime:
                                        {
                                            if (rdr.IsDBNull(colsIndexes[col]))
                                            {
                                                var emptyCol = rdr[col.Name];
                                                sqlRow.Add(col.Name, new ConvertibleValue(emptyCol));
                                            }
                                            else
                                            {
                                                var rdrSqlDateTime = rdr.GetDateTime(colsIndexes[col]);
                                                sqlRow.Add(col.Name, new ConvertibleValue(new SqlDateTime(rdrSqlDateTime)));
                                            }
                                            break;
                                        }
                                    default:
                                        sqlRow.Add(col.Name, new ConvertibleValue(rdr[col.Name]));
                                        break;
                                }
                            }
                            resultSet.Add(sqlRow);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Debug(ex, "Sql exception while reading DB rows. Sync may be disrupted.");

                throw;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception while reading DB rows");

                throw;
            }
            finally
            {
                _sqlOperationSemaphore.Release();
            }

            return resultSet;
        }


        // When do rows need to be deleted?
        // 1) Fixed values changed => can address with column update query (if primary key changes, no rows can be found)
        // 2) Normalization changes and either
        //    -- Most or all rows changed => just DELETE ALL
        //    -- New setting is a multiple of old, making some fraction of rows the same => at best 1/2 are correct
        // 3) Old rows need to be deleted (simple time-based trim)
        // In conclusion, in most cases no more than half of all rows will remain when deletions are required, and more like less, 
        //  thus, we can just delete all rows if any need to be deleted
        public async Task<int> DeleteSqlRows(
            ICachedCygNetPoint srcPoint,
            CancellationToken ct)
        {
            Log.Warning($"Starting sql delete process from HistoryDeleteAll for point {srcPoint.LongId}");

            var fixedPriKeyVals = await TableSchema.GetUniquePrimaryKeysAndValues(srcPoint, ct);
            var conditions = fixedPriKeyVals.Keys.ToDictionary(key => key, key => fixedPriKeyVals[key].RawValue);

            try
            {
                await _sqlOperationSemaphore.WaitAsync(ct);

                var connStr = SqlServerConnectionUtility.SqlStringConnectionBuilder(TableSchema.SrcSqlGeneralOptions, true);

                using (var sqlConnection = new SqlConnection(connStr))
                using (var selectCmd = GetDeleteQueryCmd(conditions))
                {

                    await sqlConnection.OpenAsync(ct);
                    selectCmd.Connection = sqlConnection;

                    return await Task.Run(() => selectCmd.ExecuteNonQueryAsync(ct), ct);
                }
            }
            catch (SqlException ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error(ex, "Sql Error - Delete query failed. Sync may be disrupted.");

                throw;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception during record deletion.");

                throw;
            }
            finally
            {
                _sqlOperationSemaphore.Release();
            }
        }

        public async Task<int> DeleteSqlRows(
            ICachedCygNetPoint srcPoint,
            double rententionDays,
            CancellationToken ct)
        {
            Log.Warning($"Starting sql delete process from HistoryDeleteOld for point {srcPoint.LongId}");

            var fixedPriKeyVals = await TableSchema.GetUniquePrimaryKeysAndValues(srcPoint, ct);
            var conditions = fixedPriKeyVals.Keys.ToDictionary(key => key, key => fixedPriKeyVals[key].RawValue);

            try
            {
                await _sqlOperationSemaphore.WaitAsync(ct);
                var connStr = SqlServerConnectionUtility.SqlStringConnectionBuilder(TableSchema.SrcSqlGeneralOptions, true);

                using (var sqlConnection = new SqlConnection(connStr))
                using (var selectCmd = GetDeleteQueryCmd(conditions, rententionDays))
                {
                    await sqlConnection.OpenAsync(ct);
                    selectCmd.Connection = sqlConnection;
                    return await Task.Run(() => selectCmd.ExecuteNonQueryAsync(ct), ct);
                }
            }
            catch (SqlException ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error(ex, "Sql Error - Delete query failed. Sync may be disrupted.");

                throw;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception during record deletion.");

                throw;
            }
            finally
            {
                _sqlOperationSemaphore.Release();
            }
        }

        private SqlCommand GetDeleteQueryCmd(
            IReadOnlyDictionary<string, object> conditions)
        {
            var deleteQuery = GetDeleteAllQuery(conditions);

            var sqlComp = new SqlServerCompiler();


            var queryCompiled = sqlComp.Compile(deleteQuery);

            //var modifiedCmd = queryCompiled.Sql.Replace("DELETE", "DELETE TOP 10000");
            //modifiedCmd = $@"declare @Start int 
            //                                select @Start = 1 
            //                                WHILE(@@ROWCOUNT > 0) 
            //                                BEGIN 
            //                                {modifiedCmd}
            //                                END";

            //            var modifiedCmd = $@"DECLARE @continue INT
            //DECLARE @rowcount INT

            //SET @continue = 1
            //WHILE @continue = 1
            //BEGIN
            //SET ROWCOUNT 10000
            //    BEGIN TRANSACTION 
            //{queryCompiled.Sql}
            //SET @rowcount = @@rowcount 
            //    COMMIT
            //    IF @rowcount = 0
            //    BEGIN
            //        SET @continue = 0
            //    END
            //END";

            var sqlCmd = new SqlCommand(queryCompiled.Sql.Replace("FROM ", "FROM [dbo]."));
            sqlCmd.CommandTimeout = Convert.ToInt32(_deleteTimeout);
            sqlCmd.Parameters.AddRange(
                queryCompiled.NamedBindings
                    .Select(x =>
                        new SqlParameter($"@{x.Key}", x.Value))
                    .ToArray());

            return sqlCmd;
        }

        private SqlCommand GetDeleteQueryCmd(
            IReadOnlyDictionary<string, object> conditions,
            double retentionDays)
        {
            return GetTimeLimitedDeleteQuery(conditions, DateTime.Now.AddDays(retentionDays * -1));
        }

        private SqlCommand GetTimeLimitedDeleteQuery(
            IReadOnlyDictionary<string, object> conditions,
            DateTime deleteBeforeDate)
        {
            var deleteQuery = GetDeleteAllQuery(conditions);

            var stringTimeConstraint = deleteBeforeDate.ToString();

            deleteQuery.Where(TableSchema.TimeColumn.SqlTableFieldName, "<", stringTimeConstraint);

            var sqlComp = new SqlServerCompiler();
            var query = sqlComp.Compile(deleteQuery);
            var sqlCmd = new SqlCommand(query.Sql);
            sqlCmd.CommandTimeout = Convert.ToInt32(_deleteTimeout);
            sqlCmd.Parameters.AddRange(
                query.NamedBindings
                    .Select(x =>
                        new SqlParameter($"@{x.Key}", x.Value))
                    .ToArray());

            return sqlCmd;
        }

        private Query GetDeleteAllQuery(
            IReadOnlyDictionary<string, object> conditions)
        {
            if (TableSchema is null)
                throw new ArgumentNullException(nameof(TableSchema), "Schema is null");

            var deleteQuery = new Query()
                .From(TableSchema.SrcSqlGeneralOptions.TableName)
                .Where(conditions)
                .AsDelete();

            return deleteQuery;
        }

        public DataTable MakeTableForInsert(
            List<ValueSet> cygRows)
        {
            var tableForInsert = new DataTable(TableSchema.SrcSqlGeneralOptions.TableName);

            var pKeyColumnsNames = TableSchema.PriKeyColNames.ToHashSet();
            var pKeyCols = new List<DataColumn>();

            foreach (var col in TableSchema.SqlColumns)
            {
                var newCol = new DataColumn
                {
                    ColumnName = col.Name,
                    DataType = col.ClrDataType,
                    AllowDBNull = col.IsNullable
                };

                if (col.IsStringTypeWithLengthLimit)
                    newCol.MaxLength = col.FieldLength;

                tableForInsert.Columns.Add(newCol);

                if (pKeyColumnsNames.Contains(col.Name))
                {
                    pKeyCols.Add(newCol);
                }
            }
            bool skipRow = false;
            tableForInsert.PrimaryKey = pKeyCols.ToArray();
            foreach (var row in cygRows)
            {
                try
                {
                    skipRow = false;

                    var newRow = tableForInsert.NewRow();
                    foreach (var name in row.Keys)
                    {
                        if (!skipRow)
                        {
                            var thisCol = TableSchema.SqlColumnsDict[name];
                            if (thisCol.IsStringTypeMax)
                            {
                                newRow[name] = row[name].StringValue;
                            }
                            else if (thisCol.IsStringTypeWithLengthLimit)
                            {
                                if (row[name].RawValue.ToString().Length > TableSchema.SqlColumnsDict[name].FieldLength)
                                {
                                    newRow[name] = row[name].StringValue.Substring(0, thisCol.FieldLength);
                                }
                                else
                                {
                                    newRow[name] = row[name].StringValue;
                                }
                            }
                            else if (thisCol.IsDateTimeType)
                            {
                                newRow[name] = row[name].RawValue;
                            }
                            else
                            {
                                double numericVal;
                                if (row[name].TryGetDouble(out numericVal))
                                {
                                    newRow[name] = numericVal;
                                }
                                else
                                {
                                    //skipping row due to incompatible types
                                    skipRow = true;
                                    Log.Warning("Failed to add row to table due value being a string and the column in sql being a numeric type.");
                                }
                            }
                        }
                    }
                    if (!skipRow)
                        tableForInsert.Rows.Add(newRow);

                }
                catch (Exception ex)
                {
                    using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                        Log.Warning(ex, "Unhandled exception, failed to generate row for table for CygNet row " + row.ToString());
                }
            }
            try
            {
                tableForInsert.AcceptChanges();
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception, failed to accept changes on insert table");

                return tableForInsert;
            }

            Log.Debug(
                $"Table changes accepted, [{tableForInsert.Rows.Count}] sql rows out of [{cygRows.Count}] CygNet rows");

            return tableForInsert;
        }
    }
}
