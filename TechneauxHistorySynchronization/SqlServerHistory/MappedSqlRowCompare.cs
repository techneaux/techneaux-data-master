﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using CygNetRuleModel.Resolvers;
using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxHistorySynchronization.SqlServerHistory
{
    public class MappedSqlRowCompare : ValueSet
    {
        public MappedSqlRowCompare(ValueSet src, CygSqlCompare compType)
        {
            ConstructItems(src);
            ComparisonValue = compType;
        }

        private void ConstructItems(ValueSet src)
        {
            foreach (var keyVal in src)
            {
                Add(keyVal.Key, keyVal.Value); 
            }
        }

        public enum CygSqlCompare
        {
            Equal,
            CygOnly,
            SqlOnly
        }

        public CygSqlCompare ComparisonValue { get; set; }

    //public bool Equals(MappedSqlRow SqlRow)
        //{
        //    if(!PrimaryKeyFieldId.Equals(SqlRow.PrimaryKeyFieldId))
        //    {
        //        return false;
        //    }
        //    foreach (var CygPair in CellValues)
        //    {
        //        foreach (var SqlPair in SqlRow.CellValues)
        //        {
        //            v
        //        }
        //    }
        //}
    }
}
