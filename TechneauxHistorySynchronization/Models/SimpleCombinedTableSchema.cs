﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CygNetRuleModel.Resolvers;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxHistorySynchronization.SqlServerHistory.DatabaseOperations;
using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using TechneauxReportingDataModel.SqlHistory.SubOptions;
using TechneauxUtility;

namespace TechneauxHistorySynchronization.Models
{
    public class SimpleCombinedTableSchema : SimpleSqlTableSchema
    {
        public IEnumerable<SimpleSqlColumn> FixedValuePrimaryKeyColumns => SqlColumnsDict
           .Values
           .Where(col => FixedValueCygNetColumns.ContainsKey(col.Name) && PriKeyCols.Contains(col));

        // Checks
        public bool ArePrimaryKeysMatchedInCygNet { get; private set; }

        public bool HasTimeColumn => TimeColumn != null;

        public IEnumerable<SimpleSqlColumn> MatchedCols => SqlColumnsDict
            .Where(col => CygNetColumnNames.Contains(col.Key))
            .Select(col => col.Value);

        public IEnumerable<SimpleSqlColumn> MatchedColsNonFixedPrimaryKeys => MatchedCols
            .Where(col => (!FixedValuePrimaryKeyColumns.Contains(col))/* && PriKeyCols.Contains(col)*/);

        public IEnumerable<Tuple<Type, string>> ColumnClrTypeMapping => SqlTableValueMapping.GetValueTypes(SqlColumns, CygNetMappingRules);

        // ---- CygNet Columns
        private List<SqlTableMapping> _cygNetMappingRules = new List<SqlTableMapping>();
        public List<SqlTableMapping> CygNetMappingRules
        {
            get => _cygNetMappingRules;
            set
            {
                _cygNetMappingRules = value ?? throw new ArgumentNullException(nameof(CygNetMappingRules), "CygNet columns list must not be null");
                
                FixedValueCygNetColumns = value
                    .Where(elm => !string.IsNullOrEmpty(elm.SqlTableFieldName))
                    .Where(elm => elm.CygNetElementRule.DataElement == CygNetRuleBase.DataElements.PointAttribute ||
                       elm.CygNetElementRule.DataElement == CygNetRuleBase.DataElements.FacilityAttribute)
                       .ToDictionary(elm => elm.SqlTableFieldName, elm => elm);
            }
        }

        public IEnumerable<string> CygNetColumnNames => CygNetMappingRules
            .Select(col => col.SqlTableFieldName);

        public Dictionary<string, SqlTableMapping> FixedValueCygNetColumns { get; private set; }

        public async Task<ValueSet> GetUniquePrimaryKeysAndValues(ICachedCygNetPoint srcPoint, CancellationToken ct)
        {
            var rulesToPass = FixedValueCygNetColumns
                .Where(item => PriKeyColNames.Contains(item.Key))
                .ToDictionary(item => item.Key, item => item.Value.CygNetElementRule);

            var vals = await ResolveCygNetHistoryRows.ResolveSingleRow(rulesToPass, srcPoint, new CygNetHistoryEntry(), ct);

            return vals;
        }

        public List<string> FixedPrimaryKeyNamesOrdered => FixedValuePrimaryKeyColumns
            .OrderBy(col => col.ID)
            .Select(col => col.SafeSqlName)
            .ToList();

        private string UnsafeSqlName(string name)
        {
            if(name.Length >= 2)
            {
                var trimmedName = name.Substring(1, name.Length - 2);
                return trimmedName;
            }
            else
            {
                return name;
            }            
        }

        public string GetRowLookupKeyFromFixedPrimaryKeys(ValueSet srcRow)
        {
            var rowVals = FixedPrimaryKeyNamesOrdered
                .Select(key => srcRow[UnsafeSqlName(key)].ToString().Trim())
                .ToList();

            var joinedString = string.Join("+", rowVals);

            return joinedString;
        }

        public async Task<Dictionary<string, IConvertibleValue>> GetFixedColumnsAndValues(ICachedCygNetPoint srcPoint, CancellationToken ct)
        {
            var rulesToPass = FixedValueCygNetColumns.ToDictionary(item => item.Key, item => item.Value.CygNetElementRule);

            var vals = await ResolveCygNetHistoryRows.ResolveSingleRow(rulesToPass, srcPoint, new CygNetHistoryEntry(), ct);

            return vals;
        }

        public bool TimeColumnIsValid
        {
            get
            {
                if (!HasTimeColumn)
                    return false;

                if((SqlColumnsDict[TimeColumn.SqlTableFieldName].IsDateTimeType) // || SqlColumnsDict[TimeColumn.SqlTableFieldName].IsDateTime2) 
                    && TimeColumn.CygNetElementRule.DataElement == CygNetRuleBase.DataElements.PointHistory
                    && TimeColumn.CygNetElementRule.PointHistoryOptions.PointValueType == TechneauxReportingDataModel.CygNet.FacilityPointOptions.PointHistoryGeneralOptions.PointValueTypesNew.Timestamp)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public SqlTableMapping TimeColumn => CygNetMappingRules.FirstOrDefault(elm => elm.TimeColumn);

        public SimpleSqlColumn SqlTimeColumn => SqlColumns.Single(col => col.Name == TimeColumn.SqlTableFieldName);

        // ---- Constructor
        public SimpleCombinedTableSchema(
            SqlGeneralOptions srcSqlGeneralOptions, 
            IEnumerable<SimpleSqlColumn> sqlColumns, 
            IEnumerable<SqlTableMapping> reportElms) : base(srcSqlGeneralOptions, sqlColumns)
        {
            CygNetMappingRules = reportElms.ToList();
        }

        public SimpleCombinedTableSchema(
            SqlGeneralOptions srcSqlGeneralOptions, 
            IEnumerable<SimpleSqlColumn> sqlColumns) : base(srcSqlGeneralOptions, sqlColumns)
        {
            CygNetMappingRules = new List<SqlTableMapping>();
        }

        public new static SimpleCombinedTableSchema Empty()
        {
            return new SimpleCombinedTableSchema();
        }

        private SimpleCombinedTableSchema() : base(new SqlGeneralOptions(), new List<SimpleSqlColumn>())
        {
            CygNetMappingRules = new List<SqlTableMapping>();
        }
    }
}




