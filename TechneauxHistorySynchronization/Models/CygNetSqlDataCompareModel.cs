﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using CygNet.API.Points;
using System.Threading.Tasks;
using CygNetRuleModel.Resolvers;
using CygNetRuleModel.Resolvers.History;
using Serilog;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using TechneauxReportingDataModel.Helper;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;
using XmlDataModelUtility;
using CygNet.API.Core;
using CygNet.API.Historian;
using CygNet.Data.Historian;
using Serilog.Context;

namespace TechneauxHistorySynchronization.Models
{
    public class CygNetSqlDataCompareModel : NotifyModelBase
    {
        public ICachedCygNetPoint SrcPoint { get; set; }

        private double _critTsTimeoutMins { get; set; } = 60;

        public CygNetSqlDataCompareModel(ICachedCygNetPoint newSrcPoint, double critTsTimeoutMins)
        {
            SrcPoint = newSrcPoint;
            _critTsTimeoutMins = critTsTimeoutMins;
        }

        public async Task<CygNetHistoryResults> GetCygNetHistory(
            DateTime earliestDate,
            DateTime latestDate,
            SqlPointSelection rule,
            IProgress<int> progress,
            bool includeNumericOnly,
            int? maxEntryCount,
            CancellationToken ct)
        {

            return await GetCygNetHistory(
                earliestDate,
                latestDate,
                rule.GeneralHistoryOptions,
                rule.HistoryNormalizationOptions,
                maxEntryCount,
                progress,
                rule.PollingOptions.RetentionDays,
                includeNumericOnly,
                ct);
        }

        public async Task<CygNetHistoryResults> GetCygNetHistory(
            DateTime earliestDate,
            DateTime latestDate,
            SqlPointSelection rule,
            IProgress<int> progress,
            bool includeNumericOnly,
            CancellationToken ct)
        {

            return await GetCygNetHistory(
                earliestDate,
                latestDate,
                rule.GeneralHistoryOptions,
                rule.HistoryNormalizationOptions,
                null,
                progress,
                rule.PollingOptions.RetentionDays,
                includeNumericOnly,
                ct);
        }

        public async Task<CygNetHistoryResults> GetCygNetHistory(
            SqlPointSelection rule,
            IProgress<int> progress,
            bool includeNumericOnly,
            CancellationToken ct)
        {
            return await GetCygNetHistory(
                DateTime.Now.AddDays(-rule.PollingOptions.RetentionDays),
                DateTime.Now,
                rule.GeneralHistoryOptions,
                rule.HistoryNormalizationOptions,
                null,
                progress,
                rule.PollingOptions.RetentionDays,
                includeNumericOnly,
                ct);
        }

        public async Task<CygNetHistoryResults> GetCygNetHistory(
            double retentionDays,
            PointHistoryGeneralOptions genHistOpts,
            PointHistoryNormalizationOptions normOpts,
            IProgress<int> progress,
            bool includeNumericOnly,
            CancellationToken ct)
        {
            return await GetCygNetHistory(
                DateTime.Now.AddDays(-retentionDays),
                DateTime.Now,
                genHistOpts,
                normOpts,
                null,
                progress,
                retentionDays,
                includeNumericOnly,
                ct);
        }

        public async Task<CygNetHistoryResults> GetCygNetHistory(DateTime earliestDate,
            DateTime latestDate,
            PointHistoryGeneralOptions genHistOpts,
            PointHistoryNormalizationOptions normOpts,
            int? numEntries,
            IProgress<int> progress,
            double retentionDays,
            bool includeNumericOnly,
            CancellationToken ct)
        {
            //var rtc = new Client((await SrcPoint.CvsService.GetAssociatedVhs()).DomainSiteService);
            //var thisName = new Name { ID = SrcPoint.Tag.GetTagPointIdFull() };

            //var hist = rtc.GetHistoricalEntries(
            //    thisName,
            //    earliestDate.ToUniversalTime(),
            //    latestDate.ToUniversalTime().AddSeconds(10),
            //    false);
            //var temp = hist.First().PointStatus;
            //var PointStat = new CygNet.API.Core.PointMetadata();
            //PointStat.LoadAmbientMetadata(out _, errors: out _, warnings: out _);
            //var alarmStat = hist.Select(x => PointStat.EvaluateAlarmCondition(x));
            //var ptState = hist.Select(x => PointStat.EvaluatePointState(x));
            //var test = PointStat.EvaluateAlarmCondition(hist.First());
            //var test2 = PointStat.EvaluatePointState(hist.First());
            //var cxPntMetaData = new CXSCRIPTLib.PointMetadata();
            //var statusBitArray = new StatusBitMetadata(cxPntMetaData.GetStatusBitMetadata());

            if (latestDate > DateTime.Now)
                latestDate = DateTime.Now;

            if (earliestDate > latestDate)
                earliestDate = latestDate;

            var histResults = new CygNetHistoryResults
            {
                EarliestTime = earliestDate,
                LatestTime = latestDate
            };

            if (!await SrcPoint.HasHistory(genHistOpts.IncludeUnreliable, includeNumericOnly))
                return histResults;

            if (normOpts.EnableNormalization)
            {
                var normalizationWindowHours = normOpts.TimespanBetweenNormEntries;
                var normEarliestDateForRawHistory = earliestDate - normalizationWindowHours - normalizationWindowHours;
                var normLatestDateForRawHistory = latestDate + normalizationWindowHours + normalizationWindowHours;
                var normalizer = new HistoryNormalization(normOpts, earliestDate, latestDate, _critTsTimeoutMins);

                var normHistoryWatch = new Stopwatch();
                normHistoryWatch.Start();

                histResults.RawHistoryEntries = await NormalizedPointHistory.GetPointHistory(
                      SrcPoint,
                      normEarliestDateForRawHistory,
                      normLatestDateForRawHistory,
                      numEntries,
                      genHistOpts.IncludeUnreliable,
                      includeNumericOnly,
                      progress,
                      ct);

                bool isExtended = false;
                bool isRollup = false;
                normHistoryWatch.Stop();
                Log.Verbose("Getting CygNet raw history for norm took " + normHistoryWatch.Elapsed.ToString() + " .");
                if (normOpts.SelectedSampleType == PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before_Extended
                    || normOpts.SelectedSampleType == PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before_After_Extended
                    || normOpts.SelectedSampleType == PointHistoryNormalizationOptions.SamplingType.Rollup_Simple_Average_Extended)
                {
                    isExtended = true;
                    List<CygNetHistoryEntry> extended = new List<CygNetHistoryEntry>();
                    var startDate = DateTime.Now.AddDays(-retentionDays);
                    normHistoryWatch.Reset();
                    normHistoryWatch.Start();
                    extended = await NormalizedPointHistory.GetPointHistoryExtended(
                                              SrcPoint,
                                              DateTime.Now.AddYears(-10),
                                              normEarliestDateForRawHistory,
                                              genHistOpts.IncludeUnreliable,
                                              includeNumericOnly,
                                              1,
                                              ct);

                    normHistoryWatch.Stop();
                    Log.Verbose("Getting CygNet extended history for norm took " + normHistoryWatch.Elapsed.ToString() + " .");
                    histResults.RawHistoryEntries.AddRange(extended);
                }
                normHistoryWatch.Reset();
                normHistoryWatch.Start();
                //rollup types window ends at normalized timestamp
                if (normOpts.SelectedSampleType == PointHistoryNormalizationOptions.SamplingType.Rollup_Simple_Average_Extended)
                {

                    histResults.HasHistLaterThanRange = await SrcPoint.HasHistoryInInterval(
                    latestDate, DateTime.Now, genHistOpts.IncludeUnreliable, includeNumericOnly);
                    isRollup = true;
                }
                else
                {
                    histResults.HasHistLaterThanRange = await SrcPoint.HasHistoryInInterval(
                    normLatestDateForRawHistory.AddMilliseconds(1), DateTime.Now,
                    genHistOpts.IncludeUnreliable, includeNumericOnly);
                }
                histResults.NormalizedHistoryEntries = await normalizer.Normalize(histResults.RawHistoryEntries, histResults.HasHistLaterThanRange, isRollup);
                histResults.NormalizedHistoryEntries = histResults.NormalizedHistoryEntries.Where(x => x.NormalizedTimestamp <= latestDate && x.NormalizedTimestamp >= earliestDate).ToList();

                normHistoryWatch.Stop();
                Log.Verbose("Normalizing CygNet history took " + normHistoryWatch.Elapsed.ToString() + " .");
                normHistoryWatch.Reset();
            }
            else
            {
                var historyWatch = new Stopwatch();
                historyWatch.Start();
                histResults.RawHistoryEntries = await NormalizedPointHistory.GetPointHistory(
                    SrcPoint,
                    earliestDate,
                    latestDate,
                    numEntries,
                    genHistOpts.IncludeUnreliable,
                    includeNumericOnly,
                    progress,
                    ct);

                historyWatch.Stop();
                Log.Verbose("Getting CygNet history for non-normalized took " + historyWatch.Elapsed.ToString() + " .");
                histResults.HasHistLaterThanRange = await SrcPoint.HasHistoryInInterval(
                  latestDate.AddMilliseconds(1), DateTime.Now,
                  genHistOpts.IncludeUnreliable, includeNumericOnly);

                histResults.NormalizedHistoryEntries = null;

            }
            return histResults;
        }

        public class CygNetHistoryResults
        {
            public List<CygNetHistoryEntry> RawHistoryEntries { get; set; }
            public List<HistoryNormalization.NormalizedHistoryEntry> NormalizedHistoryEntries { get; set; }
            public bool IsNormalized => NormalizedHistoryEntries != null;
            public bool IsAnyRawHistory => RawHistoryEntries.IsAny();
            public DateTime EarliestTime { get; set; }
            public DateTime LatestTime { get; set; }

            public bool HasHistLaterThanRange { get; set; }
        }

        public class HistoryCompareResults
        {
            public List<CygNetHistoryRow> CygNetRows { get; set; }
        }

        public static (bool IsValid, string FailureReason) ValidateRulesDataOnly(SimpleCombinedTableSchema schema)
        {
            const string primaryKeysNotSelected = "Validation Error: The following primary key is not mapped : ";
            const string noDateTimePrimaryKey = "Validation Error: The table does not contain a primary key that includes a datetime column.";

            const string emptySchema = "Validation Error: The schema is either null or empty.";
            const string badRule = "Validation Error: A rule is invalid.";
            const string noTimeColumn = "Validation Error: Please verify a time column is selected, the SQL field datatype is DateTime or DateTime2 of at least (3), as well as the rule is a history type.";
            const string noFieldName = "Validation Error: Please verify every rule has a valid field name.";
            const string nonNullableColumns = "Validation Error: The following nonnullable column is not mapped : .";
            const string noPrimaryKey = "Validation Error: Please verify a primary key exists in SQL and that it includes a datetime column.";
            //check primary keys and column availability
            if (schema == null || schema.IsEmptySqlSchema)
            {
                return (false, emptySchema);
            }
            var sqlTableMappings = schema.CygNetMappingRules;
            foreach (var elm in sqlTableMappings)
            {

                if (!schema.SqlColumnNames().ToList().Contains(elm.SqlTableFieldName))
                {
                    return (false, noFieldName);
                }

                if (!elm.CygNetElementRule.IsRuleValid)
                {
                    return (false, badRule);
                }
            }

            if (!schema.TimeColumnIsValid)
            {
                return (false, noTimeColumn);
            }

            var missingNn = schema.NonNullableColumns.Select(x => x.Name).Where(x => !schema.CygNetColumnNames.ToList().Contains(x)).ToList();
            if (missingNn.Any())
            {
                return (false, nonNullableColumns + missingNn.First());
            }

            var missingPKey = schema.PriKeyColNames.Where(x => !schema.CygNetColumnNames.ToList().Contains(x)).ToList();
            if (missingPKey.Any())
            {
                return (false, primaryKeysNotSelected + missingPKey.First());
            }

            if (schema.PriKeyCols.IsAny())
            {
                if (!schema.PriKeyCols.Select(x => x.DataType).ToList().Contains(System.Data.SqlDbType.DateTime))
                {
                    return (false, noDateTimePrimaryKey);
                }

            }
            else
            {
                return (false, noPrimaryKey);
            }

            return (true, "");
        }

        public static (bool IsValid, string FailureReason) ValidateRulesDataOnly(
            SimpleCombinedTableSchema schema,
            CygNetHistoryResults cygHist,
            CachedCygNetPointWithRule srcPoint)
        {
            const string noHist = "Validation Error: There is no history for this point from the given rules.";

            if (srcPoint == null)
                return (false, "");
            if (cygHist == null || !cygHist.IsAnyRawHistory)
                return (false, noHist);

            return ValidateRulesDataOnly(schema);

        }

        public async Task<List<CygNetHistoryRow>> GetCygNetRows(
            CygNetHistoryResults srcHist,
            List<SqlTableMapping> rules,
            CancellationToken ct)
        {
            var configuredRules = rules.ToDictionary(item => item.SqlTableFieldName, item => item.CygNetElementRule);

            if (srcHist.IsNormalized)
            {
                var normHist = srcHist.NormalizedHistoryEntries
                    .Where(ne => ne.IsValid && !ne.IsValueUncertain)
                    .ToList();

                var path = $@"{Environment.CurrentDirectory}\NormSourceEntries.txt";


                return await ResolveCygNetHistoryRows.ResolveAllRows(configuredRules, SrcPoint, normHist, ct);
            }

            //trim raw hist down to latest date
            var trimmedRaw = srcHist.RawHistoryEntries.Where(y => y.AdjustedTimestamp <= srcHist.LatestTime).ToList();
            return await ResolveCygNetHistoryRows.ResolveAllRows(configuredRules, SrcPoint, trimmedRaw, ct);
        }

        private async Task<List<SqlHistoryRow>> GetSqlRows(
            SimpleCombinedTableSchema curSchema,
            DateTime earliestDate,
            DateTime latestDate,
            CancellationToken ct)
        {
            try
            {
                //gets proccessed rows from sql table and cygnet
                return await SqlRowResolver.GetProcessedRowsFromDb(curSchema, earliestDate, latestDate, SrcPoint, ct);

            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception processing rows from SQL select query");

                throw;
            }
        }

        public List<MappedSqlRowCompare> GetComparedRowsCygRounded(
         string timeColumn,
         IEnumerable<CygNetHistoryRow> srcCygNetRows,
         IEnumerable<SqlHistoryRow> srcSqlRows)
        {
            try
            {
                var newComparedRows = SqlComparison.CompareCygRounded(timeColumn, srcCygNetRows.ToList(), srcSqlRows.ToList());
                return newComparedRows;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception updating SQL comparison with rounded cygnet timestamps.");

                throw;
            }
        }

        public List<MappedSqlRowCompare> GetComparedRows(
            string timeColumn,
            IEnumerable<CygNetHistoryRow> srcCygNetRows,
            IEnumerable<SqlHistoryRow> srcSqlRows)
        {
            try
            {
                var newComparedRows = SqlComparison.Compare(timeColumn, srcCygNetRows.ToList(), srcSqlRows.ToList());
                return newComparedRows;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, @"Unhandled exception while generating SQL/Cygnet comparison");

                throw;
            }
        }

        public async Task<SqlCompareResults> GetNewSqlComparisonResults(
            SimpleCombinedTableSchema tableSchema,
            double retentionDays,
            PointHistoryGeneralOptions genHistOpts,
            PointHistoryNormalizationOptions normOpts,
            IProgress<int> progress,
            bool includeNumericOnly,
            CancellationToken ct)
        {
            return await GetNewSqlComparisonResults(
                tableSchema,
                DateTime.Now.AddDays(-retentionDays),
                DateTime.Now,
                genHistOpts,
                normOpts,
                null,
                progress,
                retentionDays,
                includeNumericOnly,
                ct);
        }

        public SqlCompareResults GetNewSqlComparisonResults(
          string timeColumnKey,
          List<SqlHistoryRow> sqlRows,
          CygNetHistoryResults cygHist,
          CancellationToken ct)
        {
            try
            {
                Log.Debug("Starting resolving");
                //LibraryLogging.StartTimingProcess();

                var results = new SqlCompareResults
                {
                    CygHistory = cygHist,
                    SqlRows = sqlRows
                };

                results.ComparedRows = GetComparedRows(timeColumnKey, results.CygNetRows, results.SqlRows);

                return results;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception while updating SQL comparison");

                return new SqlCompareResults
                {
                    CompException = ex
                };
            }

        }

        public async Task<SqlCompareResults> GetNewSqlComparisonResults(
            SimpleCombinedTableSchema tableSchema,
            CygNetHistoryResults cygHist,
            CancellationToken ct)
        {
            try
            {
                // IsCompareViewBusy = true;
                Log.Debug("Starting resolving");
                //LibraryLogging.StartTimingProcess();
                var results = new SqlCompareResults { CygHistory = cygHist };
                results.CygNetRows = await GetCygNetRows(results.CygHistory, tableSchema.CygNetMappingRules, ct);
                //if(results.CygHistory.HasHistLaterThanRange)
                //    results.CygNetRows.Where(x => x.CellValues.Values[tableSchema.TimeColumn.SqlTableFieldName])
                results.SqlRows = await GetSqlRows(tableSchema, results.CygHistory.EarliestTime, results.CygHistory.LatestTime, ct);
                results.ComparedRows = GetComparedRows(tableSchema.TimeColumn.SqlTableFieldName, results.CygNetRows, results.SqlRows);

                return results;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception updating SQL comparison");

                throw;
            }
        }

        public async Task<SqlCompareResults> GetNewSqlComparisonResults(SimpleCombinedTableSchema tableSchema,
            DateTime earliestDate,
            DateTime latestDate,
            PointHistoryGeneralOptions genHistOpts,
            PointHistoryNormalizationOptions normOpts,
            int? maxEntries,
            IProgress<int> progress,
            double retentionDays,
            bool includeNumericOnly,
            CancellationToken ct)
        {
            try
            {
                // IsCompareViewBusy = true;
                Log.Debug("Starting resolving");
                //LibraryLogging.StartTimingProcess();

                var results = new SqlCompareResults
                {
                    CygHistory = await GetCygNetHistory(earliestDate, latestDate, genHistOpts, normOpts, maxEntries, progress, retentionDays, includeNumericOnly, ct)
                };

                results.CygNetRows = await GetCygNetRows(results.CygHistory, tableSchema.CygNetMappingRules, ct);
                results.SqlRows = await GetSqlRows(tableSchema, earliestDate, latestDate, ct);
                results.ComparedRows = GetComparedRows(tableSchema.TimeColumn.SqlTableFieldName, results.CygNetRows, results.SqlRows);

                return results;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception updating SQL comparison");

                return new SqlCompareResults
                {
                    CompException = ex
                };
            }
        }

        public class SqlCompareResults
        {
            public bool IsSuccessful =>
                CygHistory != null && CygNetRows != null && SqlRows != null && ComparedRows != null;

            public bool AreMappingRulesValid { get; set; } = true;

            public string MappingRulesValidationErrorMessage { get; set; }

            public CygNetHistoryResults CygHistory { get; set; }

            public List<CygNetHistoryRow> CygNetRows { get; set; }

            public List<SqlHistoryRow> SqlRows { get; set; }

            public List<MappedSqlRowCompare> ComparedRows { get; set; }

            public bool IsRowMismatch => ComparedRows.IsAny() && ComparedRows.Any(row => row.ComparisonValue != MappedSqlRowCompare.CygSqlCompare.Equal);

            public Exception CompException { get; set; }
        }

        //update sql table process with a delete
        public async Task UpdateSqlTable(
            SimpleCombinedTableSchema srcSchema,
            List<MappedSqlRowCompare> srcRows,
            CancellationToken ct)
        {
            if (srcRows == null)
                return;
            var sqlRows = srcRows
                .Where(row => row.ComparisonValue == MappedSqlRowCompare.CygSqlCompare.SqlOnly)
                .Cast<ValueSet>()
                .ToList();

            var cygRows = srcRows
                .Where(row => row.ComparisonValue == MappedSqlRowCompare.CygSqlCompare.CygOnly)
                .Cast<ValueSet>()
                .ToList();

            // Delete and insert
            try
            {
                var sw = new Stopwatch();
                sw.Start();

                await SqlRowResolver.DeleteBadSqlRows(srcSchema, sqlRows, ct);
                var validatedCygRows = ResolveNullableCygRows(srcSchema, cygRows);

                sw.Restart();

                await SqlRowResolver.InsertMissingSqlRows(srcSchema, validatedCygRows, ct);
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Error updating sql table");

                throw;
            }
        }

        public async Task UpdateSqlTableFast(
            SimpleCombinedTableSchema tableSchema,
            List<ValueSet> cygNetHistoryRows,
            CancellationToken ct)
        {
            throw new NotImplementedException();

            Log.Debug("Starting resolving");
            //LibraryLogging.StartTimingProcess();

            //var time = LibraryLogging.EndTimingProcess().TotalMilliseconds;
            //Log.Debug($"Finished row resolver at {time}");

            try
            {
                var sw = new Stopwatch();

                sw.Start();
                var validatedCygRows = ResolveNullableCygRows(tableSchema, cygNetHistoryRows);
                sw.Restart();

                await SqlRowResolver.InsertMissingSqlRows(tableSchema, validatedCygRows.ToList(), ct);
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception updating sql table");

                throw;
            }
        }

        private List<ValueSet> ResolveNullableCygRows(
            SimpleCombinedTableSchema tableSchema,
            List<ValueSet> cygNetHistoryRows)
        {
            var resolvedRows = cygNetHistoryRows;

            try
            {
                var columnsList = tableSchema.NonNullableColumns.Select(item => item.Name).ToList();
                var garbage = new List<ValueSet>();
                foreach (var row in cygNetHistoryRows)
                {
                    foreach (var pair in row)
                    {
                        if (columnsList.Contains(pair.Key))
                        {
                            if (pair.Value.StringValue.Trim() == "")
                            {
                                garbage.Add(row);
                            }
                        }
                    }
                }
                foreach (var garb in garbage)
                {
                    resolvedRows.Remove(garb);
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception while resolving nullable cygnet rows");

                throw;
            }

            return resolvedRows.ToList();
        }
    }
}
