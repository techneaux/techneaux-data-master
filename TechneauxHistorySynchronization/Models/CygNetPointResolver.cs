﻿using CygNet.Data.Core;
using CygNetRuleModel.Resolvers;
using Microsoft.VisualBasic.CompilerServices;
using Serilog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog.Context;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using Techneaux.CygNetWrapper.Services.FAC;
using Techneaux.CygNetWrapper.Services.PNT;
using Techneaux.CygNetWrapper.Services.TRS;
using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxReportingDataModel.Helper;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace TechneauxHistorySynchronization.Models
{
    public class CygNetPointResolver
    {
        public static async Task<(List<CachedCygNetPointWithRule> Points, List<FacilityUdcResult> UdcResults, List<CachedCygNetPointWithRule> Duplicates)> GetPointsAsync(
           List<SqlPointSelection> fullPntRules,
           List<CachedCygNetFacility> cachedFacs,
           List<ChildFacilityGroup> childGroups,
           FacilityService facServ,
           CancellationToken ct)
        {
            try
            {
                var swi = new Stopwatch();
                swi.Start();

                var finalList = new List<CachedCygNetPointWithRule>();
                var duplicateList = new List<CachedCygNetPointWithRule>();
                var facUdcResults = new Dictionary<ICachedCygNetFacility, FacilityUdcResult>();
                var uniquePointRules = new Dictionary<FacilityPointSelection, SqlPointSelection>();
                if (facServ == null)
                {
                    return (finalList, facUdcResults.Values.ToList(), duplicateList);
                }

                var temp = await facServ.GetAssociatedPntServices();
                var pntServs = temp.Values.ToList();

                var uniqueUdcList = new List<string>();
                var trsServs = new HashSet<TableReferenceService>();
                foreach (var pntServ in pntServs)
                {
                    trsServs.Add(await pntServ.GetAssociatedTRS());
                    uniqueUdcList.AddRange(await Task.Run(() => pntServ.GetUniqueUdcList(), ct));
                }

                uniqueUdcList = uniqueUdcList.Distinct().ToList();

                var udcCounts = new Dictionary<string, int>();

                fullPntRules.ForEach(rule => uniquePointRules[rule.PointSelection] = rule);

                ct.ThrowIfCancellationRequested();

                Console.WriteLine($"Point search init took {swi.Elapsed.TotalSeconds} secs");

                var pointList = new List<CachedCygNetPointWithRule>();
                if (cachedFacs != null)
                {
                    foreach (var pntSelRule in uniquePointRules.Keys)
                    {
                        var sw = new Stopwatch();

                        var checkedUniqueUdcList = new List<UdcEntry>();
                        if (!pntSelRule.IsRuleValid)
                        {
                            continue;
                            throw new ArgumentException("Invalid point selection rule");
                        }

                        if (pntSelRule.FilteringChoice == FacilityPointSelection.FilteringTypes.Description)
                        {
                            foreach (var trs in trsServs)
                            {
                                checkedUniqueUdcList.AddRange(CheckTrsUdcList(uniqueUdcList, pntSelRule, trs));
                            }
                        }
                        else if (pntSelRule.FilteringChoice == FacilityPointSelection.FilteringTypes.UDC)
                        {
                            var checkedUniqueUdcs = CygNetPointResolver.CheckUdcList(uniqueUdcList, pntSelRule);
                            checkedUniqueUdcList = checkedUniqueUdcs.Select(x => new UdcEntry(x)).ToList();
                        }

                        List<string> filteredUdcList;
                        if (pntSelRule.UdcExclusion != "")
                        {
                            filteredUdcList = ResolveUdcExclusionList(checkedUniqueUdcList, pntSelRule);
                        }
                        else
                        {
                            filteredUdcList = checkedUniqueUdcList.Select(x => x.udc).ToList();
                        }

                        foreach (var udc in filteredUdcList)
                        {
                            if (udcCounts.TryGetValue(udc, out var count))
                            {
                                udcCounts[udc] = count + 1;
                            }
                            else
                            {
                                udcCounts[udc] = 1;
                            }
                        }

                        ct.ThrowIfCancellationRequested();

                        foreach (var fac in cachedFacs)
                        {

                            sw.Start();

                            ct.ThrowIfCancellationRequested();

                            var (facility, error) = CygNetResolver.ResolveChildFacility(pntSelRule, fac, childGroups);
                            if (error == CygNetResolver.ResolverErrorConditions.None)
                            {
                                if (filteredUdcList.IsAny())
                                {
                                    if (!facUdcResults.TryGetValue(facility, out var thisFacResult))
                                    {
                                        thisFacResult = new FacilityUdcResult(facility, new List<string>());
                                        facUdcResults[facility] = thisFacResult;
                                    }

                                    //foreach (var udc in excludedList)
                                    //{

                                    var points = (await Task.Run(() => facility.GetPoints(filteredUdcList, ct), ct));

                                    foreach (var cachedCygNetPoint in points.Where(pt => pt != null))
                                    {
                                        pointList.Add(new CachedCygNetPointWithRule(cachedCygNetPoint,
                                            uniquePointRules[pntSelRule]));
                                        thisFacResult.AddUdc(cachedCygNetPoint.Tag.UDC);
                                    }

                                    //var point = (await Task.Run(() => facility.GetPoint(udc, cts.Token)));
                                    //if (point != null)
                                    //{
                                    //    pointList.Add(new CachedCygNetPointWithRule(point, uniquePointRules[pntSelRule]));
                                    //    thisFacResult.AddUdc(udc);
                                    //}
                                    //}
                                }
                            }
                            else
                            {
                                //throw new InvalidOperationException(error.GetDescription());
                            }


                        }

                        sw.Stop();
                        Console.WriteLine(
                            $"Cache collecting pass with rule {pntSelRule} took {sw.Elapsed.TotalSeconds} secs");
                    }
                }
                //pointList.Gr

                var swd = new Stopwatch();
                swd.Start();

                //duplicateList = ResolveDuplicateList(pointList);

                var duplicatedUdcs = new HashSet<string>(udcCounts.Keys.Where(udc => udcCounts[udc] > 1));
                duplicateList = pointList.Where(pnt => duplicatedUdcs.Contains(pnt.Tag.UDC)).ToList();

                swd.Stop();
                Console.WriteLine($"Duplicates resolved in {swd.Elapsed.TotalSeconds} secs");

                finalList = new List<CachedCygNetPointWithRule>(pointList.Distinct().ToList());

                Console.WriteLine($"Point search total time was {swi.Elapsed.TotalSeconds} secs");

                return (finalList, facUdcResults.Values.ToList(), duplicateList);
            }
            catch (OperationCanceledException)
            {
                // Do nothing
                Log.Debug("Point resolver canceled");
            }
            catch (Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show(
                //  $@"Failed to get point list with: {ex.Message} {Environment.NewLine}Operation Failed");

                //if (ex.InnerException != null)
                //    System.Windows.Forms.MessageBox.Show($@"Inner exception = {ex.InnerException.Message}");
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Warning(ex, "Unhandled exception resolving points.");
            }
            
            return (new List<CachedCygNetPointWithRule>(), new List<FacilityUdcResult>(), new List<CachedCygNetPointWithRule>());
        }

        private static List<string> ResolveUdcExclusionList(List<UdcEntry> exclusion, FacilityPointSelection pntSelRule)
        {
            List<string> excludedList = new List<string>();
            if (pntSelRule.FilteringChoice == FacilityPointSelection.FilteringTypes.UDC)
            {
                foreach (var entry in exclusion)
                {
                    if (!LikeOperator.LikeString(entry.udc, pntSelRule.UdcExclusion, Microsoft.VisualBasic.CompareMethod.Text) && entry.udc.IsAny())
                    {
                        excludedList.Add(entry.udc);
                    }
                }
            }
            else if (pntSelRule.FilteringChoice == FacilityPointSelection.FilteringTypes.Description)
            {
                foreach (var entry in exclusion)
                {
                    if (!LikeOperator.LikeString(entry.description, pntSelRule.UdcExclusion, Microsoft.VisualBasic.CompareMethod.Text) && entry.description.IsAny())
                    {
                        excludedList.Add(entry.udc);
                    }
                }
            }
            return excludedList;
        }

        //private static List<CachedCygNetPointWithRule> ResolveDuplicateList(List<CachedCygNetPointWithRule> pointList)
        //{
        //    var finalList = pointList.Distinct().ToList();
        //    var duplicateList = new List<CachedCygNetPointWithRule>();

        //    var pointCountDict = new HashSet<CachedCygNetPointWithRule>();
            
        //    foreach (var pnt in pointList)
        //    {
        //        if (pnt.Equals(fnl) && !pnt.SrcRule.PointSelection.Equals(fnl.SrcRule.PointSelection))
        //        {
        //            duplicateList.Add(pnt);
        //        }
        //    }

            
        //    //foreach (var fnl in finalList)
        //    //{
        //    //    foreach (var pnt in pointList)
        //    //    {
        //    //        if (pnt.Equals(fnl) && !pnt.SrcRule.PointSelection.Equals(fnl.SrcRule.PointSelection))
        //    //        {
        //    //            duplicateList.Add(pnt);
        //    //        }
        //    //    }
        //    //}

        //    return duplicateList;
        //}

        private static List<string> CheckUdcList(List<string> uniqueUdcList, FacilityPointSelection pntSelRule)
        {

            List<string> checkedList = new List<string>();
            foreach (var udc in uniqueUdcList)
            {
                if (LikeOperator.LikeString(udc, pntSelRule.UDC, Microsoft.VisualBasic.CompareMethod.Text))
                {
                    checkedList.Add(udc);
                }
            }
            return checkedList;
        }

        private static List<UdcEntry> CheckTrsUdcList(List<string> uniqueUdcList, FacilityPointSelection pntSelRule, TableReferenceService trs = null)
        {
            List<UdcEntry> unique = new List<UdcEntry>();

            if (trs == null)
                return new List<UdcEntry>();
            Table udcTable = trs.GetUdcTable();
            foreach (var udc in uniqueUdcList)
            {

                if (udcTable.HasTableEntry(udc))
                {
                    if (LikeOperator.LikeString(udcTable[udc], pntSelRule.UDC, Microsoft.VisualBasic.CompareMethod.Text))
                    {
                        unique.Add(new UdcEntry(udc, udcTable[udc]));
                    }
                }
            }
            return unique;
        }

        public class UdcEntry
        {
            public UdcEntry(string udcEntry, string desc = null)
            {

                udc = udcEntry;
                description = desc;
            }
            public string udc;
            public string description;

        }

    }
}
