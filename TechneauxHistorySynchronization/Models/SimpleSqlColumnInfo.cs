﻿using System;
using System.Data;

namespace TechneauxHistorySynchronization.Models
{
    public class SimpleSqlColumn
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public string SafeSqlName => $"[{Name}]";

        public bool InPrimaryKey { get; set; }
        public bool IsNullable { get; set; }

        public bool IsDateTimeType =>
            DataType == SqlDbType.DateTime ||
            DataType == SqlDbType.DateTime2;

        public SqlDbType DataType { get; set; }
        public int FieldLength { get; set; }

        public bool IsStringType => DataType == SqlDbType.Char ||
                                    DataType == SqlDbType.Text ||
                                    DataType == SqlDbType.VarChar ||
                                    DataType == SqlDbType.NChar ||
                                    DataType == SqlDbType.NText ||
                                    DataType == SqlDbType.NVarChar;


        public bool IsStringTypeWithLengthLimit => IsStringType && FieldLength > 0;
        public bool IsStringTypeMax => IsStringType && FieldLength == -1;
        public Type ClrDataType
        {
            get
            {
                var typ = typeof(string);
            
                switch (DataType)
                {
                    case SqlDbType.BigInt:
                        typ = typeof(Int64);
                        break;
                    case SqlDbType.Bit:
                        typ = typeof(Boolean);
                        break;
                    case SqlDbType.Char:
                    case SqlDbType.VarChar:
                        //case SqlDbType.VarCharMax:

                        typ = typeof(String);
                        break;
                    case SqlDbType.DateTime:
                    case SqlDbType.DateTime2:
                    case SqlDbType.SmallDateTime:
                        typ = typeof(DateTime);
                        break;
                    case SqlDbType.Decimal:
                    case SqlDbType.Money:
                    case SqlDbType.SmallMoney:
                        typ = typeof(Decimal);
                        break;
                    case SqlDbType.Int:
                        typ = typeof(Int32);
                        break;
                    case SqlDbType.NChar:
                    case SqlDbType.NText:
                    case SqlDbType.NVarChar:
                    //case SqlDbType.NVarCharMax:
                    case SqlDbType.Text:
                        typ = typeof(String);
                        break;
                    case SqlDbType.Real:
                    //case SqlDbType.Numeric:
                    case SqlDbType.Float:
                        typ = typeof(Double);
                        break;
                    case SqlDbType.Timestamp:
                    case SqlDbType.Binary:
                        typ = typeof(Byte);
                        break;
                    case SqlDbType.TinyInt:
                    case SqlDbType.SmallInt:
                        typ = typeof(Int16);
                        break;
                    case SqlDbType.UniqueIdentifier:
                        typ = typeof(Guid);
                        break;
                    //case SqlDbType.UserDefinedDataType:
                    //case SqlDbType.UserDefinedType:
                    case SqlDbType.Variant:
                    case SqlDbType.Image:
                        typ = typeof(Object);
                        break;
                    default:
                        typ = typeof(String);
                        break;
                }
                return typ;
            }
        }


        public override bool Equals(object obj)
        {
            if (obj != null && obj is SimpleSqlColumn)
            {
                var compObj = obj as SimpleSqlColumn;

                return Name == compObj.Name;
            }
            return false;
        }

        public override string ToString()
        {
            return Name;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}




