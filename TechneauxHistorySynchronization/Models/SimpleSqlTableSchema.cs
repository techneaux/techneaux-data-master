﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TechneauxHistorySynchronization.SqlServerHistory.DatabaseOperations;
using TechneauxReportingDataModel.SqlHistory.SubOptions;
using TechneauxUtility;

namespace TechneauxHistorySynchronization.Models
{
    //public interface ISimpleTableColumnDef
    //{

    //}
    
    
    public class SimpleSqlTableSchema
    {
        public bool IsEmptySqlSchema { get; private set; }

        public SqlGeneralOptions SrcSqlGeneralOptions { get; }

        // ---- SQL Columns
        private List<SimpleSqlColumn> _sqlColumns;
        public List<SimpleSqlColumn> SqlColumns
        {
            get => _sqlColumns;
            set
            {
                _sqlColumns = value ?? throw new ArgumentNullException(nameof(value), "Sql Columns must not be null");

                IsEmptySqlSchema = !value.IsAny();

                SqlColumnsDict = value.ToDictionary(col => col.Name, col => col);
                
                // build list of simplecolinfo
                //foreach (var col in SqlColumns)
                //{
                //    bool dateTimeValid = col.SqlType == SqlDbType.DateTime;

                //    var newCol = new SimpleSqlColumn
                //    {
                //        Name = col.Name,
                //        InPrimaryKey = col.InPrimaryKey,
                //        IsNullable = col.Nullable,
                //        //IsDateTimeType = dateTimeValid,
                //        DataType = col.SqlType
                //    };

                 //   SqlColumnsDict.Add(col.Name, newCol);
                //}
            }
        }
        
        public Dictionary<string, SimpleSqlColumn> SqlColumnsDict { get; private set; }

        public IEnumerable<string> SqlColumnNames() => SqlColumnsDict
            .Select(col => col.Key);

        public IEnumerable<SimpleSqlColumn> PriKeyCols => SqlColumnsDict
            .Values
            .Where(col => col.InPrimaryKey);
        
        public IEnumerable<string> PriKeyColNames => PriKeyCols
            .Select(col => col.Name);

        public IEnumerable<SimpleSqlColumn> NonPriKeyCols => SqlColumnsDict
            .Values
            .Where(col => !col.InPrimaryKey);

        public IEnumerable<SimpleSqlColumn> NullableColumns => SqlColumnsDict
            .Values
            .Where(col => col.IsNullable);

        public IEnumerable<SimpleSqlColumn> NonNullableColumns => SqlColumnsDict 
            .Values
            .Where(col => !col.IsNullable);        
        
        // ---- Constructor
        public SimpleSqlTableSchema(SqlGeneralOptions srcSqlOpts, IEnumerable<SimpleSqlColumn> sqlColumns)
        {
            SrcSqlGeneralOptions = srcSqlOpts;
            SqlColumns = sqlColumns.ToList();   
        }

        public static SimpleSqlTableSchema Empty()
        {
            return new SimpleSqlTableSchema();
        }

        private SimpleSqlTableSchema()
        {
            SqlColumns = new List<SimpleSqlColumn>();
        }
    }
}





