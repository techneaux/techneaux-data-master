﻿using System.Collections.Generic;
using System.Linq;
using TechneauxHistorySynchronization.Models;
using TechneauxReportingDataModel.SqlHistory.Enumerable;

namespace TechneauxHistorySynchronization.Helper
{
    public static class SchemaExtensions
    {
        public static SimpleCombinedTableSchema WithCygNetColumns(this SimpleCombinedTableSchema thisSchema, IEnumerable<SqlTableMapping> reportElms)
        {
            thisSchema.CygNetMappingRules = reportElms.ToList();
            return thisSchema;
        }

        public static SimpleCombinedTableSchema WithCygNetColumns(this SimpleSqlTableSchema thisSchema, IEnumerable<SqlTableMapping> reportElms)
        {
            var combSchema = new SimpleCombinedTableSchema(thisSchema.SrcSqlGeneralOptions, thisSchema.SqlColumns, reportElms);
            return combSchema;
        }
    }
}
