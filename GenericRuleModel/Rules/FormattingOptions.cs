﻿using System;
using GenericRuleModel.Rules;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Linq;
using XmlDataModelUtility;
using GenericRuleModel.Helper;
using System.Collections.Generic;

namespace GenericRuleModel.Rules
{
    public class FormattingOptions : NotifyCopyDataModel, IValidatedRule
    {
        public FormattingOptions()
        {
            EnableFormat = false;
            DataType = OutputDataType.Text;
            CustomFormat = "";
            DefaultIfNotDateTime = "";
            DefaultIfNotNumeric = 0;
            ScalingTypeChoice = ScalingType.NoScaling;
            DefaultIfUnavailable = "";
            TableName = "";
            TableDescription = "";
            DefaultIfNotBoolean = false;
            RoundingTypeChoice = RoundingType.Normal;
            NumDecimalPlaces = 0;
            NumberPartOption = NumberPart.IntegerPart;
            ScalingFactor = 1;
            SmartSheetDataTypes = EnumHelper.GetAllValuesAndDescriptions(DataType.GetType()).ToList();
            SmartSheetRoundingTypes = EnumHelper.GetAllValuesAndDescriptions(DataType.GetType()).ToList();
            SmartSheetScalingTypes = EnumHelper.GetAllValuesAndDescriptions(DataType.GetType()).ToList();
            SmartSheetNumberParts = EnumHelper.GetAllValuesAndDescriptions(DataType.GetType()).ToList();
        }
        public List<ValueDescription> SmartSheetDataTypes        {
            get => GetPropertyValue<List<ValueDescription>>();
            set => SetPropertyValue(value);
        }

        public List<ValueDescription> SmartSheetRoundingTypes
        {
            get => GetPropertyValue<List<ValueDescription>>();
            set => SetPropertyValue(value);
        }
        public List<ValueDescription> SmartSheetScalingTypes
        {
            get => GetPropertyValue<List<ValueDescription>>();
            set => SetPropertyValue(value);
        }

        public List<ValueDescription> SmartSheetNumberParts
        {
            get => GetPropertyValue<List<ValueDescription>>();
            set => SetPropertyValue(value);
        }


        [XmlAttribute()]
        public bool EnableFormat
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        public enum OutputDataType
        {
            [Description("Text")]
            Text,
            [Description("Date Time")]
            DateTime,
            [Description("Numeric")]
            Numeric,
            [Description("Boolean")]
            Boolean,
            [Description("Enumeration Table")]
            EnumerationTable
        }

        [XmlAttribute()]
        public OutputDataType DataType
        {
            get => GetPropertyValue<OutputDataType>();
            set => SetPropertyValue(value);
        }

        //********** Text only  **********
        [XmlAttribute()]
        public string CustomFormat
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute()]
        public string TableDescription
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        //********** Date Times only **********
        [XmlAttribute()]
        public string DefaultIfNotDateTime
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        //********** Numeric only **********
        [XmlAttribute()]
        public Double DefaultIfNotNumeric
        {
            get => GetPropertyValue<Double>();
            set => SetPropertyValue(value);
        }

        //Custom scaling only
        public enum ScalingType
        {
            [Description("No Scaling")]
            NoScaling,
            [Description("Feet To Inches")]
            FeetToInches,
            [Description("Inches To Feet")]
            InchesToFeet,
            [Description("Custom Scaling")]
            CustomScaling
        }

        [XmlAttribute()]
        public ScalingType ScalingTypeChoice
        {
            get => GetPropertyValue<ScalingType>();
            set => SetPropertyValue(value);
        }

        //Custom scaling only

        [XmlAttribute()]
        public Double ScalingFactor
        {
            get => GetPropertyValue<Double>();
            set => SetPropertyValue(value);
        }

        public enum NumberPart
        {
            [Description("Integer Part")]
            IntegerPart,
            [Description("Floating Point")]
            FloatingPoint,
            [Description("Decimal Part")]
            DecimalPart
        }

        [XmlAttribute()]
        public NumberPart NumberPartOption
        {
            get => GetPropertyValue<NumberPart>();
            set => SetPropertyValue(value);
        }

        //Floating point and Decimal Part only
        [XmlAttribute()]
        public int NumDecimalPlaces
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }

        public enum RoundingType
        {
            Ciel,
            Floor,
            Normal
        }

        [XmlAttribute()]
        public RoundingType RoundingTypeChoice
        {
            get => GetPropertyValue<RoundingType>();
            set => SetPropertyValue(value);
        }

        //********** Boolean only **********
        [XmlAttribute()]
        public Boolean DefaultIfNotBoolean
        {
            get => GetPropertyValue<Boolean>();
            set => SetPropertyValue(value);
        }

        //********** Enumeration table only **********

        [XmlAttribute()]
        public string TableName
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute()]
        public string DefaultIfUnavailable
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

 
        public  string ValidationErrorMessage => CheckIfValid.message;

        public  bool IsRuleValid => CheckIfValid.isValid;

        private (bool isValid, string message) CheckIfValid
        {
            get
            {

                if (this.DataType == OutputDataType.EnumerationTable && EnableFormat == false)
                {
                    if (string.IsNullOrWhiteSpace(TableName))
                        return (false, $"Enumeration table is invalid because the name is either blank or null.");
                }

                return (true, "No Error");


            }
        }

    }
}
