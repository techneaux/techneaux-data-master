using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using XmlDataModelUtility;

namespace GenericRuleModel.Rules
{
    public static class EnumerationHelper
    {
        public static  EnumerationTable GetTableByName(this ICollection<EnumerationTable> tableList, string tableName)
        {
            foreach (var table in tableList)
            {
                if (table.Name.Equals(tableName))
                    return table;
            }
            return null;
        }
    }

    public class EnumerationTable : NotifyCopyDataModel
    {
        [XmlAttribute]
        public string Name
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string Description
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string Category
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
        public BindingList<EnumerationPair> EnumerationPairs
        {
            get => GetPropertyValue<BindingList<EnumerationPair>>();
            set => SetPropertyValue(value);
        }
    

        public EnumerationTable()
        {
            Name = "";
            Description = "";
            Category = "";
            EnumerationPairs = new BindingList<EnumerationPair>();
        }

        internal string LookupValue(string key)
        {
            foreach (var pair in EnumerationPairs)
            {
                if (pair.Key.Equals(key))
                    return pair.Value;
            }
            return null;
        }

        public class EnumerationPair : NotifyCopyDataModel
        {
            public EnumerationPair()
            {
                Key = "";
                Value = "";
            }
            public EnumerationPair(string KeyRef, string ValueRef)
            {
                Key = KeyRef;
                Value = ValueRef;
            }

            public string Key
            {
                get => GetPropertyValue<string>();
                set => SetPropertyValue(value);
            }
            public string Value
            {
                get => GetPropertyValue<string>();
                set => SetPropertyValue(value);
            }

        }
    }

}
