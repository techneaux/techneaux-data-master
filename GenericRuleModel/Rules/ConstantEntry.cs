﻿using XmlDataModelUtility;

namespace GenericRuleModel.Rules
{
    public class ConstantEntry : NotifyCopyDataModel
    {

        public ConstantEntry()
        {
            ConstValue = "";
        }

        //********************************** Constant Attributes **********************************

        [HelperClasses.ControlTitle("Constant", "Con")]
        public string ConstValue
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
    }
}
