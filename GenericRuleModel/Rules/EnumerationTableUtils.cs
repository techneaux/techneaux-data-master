using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Microsoft.VisualBasic;
using Microsoft.Win32;
using Serilog;
using Newtonsoft.Json.Serialization;
using System.Xml;
using Newtonsoft.Json;
using Serilog.Context;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace GenericRuleModel.Rules
{
    public class EnumerationTableUtils
    {
        //public void TestSerialize()
        //{
        //    var TestEnums = new EnumerationTables();
        //    TestDeserialize();
        //    TestEnums = LoadEnumTables();
        //}

        //public void TestDeserialize()
        //{
        //    var TestEnums = new EnumerationTables();
        //    var TestTable = new EnumerationTable();
        //    var TestPair = new EnumerationTable.EnumerationPair();
        //    TestTable.Name = "Test";
        //    TestTable.Description = "Test Table";
        //    TestPair.Key = "Test2";
        //    TestPair.Value = "Test1";
        //    TestTable.EnumerationPairs.Add(TestPair);
        //    TestEnums.EnumTables = new BindingList<EnumerationTable>
        //    {
        //        TestTable
        //    };
        //    var Result = SaveEnumTables(TestEnums);
        //}

        /// <summary>
        /// Returns the directory path without trailing backslash for the enum tables XML file used for data persistence.
        /// </summary>
        /// <returns></returns>
        public static string GetEnumTablesDirectoryPath()
        {
            return $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Enum\";
        }

        /// <summary>
        /// The file name to use for the enum tables XML file used for data persistence.
        /// </summary>
        public const string EnumTablesFileName = "EnumTables.json";

        public static async Task<bool> SaveEnumTables(EnumerationTables Enumobject, string strFileName = null, string strFilePath = null)
        {
            try
            {
                strFilePath = strFilePath ?? $@"{GetEnumTablesDirectoryPath()}";
                strFileName = strFileName ?? EnumTablesFileName;
                Directory.CreateDirectory(strFilePath);

                Log.Debug($"new streamwriter: {strFilePath + strFileName}");
                var serializer = new JsonSerializer();
                using (var sw = new StreamWriter(strFilePath + strFileName))
                {
                    using (JsonWriter writer = new JsonTextWriter(sw))
                    {
                        serializer.Serialize(writer, Enumobject);
                        Log.Debug("Finished Enum Table Save.");

                        Console.WriteLine("Saved Enum");

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Error saving enum tables");

                Console.WriteLine("Failed to Save Enum");

                return false;
            }
        }

        public static EnumerationTables LoadEnumTables(string strFileName = null, string strFilePath = null)
        {
            try
            {
                strFilePath = strFilePath ?? $@"{GetEnumTablesDirectoryPath()}";
                strFileName = strFileName ?? EnumTablesFileName;

                Directory.CreateDirectory(strFilePath);

                if (File.Exists(strFilePath + strFileName))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    using (var sr = new StreamReader(strFilePath + strFileName))
                    {
                        using (JsonReader reader = new JsonTextReader(sr))
                        {
                            var LoadedTables = (EnumerationTables)serializer.Deserialize(reader, typeof(EnumerationTables));

                            if (LoadedTables.EnumTables == null)
                            {
                                var EnumTableList = new BindingList<EnumerationTable>();
                                LoadedTables.EnumTables = EnumTableList;
                            }
                            return LoadedTables;
                        }
                        //var serializer = new XmlSerializer(typeof(EnumerationTables), new XmlRootAttribute("EnumRoot"));

                    }
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception trying to load EnumTables file");
            }

            var EnumObject = new EnumerationTables();

            return EnumObject;
        }

        public static string GetCSVFilePath()
        {
            var OpenFileDlg = new OpenFileDialog
            {
                Filter = "CSV Files (*.csv) | *.csv",
                DefaultExt = "csv"
            };

            if (OpenFileDlg.ShowDialog() == true)
            {
                return OpenFileDlg.FileName;
            }
            else
            {
                return "N/A";
            }
        }

        public static void LoadTablesFromFile(ref EnumerationTables objTableRef)
        {
            var FilePath = GetCSVFilePath();
            try
            {
                using (var sr = new StreamReader(FilePath))
                {
                    string line;

                    while ((line = sr.ReadLine()) != null)
                    {
                        var strTableName = Strings.Trim(Strings.Split(line, ",")[0]);
                        var strTableDesc = Strings.Trim(Strings.Split(line, ",")[1]);
                        var strTableCategory = Strings.Trim(Strings.Split(line, ",")[2]);
                        var strValueName = Strings.Trim(Strings.Split(line, ",")[3]);
                        var strValueEntry = Strings.Trim(Strings.Split(line, ",")[4]);

                        if (objTableRef.EnumTables.Any(obj => obj.Name == strTableName) == true)
                        {
                            var EnumTable = objTableRef.EnumTables.First(obj => obj.Name == strTableName);
                            EnumTable.Description = strTableDesc;
                            EnumTable.Category = strTableCategory;

                            if (EnumTable.EnumerationPairs.Any(obj => obj.Key == strValueName) == true)
                            {
                                var EnumPair = EnumTable.EnumerationPairs.First(obj => obj.Key == strValueName);
                                EnumPair.Value = strValueEntry;
                            }
                            else
                            {
                                var EnumPair = new EnumerationTable.EnumerationPair
                                {
                                    Value = strValueEntry,
                                    Key = strValueName
                                };
                                EnumTable.EnumerationPairs.Add(EnumPair);
                            }
                        }
                        else
                        {
                            var EnumTable = new EnumerationTable
                            {
                                Name = strTableName,
                                Description = strTableDesc,
                                Category = strTableCategory
                            };
                            var EnumPair = new EnumerationTable.EnumerationPair
                            {
                                Key = strValueName,
                                Value = strValueEntry
                            };
                            EnumTable.EnumerationPairs.Add(EnumPair);
                            objTableRef.EnumTables.Add(EnumTable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Debug(ex, "Unhandled exception while loading enum tables from file");
            }
        }

    }


}
