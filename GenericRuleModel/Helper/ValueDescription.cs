using XmlDataModelUtility;

namespace GenericRuleModel.Helper
{
    public class ValueDescription : NotifyCopyDataModel
    {
        public object Value
        {
            get;set;
        }
        public string Description
        {
            get;set;
        }
    }
}
